Quicklinks: [Demo](#demo) - [Installation](#installation) - [Recommendations](#personal-recommendations) - [Shortcuts](#shortcuts) - [Aliases & Functions](#aliases-functions)

![](https://gitlab.com/psicho/dotfiles/badges/main/pipeline.svg)
![](https://tokei.rs/b1/gitlab/psicho/dotfiles?category=files)
![](https://tokei.rs/b1/gitlab/psicho/dotfiles?category=lines)
![](https://tokei.rs/b1/gitlab/psicho/dotfiles?category=code)
<!-- deactivate the badge as long as we don't trigger the build automatically
reactivating needs to add a key again, which has been removed
![](https://gitlab.com/psicho/docker-ubuntu/badges/main/pipeline.svg?key_text=Ubuntu%20build&key_width=90)
-->

## Introduction
Dotfile collection for combined zsh/bash usage for the following OS/architectures combinations:
* Ubuntu 22.04 on amd64 (native)
* Ubuntu 22.04 on amd64 (WSL2)
* Ubuntu 22.04 on arm64 (e.g., Raspberry Pi)
* macOS on Apple Silicon

Features:
 * Theming of all tools including Windows Terminal, Vim, VS Code, prompt, tmux, pager, git status - even via SSH and in cascaded tmux sessions
 * Aliases for shell, docker and git
 * Neovim (default editor) configuration (compatible with Vim)
 * Nano configuration
 * tmux configuration, responsive airline status bar, screen saver and 24-bit color support (works even via SSH)
 * zsh as default shell
   * zle (Z-Shell Line Editor) using vim style
   * Auto pair braces and quotation marks, autocomplete former commands, syntax highlight current command
   * change directories without prefixing `cd`
   * Toggle Switch: put current command into buffer
   * command-not-found: proposes how to install some command which was not found
   * Directory navigation: cycle through recent and easily go up one directories
   * Add/remove sudo via shortcut from beginning of line
 * fzf (fuzzy finder) integration with tab completion, cd, file completion, history, z, ripgrep, vim, git, docker
 * Utility scripts for login, colors, counting dirs & files, clipboard, help, zip, SSH key management
 * Configuration utility for customizing these dotfiles
 * Install & upgrade scripts including tool installation of:  
   [as-tree](https://github.com/jez/as-tree), [autopair](https://github.com/hlissner/zsh-autopair), [autosuggestions](https://github.com/zsh-users/zsh-autosuggestions), [bat](https://github.com/sharkdp/bat), [bat-extras](https://github.com/eth-p/bat-extras), [cmatrix](https://github.com/abishekvashok/cmatrix), [delta](https://github.com/dandavison/delta), [dive](https://github.com/wagoodman/dive), [docker](https://www.docker.com), [eza](https://github.com/eza-community/eza), [fast-syntax-highlighting](https://github.com/zdharma-continuum/fast-syntax-highlighting), [fd](https://github.com/sharkdp/fd), [fzf](https://github.com/junegunn/fzf), [fzf-tab](https://github.com/Aloxaf/fzf-tab), [fzf-tab-completion](https://github.com/lincheney/fzf-tab-completion), [forgit](https://github.com/wfxr/forgit), [lazydocker](https://github.com/jesseduffield/lazydocker), [lazygit](https://github.com/jesseduffield/lazygit), [prettyping](https://github.com/denilsonsa/prettyping), [ripgrep](https://github.com/BurntSushi/ripgrep), [shellcheck](https://github.com/koalaman/shellcheck), [starship](https://starship.rs), [tldr client](https://github.com/raylee/tldr-sh-client), [yq](https://github.com/mikefarah/yq), [z](https://github.com/skywind3000/z.lua)
* Vim plugins:
  [vim-plug](https://github.com/junegunn/vim-plug), [A.L.E.](https://github.com/dense-analysis/ale), [fzf.vim](https://github.com/junegunn/fzf.vim), [Solarized 8](https://github.com/lifepillar/vim-solarized8), [lh-brackets](https://github.com/LucHermitte/lh-brackets), [fugitive.vim](https://github.com/tpope/vim-fugitive), [which-key.nvim](https://github.com/folke/which-key.nvim), [vim-repeat](https://github.com/tpope/vim-repeat), [vim-surround](https://github.com/tpope/vim-surround), [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim), [vim-airline](https://github.com/vim-airline/vim-airline) plus a few Vim themes

Also, this is a practical, small [Swiss Army Knife](docs/swiss-army-knife.webp) that can additionally brew coffee. Has anybody ever heard of that ominous Unix philosophy?

## Table of contents

[[_TOC_]]

## Demo

The [complete setup](https://gitlab.com/psicho/docker-ubuntu) can be tested via:
```bash
docker run --rm -it registry.gitlab.com/psicho/docker-ubuntu
```

It is even possible to pass the host theme to the docker client:
```bash
docker run --rm -it -e CLIENT_THEME="$THEME" -e CLIENT_BG="$BACKGROUND" registry.gitlab.com/psicho/docker-ubuntu
```

### Themes

Themes can be switched via commands:
* `fg`: interactive theme chooser
* `bg`: toggle dark/light
* `cfg theme`: set theme (and optionally background)
* `cfg background`: set background to dark/light

Theme can even be switched from within Vim via shortcuts:
* F3/F4: cycle backward/forward
* ␣F3: toggle dark/light
* ␣F4: interactive theme chooser

_**Note**: when switching theme in Vim or Tmux, for some settings to work it is necessary to reload the parent shell._

<details>
<summary>Solarized dark</summary>

![Solarized dark theme](docs/theme-solarized-dark.png "Solarized dark theme")

</details>

![Solarized dark palette](docs/palette-solarized.png "Solarized dark palette")

---
<details>
<summary>Solarized light</summary>

![Solarized light theme](docs/theme-solarized-light.png "Solarized light theme")

</details>

![Solarized light palette](docs/palette-solarized.png "Solarized light palette")

---
<details>
<summary>Everforest dark</summary>

![Everforest dark theme](docs/theme-everforest-dark.png "Everforest dark theme")

</details>

![Everforest dark palette](docs/palette-everforest-dark.png "Everforest dark palette")

---
<details>

<summary>Everforest light</summary>

![Everforest light theme](docs/theme-everforest-light.png "Everforest light theme")

</details>

![Everforest light palette](docs/palette-everforest-light.png "Everforest light palette")
---
<details>
<summary>Gruvbox dark</summary>

![Gruvbox dark theme](docs/theme-gruvbox-dark.png "Gruvbox dark theme")

</details>

![Gruvbox dark palette](docs/palette-gruvbox-dark.png "Gruvbox dark palette")

---
<details>

<summary>Gruvbox light</summary>

![Gruvbox light theme](docs/theme-gruvbox-light.png "Gruvbox light theme")

</details>

![Gruvbox light palette](docs/palette-gruvbox-light.png "Gruvbox light palette")

---
<details>
<summary>Nord</summary>

![Nord theme](docs/theme-nord.png "Nord theme")

</details>

![Nord palette](docs/palette-nord.png "Nord palette")

---
<details>
<summary>Palenight</summary>

![Palenight theme](docs/theme-palenight.png "Palenight theme")

</details>

![Palenight palette](docs/palette-palenight.png "Palenight palette")

---
<details>
<summary>Monokai</summary>

![Monokai theme](docs/theme-monokai.png "Monokai theme")

</details>

![Monokai palette](docs/palette-monokai.png "Monokai palette")
### Prompt theming

Using fast starship.rs prompt showing username/hostname, directory, git info, java/node/python package info, kubernetes and aws environment, current shell and shell level, command duration, time, and result of last command.

<details><summary>Show prompt demo</summary>

|   |           |   |           |   |           |   |           |   |           |
|---|-----------|---|-----------|---|-----------|---|-----------|---|-----------|
| ⇕ | Diverged  | ↑ | Ahead     | $ | Stash     | ! | Modified  | » | Renamed   |
|   |           | ↓ | Behind    | ? | Untracked | + | Staged    | ✘ | Deleted   |

Starship Prompt

![Starship Powerline Prompt](docs/prompt-starship-powerline.png "Starship Powerline Prompt")

</details>

### Color support

24-bit colors in supported terminals

<details><summary>Show color demos</summary>

![Color Gradient](docs/gradient.png "Color gradient")
![Selected Colors](docs/colors.png "Selected colors")

</details>

### Directory navigation

<details><summary>Show navigation demos</summary>

Auto cd: No need to type `cd` or complete directory names

![Auto cd](docs/auto-cd.gif "Auto cd")

Cycle directories: Cycle last/next directory change from history using Ctrl+Alt+H/L (Ctrl+Cmd+H/L)

![Cycle directories](docs/cycle.gif "Cycle directories")

Directory up: Type Ctrl+Alt+K (Ctrl+Cmd+K) instead of `cd ..`

![Directory up](docs/updir.gif "Directory up")

Jumping around directories from history with `z.lua` and auto completion

![Jumping around](docs/z.gif "Jumping around")

</details>

### Command line editing

<details><summary>Show editing demos</summary>

Auto suggestions: Quickly complete suggested commands matching current input via Ctrl+L / →

![Auto suggestions](docs/autosuggestions.gif "Auto suggestions")

Syntax highlighting: Syntax highlight commands

![Syntax highlighting](docs/syntax-highlighting.gif "Syntax highlighting")

Auto pair: Automatically add/remove matching parentheses, brackets and quotation marks

![Auto pair](docs/autopair.gif "Auto pair")

Fuzzy completion: Use built-in fzf auto completion and filtering

![Fuzzy completion](docs/fzf-tab.gif "Fuzzy completion")

Sudo fix: Use Ctrl+F to add/remove `sudo` from current command, or last command (if line empty)

![Sudo fix](docs/sudo-fix.gif "Sudo fix")

Buffer toggle: Send current input to memory with Ctrl+Z to type another command, getting the original command back afterwards

![Buffer toggle](docs/buffer-toggle.gif "Buffer toggle")

Vi mode: Enter vi mode normal via Ctrl+V / Esc to edit the command line using vi commands

![Vi mode](docs/vi-mode.gif "Vi mode")

</details>

### Interactive Tools
<details><summary>Show tool demos</summary>

Interactive theme chooser: `fg` (foreground theme)

![Interactive theme chooser](docs/theme-chooser.gif "Interactive theme chooser")

Interactive git diff: `gi` (git interactive) / `gdi` (git diff interactive)

![Interactive git diff](docs/git-diff-interactive.gif "Interactive git diff")

Interactive grep: `fif` (find in files)

![Interactive grep](docs/find-in-files.gif "Interactive grep")

Interactive help: Meta+Shift+H

![Interactive help](docs/show-help.gif "Interactive help")

Synchronized SSH login: `login` (`l`) into several servers simultaneously and execute commands in parallel

![Login](docs/login.gif "Login")

`eza` as `ls` replacement

![eza](docs/eza.png "eza")

</details>

## Installation
The install scripts are targeted towards and tested against Ubuntu 22.04. Use with care for any other distro. Especially, we depend on the following versions:
 * zsh v5.8
 * fzf v0.37
 * fd-find v7
 * tmux v3.2
 * Neovim 0.7
 * bat v0.17
 * ripgrep v12
 * python 3

The versions of bat, fzf and Neovim are guaranteed by the installation process itself.

Note that [minimal setup](install/setup-minimal.sh) does not install anything.

### Install steps (Ubuntu)

 1. Basic installation (a):
    * If you provide git key as keyfile to bootstrap.sh, the dotfiles will be cloned via SSH (works only if you own the repo, e.g. by forking it). Otherwise it will be cloned via HTTPS.
    ```bash
    curl -LO https://vipc.de/bootstrap && bash bootstrap --all [--keyfile keyfile] [install-location]
    ```
    If using a different branch name, replace both occurrences of `<branch>`:
    ```bash
    curl -LO https://gitlab.com/psicho/dotfiles/-/raw/<branch>/install/bootstrap.sh && bash bootstrap.sh --all --branch <branch> [--keyfile keyfile] [install-location]
    ```
 1. To make full use of Powerline symbols, glyphs and file icons, use CaskaydiaCove NF from https://github.com/ryanoasis/nerd-fonts/releases/latest/download/CascadiaCode.tar.xz.
 1. Optional installation:
    ```bash
    "${DOTFILES_DIR}"/install/install-*.sh
    ```
    * Docker installation is _not_ necessary for WSL2 if using Docker Desktop
    * Omit password for sudo (replacing `<username>`): `sudo sh -c "echo '<username> ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/10-<username>"`
    * On WSL, one might (repeat for each drive): `sudo ln -s /mnt/c/ /c`

### Install steps (macOS)

 1. Clone this repo to any location
 2. Ensure the user has admin rights (i.e., sudo access)
 3. Execute `install/bootstrap-macos.sh` in chosen location

### Configuration
dotfiles specific configuration is stored under `~/.config/dotfiles/`.

 1. Change theme (tab complete for available themes):
    ```bash
    cfg theme
    ```
    * _Theming makes use of Windows Terminal. If not using Windows Terminal, set the theme according to `./theming/terminal`._
 1. Change default shell back to bash (installer will set it to zsh):
    ```bash
    chsh -s /bin/bash
    ```
 1. Add custom aliases in `~/.config/dotfiles/custom.sh` to your liking:
    ```bash
    cfg edit
    ```
 1. Configure to your liking (for details, see `cfg -h`):
    * using configurer:
      ```bash
      cfg <subcommand>
      ```
    * manually:
      ```bash
      cfg edit dotfiles
      ```
 1. Create a script to ssh to a predefined set of instances:
    ```bash
    #!/usr/bin/env bash
    source "${DOTFILES_DIR}"/lib/login.sh
    login host1 host2
    ```
 1. Edit file `~/.config/dotfiles/gitconfig.local` to configure URL shorthands and user:
    ```bash
    cfg edit gitconfig
    ```
 1. Configure your `~/.ssh/config`:
    ```bash
    cfg edit sshconfig
    ```
    Example content:
    ```
    Host *
        Compression yes
        ServerAliveInterval 240
    Host prefix*
        HostName %h.example.com
        IdentityFile ~/.ssh/example.pem
        User ubuntu
    ```

### Minimal installation (e.g. for docker containers)
Contains `config-shell.sh` (renamed to `.bash_profile`), `.inputrc`, `.vimrc` and `plugins.bash`

**Installation**
 * Prerequisites: bash, curl
 * `curl -L https://vipc.de/setup-min | bash && exec bash -l` (b)

or
 * Prerequisites: apt-get, bash
 * Call `fzd`, choose container, press `Meta+E` to install curl and dotfiles

### Symlinking
 * `"${DOTFILES_DIR}"/install/symlink-dotfiles.sh` symlinks all dotfiles to home directory (automatically done via `bootstrap.sh`)
 * `"${DOTFILES_DIR}"/install/unlink-dotfiles.sh` removes all dotfile symlinks from home directory

### Working with multiple GitHub accounts
`~/.ssh/config` defines the hosts:
```
Host github-work
    HostName github.com
    IdentityFile ~/.ssh/key_of_account1
    User git
Host github-priv
    HostName github.com
    IdentityFile ~/.ssh/key_of_account2
    User git
```

`gitconfig.local` uses email of most repos:
```
[url "git@github-work:account1/"]
    insteadof = ghw:
[url "git@github-priv:account2/"]
    insteadof = gh:
[user]
    name = Your Name
    email = your.name@example.com
```

For all other repos, just configure the email locally:  
`git config --local user.email "your.other.name@example.com"`

_**Notes**_
 * _If `~/.ssh/config` defines ssh keys for all necessary hosts, `ssh-add` (used by `ssh-load-identities`) only needs to store identities with a passphrase. If no `~/.ssh/config` shall be defined, `ssh-load-identities` can be used to define all identities._
 * _`ssh-load-identities` loads the keys in the exact order they appear in `~/.config/dotfiles/config.yml`._
 * _Kill all remaining ssh-agents before reloading._

**Troubleshooting**  
It seems to be necessary to store public keys in `$HOME/.ssh`.

If e.g. git tries to authenticate with account1 for repos of account2:
 * Delete the key of account1 from ssh-agent: `ssh-add -d $HOME/.ssh/key-of-account1`
   * _Note: for `ssh-add -d` to work, the corresponding public key must bey in `$HOME/.ssh`_
 * Log into account2 (by cloning from and pushing to account2)
 * Add key of account1 again (re-login or `ssh-add $HOME/.ssh/key-of-account1`)

### Working with multiple kubernetes configurations

 * Put each configuration into a separate yml
 * Use the current config: `export KUBECONFIG=<target-kube-config.yml>`

### Working with multiple AWS profiles

 * Edit `~/.aws/credentials`
 * For each profile, add a section `[<profile-name>]`
   * each section contains `aws_access_key_id` and `aws_secret_access_key`
 * Use a profile: `export AWS_PROFILE=<profile-name>`

### Installing for a new user
1. Create a key with `ssh-keygen` and name it `${username}_id_rsa.pub`, if not done yet.
1. Copy `${username}_id_rsa.pub` and `git_with_pass_rsa` to `$HOME/.ssh`
   * `${username}_id_rsa.pub` is used for logging into the machine.
   * `git_with_pass_rsa` is used for logging into GitHub/GitLab. A passphrase is highly encouraged if the machine is used by multiple users.
   * _Note:_ Path to ssh keys may not contain spaces currently.
1. As primary user:  
   `curl -LO https://vipc.de/create-user && bash create-user username fullname` (c)
1. As user $username:
    ```
    curl -LO https://vipc.de/bootstrap && bash bootstrap --silent --all
    ```
1. Configure:
   * `~/.config/dotfiles/gitconfig.local`
   * `~/.config/dotfiles/custom.sh`
   * `~/.config/dotfiles/config.yml`  
   See above chapter [Configuration](#configuration).

### Global theming - prerequisites

Install following VS Code theme plugins:

 * sainnhe.everforest
 * jdinhlife.gruvbox
 * arcticicestudio.nord-visual-studio-code
 * whizkydee.material-palenight-theme

Microsoft Windows Terminal schemes:

 * Open `%LocalAppData%/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/settings.json`
 * Ensure the default profile is Ubuntu and has no settings for `cursorColor` and `backround`

### Installing Homebrew on Linux

Additionally, one can install [Homebrew](https://brew.sh) on Linux:

1. `bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
1. `cfg edit` and add `source "${DOTFILES_DIR}"/config/linuxbrew.sh`

### Troubleshooting

On debian, there is not lazygit. Use forgit and git aliases instead.

On debian, fzf and zsh versions are too low. Install fzf >= 0.37 and zsh >= 5.8.

## Personal recommendations

A great man Website is https://www.mankier.com/; it even already includes tldr. Create a custom search with query URL https://www.mankier.com/1/%s and shortcut `man`!

### Use zsh
It is simply more powerful than bash.  
 * Provides great ways of changing directories, editing files directly by their name (suffix aliases are enabled for file types java,json,log,md,txt,xml,yaml,yml).
 * The autocompletion is really awesome: it can be traversed with arrow keys and vim keys, provides completion for options and help for commands like git.
 * Vim mode of zle (zsh line editor) rocks! It can be activated with `<Esc>` and is capable of all usual vim editing. It can be exited with i/I/a/A.
 * Provides completion for SSH and SCP: completes hosts from `~/.ssh/config` and `~/.ssh/known_hosts` for SSH and SCP; completes remote files for SCP (wait a little after `scp hostname:<Tab>`).

### Changing directories
 * Use fzf's Meta+C (for subdirs)
 * Use z (for already visited dirs); without args: search interactively. zz: same, limiting to subdirs of current directory.
 * Use fzf-tab in combination with cd: `cd` -> press Tab -> navigate or filter -> `/` -> repeat
 * Cycle directories with Ctrl+Alt+H/L (Ctrl+Cmd+H/L) and use Ctrl+Alt+K (Ctrl+Cmd+K) to go up one dir.

zsh specific:
 * Just type the dirname (zsh option `AUTO_CD` is active). In a path, first char is enough (tab completion) - e.g. `/h/u<Tab>` completes to `/home/ubuntu`.

### Use fzf - it is awesome
 * `kill <Tab>`, then filter and select any number of processes to kill them simultaneously (e.g. ssh-agent)
 * Search in history (Ctrl+R) just got great!
 * Use some command with Meta+S most of the times (e.g. `vi <Meta+S>`). In rare cases combination with Ctrl+E/V (edit current file in default editor/VS Code) could make sense (but it makes command repetition impossible!)
 * Search in any directory using trigger sequence `,,`: vi /path/to/base/,,<Tab>` or alias `fz [path]`
 * Move in results with Ctrl+J/K, mark/unmark with Tab, toogle/unselect all marks with Ctrl+A/U, disable preview with ?, dive into item with Enter, copy item with Ctrl+Y.
 * For command chains (like `docker volume inspect 12345abde`), select the next command and press `/` which pastes the command into command line and executes completion again. Alternative: Ctrl+O (paste result into command line), Ctrl+I (= Tab).
 * When tab completion provides groups, use F1/F2 to [switch between the groups](https://github.com/Aloxaf/fzf-tab/pull/183).
 * When in a dig-down menu like fzd, fif, fz, gi, gdi, gfi or kube, use Ctrl+G to go one step back.

### Finding stuff
fzf powered search:
 * Use fz as an interactive find replacement
 * Use fif (find in files) as an interactive grep replacement

Plain search:
 * Use fd (find) or fdh (find hidden) and optionally pipe the result into as-tree
 * Use rg (ripgrep), rgh (ripgrep hidden) or batgrep (ripgrep with bat's coloring)

### Git
 * Using rebasing with care (don't do it with shared branches!) can clean up your git history.
 * If you rebase, you are changing the committer date. The (original) author date stays the same. `git log` aliases (see `.gitconfig`) show author dates. If you want to see the committer date, use `git log --format=fuller`.
 * `git log --oneline|grep -n commit-hash` (or shorter and nicer: `g la|rg -n commit-hash`) returns the commit number from HEAD the given hash belongs to.
   * Note: abbreviated commit hash lengths may differ, as git is smart about that. Ensure to use a commit-hash that is short enough to match `git log --oneline|head -1`.

### Docker

 * Use fzd to view and work with containers
 * To debug a container, run the underlying image with drb (autocompletes) and open fzd in another pane to install minimal dotfiles with Meta+E, and enter it with Ctrl+E afterwards (or `exec bash -l` in first pane)

### Kubernetes
 * Use kn, kp, kr, kd, ks, ke, ki to view with Kubernetes Nodes, Pods, ReplicaSets, Deployments, Services, Events or Ingresses
   * logs can be viewed in a Pod via Meta+Enter
 * Use kes/keb to enter a Pod (execute sh/bash)

### tmux
 * Use Shift+F1 to show all available shortcuts.
 * tmux sessions should not be nested within the same environment. It is possible, though, to create one session on your client and another one on a host (via SSH). The drawback is that only the host catches the tmux shortcuts.
 * See most used [tmux shortcuts](docs/tmux-help.txt).
 * Tmux windows status legend:  
   ![Tmux windows status](docs/tmux-windows-status.png "Tmux windows status")

### Working with colors
 * Print selected color palettes with command `colors`.
 * Print complete 256-color palette with command `colorSpectrum`.
 * Print customizable 24-bit spectrum (1D or 2D) with command `color2DGradient`. See it's help for more info.
 * `print-hex-colors-from-file` greps all hex codes from a given file and prints the colors. Can be used in combination with [bat theme files](https://github.com/sharkdp/bat/tree/master/assets/themes), for example.
 * Use [powertoys](https://github.com/microsoft/PowerToys) color picker to find out the representation of any given color, e.g. when printed with the above commands.
 * Commands `tohex` and `fromhex` can help.
 * Additional info: [ANSI Escape Sequences](docs/ANSI.md) and subsection [**Colors**](https://gitlab.com/psicho/dotfiles#tools)

### Buffer Toggle

An alternative to zsh's `Ctrl+Z` is to use `Ctrl+U`, ..., `Ctrl+P` (works in both shells).

## Global variables

| Variable             | Description                                                                         |
|----------------------|-------------------------------------------------------------------------------------|
| DOTFILES_DIR         | Install location of this project                                                    |
| CUSTOM_FILE          | `${HOME}/.config/dotfiles/custom.sh`                                                |
| DOTFILES_CONFIG_FILE | `${HOME}/.config/dotfiles/config.yml`                                               |
| GITCONFIG_FILE       | `${HOME}/.config/dotfiles/gitconfig.local`                                          |
| SSHCONFIG_FILE       | `${HOME}/.ssh/config`                                                               |
| IS_RELOAD            | true if and only if not invoked for the first time in the current login session     |

## Shortcuts
For more details (including Vim, Bash and tmux), see my [Shortcut Cheatsheet](https://github.com/psicho2000/wiki/blob/master/Settings/Shortcuts.docx).  
Bracket surrounding is documented at [lh-brackets](https://github.com/LucHermitte/lh-brackets/blob/master/doc/default_brackets.md#global-mappings).

[arg] denotes an optional argument. {arg} denotes a mandatory argument.

### General (minimal setup)
| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| Meta+Shift+L    | Clear screen                   | Meta+Shift+R      | Reload shell                        |
| Ctrl+F          | Add/remove sudo                | Ctrl+W            | Cut until beginning of word         |
| Meta+M          | Copy previous word             | Meta+. / !$       | Cycle & insert prev last arguments  |
| Ctrl+P          | Paste from last cut command    | Ctrl+G            | Swap words                          |
| Meta+<          | Undo                           | Ctrl+U            | Delete whole line                   |
| Ctrl+H/L        | Prev/next character            | Meta+H/L          | Prev/next word                      |
| Ctrl+K/J        | Prev/next history command      | Ctrl+A / Ctrl+E   | Beginning/end of line               |
| Ctrl+Alt+H/L    | Cycle directories (WSL)        | Ctrl+Alt+K        | Up one directory (WSL)              |
| Ctrl+Cmd+H/L    | Cycle directories (macOS)      | Ctrl+Cmd+K        | Up one directory (macOS)            |
| Ctrl+V,Ctrl+V   | Edit current line in vim       | Ctrl+V/Esc        | Switch from viins to vicmd mode     |
|                 |                                | Meta+Shift+H      | Show help for current command       |
| Cmd+Back        | Delete until beginning of line | Cmd+Del           | Delete until end of line            |

**Alternative Shortcuts**

| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| ←/→             | Prev/next character            | Ctrl+←/→          | Prev/next word (WSL)                |
| ↑/↓             | Prev/next history command      | Opt+←/→           | Prev/next word (macOS)              |
| Ctrl+Alt+←/→    | Cycle directories (WSL only)   | Ctrl+↑            | Up one directory (WSL only)         |
| Meta+Back       | Cut until beginning of word    | Meta+Delete       | Cut until end of word               |
| Home / End      | Beginning/end of line          |                   |                                     |

### zsh
| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| Meta+>          | Redo                           | Ctrl+Z            | Put command into buffer             |

**Completion menu**

| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| Ctrl+J/K        | Move down/up                   | Ctrl+H/L          | Move left/right                     |
| Ctrl+I / Tab    | Interactive search             | Ctrl+O            | Complete entry and stay in menu     |

### fzf
Provides completion for filenames, `ssh`, `kill`, environment vars (`unset`, `export`, `unalias`).  
All commands (except `kill`) are triggered by appending `,,`. Examples:
 * `vi /path/to/dir/,,<Tab>`
 * `ssh ,,<Tab>`
 * `kill <Tab>`
 * `unalias ,,<Tab>`

| Key/Function    | Description                                                                              |
|-----------------|------------------------------------------------------------------------------------------|
| fzf / fz [path] | Fuzzy find in current directory including subdirectories / with [path]                   |
| fif {search}    | Find in files - fuzzy search in ripgrep result; `-I`: search in `.gitignore` locations   |
| fzd / Meta+D    | Find in docker containers                                                                |
| Meta+C          | Change directory                                                                         |
| Meta+S          | Anywhere in the command-line: paste search result into current command                   |
| Ctrl+R          | History reverse search (search String can be typed before or after Ctrl+R)               |

**fzf/fzd within search**

| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| Ctrl+J/K        | Move down/up                   | Ctrl+E/V          | Open in default editor/VS Code      |
| Ctrl+A/U        | Toggle/unselect all marks      | Ctrl+O            | Paste result into command line      |
| Ctrl+Space      | Mark/unmark                    | ?                 | Toggle preview                      |
| Ctrl+F/B        | Preview page down/up           | Meta+J/K          | Preview line down/up                |
| Ctrl+Y          | Copy entry to clipboard        | Enter             | Dive into item                      |
| Ctrl+G/Q/C / ESC | Abort                         |                   |                                     |

**fzd within search**

| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| Ctrl+E          | Exec bash                      | Meta+I            | Inspect                             |
| Meta+E          | Install minimal dotfiles       | Enter             | Logs (quit with q)                  |
| Ctrl+R          | Reload view                    | Meta+Enter        | Dive into underlying image          |

**fif within search**

| Key             | Description                    | Key               | Description                         |
|-----------------|--------------------------------|-------------------|-------------------------------------|
| Ctrl+R          | Reload view                    | Ctrl+E            | Edit                                |

### Kubernetes

**fzf Tools**

| Key/Function    | Description                                                                              |
|-----------------|------------------------------------------------------------------------------------------|
| kd, ke, kn      | View Kubernetes Deployments, Events or Nodes (shortcut for kube {resourcename}           |
| kp, kr, ks, ki  | View Kubernetes Pods, ReplicaSets, Services or Ingress (shortcut for kube {resourcename} |
| kube {resource} | View any Kubernetes                                                                      |

**Shortcut functions**

| Key/Function    | Description                                                                              |
|-----------------|------------------------------------------------------------------------------------------|
| kes/keb {f} [o] | Enter a Pod's sh/bash console. First arg: name filter; second arg: kubectl options       |
| kip             | Get external node IPs                                                                    |
| kipc            | Copy an external node IP to clipboard                                                    |
| kns {namespace} | Set a namespace                                                                          |
| ksec {secret}   | View given secret or list all secrets, if none provided                                  |

### less

| Key                | Description                 |
|--------------------|-----------------------------|
| (Shift+)Q / Ctrl+G | Quit                        |

### Interaction with clipboard
 * vi copies automatically to clipboard when yanking
 * in ssh, vi and fzf's Ctrl+Y copy to tmux buffer, which can be obtained via `clippaste`
 * copying in vi is only possible if vi is not in mouse mode and has no vertical splits
 * copying in tmux is only possible if
    * tmux is in mouse mode (copies instantly) or
    * tmux is not in mouse mode with only horizontal splits (copy using right click)

| Key          | Description                       | Key          | Description                              |
|--------------|-----------------------------------|--------------|------------------------------------------|
| Ctrl+Y       | zsh vi: Copy buffer to clipboard  | Ctrl+P       | zsh vi: Paste from clipboard             |
| Ctrl+U       | Shell insert: Cut line internally | Ctrl+P       | Shell insert: Paste internally           |
| F12          | Vim: Toggle copy mode             | y            | tmux copy: Copy to clipboard (also ssh)  |
| clipcopy     | Terminal/ssh: Copy to clipboard   | clippaste    | Terminal/ssh: Paste from clipboard       |

#### Window Terminal

| Key          | Description                       | Key          | Description                              |
|--------------|-----------------------------------|--------------|------------------------------------------|
| Shift+Cmd+C  | All apps: Terminal mark mode      | Ctrl+Shift+F | All apps: Find in terminal               |
| Cmd+C        | All apps: Copy to clipboard       | Cmd+V        | All apps: Paste from clipboard           |

#### iTerm2

| Key          | Description                       | Key          | Description                              |
|--------------|-----------------------------------|--------------|------------------------------------------|
| Ctrl+Shift+M | All apps: Terminal mark mode      | Cmd+F        | All apps: Find in terminal               |
| Ctrl+Shift+C | All apps: Copy to clipboard       | Ctrl+Shift+V | All apps: Paste from clipboard           |
| Ctrl+Ins     | All apps: Copy to clipboard       | Shift+Ins    | All apps: Paste from clipboard           |

## Aliases & Functions
| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| declare -f             |   list all functions (shell inbuilt)                                              |
| declare -F             |   list all function names (bash inbuilt)                                          |
| aliases                |   list all aliases (fzf Addon)                                                    |
| exports                |   list all exports (fzf Addon)                                                    |

[arg] denotes an optional argument. {arg} denotes a mandatory argument.

### General (available in minimal setup)
| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| ../.../..../.....      | Go up one/two/three/four directories                                              |
| ~ / -                  | Go to home/last directory                                                         |
| alldirs                | Print directory history                                                           |
| calc                   | Calculator `bc` in interactive mode                                               |
| diffgit [-s]           | Use git diff/delta to compare two files; -s: simple                               |
| ex / explain [-h]      | Explain alias, function or export. Without args: show all aliases. -h: help       |
| filter {file} {filter} | Continuously filter a file for string filter                                      |
| fs / file-size [args]  | Show file size; args can be any arguments to `du` - especially target directory   |
| g / gc / gl / gs       | git / git clone / git log / git status                                            |
| gd [-s] [args]         | git diff; git diff --cached, if no diff; git show, if neither of them; -s: simple |
| gp / gu                | git push / git pull ("update")                                                    |
| gsa                    | git stash including untracked files                                               |
| gh_pr {title}          | Create a GitHub PR (needs extra GitHub CLI to be installed)                       |
| h                      | history                                                                           |
| history-stats          | Print history statistics                                                          |
| hs {search}            | history search using smart-case (zsh) / ignore-case (bash)                        |
| ll                     | Long listing with hidden files, no . or .., human readable, colored, dirs first   |
| mansearch / ms         | Search a man page for a given option                                              |
| map                    | Interactive ascii map MapSCII.me based on OpenStreetMap                           |
| md / make-dir          | Make directory recursively and cd to it                                           |
| myip / myipv6          | Print IPv4/IPv6 of this network seen from the internet                            |
| pack                   | tar gzip target verbosely                                                         |
| path                   | print path linewise                                                               |
| rd                     | rmdir                                                                             |
| unpack                 | Extract file verbosely (bz2, gz, rar, zip, Z, 7z, if respective tools installed)  |
| v                      | vim (minimal setup) / neovim (complete setup)                                     |

### Dotfiles
| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| create-test            | Creates an executable test.sh, if not existing                                    |
| demo                   | Create a demo for taking screenshot in this README's demo section for themes      |
| demo-docker [-p]       | Start (-p: and pull) the docker dotfiles demo                                     |
| dotlog                 | Paginate ~/dotfiles-install.log in a colorized way                                |
| nerdfont               | Show and filter nerdfont (needs `$WIKI_DIR` and nerdfonts)                        |
| reload [-d] [-h]       | Reload current shell, staying in current dir                                      |
| rh / repairhist        | Delete last entry from history                                                    |
| t                      | Attached to an existing tmux session or create a new one if none exists           |
| tl / tn                | List tmux session / create a new tmux session                                     |
| todo [-a] [-e] [-h]    | Print the todos (-a: everything) from this dotfiles todo.md; -e: edit instead     |
| update                 | pull wiki (if `$WIKI_DIR` exists), pull dotfiles and reload                       |
| upgrade [-a] [-h]      | Upgrade all debian packages and all packages installed with these dotfiles        |

### Tools

**Directory Navigation**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| bd / desktop           | Change to (sub) directory of `$DESKTOP_DIR`   (arg can be substring). See --help. |
| bf / b.                | Change to (sub) directory of `$DOTFILES_DIR`  (arg can be substring). See --help. |
| bp / pj                | Change to (sub) directory of `$PROJECT_DIR`   (arg can be substring). See --help. |
| bs                     | Change to (sub) directory of `$WIKI_DIR/Settings` (arg can be substring). See --help. |
| bw / wiki              | Change to (sub) directory of `$WIKI_DIR`      (arg can be substring). See --help. |

**Helper**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| bat-theme-preview file | Preview any bat theme on given file                                               |
| cfg [-h]               | Configure a dotfiles option. Call without args to see usage.                      |
| checkscripts [-v] [-h] | Use shellcheck to check all files with a shebang in current dir and subdirs.      |
| clipcopy / clippaste   | Copy to/paste from system clipboard                                               |
| colorize-help          | Colorize a given help. Use like `ls --help | colorize-help`.                      |
| count [path] [-n]      | Count size, number of files and directories of all child directories [in path]    |
| count-lines [path] [-n]| Count lines of all ASCII and UTF_8 files in current directory tree [in path]      |
| git-search regex file  | Search across all branches for regex in file                                      |
| gt / git-tools         | Use a git tool on all subdirs or populate autocompletion for clone. See --help.   |
| help / F1              | Show this README in Vim. Then, search and navigage with Space+/.                  |
| l / login [-h]         | Login to one or multple machines using `~/.ssh/config` and completing from there  |
| stats                  | Show stats for the current repository. Needs github/IonicaBizau/git-stats.        |
| tre / tree-improved    | Colored tree, excluding .git and sorting directories first, paging with less      |
| tman                   | tldr + man in one command                                                         |

**Colors/Theming**

For details, see [ANSI Escape Sequences](docs/ANSI.md).

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| bg                     | Toggle background between dark and light                                          |
| colors                 | Print default, custom and solarized palette                                       |
| color2DGradient [-h]   | Print 2D 24-bit color gradients in two given colors of red, green, blue           |
| colorGradient          | Print 24-bit color gradients in red, green, blue                                  |
| colorSpectrum          | Print 256 color spectrum                                                          |
| fg                     | Choose and change the theme                                                       |
| fromhex                | Convert hex color to nearest 256 color index                                      |
| print-hex-colors-from-file | Print all hex colors found in given file                                      |
| print-solarized-diff   | Visualize the color modification of `solarized8`                                  |
| tohex                  | Convert color from the default 256 color palette into hex color                   |

**SSH**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| ssh-find-agents        | Print all running ssh-agents                                                      |
| ssh-get-fingerprint    | Get fingerpint of an ssh key                                                      |
| ssh-is-remote-session  | Helper function to determinate if we are in a remote SSH session                  |
| ssh-list-keys          | List all currently added SSH keys                                                 |
| ssh-load-identities    | Load SSH identities for usage with SSH logins. For details, see ssh-manager.sh.   |

**forgit (fzf and delta powered)**

| Function         | Description                    | Function         | Description                         |
|------------------|--------------------------------|------------------|-------------------------------------|
| gi [file/commit] | Interactive git log & show     | gfi {search}     | Interactive git find in commits     |
| gdi [commit]     | Interactive git diff/git show  | gli [files]      | Interactive git log viewer          |
| gai [files]      | Interactive git add selector   | gsi              | Interactive git stash viewer        |
| gcf              | Interactive git checkout file  | gri              | Interactive git reset HEAD          |
| gclean           | Interactive git clean selector | gii              | Interactive .gitignore generator    |

### Tool/environment specific

**External tools**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| as-tree                | Great for piping from fd, e.g. `fd md | as-tree`                                  |
| cat / batgrep          | Alias to bat (colored, better cat) / combination of bat and ripgrep               |
| fd/fdh pattern [path]  | Find pattern files [in path] / including hidden (using fdfind)                    |
| ld / lg                | lazydocker / lazygit                                                              |
| msg                    | Continuous system log, uses dmesg (Ubuntu only)                                   |
| py / pyblack / pytest  | python3 / python format / run python unittests                                    |
| pyflake / pysort       | Remove unused python imports / sort python imports                                |
| rgh/rgh pattern [path] | Grep pattern files [in path] with smart case / including hidden (using ripgrep)   |
| tldr                   | Man replacement. Compatible with tldr, fd, fzf, rg, bat, z and more.              |
| z / zz                 | cd using most recent ranking entries (fzf powered) / only into subdir of pwd      |

**zsh**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| fpath                  | Print fpath linewise                                                              |

**WSL only**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| explorer               | Open explorer.exe                                                                 |
| find-all               | Find in all of distribution, excluding windows mounts                             |
| wg/winget              | Use winget.exe                                                                    |
| winget-recache         | Update cache for completing installed winget packages                             |
| wgu                    | winget.exe upgrade                                                                |

**macOS & WSL**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| gr [user/repo]         | Open GitHub repository user/repo. Defaults to current repo's remote.              |
|                        | If only repo provided, opens github with $GITHUB_USER/part                    	 |
|                        | If not in a repo, opens github with the $GITHUB_USER according to config.         |
| here                   | Open explorer.exe / finder with current directory                                 |
| o / oo                 | Open most recent ranking directory with explorer/finder / only in subdir of pwd   |
| od                     | Open `$DESKTOP_DR` in explorer.exe / finder                                       |
| of / o.                | Open `$DOTFILES_DIR` in explorer.exe / finder                                     |
| op                     | Open `$PROJECT_DIR` in explorer.exe / finder                                      |
| os                     | Open `$WIKI_DIR/Settings` in explorer.exe / finder                                |
| ow                     | Open `$WIKI_DIR` in explorer.exe / finder                                         |
| ytaudio                | Download from youtube as audio. Needs youtube-dl and AtomicParsely.               |
| ytbatch                | Download video as batch. Needs youtube-dl.                                        |

### Git

**Checkout**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| co                     | Shortcut for checkout                                                             |
| col                    | Checkout last                                                                     |
| new                    | Checkout new branch                                                               |

**Commit & Push**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| ac                     | Stage everything & commit                                                         |
| ci                     | Stage changes & commit (a.k.a. "checkin")                                         |
| amend                  | Amend changes to the last commit, keeping the same commit message                 |
| rename                 | Rename last commit                                                                |
| publish                | Push unpublished branch                                                           |

**Resetting & cleanup**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| undo-commits           | Undo last commit/given number of commits                                          |
| unstage                | Unstage a file                                                                    |
| drop-commits           | Drop last commit/given number of commits                                          |
| join                   | Rebase (e.g. join) last two/given number of commits interactively                 |
| prune-branches         | Delete local branches without a remote                                            |
| hard-reset             | Reset to remote, overwriting any differing local history                          |
| delete-remotely        | Delete current branch on remote, keeping local copy                               |

**Logs & Statistics**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| l                      | One-line log including branching in/out (last 30 commits)                         |
| la                     | One-line log including branching in/out (all commits)                             |
| ll                     | Detailed log including branching in/out and stats (long, last 15 commits)         |
| ls                     | Specific commit including stats                                                   |
| hist                   | One-line log including branching in/out (graphical)                               |
| last                   | Last commit including stats                                                       |
| last-hash              | Last commit hash                                                                  |
| last-message           | Last commit message                                                               |
| since                  | Commits since last git command (typically aftger a git pull)                      |
| contributors           | List contributors with number of commits                                          |
| statistics             | Statistics about daily commits                                                    |

**Info**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| s                      | Status                                                                            |
| alias                  | Alias to show all or specific aliases                                             |
| tags                   | Show verbose output about tags                                                    |
| branches               | Show verbose output about branches                                                |
| remotes                | Show verbose output about remotes                                                 |
| local-branches         | Show branches that only exist locally                                             |

**Working with remotes**

| Alias/Function         | Description                                                                       |
|------------------------|-----------------------------------------------------------------------------------|
| set-origin-https       | Switch remote url origin from ssl to https                                        |
| set-origin-ssl         | Switch remote url origin from https to ssl                                        |

### docker
| Alias/Function                  | Description                                                              |
|---------------------------------|--------------------------------------------------------------------------|
| d                               | docker                                                                   |
| docker-copy-from-volume         | Copy contents of docker volume to current directory                      |
| docker-copy-to-volume           | Copy local file to a docker volume                                       |
| de / docker-exec {container}    | Exec bash in given container                                             |
| di / docker-inspect {container} | Inspect a container containing given String as name                      |
| docker-stats-sorted             | Sorted, static docker stats                                              |
| dps / docker-ps [opts] [filter] | Docker ps with convenience, see `--help`                                 |
| drb                             | docker run bash: enter a new, disposable copy of an image via bash       |
| ds / docker-stats [filter]      | Short docker stats                                                       |

### docker-compose
| Alias/Function                  | Description                                                              |
|---------------------------------|--------------------------------------------------------------------------|
| dc                              | docker-compose                                                           |
| dce / docker-compose-exec       | Exec bash in given service                                               |
| dcer / docker-compose-exec-root | Exec bash as root in given service                                       |
| dcl / docker-compose-logs [-n]  | Follow service logs with [show last 500 lines less, a.k.a. no-follow]    |
| dcls / docker-compose-list      | List contents of a service's container (optional second arg: path)       |
| dci / docker-compose-inspect    | Inspect a service                                                        |
| dcs / docker-compose-stats      | Short docker stats                                                       |
| dcu / docker-compose-update     | Pull and recreate a service                                              |

## Known issues

### apt may take a long time when contacting ppa:rmescandon/yq

Solution: Use a different nameserver, e.g.:
```bash
echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf > /dev/null
```

As `/etc/resolv.conf` might be symlinked or auto generated, backup the original content. In WSL, follow the advice to disable auto generation.

## Implementation details
### Color rewriting
The function `modify-palette-solarized`, called during login, rewrites some colors used by Vim and tmux like the following:

![Solarized Colors](docs/solarized8.png "Solarized Colors")

### File layout & Loading order
```
.nanorc     .vimrc     .inputrc     .tmux.conf     .gitconfig
                                                      └── gitconfig.local

dotfiles/.zlogin                             dotfiles/.bash_profile
   │                                                 │
   ├── lib/zsh/completion/completion.zsh             │
   │                                                 │
   ├─▶ load-common-files.sh ◀────────────────────────┤
   │     │                                           │
   │     ├── config/gnu-binaries.sh                  │
   │     ├── config/config-shell.sh (1)              │
   │     ├── config/config-environment.sh            │
   │     ├── theming/set-theme.sh                    │
   │     ├── config/fzf-config.sh                    │
   │     ├── config/forgit.sh (4)                    │
   │     ├── config/kube-config.sh                   │
   │     ├── config/z.sh (4)                         │
   │     ├── config/nnn.sh (4)                       │
   │     ├── config/docker-commands.sh (3)           │
   │     ├── config/docker-ps.sh                     │
   │     ├── lib/colors.sh                           │
   │     ├── lib/dir-navigation.sh (3)               │
   │     ├── lib/login.sh (3)                        │
   │     ├── lib/count.sh                            │
   │     ├── lib/ssh-manager.sh                      │
   │     ├── lib/configurer.sh (3)                   │
   │     ├── lib/shellcheck.sh                       │
   │     ├── lib/packer.sh                           │
   │     ├── lib/clipboard.sh                        │
   │     ├── lib/help.sh                             │
   │     ├── lib/git-tools.sh                        │
   │     ├── lib/library/winget.sh                   │
   │     ├── lib/library/system.sh                   │
   │     ├── lib/library/github.sh                   │
   │     ├── ~/.config/dotfiles/config.yml           │
   │     ├── ~/.config/dotfiles/custom.sh            │
   │     └── fastfetch                               │
   │                                                 │
   ├── fzf --zsh (3)                                 ├── lib/bash/plugins.bash
   ├── lib/zsh/completion/*.*  (3)                   ├── lib/bash/show-help.bash
   ├── lib/zsh/*.*  (2)                              └── fzf --bash (3)
   ├── ~/bin/autopair/autopair.zsh (4)
   ├── ~/bin/fzf-tab/fzf-tab.zsh (4)
   ├── /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh (4)
   ├── ~/bin/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh (4)
   └── config/key-bindings.zsh
```
(1) `config-shell.sh` is `.bash_profile` in minimal setup  
(2) completion, configure-vi-mode, dir-navigation, amongst others  
(3) provide completion  
(4) external tools

### zsh Dependencies
The following parts depend on each other:

```
bashcompinit──┐                 FPATH         ┌─────────> dir-navigation ─┐
       load-common-files          │ configure-vi-mode --> buffer-toggle   │
`bindkey -v`──┘   │               │           │                           ├──> key-bindings.zsh
                  v               v           v       autosuggestions     │            │
       zsh alias `reload` ──> compinit ──> fzf-tab─────────┤              │            v
                                        autopair ──> syntax-highlighting ─┘      set_last_command
```

## Development guidelines
### Use Shellcheck
 * Function `shellcheck` checks all scripts with a shebang for `sh` or `bash`.
 * Use shebang in form of `#!/usr/bin/env <shell>`, if a shell script
 * Use `# shellcheck shell=bash` only for bash configuration scripts (i.e. `.bash_profile`, `.inputrc`)
 * Script rationale: if it is a shell script for both bash and zsh, use shebang for bash - it enforces coloring in vim.
 * Use shellcheck only for scripts purely compatible with bash
 * Let shellcheck/CI determine scripts to be checked via shebang
 * For bash files not to be shellshecked but colored, use suffix `.bash` and omit shebang

### Coding
 * Indent using 4 spaces, no tabs. Use heredoc `<<-` with tabs and indent the heredoc content like a method{} block.
 * Maximize compatibility in bash & zsh: Every script must be working in zsh, and only zsh specifics (like widgets) need not work in bash.
 * When calling executables inside scripts, use long options whenever possible to improve readability.
 * Prefix private functions with `__`. A private function is a function only intended to be called as a subroutine within the script.
 * Use local variables. Define functions inside scripts instead of coding at the main level.
 * When offering options, always provide a long (e.g. `--options`) and a short (e.g. `-o`) option.
 * Use `source` instead of `.`: These dotfiles are not POSIX compatible and aim at bash/zsh, so we can use the more readable and searchable syntax.
 * Use `function_name() {}` instead of `function function_name {}`: Just a personal choice out of readability (in contrast to searchability).
 * Add a newline at each end of file: https://thoughtbot.com/blog/no-newline-at-end-of-file
 * Comply with https://google.github.io/styleguide/shellguide.html
 * [Developer notes](docs/developer-notes.md)

### Help & Docs ("Discoverability")
 * Write help via options `-h` and `--help` for all functions and scripts that have args or options. Add them to show-help.
 * Provide separate autocompletion for bash and zsh. Offer descriptions in zsh autocompletion.
 * `README.md`
   * Document every function, script, alias and key binding; use [] for optional args, {} for mandatory args
   * Document main features in Introduction
   * Document eye catchers in Demo with an image width of 892 pixels
     * Use theme solarized dark
     * Use branch `main` of dotfiles
     * Use neutral username `ubuntu`: `export USER=ubuntu` (as an alternative, see [rename user](docs/rename-user.md))
     * Use [LICEcap](https://www.cockos.com/licecap/) for animated gifs, displaying elapsed time
     * Use [ShowOff](https://www.dcmembers.com/skrommel/download/showoff/) to demonstrate shortcuts with the [given config](docs/ShowOff.ini). If using it, place it at the bottom of licecap's window to the far right just left of elapsed time.
     * Alternatives: [Screen recording solutions](docs/screen-recording.md)
  * Use https://asciiflow.com for drawing boxes and arrows

### Git commits
 * prefix each commit with `feat: `, `fix: `, `docs: ` or `refactor: `

### Adding a theme
 * Rationale: Vim, VS Code, Terminal and all tools used in these dotfiles shall align their theme
   * Recommendation: VS Code and Vim theme should already exist (don't do yourself)
 * Find good resources about the theme:
   * a documentation how the theme works (e.g., https://github.com/altercation/solarized)
   * or a palette documentation (e.g., https://github.com/sainnhe/everforest/blob/master/palette.md)
   * If no resources are available, try to understand the theme by looking at how Vim and VS Code use them
      * pick the colors used by the theme, e.g. using Powertoys Color Chooser and document them in `HexColorlxlsm`, sheet `Hex-to-Background`.
 * Add theming for all tools used in these dotfiles. Use the checklist in `HexColorlxlsm`, sheet `Project` and look into all subfolders of `./theming`.
   * For terminal theme, try to find a `.minttyrc` configuration and convert via `convert_mintty_colors.py`.
      * Add the terminal theme to `HexColorlxlsm`, sheet `Hex-to-Background`.
      * Ensure bright black is well distinguishable from black (i.e. bright black text is readable on black background)
 * Test theme using `demo -t`. See section `Test notes` in `demo -h`.
 * Add theming example using `demo -d`. Save screenshot as `./docs/theme-${THEME}-${BACKROUND}.png`.
 * Add palette by copying the terminal colors 0-15 from sheet `Hex-to-Background` to sheet `Doku` transposed.
   * Screenshot the palette using 100% zoom in Excel and crop the image pixel exact to its borders.
   * Save the screenshot as `palette-${THEME}-${BACKROUND}.png`.
 * Add a section to this README's [Themes Demo](#themes).

## References
### How to manage dotfiles
 * https://dotfiles.github.io/
 * https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
 * https://www.foraker.com/blog/get-your-dotfiles-under-control

### zsh Info
 * https://linuxaria.com/howto/globbing-con-zsh
 * https://opensource.com/article/18/9/tips-productivity-zsh
 * https://www.mankier.com/1/zshexpn
 * https://unix.stackexchange.com/questions/71253/what-should-shouldnt-go-in-zshenv-zshrc-zlogin-zprofile-zlogout/71258#71258
 * http://www.bash2zsh.com/zsh_refcard/refcard.pdf

**Completion**
 * https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org
 * https://tylerthrailkill.com/2019-01-13/writing-zsh-completion-scripts/
 * http://zv.github.io/a-review-of-zsh-completion-utilities#orgea014fe
 * https://mrigank11.github.io/2018/03/zsh-auto-completion/

### Screen capture tools
 * https://www.cockos.com/licecap/
 * https://www.dcmembers.com/skrommel/download/showoff/

### Redirections
 (a) https://vipc.de/bootstrap   redirects to https://gitlab.com/psicho/dotfiles/-/raw/main/install/bootstrap.sh  
 (b) https://vipc.de/setup-min   redirects to https://gitlab.com/psicho/dotfiles/-/raw/main/install/setup-minimal.sh  
 (c) https://vipc.de/create-user redirects to https://gitlab.com/psicho/dotfiles/-/raw/main/install/create-user.sh

### WSL
 * https://github.com/wslutilities/wslu
 * https://wheatevo.com/wsl-2-and-vpn/

### macOS
 * https://blog.kandji.io/mac-logging-and-the-log-command-a-guide-for-apple-admins

### Git statistics
 * https://github.com/IonicaBizau/git-stats
 * https://github.com/arzzen/git-quick-stats

### Related work
 * https://github.com/black7375/BlaCk-Void-Zsh
 * https://github.com/romkatv/zsh4humans/

### Additional install notes
 * [tmux installation from scratch](docs/tmux-install.md)
