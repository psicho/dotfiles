# ToDo

## macOS related
 * fix:  automatic background change on macOS does not change bg in Terminal when new tab is opened (only after reload)
 * fix:  Insert key in zle produces capitalization swap for next two chars (= `2~`)
    * Insert key works in VS Code and in neovim via iTerm2
    * iTerm2 mapping of Insert key to `^[[2~` is correct (working in vim and reported in the same manner on WSL)
    * zsh version 5.9 is the same as on Ubuntu 24.04
 * fix:  neovim cursor color is always taken form iTerm2 > Settings > Profiles > Colors > Cursor colors > Cursor
    * Cursor color works in zsh, nano, /usr/bin/vim, though
 * fix:  brew install/upgrade only when admin: `groups $(whoami) | rg admin --quiet --word-regexp`.
    * Note: this affects also places where we `sudo` (often installing into `/usr/`):
       * `./install/*`
       * `theming-demo.sh`
       * `screen-recording.md`
    * maybe add an option "install for all users/current user"?
       * need to analyse if this is even possible, there might be many exceptions

## eval: nnn (https://github.com/jarun/nnn)
⚠️ Finalize | 💥 Next hot feature | ⏲ Waiting for answer

 * lowdown issue: https://github.com/kristapsdz/lowdown/issues/153 ⏲
    * fixed in master, waiting for release & brew update
    * workaround: [build lowdown](#build-lowdown)
 * evaluate preview-tui ⚠️
    * mpv
      * use to render gif
    * test in WSL (supported via Windows Terminal (preferred) and QuickLook)
      * tmux works; this is only about direct support ('winterm')
      * the following command fails: https://github.com/jarun/nnn/blob/master/plugins/preview-tui#L209
      * see https://learn.microsoft.com/en-us/windows/terminal/command-line-arguments
 * create a Norton/Midnight Commander like plugin (for copying/moving) 💥
   * nnn sets `NNN_PREFER_SELECTION` for plugins depending on `-u`
      * if `NNN_PREFER_SELECTION=1`: use hovered item if nothing selected, else selection
      * if `NNN_PREFER_SELECTION=0`: if selection is active, ask `'c'urrent/'s'el?`, otherwise use hovered
   * see https://github.com/jarun/nnn/wiki/Concepts#special-variables
 * align shortcuts with vim/fzf
    * space: use plugin (like leader key in vim)
    * ctrl+space: mark element (like in fzf)
    * https://github.com/jarun/nnn/wiki/Usage#broken-control-keybind
 * install on Raspberry Pi (from source?); test building from source on WSL

## all platforms
 * fix:  filenames with spaces show create line breaks in `gi`
 * fix:  lh-vim-lib fails for nvim 0.9.5 with commit > 17d75d6 -> create bug report
 * eval: https://github.com/charmbracelet/vhs - see [vhs](docs/screen-recording.md): re-evaluate, issues seem to be solved
 * eval: https://github.com/zsh-vi-more/
 * fix:  `watch -c echo` changes colors in current Window Terminal Tab after exit. Workarounds:
    * WSL (separate alternatives):
       * set theme (via `fg` or `cfg theme ...`)
       * Terminal > Settings > Save
       * use a new tab
    * macOS:
       * reload
 * feat: respect `XDG_CONFIG_HOME` for `~/.config/dotfiles` and `~/.config/git`
 * eval: fzf feats since v0.44, e.g. follow (see below), native tmux integration, wrap, highlight-line, with-shell, transform, etc.
    * consider using `▌` for pointer and `┃` for marker
 * eval: HTTPie for Terminal - install via brew and Ubuntu (https://httpie.io/docs/cli/installation)
 * feat: create a PR against junegunn/fzf to configure key-bindings and `CTRL_R_COMMAND`
 * feat: add docker-ps options to fzd
 * feat: make `zstyle ':completion:*' special-dirs true` toggleable in file/dir completions (needs a reload)
    * `1-completions.zsh`
    * possibly `fzf-config.sh:FZF_DEFAULT_OPTS`
    * could use shortcut `Ctrl+.` (= `Ctrl+n`)
 * fix:  nvim starts with `W18: Invalid character in group name` due to lualine's tabline being incompatible with nvim 0.7
    * can be fixed via upgrading to nvim 0.8+: https://github.com/neovim/neovim/blob/master/INSTALL.md#install-from-download
    * update also on ARM
    * Ubuntu 24.04 apt brings nvim v0.9.5
    * revert neovim installation from ppa to Ubuntu
 * feat: upgrade which-key.nvim to latest version when a compatible nvim version is installed (which-key currently needs nvim >= 0.9)
    * Ubuntu 24.04 apt brings nvim v0.9.5
    * revert which-key installation to latest in `.vimrc`
 * fix:  Vim8/Vim9 crashes when opened via F1 in zsh
 * fix:  Vim8 crashes when using fzf file finder (Space+F)
    * works on Ubuntu 24/macOS Vim9
 * feat: when installing a module (`install/install-*.sh`), add that module to `config.yml`. When doing `upgrade -a`, re-execute all those scripts.
 * fix:  git show / gi / gdi on merge commits looks broken in several use cases (read: https://stackoverflow.com/a/40986893)
 * theme monokai: separate monokai variants: vivid (high contrast), mild (latest version), ristretto
 * add themes nova, ocean, solarflare, snazzy
    * precondition: VS Code and Vim theme must exist (won't do myself!) -> test first: are they pleasant to the eyes?
    * mintty schemes as template for terminal can be found in https://github.com/iamthad/base16-mintty
    * nova: https://github.com/o0th/tmux-nova/blob/master/Gallery.md
 * feat: add bash completion for git clone and git alias (see zsh completion)
 * refactor: generalize Vim colorschemes into a constant (`"${DOTFILES_DIR}"/theming/themes`)
 * docs: demo vim theme switcher
 * fix:  In Vim8/Vim9, echoing background/colorscheme cycling does not work
 * fix:  cursor not working in bash+Vim8
    * not reproducible on remote
 * eval: [erdtree](https://github.com/solidiquis/erdtree) -> blazingly fast, but can't replace tre/eza on Ubuntu, until bug is solved:
    * fix: https://github.com/solidiquis/erdtree/issues/226

# later
 * eval: Amazon Q (~~https://fig.io~~ / ~~CodeWhisperer~~)
    * conflicts with fzf/zsh completion -> needs to be solved
    * evaluate to write custom completions
 * fix:  fzf-tab-completion does not work on macOS
    * there are known issues on github with fzf-tab-completion on macOS [wait until solved by lincheney]
 * feat: `install-kubernetes.sh` on macOS (when needed)
 * feat: `install-youcompleteme.sh` on macOs (when needed)
    * see https://github.com/ycm-core/YouCompleteMe?tab=readme-ov-file#macos
 * eval: https://www.nushell.sh/blog/2022-03-22-nushell_0_60.html
 * eval: https://github.com/junegunn/fzf/wiki/related-projects
 * eval: https://github.com/unixorn/awesome-zsh-plugins
 * eval: https://dotfiles.github.io/utilities/
 * fix:  blinking does not work in tmux 3.2a (fixed in tmux 3.3a, see https://github.com/tmux/tmux/issues/2682)
 * fix:  as soon as cursor styling is set within tmux, the cursor will flicker. neovim sets the cursor, so after exiting the flickering starts.
    * happens only in Windows Terminal, not in iTerm2
    * cf. `4b-cursor-style.zsh` and `printf "\e[5 q"` in function `t()`
    * only reproducible with a blinking cursor
    * workaround: always use steady cursor in tmux
 * feat: add theming for batgrep
    * Option a: wait for https://github.com/sharkdp/bat/issues/339 (favored)
    * Option b: https://gitlab.com/psicho/dotfiles/-/blob/main/config/bat/themes/Monokai-Extended-Dark.tmTheme#L54
    * Note: at least the highlight color seems to snap to 8-Bit colors, i.e. when defining a 24-Bit color, the nearest 8-Bit  color will be taken instead
 * eval: winget yaml config, see https://devblogs.microsoft.com/commandline/winget-configuration-preview/
 * feat: add id autocompletion for commands `wg pin add/remove`

# stuck/won't do
 * fix:  login with multiple targets is not working on macOS [not reproducible]
 * fix:  completions do not work for aliases - only custom solutions existing [workaround: use fzf-tab-completion]
 * feat: instead of not using gutentags on remote machines at all, make it configurable [not needed; reopen, if demand arises]
 * eval: akinsho/bufferline.nvim [depends on nvim 0.8, but we're currently installing nvim 0.7; using tabline instead]
 * eval: https://github.com/folke/tokyonight.nvim [could not get it working; only compatible with NeoVim]
 * fix:  Autocompletion does not work in bash: cursor positions at start of line. WA: Alt+Shift+L (clear screen). Maybe related to https://github.com/lincheney/fzf-tab-completion/issues/40.
    * [not reproducible anymore - seems to be fixed with Ubuntu 22.04 and/or latest fzf-tab-completion]
 * fix:  On some systems, vertical tmux panes break. The prompt of the right pane goes into the left pane. WA: logout/login. [not reproducible]
 * fix:  [zsh only] Repeatedly fetching history of the same entry does not work. [is an issue with vi-fetch-history itself]
    * Reproduction: Ctrl+R > delete line > Ctrl+R with same entry
 * eval: Shortcut to do the opposite of `Alt+.` (insert-last-word / yank-last-arg) [not possible after a few trials]
 * feat: delete ] from end of line automatically from command and from history [alternative: train muscle memory to hit enter only]
 * eval: antigen/zinit/zplug/zgen: start with no loading and add library by library (see branch zinit) [not planned anymore]
 * fix:  autocompletion sometimes stops working. when completing, cursor positions at start of line (before clock symbol). WA: relogin (does alt+shift+l/clear screen work aswell?). [not reproducible]
 * feat: bashify alt+a, alt+d, alt+f [never had the need to use it; reopen this issue if wanted]
 * feat: enable copy/paste via ssh [needs forwarding of powershell.exe via ssh; this is a security leak; therefore using terminal copy instead]
 * feat: enable variable preview for echo in fzf-tab [could not solve after several tries]
 * feat: write completion for reload [does not work, as `/usr/share/zsh/functions/completion/unix/_initctl` defines a completion for reload]
 * feat: replace `fzf_completion_trigger` with `fzf-tab` [both serve different purposes: completion trigger searches in all subdirs, fzf-tab completes only directly related files]
 * feat: enable redo in bash [found no way of doing it]
 * feat: save input on alt+shift+r in bash [there seems to be no bash analogy to `accept-line` and `print -z`]

# fzf follow example

```bash
kubectl get pods |
      fzf --header-lines=1 --border \
          --prompt "$(kubectl config view --minify -o jsonpath='{..namespace}')> " \
          --preview-window down:follow \
          --preview 'kubectl logs --follow --tail=100000 {1}' "$@"
```

# Build lowdown

```bash
git clone https://github.com/kristapsdz/lowdown
cd lowdown
git apply lowdown-build.patch
./configure
sudo make install install_libs
```

Installs to `/usr/local/bin/lowdown`. Note that homebrew path has higher priority.

## lowdown-build.patch
```
diff --git a/Makefile b/Makefile
index 3b61566..e310508 100644
--- a/Makefile
+++ b/Makefile
@@ -128,13 +128,7 @@ CFLAGS		+= -DVERSION=\"$(VERSION)\"
 # also set SANDBOX_INIT_ERROR_IGNORE in their environment to ignore
 # failure.
 # Has no effect unless HAVE_SANDBOX_INIT is defined.
-.ifdef SANDBOX_INIT_ERROR_IGNORE
-.if $(SANDBOX_INIT_ERROR_IGNORE) == "always"
 CFLAGS		+= -DSANDBOX_INIT_ERROR_IGNORE=2
-.else
-CFLAGS		+= -DSANDBOX_INIT_ERROR_IGNORE=1
-.endif
-.endif
 # Because the objects will be compiled into a shared library:
 CFLAGS		+= -fPIC
 # To avoid exporting internal functions (lowdown.h has default visibility).
diff --git a/configure b/configure
index 8ae6a78..23e0136 100755
--- a/configure
+++ b/configure
@@ -95,7 +95,7 @@ LDADD_SCAN_SCALED=
 LDADD_STATIC=
 CPPFLAGS=
 LDFLAGS=
-LINKER_SONAME=
+LINKER_SONAME="-install_name"
 DESTDIR=
 PREFIX="/usr/local"
 BINDIR=
```
