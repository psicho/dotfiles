#!/usr/bin/env bash

# this fixes "zsh: exec format error: <windows program name>"
# see https://github.com/microsoft/WSL/issues/8952
# needs to be called after minikube start/minikube stop

sudo sh -c 'echo :WSLInterop:M::MZ::/init:PF > /usr/lib/binfmt.d/WSLInterop.conf'
sudo systemctl unmask systemd-binfmt.service
sudo systemctl restart systemd-binfmt
sudo systemctl mask systemd-binfmt.service
