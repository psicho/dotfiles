#!/usr/bin/env bash

# Url template
template="https://www.example.com/gallery/image-%s.jpg"
for page in {1..69}; do
    # %0nd pads source/target with n zeros
    printf -v source "%03d" "${page}"
    printf -v target "%02d" "${page}"
    # shellcheck disable=SC2059
    printf -v url "${template}" "${source}"
    echo "Downloading ${url} to ${target}.jpg."
    curl --silent "${url}" --output "${target}.jpg"
done
