#!/usr/bin/env bash

# System clipboard integration
# Adapted from https://github.com/ohmyzsh/ohmyzsh/blob/master/lib/clipboard.zsh
#
# This file has support for doing system clipboard copy and paste operations
# from the command line in a generic cross-platform fashion.
#
# This uses essentially the same heuristic as neovim, with the additional
# special support for Cygwin.
# See: https://github.com/neovim/neovim/blob/e682d799fa3cf2e80a02d00c6ea874599d58f0e7/runtime/autoload/provider/clipboard.vim#L55-L121
#
# - powershell (if running in WSL)
# - tmux (if tmux is installed)
# - pbcopy, pbpaste (macOS)
# - cygwin (Windows running Cygwin)
# - wl-copy, wl-paste (if $WAYLAND_DISPLAY is set)
# - xclip (if $DISPLAY is set)
# - xsel (if $DISPLAY is set)
# - lemonade (for SSH) https://github.com/pocke/lemonade
# - doitclient (for SSH) http://www.chiark.greenend.org.uk/~sgtatham/doit/
# - win32yank (Windows)
# - termux-clipboard (Android)
#
# Defines two functions, clipcopy and clippaste, based on the detected platform.
#
### clipcopy - Copy data to clipboard
#
# Usage:
#   <command> | clipcopy         - copies stdin to clipboard
#   clipcopy <filename>          - copies a file's contents to clipboard
#   clipcopy < <filename>        - copies a file's contents to clipboard
#   clipcopy <<< $(<command>)    - copies a command's result to clipboard
#
### clippaste - Paste data from clipboard to stdout
#
# Usage:
#   clippaste               - writes clipboard's contents to stdout
#   clippaste | <command>   - pastes contents and pipes it to another process
#   clippaste > <file>      - paste contents to a file

__detect_clipboard() {
    [[ -n "${ZSH_VERSION}" ]] && emulate -L zsh

    if [[ $(uname -r) == *icrosoft* ]]; then
        clipcopy() { if __is_socket_input; then __wsl_copy; else __wsl_copy < "${1:-/dev/stdin}"; fi }
        clippaste() { powershell.exe -noprofile -command Get-Clipboard; }
    elif [[ "${OSTYPE}" == darwin* && $(command -v pbcopy) && $(command -v pbpaste) ]]; then
        clipcopy() { if [[ $# -gt 0 ]]; then __mac_copy < "$1"; else __mac_copy; fi }
        clippaste() { pbpaste; }
    elif [[ $(command -v tmux) ]]; then
        clipcopy() { __check_tmux_session "create"; tmux load-buffer "${1:--}"; }
        clippaste() { __check_tmux_session && tmux save-buffer - 2>/dev/null; }
    elif [[ "${OSTYPE}" =~ (cygwin|msys).* ]]; then
        clipcopy() { cat "${1:-/dev/stdin}" > /dev/clipboard; }
        clippaste() { cat /dev/clipboard; }
    elif [[ -n ${WAYLAND_DISPLAY:-} && $(command -v wl-copy) && $(command -v wl-paste) ]]; then
        clipcopy() { wl-copy < "${1:-/dev/stdin}"; }
        clippaste() { wl-paste; }
    elif [[ -n ${DISPLAY:-} && $(command -v xclip) ]]; then
        clipcopy() { xclip -in -selection clipboard < "${1:-/dev/stdin}"; }
        clippaste() { xclip -out -selection clipboard; }
    elif [[ -n ${DISPLAY:-} && $(command -v xsel) ]]; then
        clipcopy() { xsel --clipboard --input < "${1:-/dev/stdin}"; }
        clippaste() { xsel --clipboard --output; }
    elif command -v lemonade; then
        clipcopy() { lemonade copy < "${1:-/dev/stdin}"; }
        clippaste() { lemonade paste; }
    elif command -v doitclient; then
        clipcopy() { doitclient wclip < "${1:-/dev/stdin}"; }
        clippaste() { doitclient wclip -r; }
    elif command -v win32yank; then
        clipcopy() { win32yank -i < "${1:-/dev/stdin}"; }
        clippaste() { win32yank -o; }
    elif [[ "${OSTYPE}" == linux-android* && $(command -v termux-clipboard-set) ]]; then
        clipcopy() { termux-clipboard-set "${1:-/dev/stdin}"; }
        clippaste() { termux-clipboard-get; }
    else
        clipcopy() { print "clipcopy: Platform ${OSTYPE} not supported or no clipboard installed" >&2; }
        clippaste() { print "clippaste: Platform ${OSTYPE} not supported or no clipboard installed" >&2; }
    fi
}

# returns 1 if no tmux session exists
# returns 0 if a tmux session exists
__check_tmux_session() {
    local create_if_not_running="$1"
    local session_result
    local result_file=~/.cache/session_result
    mkfifo ${result_file}
    # get sessions in a subshell, writing stderr to fifo, assuring to not block
    (tmux list-sessions 1>/dev/null 2> ${result_file} &)
    session_result=$(cat ${result_file})
    rm ${result_file}
    if [[ -n "${session_result}" ]]; then
        [[ -n "${create_if_not_running}" ]] && tmux new-session -d &>/dev/null
        return 1
    else
        return 0
    fi
}

__mac_copy() {
    local output="" line
    while IFS= read -r line || [[ -n "${line}" ]]; do
        if [[ -z "${output}" ]]; then
            output="$line"
        else
            printf -v output "%s\n%s" "${output}" "${line}"
        fi
    done
    printf "%s" "${output}" | pbcopy
}

__wsl_copy() {
    local output="" line
    while IFS= read -r line || [[ -n "${line}" ]]; do
        if [[ -z "${output}" ]]; then
            output="$line"
        else
            printf -v output "%s\n%s" "${output}" "${line}"
        fi
    done
    # escape single quotation marks
    output=${output//\'/\'\'}
    powershell.exe -noprofile -command "Set-Clipboard -Value '${output}'"
}

# input via reading a file (<)
# see https://gist.github.com/zackdouglas/3123584
__is_socket_input() {
    local input
    input=$(readlink /proc/$$/fd/0)
    [[ ${input} =~ ^socket ]] && return 0 || return 1
}

__detect_clipboard

# needed if called from a separate shell
if [[ "$1" == "--copy" ]]; then
    shift
    clipcopy "$@"
fi
