#!/usr/bin/env bash

count() {
    local num_in_dir dir pad padlength1 padlength2 padlength3
    local subdir dirs files size size_string size_sum_string
    local dir_sum=0 file_sum=0 size_sum=0
    local options ret no_git find_opts

    no_git=false
    options=$(getopt --options hn --longoptions help,no-git -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __count_usage; return 0;;
            -n | --no-git) no_git=true;;
            --) shift; break;;
        esac
        shift
    done

    if [[ -n "$1" ]]; then
        dir="$1"
    else
        dir=${PWD}
    fi
    pad=$(printf '%0.1s' " "{1..60})
    padlength1=40
    padlength2=7
    padlength3=18

    if [[ "${no_git}" == true ]]; then
        find_opts=(--hidden --exclude ".git" --exclude ".mypy_cache")
    else
        find_opts=(--hidden --no-ignore)
    fi
    # shellcheck disable=SC2068
    num_in_dir="$(fdfind --max-depth 1 ${find_opts[@]} . "${dir}" | wc --lines)"
    echo "${COL_YELLOW_FG}${num_in_dir} ${COL_RESET}files and subdirs in directory ${COL_BLUE_FG}${dir}${COL_RESET}; recursing:"
    echo "${COL_WHITE_BG}${COL_BLACK_FG}Directory                           Dirs  Files              Byte${COL_RESET}"
    # shellcheck disable=SC2068
    for d in $(fdfind --type directory --max-depth 1 ${find_opts[@]} . "${dir}"); do
        subdir=${d#"${dir}"}
        subdir=${subdir//\//}
        [[ "${no_git}" == true && "${subdir}" == ".git" ]] && continue
        # in this case, find is faster than fd
        dirs=$(find "${d}" -type d | wc --lines)
        files=$(find "${d}" -type f | wc --lines)
        size=$(du --bytes --summarize "${d}" | cut --fields=1)
        size_string=$(echo "${size}" | sed ':a;s/\B[0-9]\{3\}\>/.&/;ta')
        printf "${COL_BLUE_FG}%s${COL_RESET}" "${subdir}"
        printf '%*.*s' 0 $((padlength1 - ${#subdir} - ${#dirs})) "${pad}"
        printf '%s' "${dirs}"
        printf '%*.*s' 0 $((padlength2 - ${#files})) "${pad}"
        printf '%s' "${files}"
        printf '%*.*s' 0 $((padlength3 - ${#size_string})) "${pad}"
        printf '%s\n' "${size_string}"
        ((dir_sum+=dirs))
        ((file_sum+=files))
        ((size_sum+=size))
    done
    printf "\e[53m${COL_YELLOW_FG}%s" "Sum"
    printf '%*.*s' 0 $((padlength1 - 3 - ${#dir_sum})) "${pad}"
    printf '%s' "${dir_sum}"
    printf '%*.*s' 0 $((padlength2 - ${#file_sum})) "${pad}"
    printf '%s' "${file_sum}"
    size_sum_string=$(echo "${size_sum}" | sed ':a;s/\B[0-9]\{3\}\>/.&/;ta')
    printf '%*.*s' 0 $((padlength3 - ${#size_sum_string})) "${pad}"
    printf "%s\n${COL_RESET}" "${size_sum_string}"
}

__count_usage() {
    colorize-help <<-EOF
		Count files and directories

		Usage:
		    count [option] [directory]

		Options:
		    -h, --help    Print this help
		    -n, --no-git  Exclude directory .git and respect .gitignore

		Arguments:
		    directory     Directory to count in. pwd, if none provided.
	EOF
}

count-lines() {
    local num_files no_git options ret dir

    no_git=false
    options=$(getopt --options hn --longoptions help,no-git -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __count_lines_usage; return 0;;
            -n | --no-git) no_git=true;;
            --) shift; break;;
        esac
        shift
    done

    if [[ -n "$1" ]]; then
        dir="$1"
    else
        dir=${PWD}
    fi

    if [[ "${no_git}" == true ]]; then
        find_opts=(--hidden --exclude ".git" --exclude ".mypy_cache")
    else
        find_opts=(--hidden --no-ignore)
    fi
    # shellcheck disable=SC2068
    num_files=$(fdfind --type file ${find_opts[@]} | wc --words)
    if [[ "${num_files}" -gt 0 ]]; then
        # shellcheck disable=SC2068
        fdfind --type file ${find_opts[@]} \
            | xargs file \
            | rg 'ASCII|UTF-8' \
            | cut --delimiter=':' --fields=1 \
            | xargs cat \
            | wc --lines
    fi
}

__count_lines_usage() {
    colorize-help <<-EOF
		Count lines in files, recursively

		Usage:
		    count [option]

		Options:
		    -h, --help    Print this help
		    -n, --no-git  Exclude directory .git and respect .gitignore

		Arguments:
		    directory     Directory to count. pwd, if none provided.
	EOF
}

stats() {
    if git rev-parse --is-inside-work-tree &>/dev/null; then
        echo "Lines: $(count-lines --no-git)"
        echo "Commits:"
        git contributors | /usr/bin/env cat
        if command -v git-stats-importer >/dev/null && command -v git-stats >/dev/null; then
            local emails
            emails=$(git log --all --format='%cE' | sort --unique | tr '\n' ',')
            git-stats-importer --emails "${emails}" &>/dev/null
            if is_dark_theme; then
                git-stats --global-activity
            else
                git-stats --global-activity --light
            fi
        else
            echo "${COL_YELLOW_FG}Execute ${DOTFILES_DIR}/install/install-git-stats.sh for more details.${COL_RESET}"
        fi
        count --no-git
    else
        echo "${COL_RED_FG}Not in a git directory.${COL_RESET}"
        echo "Lines: $(count-lines)"
        count
    fi
}
