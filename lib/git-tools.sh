#!/usr/bin/env bash

git-tools() {
    local options ret _command
    options=$(getopt --options h --longoptions help -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __git_tools_usage; return 0;;
            --) shift; break;;
        esac
        shift
    done

    _command="$1"
    case "${_command}" in
        f | fetch) __git_all_fetch;;
        i | info) __git_all_info;;
        p | prune) __git_all_prune;;
        r | recache) __git_recache;;
        s | sync) __git_all_sync;;
        u | update) __git_all_update;;
        *)
            echo "${COL_RED_FG}Unknown command:${COL_RESET} ${_command}"
            __git_tools_usage
            ;;
    esac
}
alias gt='git-tools'

__git_all_fetch() {
    for d in "${PROJECT_DIR}/"*/ ; do
        cd "${d}" || continue
        # Check if the current directory is in a Git repository.
        if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" ]; then
            echo "${COL_CYAN_FG}Fetching ${d}...${COL_RESET}"
            git fetch
        fi
    done
    cd "${PROJECT_DIR}" || return
}

__git_all_info() {
    local reference_dir padlength pad git_prompt

    padlength=50
    pad=$(printf '%0.1s' " "{1..50})
    reference_dir=${PWD}

    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/lib/prompt-tools.sh"
    for d in */ ; do
        cd "${reference_dir}/${d}" || continue
        printf "${COL_BLUE_FG}%s" "${d}"
        printf '%*.*s' 0 $((padlength - ${#d})) "${pad}"
        git_prompt=$(git_prompt "${COL_CYAN_FG}" "${COL_BLUE_FG}")
        printf "${git_prompt}%s\n" ""
    done
    echo -n "${COL_RESET}"

    cd "$reference_dir" || return
}

__git_all_prune() {
    for d in "${PROJECT_DIR}/"*/ ; do
        cd "${d}" || continue
        # Check if the current directory is in a Git repository.
        if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" ]; then
            echo "${COL_CYAN_FG}Pruning ${d}...${COL_RESET}"
            git prune-branches
        fi
    done
    cd "${PROJECT_DIR}" || return
}

__git_recache() {
    __validate "${GITHUB_USER}" "GITHUB_USER" || return 1
    __validate "${GITHUB_TOKEN}" "GITHUB_TOKEN" || return 1
    __validate "${GITHUB_ACCOUNT}" "GITHUB_ACCOUNT" || return 1

    echo "${COL_CYAN_FG}Updating repo cache for user ${GITHUB_USER} on account ${GITHUB_ACCOUNT} using GITHUB_TOKEN.${COL_RESET}"

    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/lib/library/github.sh"
    rm --force ~/.cache/repos.cache
    get-all-repos "${GITHUB_USER}" "${GITHUB_TOKEN}" >> ~/.cache/repos.cache
}

__git_all_sync() {
    __git_all_update
    __git_all_prune
}

__git_all_update() {
    for d in "${PROJECT_DIR}/"*/ ; do
        cd "${d}" || continue
        # Check if the current directory is in a Git repository.
        if [ "$(git rev-parse --is-inside-work-tree 2>/dev/null)" ]; then
            echo "${COL_CYAN_FG}Pulling ${d}...${COL_RESET}"
            git pull
        fi
    done
    cd "${PROJECT_DIR}" || return
}

__validate() {
    local validatee="$1"
    local validateeName="$2"
    if [[ -z "${validatee}" ]]; then
        printf "%s not set." "${validateeName}"
        return 1
    fi
}

__git_tools_usage() {
    colorize-help <<-EOF
		Call a git tool command

		Usage:
		    git-tools <command>

		Options:
		    -h, --help          Print this help

		Commands:
		    f, fetch            Fetch all repos in \$PROJECT_DIR $PROJECT_DIR
		    i, info             Show git info for all subdirectories
		    p, prune            Prune all branches of all repos in \$PROJECT_DIR $PROJECT_DIR
		    r, recache          Prepare repo autocompletion for GitHub. Needs vars GITHUB_{USER/ACCOUNT/TOKEN}.
		    s, sync             Convenience command for update and prune
		    u, update           Pull all repos in \$PROJECT_DIR $PROJECT_DIR
	EOF
    return 0
}

__git_tools_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local options="fetch info prune recache sync update"
    if [[ "${COMP_CWORD}" -eq 1 ]]; then
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "${options}" -- "${cur}"))
    fi
    return 0
}

if [[ -n "${BASH_VERSION}" ]]; then
    complete -F __git_tools_completion git-tools
fi

# WSL: needs BROWSER configured appropriately and xdg-open installed
#      open is a symlink to xdg-open
gr() {
    local repo host branch branch_suffix target
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/lib/library/github.sh"
    if [[ -n "$1" ]]; then
        host="https://github.com"
        if [[ "$1" =~ "/" ]]; then
            repo="$1"
        else
            repo="${GITHUB_USER}/$1"
        fi
    elif git rev-parse --is-inside-work-tree &>/dev/null; then
        host="$(get-current-host)"
        repo="$(get-current-repo)"
        branch="$(git symbolic-ref --quiet --short HEAD 2>/dev/null)"
        branch_suffix="/tree/${branch}"
    else
        host="https://github.com"
        repo="${GITHUB_USER}"
    fi
    target="${host}/${repo}${branch_suffix}"
    if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
        open "${target}"
    fi
}

git-search() {
    local regex="$1"
    local filename="$2"
    shift 2
    local extra_git_grep_args="$*"

    # extra_git_grep_args: -h = suppress output of filename for each match

    # list all branches (including remote)
    # remove first two characters (containing "  " or "* ")
    # take input before first space
    # search for regex in filename
    # shellcheck disable=SC2086
    git branch --all \
        | cut --characters=3- \
        | cut --delimiter=' ' --fields=1 \
        | xargs --replace=% git grep ${extra_git_grep_args} "${regex}" % -- "${filename}"
}

if [[ "$1" == "--open" ]]; then
    shift
    gr "$@"
fi
