#!/usr/bin/env bash

# Configure it in ~/.ssh/config

alias l='login'

login() {
    local host
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/lib/library/system.sh"

    if [[ "$1" == "-h" || "$1" == "--help" ]]; then
        __login_usage
        return 0
    fi
    if [[ "$#" -eq 0 ]]; then
        echo "Please provide at least one machine name."
        __login_usage
        return 1
    fi
    if [[ "$#" -gt 9 ]]; then
        echo "Maximum number of machines is 9."
        __login_usage
        return 1
    fi

    machines="$(__extract_machines)"
    for host in "$@"; do
        if [[ ! "${machines}" =~ (^|$'\n')${host}($|$'\n') ]]; then
            echo "Unknown machine: '${host}'."
            __login_usage
            return 1
        fi
    done

    if [[ "$#" -eq 1 ]]; then
        eval "$(__get_ssh_command "$1")"
    else
        if [[ "$#" -gt 1 ]]; then
            if [[ -z "${TMUX_PANE}" ]]; then
                # if we're not already in tmux, just prepare the first pane
                tmux new-session -d "$(__get_ssh_command "$1")"
            fi
            tmux split-window -h "$(__get_ssh_command "$2")"
        fi
        [[ "$#" -gt 2 ]] && tmux split-window -h "$(__get_ssh_command "$3")"
        [[ "$#" -gt 3 ]] && tmux split-window -t 3 -v "$(__get_ssh_command "$4")"
        [[ "$#" -gt 4 ]] && tmux split-window -t 3 -v "$(__get_ssh_command "$5")"
        [[ "$#" -gt 5 ]] && tmux split-window -t 2 -v "$(__get_ssh_command "$6")"
        [[ "$#" -gt 6 ]] && tmux split-window -t 2 -v "$(__get_ssh_command "$7")"
        [[ "$#" -gt 7 ]] && tmux split-window -t 1 -v "$(__get_ssh_command "$8")"
        [[ "$#" -gt 8 ]] && tmux split-window -t 1 -v "$(__get_ssh_command "$9")"
        if [[ "$#" -gt 3 ]]; then
            tmux select-layout tiled
        else
            tmux select-layout even-horizontal
        fi
        tmux select-pane -t 1
        if [[ -n "${TMUX_PANE}" ]]; then
            # we're already in tmux, directly connect to the remote machine interactively
            eval "$(__get_ssh_command "$1")"
        else
            # we're not yet in tmux, connect to the first pane
            tmux -2 attach-session -d
        fi
    fi
}

__get_ssh_command() {
    local target="$1"
    # shellcheck disable=SC2153
    local background="${BACKGROUND}"
    if [[ "${background}" == "system" && (-n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin*) ]]; then
        # shellcheck disable=SC1091
        source "${DOTFILES_DIR}/theming/set-theme.sh"
        background=$(get_system_background)
    fi
    echo "ssh -t ${target} 'export CLIENT_THEME=${THEME}; export CLIENT_BG=${background}; \$SHELL --login'"
}

__login_usage() {
    local hosts
    hosts=$(__extract_machines)
    # indent each host with 4 spaces
    hosts=$(echo "${hosts}" | sed --expression='s/^/    /g')
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/lib/help.sh"
    colorize-help <<-EOF
		Login to one or more machines using ssh with autocompletion

		Up to 9 machines names from ~/.ssh/config can be provided. tmux with an evenly tiled layout is used for multiple machines.

		Usage:
		    login <machine> [...]

		Options:
		    -h, --help  Print this help

		Available machines:
		${hosts}
	EOF
}

__extract_machines() {
    local hosts
    hosts=$(rg "^Host [^*]" "${HOME}/.ssh/config" | sort)
    hosts=${hosts//Host/}
    # trim hosts and return value through command substitution
    echo "${hosts//[[:blank:]]/}"
}

__login_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(__extract_machines)" -- "${cur}"))
    return 0
}

# In non-interactive shell (e.g. tmux help pane), this command would break
if [[ $- == *i* ]]; then
    complete -F __login_completion login l
fi
