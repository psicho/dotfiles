# Adapted from https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/sudo

# ------------------------------------------------------------------------------
# Description
# -----------
#
# sudo or sudoedit will be inserted before current command
# or before last command, if current command is empty
#
# ------------------------------------------------------------------------------
# Authors
# -------
#
# * Dongweiming <ciici123@gmail.com>
#
# ------------------------------------------------------------------------------

sudo-command-line() {
    [[ -z "${BUFFER}" ]] && zle up-history
    if [[ "${BUFFER}" == sudo\ * ]]; then
        LBUFFER="${LBUFFER#sudo }"
    elif [[ "${BUFFER}" == "${EDITOR}"\ * ]]; then
        LBUFFER="${LBUFFER#${EDITOR} }"
        LBUFFER="sudoedit ${LBUFFER}"
    elif [[ "${BUFFER}" == "sudoedit"\ * ]]; then
        LBUFFER="${LBUFFER#sudoedit }"
        LBUFFER="${EDITOR} ${LBUFFER}"
    else
        LBUFFER="sudo ${LBUFFER}"
    fi
}
zle -N sudo-command-line
# Defined shortcut keys: <C-f>
bindkey -M emacs '^F' sudo-command-line
bindkey -M vicmd '^F' sudo-command-line
bindkey -M viins '^F' sudo-command-line
