# reload deleting zcompdump
reload_zsh() {
    if [[ "$#" -eq 1 ]] && { [[ "$1" == "--help" || "$1" == "-h" ]]; }; then
        colorize-help <<-EOF
			Reload current shell

			Usage:
			    reload [options]

			Options:
			    -d, --delete-zcompdump  Delete zcompdump to recreate zsh completion
			                            Note: On macOS/WSL, zcompdump is always deleted
			    -h, --help              Print this help
		EOF
        return
    fi
    if [[ "$#" -eq 1 ]] && { [[ "$1" == "--delete-zcompdump" || "$1" == "-d" ]]; }; then
        rm --force ~/.cache/zsh/zcompdump ~/.zcompdump
    fi
    if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
        rm --force ~/.cache/zsh/zcompdump ~/.zcompdump
    fi
    reload-shell
}

# reload using keybinding
reload_widget() {
    printf "${BUFFER}" > "${HOME}/.cache/last_command"
    BUFFER="reload"
    zle accept-line
}
zle -N reload_widget
bindkey -M viins '^[R'    reload_widget

# after a reload, load the last command into the buffer
set_last_command() {
    local last_command
    local last_command_file="${HOME}/.cache/last_command"

    if [[ -f "${last_command_file}" ]]; then
        last_command="$(head --lines=1 ${last_command_file})"
        if [[ -n "${last_command}" ]]; then
            rm "${last_command_file}"
            print -z "${last_command}"
            bind-backspace
        fi
	fi
}
