# Adapted from https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/copybuffer

# turn CTRL+Z into a toggle switch
ctrlz() {
    if [[ $#BUFFER == 0 ]]; then
        fg &>/dev/null && zle redisplay
    else
        zle push-input
    fi
}
zle -N ctrlz
bindkey '^Z' ctrlz
