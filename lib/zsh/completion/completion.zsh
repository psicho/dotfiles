# Adapted from https://github.com/ohmyzsh/ohmyzsh/blob/master/lib/completion.zsh
zmodload -i zsh/complist

LISTMAX=400              # ask to show autocomplete menu if at least 400 entries
setopt MENU_COMPLETE     # show autocomplete menu on first press of tab
setopt COMPLETE_IN_WORD  # when completing, keep the cursor and complete from both ends
setopt ALWAYS_TO_END     # move cursor to end if a single match is inserted or menu completion is performed
setopt GLOB_DOTS         # do not require a leading ‘.’ in a filename to be matched explicitly

zstyle ':completion:*'                  list-colors ${(s.:.)LS_COLORS}                    # activate color-completion(!)
zstyle ':completion:*:matches'          group 'yes'
zstyle ':completion:*'                  group-name ''
zstyle ":completion:*:git-checkout:*"   sort false
zsh-completion-format() {
    zstyle ':completion:*:descriptions' format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'  # format on completion
    zstyle ':completion:*'              menu select
}
fzf-tab-completion-format() {
    zstyle ':completion:*:descriptions' format '%d'
    zstyle ':completion:*'              menu no
}
# default to zsh completion
zsh-completion-format

# configure fzf-tab (https://github.com/Aloxaf/fzf-tab#configure)
## Use tmux popup
zstyle ':fzf-tab:*'                     fzf-command ftb-tmux-popup
zstyle ':fzf-tab:*'                     use-fzf-default-opts yes

## use colors from theming/fzf-tab/
if [[ -n "${FZF_TAB_DEFAULT_COLOR}" ]]; then
    zstyle ':fzf-tab:*'                 default-color $FZF_TAB_DEFAULT_COLOR
fi
if [[ -n "${FZF_TAB_GROUP_COLORS}" ]]; then
    zstyle ':fzf-tab:*'                 group-colors $FZF_TAB_GROUP_COLORS
fi

# Note: COL_FZF_FG_PLUS has no effect here, as it is overriden by default-color
_FZF_TAB_FZF_FLAGS=(
    '--height=80%' \
    '--layout=reverse' \
    '--color='"hl:${COL_FZF_HL},hl+:${COL_FZF_HL_PLUS},pointer:${COL_FZF_POINTER},marker:${COL_FZF_MARKER},bg+:${COL_FZF_BG_PLUS},header:${COL_FZF_HEADER},fg+:${COL_FZF_FG_PLUS}"
)

## preview files and directories for any command, especially ls and eza (though also useful for others), initially hidden
if command -v eza &>/dev/null; then
    zstyle ':fzf-tab:complete:*'    fzf-preview  '([[ -f $realpath ]] && bat --style=numbers --color=always "$realpath") || \
                                                  ([[ -d $realpath ]] && (eza --all --long --group --git --links \
                                                    --time-style=long-iso --header --group-directories-first --icons=always \
                                                    --tree --ignore-glob \.git --color=always "$realpath" | bat --plain))'
else
    zstyle ':fzf-tab:complete:*'    fzf-preview  '([[ -f $realpath ]] && bat --style=numbers --color=always "$realpath") || \
                                                  ([[ -d $realpath ]] && (ls -l --almost-all --human-readable --color \
                                                    --group-directories-first "$realpath" | bat --plain))'
fi
zstyle ':fzf-tab:complete:*'        fzf-flags    '--preview-window=hidden:80%' $_FZF_TAB_FZF_FLAGS
zstyle ':fzf-tab:complete:*'        popup-pad    1000 1000
zstyle ':fzf-tab:complete:*'        fzf-bindings 'ctrl-y:execute-silent({_FTB_INIT_} source '"${DOTFILES_DIR}"'/lib/clipboard.sh \
                                                    && echo "$word" | clipcopy)'
# ...but show preview window for eza and ls
zstyle ':fzf-tab:complete:eza:*'    fzf-flags    '--preview-window=right:80%' $_FZF_TAB_FZF_FLAGS
zstyle ':fzf-tab:complete:eza:*'    fzf-bindings 'ctrl-v:execute({_FTB_INIT_}code "$realpath")' \
                                                 'ctrl-e:execute({_FTB_INIT_}$EDITOR "$realpath")' \
                                                 'ctrl-y:execute-silent({_FTB_INIT_} source '"${DOTFILES_DIR}"'/lib/clipboard.sh \
                                                    && echo "$word" | clipcopy)'
zstyle ':fzf-tab:complete:ls:*'     fzf-flags    '--preview-window=right:80%' $_FZF_TAB_FZF_FLAGS
zstyle ':fzf-tab:complete:ls:*'     fzf-bindings 'ctrl-v:execute-silent({_FTB_INIT_}code "$realpath")' \
                                                 'ctrl-e:execute({_FTB_INIT_}$EDITOR "$realpath" < /dev/tty > /dev/tty)' \
                                                 'ctrl-y:execute-silent({_FTB_INIT_} source '"${DOTFILES_DIR}"'/lib/clipboard.sh \
                                                    && echo "$word" | clipcopy)'
## preview directories for cd
if command -v eza &>/dev/null; then
    zstyle ':fzf-tab:complete:cd:*' fzf-preview  'eza --all --long --group --git --links --time-style=long-iso --header \
                                                    --group-directories-first --icons=always --tree --ignore-glob \.git \
                                                    --color=always "$realpath" \
                                                    | bat --plain'
else
    zstyle ':fzf-tab:complete:cd:*' fzf-preview  'ls -l --almost-all --human-readable --color --group-directories-first "$realpath" \
                                                    | bat --plain'
fi
zstyle ':fzf-tab:complete:cd:*'     fzf-flags    '--preview-window=right:80%' $_FZF_TAB_FZF_FLAGS
zstyle ':fzf-tab:complete:cd:*'     fzf-bindings 'ctrl-v:execute-silent({_FTB_INIT_}code "$realpath")' \
                                                 'ctrl-e:execute({_FTB_INIT_}$EDITOR "$realpath" < /dev/tty > /dev/tty)' \
                                                 'ctrl-y:execute-silent({_FTB_INIT_} source '"${DOTFILES_DIR}"'/lib/clipboard.sh \
                                                    && echo "$word" | clipcopy)'
## preview environment variables for export
zstyle ':fzf-tab:complete:export:*' fzf-preview  '${DOTFILES_DIR}/config/fzf-config.sh --echo "$word"'
zstyle ':fzf-tab:complete:export:*' fzf-flags    '--preview-window=right:80%' $_FZF_TAB_FZF_FLAGS
zstyle ':fzf-tab:complete:export:*' fzf-bindings 'ctrl-y:execute-silent({_FTB_INIT_} source '"${DOTFILES_DIR}"'/lib/clipboard.sh \
                                                    && echo "$word" | clipcopy)'

# Use vi like keys in tab complete menu
bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^j' vi-down-line-or-history
bindkey -M menuselect '^k' vi-up-line-or-history
bindkey -M menuselect '^l' vi-forward-char
# complete current entry and stay in completion menu
bindkey -M menuselect '^o' accept-and-hold
# Filter menu using interactive mode; observe: Ctrl+I = Tab!
bindkey -M menuselect '^i' vi-insert
# Use Ctrl+G to exit the menu, just like lesskey does
bindkey -M menuselect '^g' send-break

# Complete . and .. special directories
#zstyle ':completion:*'                      special-dirs true

zstyle ':completion:*:*:kill:*:processes'   list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

if [[ "${OSTYPE}" == solaris* ]]; then
    zstyle ':completion:*:*:*:*:processes'  command "ps -u "${USER}" -o pid,user,comm"
else
    zstyle ':completion:*:*:*:*:processes'  command "ps -u "${USER}" -o pid,user,comm -w -w"
fi

# disable named-directories autocompletion
zstyle ':completion:*:cd:*'                 tag-order local-directories directory-stack path-directories

# Use caching so that commands like apt and dpkg complete are useable
zstyle ':completion:*'                      use-cache yes
zstyle ':completion:*'                      cache-path "${ZSH_CACHE_DIR}"

# Don't complete uninteresting users
zstyle ':completion:*:*:*:users'            ignored-patterns \
        adm amanda apache at avahi avahi-autoipd beaglidx bin cacti canna \
        clamav daemon dbus distcache dnsmasq dovecot fax ftp games gdm \
        gkrellmd gopher hacluster haldaemon halt hsqldb ident junkbust kdm \
        ldap lp mail mailman mailnull man messagebus  mldonkey mysql nagios \
        named netdump news nfsnobody nobody nscd ntp nut nx obsrun openvpn \
        operator pcap polkitd postfix postgres privoxy pulse pvm quagga radvd \
        rpc rpcuser rpm rtkit scard shutdown squid sshd statd svn sync tftp \
        usbmux uucp vcsa wwwrun xfs '_*'

# ... unless we really want to.
zstyle '*'                                  single-ignored show

if [[ "${COMPLETION_WAITING_DOTS}" == true ]]; then
    expand-or-complete-with-dots() {
        # toggle line-wrapping off and back on again
        [[ -n "$terminfo[rmam]" && -n "$terminfo[smam]" ]] && echoti rmam
        print -Pn "%{%F{red}......%f%}"
        [[ -n "$terminfo[rmam]" && -n "$terminfo[smam]" ]] && echoti smam

        zle expand-or-complete
        zle redisplay
    }
    zle -N expand-or-complete-with-dots
    bindkey "^I" expand-or-complete-with-dots
fi

# Add all completions to fpath. This must be done before running compinit.
FPATH=$FPATH:"${DOTFILES_DIR}/lib/zsh/completion"

# automatically load bash completion functions
autoload -U +X bashcompinit && bashcompinit

# Notes
#   aliases must be defined before compinit, so that autocompletion works
#       (e.g. alias g=git and d=docker)
#       This seems to be deprecated - it works now doing compinit and defining aliases afterwards
#   In WSL, compinit prints a warning, if docker is not running
#       `compinit:503: no such file or directory: /usr/share/zsh/vendor-completions/_docker`
#   Another compinit is being performed by Ubuntu with default location ~/.zcompdump before .zlogin is called.
#       This can be disabled, but then logins are a lot slower.
#       See https://github.com/zdharma-continuum/zinit#disabling-system-wide-compinit-call-ubuntu on how to disable it.
#   On macOS, there is no system wide compinit call. Therefore, we need to perform that before load-common-files.
#       Without any copy of compinit there, on macOS we get multiple 'complete:13: command not found: compdef'
#       With compinit -u -C -d "${HOME}/.cache/zsh/zcompdump", custom zsh autocompletions (lib/zsh/completion) don't work.
#   If completions are not working as expected, delete any zcompdump file and reload:
#       "reload -d"
autoload -Uz compinit && compinit -u -C -d "${HOME}/.cache/zsh/zcompdump"
