#compdef cfg

__cfg() {
    _option_list() {
        local -a option_list=('auto-pair:toggle auto pairing of braces and quotations marks'
                              'auto-suggestions:toggle auto suggestions'
                              'background:Background in Terminal, VS Code and Vim'
                              'desktop-dir:set path of desktop'
                              'edit:edit config file (default: custom)'
                              'fzf-tab:toggle fzf tab completion'
                              'github-account:set account for git clone completion'
                              'github-token:set token for git clone completion'
                              'github-user:set user for git clone completion'
                              'globalias:toggle auto replacement of aliases and vars when typing space'
                              'list:print config file (default: custom)'
                              'nickname:Nickname of the machine being shown in prompt'
                              'project-dir:set path of projects'
                              'ssh-identities:list of identity files to be loaded when initializing the shell'
                              'sudo-docker:toggle sudo prefix for docker commands'
                              'syntax-highlighting:toggle syntax highlighting'
                              'theme:Theme / colorscheme in Terminal, VS Code and Vim'
                              'wiki-dir:set path of wiki')
        _describe 'option' option_list
    }

    _arguments -C \
        "1: :_option_list" \
        "*::arg:->args"

    if [[ "${CURRENT}" -eq 2 ]]; then
        case $line[1] in
            desktop-dir | project-dir | wiki-dir)
                local setting current_dir
                setting=$(echo "$line[1]" | tr '[:lower:]' '[:upper:]' | sed -e "s/-/_/g")
                current_dir="${(P)setting}"
                _describe 'setting' current_dir
                ;;
            edit)
                local -a settings=('custom:custom config file (default)'
                                   'dotfiles:yaml configuration of the dotfiles'
                                   'gitconfig:included local gitconfig'
                                   'sshconfig:~/.ssh/config')
                _describe 'setting' settings
                ;;
            list)
                local -a settings=('custom:custom config file (default)'
                                   'dotfiles:yaml configuration of the dotfiles'
                                   'gitconfig:included local gitconfig'
                                   'sshconfig:~/.ssh/config')
                _describe 'setting' settings
                ;;
            background)
                local -a settings=('dark:dark background' 'light:light background')
                if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
                    settings+=('system:use system settings (WSL and macOS only)')
                fi
                _describe 'setting' settings
                ;;
            theme)
                local -a settings=('everforest:https://github.com/sainnhe/everforest'
                                   'gruvbox:https://github.com/morhetz/gruvbox'
                                   'monokai:https://monokai.pro'
                                   'nord:https://github.com/arcticicestudio/nord-vim'
                                   'palenight:https://github.com/drewtempelmeyer/palenight.vim'
                                   'solarized:https://github.com/lifepillar/vim-solarized8')
                _describe 'setting' settings
                ;;
        esac
    elif [[ "${CURRENT}" -eq 3 && $line[1] == "theme" ]]; then
        local -a settings=('light:light background' 'dark:dark background')
        if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
            settings+=('system:use system settings (WSL and macOS only)')
        fi
        _describe 'setting' settings
    fi
}

__cfg "$@"
