#!/usr/bin/env zsh

# Adapted from https://github.com/softmoth/zsh-vim-mode

# Configuration: define cursor shape for each use case
MODE_CURSOR_VIINS="blinking bar"
MODE_CURSOR_REPLACE="blinking underline"
MODE_CURSOR_VICMD="blinking block"
MODE_CURSOR_SEARCH="steady underline"
MODE_CURSOR_VISUAL="steady block"
MODE_CURSOR_VLINE="${MODE_CURSOR_VISUAL}"

### Identifying the editing mode

# Export the main variable so higher-level processes can view it;
# e.g., github:starship/starship uses this
export VIM_MODE_KEYMAP

autoload -Uz add-zsh-hook
autoload -Uz add-zle-hook-widget

vim_mode_precmd           () { vim_mode_handle_event precmd           "${KEYMAP}" }
add-zsh-hook precmd vim_mode_precmd

vim_mode_isearch_update   () { vim_mode_handle_event isearch-update   "${KEYMAP}" }
vim_mode_isearch_exit     () { vim_mode_handle_event isearch-exit     "${KEYMAP}" }
vim_mode_line_pre_redraw  () { vim_mode_handle_event line-pre-redraw  "${KEYMAP}" }
vim_mode_line_init        () { vim_mode_handle_event line-init        "${KEYMAP}" }
add-zle-hook-widget isearch-update vim_mode_isearch_update
add-zle-hook-widget isearch-exit vim_mode_isearch_exit
add-zle-hook-widget line-pre-redraw vim_mode_line_pre_redraw
add-zle-hook-widget line-init vim_mode_line_init

typeset -g vim_mode_keymap_state=

vim_mode_handle_event() {
    local hook="$1"
    local keymap="$2"

    case "${hook}" in
        line-init)
            [[ "${VIM_MODE_KEYMAP}" == vicmd ]] && zle && zle vi-cmd-mode
            ;;
        line-pre-redraw)
            # This hook is called (maybe several times) on every action except
            # for the initial prompt drawing
            case "${vim_mode_keymap_state}" in
                '' )
                    vim_mode_set_keymap "${keymap}"
                    ;;
                *-escape)
                    vim_mode_set_keymap "${vim_mode_keymap_state%-escape}"
                    vim_mode_keymap_state=
                    ;;
                *-update)
                    # Normal update in isearch mode
                    vim_mode_keymap_state=${vim_mode_keymap_state%-update}
                    vim_mode_set_keymap isearch
                    ;;
                *)
                    # ^C was hit during isearch mode!
                    vim_mode_set_keymap "${vim_mode_keymap_state}"
                    vim_mode_keymap_state=
                    ;;
            esac
            ;;
        isearch-update)
            if [[ "${keymap}" == vicmd ]]; then
                # This is an abnormal exit from search (like <Esc>)
                vim_mode_keymap_state+='-escape'
            elif [[ "${VIM_MODE_KEYMAP}" != "isearch" ]]; then
                # Normal update, starting search mode
                vim_mode_keymap_state="${VIM_MODE_KEYMAP}-update"
            else
                # Normal update, staying in search mode
                vim_mode_keymap_state+="-update"
            fi
            ;;
        isearch-exit)
            if [[ "${VIM_MODE_KEYMAP}" == "isearch" ]]; then
                # This could be a normal (movement key) exit, but it could also
                # be ^G which behaves almost like <Esc>. So don't trust $keymap.
                vim_mode_keymap_state+='-escape'
            fi

            # Otherwise, we already exited search via abnormal isearch-update,
            # so there is nothing to do here.
            ;;
        precmd)
            # When the prompt is first shown line-pre-redraw does not get called
            # so the state must be initialized here
            vim_mode_keymap_state=
            vim_mode_set_keymap $(vim_mode_initial_keymap)
            ;;
        *)
            # Should not happen
            zle && zle -M "zsh-vim-mode internal error: bad hook ${hook}"
            ;;
    esac
}

vim_mode_set_keymap() {
    local keymap="$1"

    [[ "${keymap}" == "main" || "${keymap}" == '' ]] && keymap="viins"

    if [[ "${keymap}" == "vicmd" ]]; then
        local active=${REGION_ACTIVE:-0}
        if [[ "${active}" -eq 1 ]]; then
            keymap="visual"
        elif [[ "${active}" -eq 2 ]]; then
            keymap="vline"
        fi
    elif [[ "${keymap}" == "viins" ]]; then
        # triggered via "Insert" key in insert mode
        # and via "R" in command mode (vi-replace),
        # but not via "r" in command mode (vi-replace-chars)
        [[ "${ZLE_STATE}" == *overwrite* ]] && keymap="replace"
    fi

    [[ "${VIM_MODE_KEYMAP}" == "${keymap}" ]] && return

    # Can be used by prompt themes, etc.
    VIM_MODE_KEYMAP="${keymap}"

    vim_mode_set_cursor_style "${keymap}"
}

vim_mode_initial_keymap() {
    case ${VIM_MODE_INITIAL_KEYMAP:-viins} in
        last)
            case "${VIM_MODE_KEYMAP}" in
                vicmd|visual|vline) print vicmd ;;
                *)                  print viins ;;
            esac
            ;;
        vicmd)
            print vicmd ;;
        *)
            print viins ;;
    esac
}

### Editing mode indicator - Cursor shape

# You may want to set this to '', if your cursor stops blinking
# when you didn't ask it to. Some terminals, e.g., xterm, don't blink
# initially but do blink after the set-to-default sequence. So this
# forces it to steady, which should match most default setups.
: ${MODE_CURSOR_DEFAULT:=steady}

# steady state has no effect in iTerm2. Instead, in iTerm2 the steady state is driven by:
# iTerm2 > Settings > Profiles > Text > Cursor > Blinking cursor
set_terminal_cursor_style() {
    local steady=
    local shape=

    for setting in ${=MODE_CURSOR_DEFAULT} "$@"; do
        case "${setting}" in
            blinking)  steady=0 ;;
            steady)    steady=1 ;;
            block)     shape=1 ;;
            underline) shape=3 ;;
            beam|bar)  shape=5 ;;
        esac
    done

    # workaround: blinking cursor leads to flickering in tmux
    [[ -n "${TMUX_PANE}" ]] && steady=1

    # CSI Ps SP q
    #   Set cursor style (DECSCUSR), VT520.
    #     Ps = 0  -> blinking bar.
    #     Ps = 1  -> blinking block
    #     Ps = 2  -> steady block (default).
    #     Ps = 3  -> blinking underline.
    #     Ps = 4  -> steady underline.
    #     Ps = 5  -> blinking bar (xterm).
    #     Ps = 6  -> steady bar (xterm).
    [[ -z "${shape}" ]] && shape=1
    [[ -z "${steady}" ]] && steady=1
    printf "\e[$((shape + steady)) q"
}

vim_mode_set_cursor_style() {
    local keymap="$1"

    case "${keymap}" in
        DEFAULT) set_terminal_cursor_style ;;
        replace) set_terminal_cursor_style ${=MODE_CURSOR_REPLACE-$=MODE_CURSOR_VIINS} ;;
        vicmd)   set_terminal_cursor_style ${=MODE_CURSOR_VICMD} ;;
        isearch) set_terminal_cursor_style ${=MODE_CURSOR_SEARCH-$=MODE_CURSOR_VIINS} ;;
        visual)  set_terminal_cursor_style ${=MODE_CURSOR_VISUAL-$=MODE_CURSOR_VIINS} ;;
        vline)   set_terminal_cursor_style ${=MODE_CURSOR_VLINE-${=MODE_CURSOR_VISUAL-$=MODE_CURSOR_VIINS}} ;;
        main|viins|*)
                 set_terminal_cursor_style ${=MODE_CURSOR_VIINS} ;;
    esac
}

# change cursor shape on single replace mode
# workaround for missing detection of single replace mode
vim_mode_single_replace() {
  set_terminal_cursor_style ${MODE_CURSOR_REPLACE}
  zle vi-replace-chars
  set_terminal_cursor_style ${MODE_CURSOR_VICMD}
}
zle -N vim_mode_single_replace
bindkey -M vicmd 'r' vim_mode_single_replace

vim_mode_cursor_init_hook() {
    vim_mode_set_cursor_style $(vim_mode_initial_keymap)
}

vim_mode_cursor_finish_hook() {
    vim_mode_set_cursor_style DEFAULT
}

if [[ "${TERM}" == (dumb|linux|eterm-color) ]]; then
    # Disable cursor styling on unsupported terminals
    :
else
    add-zsh-hook        precmd      vim_mode_cursor_init_hook
    add-zle-hook-widget line-finish vim_mode_cursor_finish_hook
fi
