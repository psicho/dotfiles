# Adapted from https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/vi-mode

# with a time below 15, the surround plugin is really hard to use
export KEYTIMEOUT=25

# load zle commands for ci", di', vi` etc.
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
    for c in {a,i}{\',\",\`}; do
        bindkey -M $m $c select-quoted
    done
done
# load zle commands for ci{, ci(, di[ etc.
autoload -U select-bracketed
zle -N select-bracketed
for m in visual viopp; do
    for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
        bindkey -M $m $c select-bracketed
    done
done

# https://github.com/zsh-users/zsh/blob/master/Functions/Zle/surround
# Implementation of some functionality of the popular vim surround plugin.
# Allows "surroundings" to be changes: parentheses, brackets and quotes.
# This doesn't allow yss to operate on a line but VS will work.
autoload -Uz surround
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround
bindkey -a cs change-surround
bindkey -a ds delete-surround
bindkey -a ys add-surround
bindkey -M visual S add-surround

# Switch to vi mode (additionally to Esc)
bindkey -M viins '^v' vi-cmd-mode

# Beginning search with j/k
bindkey -M vicmd 'k' up-line-or-beginning-search
bindkey -M vicmd 'j' down-line-or-beginning-search

# Better searching in command mode - fzf history search is better, though
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward

### Integration with OS clipboard
### Paste from clipboard with <C-p>, copy current buffer to clipboard with <C-y>
vi-insert-x-selection() { RBUFFER="$(clippaste | tr --delete '\r')${RBUFFER}"; }
zle -N vi-insert-x-selection
bindkey -M vicmd '^p' vi-insert-x-selection
vi-yank-x-selection() { print -rn -- "${CUTBUFFER}" | clipcopy; }
zle -N vi-yank-x-selection
bindkey -M vicmd '^y' vi-yank-x-selection
