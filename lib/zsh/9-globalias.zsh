# Inspired by https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/globalias/globalias.plugin.zsh

globalias() {
   # Get last word to the left of the cursor:
   # (z) splits into words using shell parsing
   # (A) makes it an array even if there's only one element
   local word=${${(Az)LBUFFER}[-1]}
   if [[ $GLOBALIAS_FILTER_VALUES[(Ie)$word] -eq 0 ]]; then
      zle _expand_alias
      zle expand-word
   fi
   LBUFFER+=" "
}
zle -N globalias

insert_space() {
   LBUFFER+=" "
}
zle -N insert_space

enable_globalias() {
   # space expands all aliases, including global
   bindkey -M emacs " " globalias
   bindkey -M viins " " globalias

   # control-space to make a normal space
   bindkey -M emacs "^ " insert_space
   bindkey -M viins "^ " insert_space

   # normal space during searches
   bindkey -M isearch " " self-insert
}

disable_globalias() {
   # space to make a normal space
   bindkey -M emacs " " self-insert
   bindkey -M viins " " self-insert

   # control-space expands all aliases, including global
   bindkey -M emacs "^ " globalias
   bindkey -M viins "^ " globalias

   # normal space during searches
   bindkey -M isearch " " self-insert
}

if [[ "${GLOBALIAS}" == 'on' ]]; then
   enable_globalias
else
   disable_globalias
fi
