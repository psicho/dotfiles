#!/usr/bin/env bash
# Make using 8-bit/24-bit colors less painful

################ Default color palette
COL_BLACK_FG="$(tput setaf 0)";              export COL_BLACK_FG
COL_RED_FG="$(tput setaf 1)";                export COL_RED_FG
COL_GREEN_FG="$(tput setaf 2)";              export COL_GREEN_FG
COL_YELLOW_FG="$(tput setaf 3)";             export COL_YELLOW_FG
COL_BLUE_FG="$(tput setaf 4)";               export COL_BLUE_FG
COL_MAGENTA_FG="$(tput setaf 5)";            export COL_MAGENTA_FG
COL_CYAN_FG="$(tput setaf 6)";               export COL_CYAN_FG
COL_WHITE_FG="$(tput setaf 7)";              export COL_WHITE_FG
COL_BLACK_BG="$(tput setab 0)";              export COL_BLACK_BG
COL_RED_BG="$(tput setab 1)";                export COL_RED_BG
COL_GREEN_BG="$(tput setab 2)";              export COL_GREEN_BG
COL_YELLOW_BG="$(tput setab 3)";             export COL_YELLOW_BG
COL_BLUE_BG="$(tput setab 4)";               export COL_BLUE_BG
COL_MAGENTA_BG="$(tput setab 5)";            export COL_MAGENTA_BG
COL_CYAN_BG="$(tput setab 6)";               export COL_CYAN_BG
COL_WHITE_BG="$(tput setab 7)";              export COL_WHITE_BG
COL_BRIGHT_BLACK_FG="$(tput setaf 8)";       export COL_BRIGHT_BLACK_FG
COL_BRIGHT_RED_FG="$(tput setaf 9)";         export COL_BRIGHT_RED_FG
COL_BRIGHT_GREEN_FG="$(tput setaf 10)";      export COL_BRIGHT_GREEN_FG
COL_BRIGHT_YELLOW_FG="$(tput setaf 11)";     export COL_BRIGHT_YELLOW_FG
COL_BRIGHT_BLUE_FG="$(tput setaf 12)";       export COL_BRIGHT_BLUE_FG
COL_BRIGHT_MAGENTA_FG="$(tput setaf 13)";    export COL_BRIGHT_MAGENTA_FG
COL_BRIGHT_CYAN_FG="$(tput setaf 14)";       export COL_BRIGHT_CYAN_FG
COL_BRIGHT_WHITE_FG="$(tput setaf 15)";      export COL_BRIGHT_WHITE_FG
COL_BRIGHT_BLACK_BG="$(tput setab 8)";       export COL_BRIGHT_BLACK_BG
COL_BRIGHT_RED_BG="$(tput setab 9)";         export COL_BRIGHT_RED_BG
COL_BRIGHT_GREEN_BG="$(tput setab 10)";      export COL_BRIGHT_GREEN_BG
COL_BRIGHT_YELLOW_BG="$(tput setab 11)";     export COL_BRIGHT_YELLOW_BG
COL_BRIGHT_BLUE_BG="$(tput setab 12)";       export COL_BRIGHT_BLUE_BG
COL_BRIGHT_MAGENTA_BG="$(tput setab 13)";    export COL_BRIGHT_MAGENTA_BG
COL_BRIGHT_CYAN_BG="$(tput setab 14)";       export COL_BRIGHT_CYAN_BG
COL_BRIGHT_WHITE_BG="$(tput setab 15)";      export COL_BRIGHT_WHITE_BG
COL_RESET="$(tput sgr0)";                    export COL_RESET

__LEGEND_SOLARIZED="•"

################ Color printing functions

# Show all 256 colors with color number
# Solarized colors are marked
colorSpectrum() {
    local color legend

    for code in {0..255}; do
        color=$(tput setaf "${code}")
        # pad code with spaces, if shorter than 4
        printf '%*.*s' 0 $((4 - ${#code})) "   "
        if __is_solarized "${code}"; then
            legend="${COL_TEXT_FG}${__LEGEND_SOLARIZED}${color}"
        else
            legend=" "
        fi
        echo -n "${code} ${color}███${legend}Demo${COL_RESET}"

        # break base colors after 8 colors, color cube (6x6x6) after 6 colors and gray scale after 8 colors
        if (( code <= 15 && code % 8 == 7 || code >= 16 && code <= 231 && code % 6 == 3 || code >= 232 && code % 8 == 7)); then
            echo
        fi
    done
    echo "        ${COL_TEXT_FG}${__LEGEND_SOLARIZED} solarized"
}

# Print 1D color gradients in red, green, blue
colorGradient() {
    local options ret
    options=$(getopt --options h --longoptions help -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __colorGradient_usage; return 0;;
            --) shift; break;;
        esac
        shift
    done

    local factor=${1:-1}
    color2DGradient r "${factor}"
    color2DGradient g "${factor}"
    color2DGradient b "${factor}"
}

color2DGradient() {
    local step xOffset yOffset xMax yMax
    local r=0 g=0 b=0
    local print_newline=true
    local inverse=false
    local options ret
    options=$(getopt --options b:g:hinr: --longoptions blue:,green:,help,inverse,no-newline,red: -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -b | --blue) shift; b="$1";;
            -g | --green) shift; g="$1";;
            -h | --help) __color2DGradient_usage; return 0;;
            -i | --inverse) inverse=true;;
            -n | --no-newline) print_newline=false;;
            -r | --red) shift; r="$1";;
            --) shift; break;;
        esac
        shift
    done
    # positional args are now $1, $2, $3
    local colorAxis=${1:-rg}
    local color_y_reference=${colorAxis:1:1}
    local color_x_reference=${colorAxis:0:1}

    if [[ "$#" -le 2 ]]; then
        step=${2:-2}
        xOffset=0
        yOffset=0
        xMax=255
        yMax=255
    else
        step=1
        xOffset="$2"
        yOffset="$3"
        xMax=$((xOffset + 128)); xMax=$((xMax>255?255:xMax))
        yMax=$((yOffset + 128)); yMax=$((yMax>255?255:yMax))
    fi

    # It is possible to have 2x the detail in y direction using ▀
    # and foreground color in addition to the background color,
    # but the printing then takes way too long.
    local cx cy colx coly
    for ((cy=yOffset; cy<=yMax; cy+=2*step)); do
        [[ "${inverse}" == "true" ]] && coly=$((yMax-cy+2*yOffset)) || coly="${cy}"
        for ((cx=xOffset; cx<=xMax; cx+=step)); do
            [[ "${inverse}" == "true" ]] && colx=$((xMax-cx+2*xOffset)) || colx="${cx}"
            case "${color_x_reference}" in
                "r") r="${colx}";;
                "g") g="${colx}";;
                "b") b="${colx}";;
            esac
            case "${color_y_reference}" in
                "r") r="${coly}";;
                "g") g="${coly}";;
                "b") b="${coly}";;
            esac
            printf '\e[48;2;%s;%s;%sm \e[0m' "${r}" "${g}" "${b}"
        done
        # just print the x axis if only one color provided
        [[ ${#colorAxis} -eq 1 ]] && break
        ((cy+2*step<=yMax)) && printf "\n"
    done
    [[ "${print_newline}" == "true" ]] && printf "\n"
}

colors() {
    paste --delimiter='\t' \
          <(echo "-------- Default ---------"; __color_words_default) \
          <(echo "- Solarized - "; __color_words_solarized) \
        | sed "s/\t/     /g"
}

print-hex-colors-from-file() {
    if [[ "$#" -eq 1 && -f "$1" ]]; then
        local colors IFS
        colors=$(rg --ignore-case '#[a-f0-9]{6}([^a-f0-9]|$)' "$1" --no-line-number --only-matching | \
                 awk '{print substr($1,1,7)}' | \
                 tr '[:lower:]' '[:upper:]' | \
                 sort -u)
        while IFS= read -r color; do
            __print_hex_color "${color}"
        done <<< "${colors}"
    else
        echo "Please provide a filename containing hex colors."
    fi
}

print-solarized-diff() {
    __get_solarized_colors

    printf "${COL_TEXT_FG}%s" "         "
    for code in "${modified_colors_result[@]}"; do
        # pad code with spaces, if shorter than $padding
        printf '%*.*s' 0 $((3 - ${#code})) "   "
        printf "   %s" "${code}"
    done

    printf "\n${COL_TEXT_FG}%s" "Default    "
    for code in "${modified_colors_result[@]}"; do
        tohex "${code}" --result-value
        __print_hex_color "${tohex_result}" " "
    done

    printf "\n${COL_TEXT_FG}%s" "Solarized  "
    for code in "${modified_colors_result[@]}"; do
        __get_solarized_color "$code"
        __print_hex_color "${modified_color_result}" " "
    done
    printf "\n"
}

################ Utility functions

# Convert color from the default 256 color palette into hex color
tohex() {
    local dec bas mul r g b gray options ret kind hex_result result
    local verbose=false
    local use_result_value=false
    local respect_solarized=false

    options=$(getopt --options hprsv --longoptions help,result-value,solarized,verbose -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __tohex_usage; return 0;;
            -r | --result-value) use_result_value=true;;
            -s | --solarized) respect_solarized=true;;
            -v | --verbose) verbose=true;;
            --) shift; break;;
        esac
        shift
    done
    dec=$(($1%256))

    if [[ "${respect_solarized}" == true ]] && __is_solarized "${dec}"; then
        kind="solarized"
        __get_solarized_color "${dec}"
        hex_result="${modified_color_result}"
    else
        if [[ "${dec}" -lt 16 ]]; then
            bas=$(( dec%16 ))
            mul=128
            [[ "${bas}" -eq "7" ]] && mul=192
            [[ "${bas}" -eq "8" ]] && bas=7
            [[ "${bas}" -gt "8" ]] && mul=255
            r="$((  (bas&1)    *mul ))"
            g="$(( ((bas&2)>>1)*mul ))"
            b="$(( ((bas&4)>>2)*mul ))"
            kind="basic"
        elif [[ "${dec}" -gt 15 && "${dec}" -lt 232 ]]; then
            r=$(( (dec-16)/36 )); r=$(( r==0?0: r*40 + 55 ))
            g=$(( (dec-16)/6%6)); g=$(( g==0?0: g*40 + 55 ))
            b=$(( (dec-16)%6  )); b=$(( b==0?0: b*40 + 55 ))
            kind="color"
        else
            gray=$(( (dec-232)*10+8 ))
            r="${gray}"; g="${gray}"; b="${gray}"
            kind="gray"
        fi
        printf -v hex_result '#%02x%02x%02x' "${r}" "${g}" "${b}"
    fi

    if [[ "${verbose}" == true ]]; then
        printf -v result 'decimal: %3s %s: %s\n' "${dec}" "${kind}" "${hex_result}"
    else
        result="${hex_result}"
    fi
    if [[ "${use_result_value}" == true ]]; then
        export tohex_result="${result}"
    else
        printf "%s" "${result}"
    fi
}

# Convert hex color to nearest default 256 color index
fromhex() {
    local hex r g b
    local options ret
    options=$(getopt --options h --longoptions help -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __fromhex_usage; return 0;;
            --) shift; break;;
        esac
        shift
    done

    hex=${1#"#"}
    r=$(printf '0x%0.2s' "${hex}")
    g=$(printf '0x%0.2s' "${hex#??}")
    b=$(printf '0x%0.2s' "${hex#????}")
    printf '%03d' "$(( (r<75?0:(r-35)/40)*6*6 +
                       (g<75?0:(g-35)/40)*6   +
                       (b<75?0:(b-35)/40)     + 16 ))"
}

################ Solarized8 functions

# Modify 256-color palette in supported terminals
# Script adapted from https://github.com/lifepillar/vim-solarized8
# shellcheck disable=SC2059
modify-palette-solarized() {
    # arg 1: 256 color to modify
    # arg 2/3/4: new r/g/b value
    local color_rewrite_template
    local color r g b

    if [[ -n "${TMUX_PANE}" ]]; then
        # tell tmux to pass the escape sequences through
        color_rewrite_template="\ePtmux;\e\e]4;%d;rgb:%s/%s/%s\a\e\\"
    else
        color_rewrite_template="\e]4;%d;rgb:%s/%s/%s\e\\"
    fi

    __get_solarized_colors
    for color in "${modified_colors_result[@]}"; do
        __get_solarized_color "${color}"
        r=${modified_color_result:1:2}
        g=${modified_color_result:3:2}
        b=${modified_color_result:5:2}
        printf "${color_rewrite_template}" "${color}" "${r}" "${g}" "${b}"
    done
}

# arg 1: color to be checked
__is_solarized() {
    __get_solarized_color "$1"
    if [[ -n "${modified_color_result}" ]]; then
        return 0 # true
    else
        return 1 # false
    fi
}

__get_solarized_colors() {
    # ordered by color, not by number
    export modified_colors_result=(235 236 242 66 246 247 254 230 136 166 160 162 61 32 37 106)
}

__get_solarized_color() {
    case $1 in
        # ordered by color, not by number
        235) modified_color_result="#002B36";;
        236) modified_color_result="#073642";;
        242) modified_color_result="#586E75";;
        66 ) modified_color_result="#657B83";;
        246) modified_color_result="#839496";;
        247) modified_color_result="#93A1A1";;
        254) modified_color_result="#EEE8D5";;
        230) modified_color_result="#FDF6E3";;
        136) modified_color_result="#B58900";;
        166) modified_color_result="#CB4B16";;
        160) modified_color_result="#DC322F";;
        162) modified_color_result="#D33682";;
        61 ) modified_color_result="#6C71C4";;
        32 ) modified_color_result="#268BD2";;
        37 ) modified_color_result="#2AA198";;
        106) modified_color_result="#859900";;
        *) unset modified_color_result;;
    esac
    [[ -n "${modified_color_result}" ]] && export modified_color_result
}

################ private functions

# set global text color
__get_text_color() {
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/theming/set-theme.sh"
    if is_dark_theme; then
        export COL_TEXT_FG="${COL_WHITE_FG}"
    else
        export COL_TEXT_FG="${COL_BLACK_FG}"
    fi
}

# arg 1: hex color with #, e.g. #FF16a0
# arg 2: suffix (optional)
__print_hex_color() {
    [[ -n "${ZSH_VERSION}" ]] && setopt local_options KSH_ARRAYS
    local suffix="$2"

    # extract r, g, b in hex form
    local r=${1:1:2}
    local g=${1:3:2}
    local b=${1:5:2}
    # convert hex colors to 256-color
    r=$((16#$r))
    g=$((16#$g))
    b=$((16#$b))

    # print color
    printf "\e[48;2;%s;%s;%sm     \e[0m${suffix}" "${r}" "${g}" "${b}"
}

__color_words_default() {
    echo "     Dark          Light  "
    __print_colorWord "black  " "0"  1
    __print_colorWord "black  " "8"  3 "\n"
    __print_colorWord "red    " "1"  1
    __print_colorWord "red    " "9"  3 "\n"
    __print_colorWord "green  " "2"  1
    __print_colorWord "green  " "10" 3 "\n"
    __print_colorWord "yellow " "3"  1
    __print_colorWord "yellow " "11" 3 "\n"
    __print_colorWord "blue   " "4"  1
    __print_colorWord "blue   " "12" 3 "\n"
    __print_colorWord "magenta" "5"  1
    __print_colorWord "magenta" "13" 3 "\n"
    __print_colorWord "cyan   " "6"  1
    __print_colorWord "cyan   " "14" 3 "\n"
    __print_colorWord "white  " "7"  1
    __print_colorWord "white  " "15" 3 "\n"
}

__color_words_solarized() {
    echo
    __get_solarized_colors
    __print_modified_colorWords
}

__print_modified_colorWords() {
    local color i=0
    for color in "${modified_colors_result[@]}"; do
        __print_colorWord "" "${color}" 3
        ((i=i+1))
        ((i % 2 == 0)) && printf "\n"
    done
}

__print_colorWord() {
    local name="$1"
    local code="$2"
    local padding="$3"
    local suffix="$4"
    local prefix="██"
    local spacer=" "
    local color

    color=$(tput setaf "${code}")
    if __is_solarized "${code}"; then
        spacer="${COL_TEXT_FG}${__LEGEND_SOLARIZED}${color}"
    fi

    # pad code with spaces, if shorter than $padding
    printf '%*.*s' 0 $((padding - ${#code})) "   "

    # cannot fix SC2059 in bash
    # shellcheck disable=SC2059
    printf "${COL_TEXT_FG}${code} ${color}${prefix}${spacer}${name}${COL_RESET}${suffix}"
}

__color2DGradient_usage() {
    colorize-help <<-EOF
		Print a color gradient

		Needs a screen 128 chars wide when using default settings, with up to 256 chars in width using maximum settings.

		Usage:
		    color2DGradient [colors] [shrink-factor] [options]
		    color2DGradient [colors] [x-offset] [y-offset] [options]
		    color2DGradient [-h] [--help]

		Options:
		    -b, --blue       Define the blue value (0-255), defaults to 0
		    -g, --green      Define the green value (0-255), defaults to 0
		    -h, --help       Print this help
		    -i, --inverse    Print gradient in inverse direction
		    -n, --no-newline Omit the final printing of a newline
		    -r, --red        Define the red value (0-255), defaults to 0

		Arguments:
		    colors           [rgb]{0,2}, defaults to rg
		                     First char: color development in x direction
		                     Second char: color development in y direction (optional)
		                     One dimensional, if only one  char provided
		    shrink-factor    Step increment, defaults to 2 (can be seen as resolution: color step per pixel)
		    x-offset         Offset of color in x-direction
		    y-offset         Offset of color in y-direction
		                     In this form, shrink-factor is set to 1 and number of steps is limited to 128
	EOF
}

__colorGradient_usage() {
    colorize-help <<-EOF
		Print 1D color gradients in red, green and blue

		Needs a screen 256 chars wide with default settings.

		Usage:
		    colorGradient [shrink-factor]

		Options:
		    -h, --help      Print this help

		Arguments:
		    shrink-factor   Step increment, defaults to 1 (can be seen as resolution: color step per pixel)
	EOF
}

__tohex_usage() {
    colorize-help <<-EOF
		Convert color from the default 256 color palette into hex color

		Usage:
		    tohex <color256>

		Options:
		    -h, --help          Print this help
		    -r, --result-value  Return the result in the global variable tohex_result instead of printing it to avoid the slow subshell (default: false)
		    -s, --solarized     Take changes of solarized8 into account (default: false)
		    -v, --verbose       Verbose output (default: false)

		Arguments:
		    color256            8-bit color (0-255)
	EOF
}

__fromhex_usage() {
    colorize-help <<-EOF
		Convert hex color to nearest default 256 color index

		Usage:
		    fromhex <hexcolor>
		    fromhex [-h] [--help]

		Options:
		    -h, --help          Print this help

		Arguments:
		    hexcolor            24-bit hex color with or without leading #
	EOF
}

__get_text_color
