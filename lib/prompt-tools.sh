#!/usr/bin/env bash

git_prompt() {
    # within this function, use 0 based arrays as bash does
    [[ -n "${ZSH_VERSION}" ]] && setopt local_options KSH_ARRAYS

    local branch_prefix="$1"
    local status_color="$2"
    local surround_with_parens="$3"
    local git_status=''
    local branch_name=''

    [[ -z "${surround_with_parens}" ]] && surround_with_parens=0

    # Check if we are in a Git repository
    if git rev-parse --is-inside-work-tree &>/dev/null; then

        # Ensure we are not in .git directory
        if [[ $(git rev-parse --is-inside-git-dir 2>/dev/null) == 'false' ]]; then

            # Ensure the index is up to date
            git update-index --really-refresh -q &>/dev/null

            # Check for untracked files
            if [[ -n $(git ls-files --others --exclude-standard) ]]; then
                git_status+='?'
            fi

            # Check for unstaged changes
            if ! git diff-files --quiet --ignore-submodules --; then
                git_status+='!'
            fi

            # Check for uncommitted changes in the index
            if ! git diff --quiet --ignore-submodules --cached; then
                git_status+='+'
            fi

            # Check for stashed files
            if git rev-parse --verify refs/stash &>/dev/null; then
                git_status+='$'
            fi

            # Check ahead/behind git_status
            # shellcheck disable=SC2207,SC2140
            local ahead_behind=($(git rev-list --left-right --count "@{upstream}"..."HEAD" 2>/dev/null))
            local ahead=${ahead_behind[1]}
            local behind=${ahead_behind[0]}
            if [[ "${ahead}" -gt 0 ]]; then
                [[ -n "${git_status}" ]] && git_status+=" "
                git_status+="${ahead}↑"
            fi
            if [[ "${behind}" -gt 0 ]]; then
                [[ -n "${git_status}" ]] && git_status+=" "
                git_status+="${behind}↓"
            fi
        fi

        [[ -n "${git_status}" ]] && git_status=" [${git_status}]"

        # Get the short symbolic ref.
        # If HEAD isn’t a symbolic ref, get the short SHA for the latest commit;
        # otherwise, just give up.
        branch_name="$(
            git symbolic-ref --quiet --short HEAD 2>/dev/null || \
            git rev-parse --short HEAD 2>/dev/null
        )"

        if [[ -n "${branch_name}" ]]; then
            if [[ "${surround_with_parens}" -eq 1 ]]; then
                branch_name="(${branch_name})"
            else
                branch_name=" ${branch_name}"
            fi
        else
            branch_name='(unknown)'
        fi

        printf "%s%s%s%s" "${branch_prefix}" "${branch_name}" "${status_color}" "${git_status}"
    fi
}

get_hostname() {
    # shellcheck disable=SC2153
    if [[ -n "${NICKNAME}" ]]; then
        echo "${NICKNAME}"
    else
        nickname=$(rg 'export NICKNAME=' /etc/profile.d/prompt.sh | sed --regexp-extended 's/export NICKNAME=//') &>/dev/null
        if [[ -n "${nickname}" ]]; then
            echo "${nickname}"
        elif [[ -n "${HOSTNAME}" ]]; then
            echo "${HOSTNAME}"
        else
            echo "${NAME}"
        fi
    fi
}

if [[ "$1" == "--get-hostname" ]]; then
    get_hostname
fi
