#!/usr/bin/env bash

unalias unpack
unpack() {
    local target="$1"
    if [[ -f "${target}" ]]; then
        case "${target}" in
            *.tar.bz2 | *.tbz2) tar --extract --bzip2 --verbose --file="${target}";;
            *.tar.gz | *.tgz) tar --extract --gunzip --verbose --file="${target}";;
            *.tar.lz | *.tlz) tar --extract --lzip --verbose --file="${target}";;
            *.tar.xz | *.txz) tar --extract --xz --verbose --file="${target}";;
            *.tar) tar --extract --verbose --file="${target}";;
            *.7z) 7z x "${target}";;
            *.bz2) bunzip2 --verbose "${target}";;
            *.gz) gunzip --verbose "${target}";;
            *.lz) lzip --decompress --verbose "${target}";;
            *.rar) unrar x "${target}";;
            *.xz) xz --decompress --verbose "${target}";;
            *.zip) unzip "${target}";;
            *.Z) uncompress "${target}";;
            *) echo "'${target}' cannot be extracted via unpack()";;
        esac
    else
        echo "'${target}' is not a valid file."
    fi
}
