#!/usr/bin/env bash

__checkscripts_usage() {
    colorize-help <<-EOF
		Run shellcheck for all files relative to current directory using given filter

		Usage:
		    checkscripts [options]

		Options:
		    -c, --check=type    Filter for selecting files (default: shebang)
		                        type=extension: filter for files with exension .sh
		                        type=shebang: filter for files having a shebang
		    -h, --help          Print this help
		    -v, --verbose       Print files to be checked and then check
	EOF
    return 0
}

# Note: Busybox (running via GitLab CI) does not support long options for cut, grep, xargs, wc, getopt
checkscripts() {
    local scripts num options ret
    local type="shebang"
    local verbose=false
    local SHEBANG_RE='^#!\s?/(usr/)?bin/(env\s+)?(sh|bash)'

    if ! command -v shellcheck &>/dev/null; then
        echo "Please install shellcheck."
        return 1
    fi

    options=$(getopt -o c:hv -l check:,help,verbose -- "$@")
    ret=$?
    [[ $ret -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -c | --check)
                shift
                type="$1"
                if [[ "${type}" != "extension" && "${type}" != "shebang" ]]; then
                    echo "type must be one of: extension, shebang (is: ${type})"
                    return 1
                fi
                ;;
            -h | --help)
                __checkscripts_usage
                return 0
                ;;
            -v | --verbose) verbose=true;;
            --) shift; break;;
        esac
        shift
    done

    if [[ "${type}" == "shebang" ]]; then
        # Get all files with a shebang in line 1:
        # 1. Get all files not in .git dir
        # 2. Get all files with a shebang
        # 3. Delete the third field (containing the search result)
        # 4. Get only those results with line number 1
        # 5. Delete the second field (containing the line number)
        scripts="$(find . -name '.git' -type d -prune -o -name '.venv' -type d -prune -o -type f -print0 |
                xargs -0 grep -n -E "${SHEBANG_RE}" |
                cut -d':' -f1,2 |
                grep ':1' |
                cut -d':' -f1
                )"
    else
        scripts="$(find . -name '.git' -type d -prune -o -name '.venv' -type d -prune -o -name '*.sh' | grep '.sh$')"
    fi
    num=$(echo "${scripts}" | wc -l)
    echo "Checking ${num} files containing a shebang in ${PWD} and sub directories..."
    if [[ "${verbose}" == true ]]; then
        echo "${scripts}"
    fi
    echo "${scripts}" | xargs shellcheck --external-sources 2>&1
    echo "Shellcheck finished."
}
