#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091,SC2207

export CUSTOM_FILE="${HOME}/.config/dotfiles/custom.sh"
export DOTFILES_CONFIG_FILE="${HOME}/.config/dotfiles/config.yml"
export GITCONFIG_FILE="${HOME}/.config/dotfiles/gitconfig.local"
export SSHCONFIG_FILE="${HOME}/.ssh/config"

cfg() {
    source "${DOTFILES_DIR}/theming/set-theme.sh"

    local options ret reload=true
    options=$(getopt --options hn --longoptions help,no-reload -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __cfg_usage; return 0;;
            -n | --no-reload) reload=false;;
            --) shift; break;;
        esac
        shift
    done

    if [[ "$#" -eq 3 && "$1" == "theme" ]]; then
        local theme="$2"
        local background="$3"
        if __set_option_theme "${theme}"; then
            if __set_option_background "${background}"; then
                set_theme "${theme}" "${background}"
                [[ "${reload}" == true ]] && reload
            fi
        fi
    elif [[ "$#" -eq 2 ]]; then
        local option="$1"
        local setting="$2"
        case "${option}" in
            github-account | github-token | github-user)
                __set_option "github" "${option}" "${setting}"
                ;;
            desktop-dir | project-dir | wiki-dir)
                if [[ -d "${setting}" ]]; then
                    __set_option "directories" "${option}" "${setting}"
                else
                    echo "Cannot set ${option} to '${setting}': directory does not exist."
                fi
                ;;
            edit)
                local config_file="${setting}"
                case "${config_file}" in
                    custom)
                        "${EDITOR}" "${CUSTOM_FILE}"
                        ;;
                    dotfiles)
                        "${EDITOR}" "${DOTFILES_CONFIG_FILE}"
                        ;;
                    gitconfig)
                        "${EDITOR}" "${GITCONFIG_FILE}"
                        ;;
                    sshconfig)
                        "${EDITOR}" "${SSHCONFIG_FILE}"
                        ;;
                    *)
                        echo "Unknown config file '${config_file}' for editing."
                        ;;
                esac
                ;;
            list)
                local config_file="${setting}"
                case "${config_file}" in
                    custom)
                        echo "Contents of ${CUSTOM_FILE}:"
                        bat --plain --language sh "${CUSTOM_FILE}"
                        ;;
                    dotfiles)
                        echo "Contents of ${DOTFILES_CONFIG_FILE}:"
                        bat --plain --language yaml "${DOTFILES_CONFIG_FILE}"
                        ;;
                    gitconfig)
                        echo "Contents of ${GITCONFIG_FILE}:"
                        bat --plain --language gitconfig "${GITCONFIG_FILE}"
                        ;;
                    sshconfig)
                        echo "Contents of ${SSHCONFIG_FILE}:"
                        bat --plain --language ssh_config "${SSHCONFIG_FILE}"
                        ;;
                    *)
                        echo "Unknown config file '${config_file}' for listing."
                        ;;
                esac
                ;;
            nickname)
                __set_option "theming" "${option}" "${setting}"
                [[ "${reload}" == true ]] && reload
                ;;
            ssh-identities)
                __set_ssh_identities "${setting}"
                ;;
            background)
                setting=$(echo "${setting}" | tr '[:upper:]' '[:lower:]')
                if __set_option_background "${setting}"; then
                    local color_scheme
                    color_scheme=$(yq eval '.theming.theme' "${DOTFILES_CONFIG_FILE}")
                    set_theme "${color_scheme}" "${setting}"
                    [[ "${reload}" == true ]] && reload
                fi
                ;;
            theme)
                if __set_option_theme "${setting}"; then
                    local background
                    background=$(yq eval '.theming.background' "${DOTFILES_CONFIG_FILE}")
                    set_theme "${setting}" "${background}"
                    [[ "${reload}" == true ]] && reload
                fi
                ;;
            *)
                echo "Unknown option: '${option}'"
                __cfg_usage
                ;;
        esac
    elif [[ "$#" -eq 1 ]]; then
        local option="$1"
        case "${option}" in
            help)
                __cfg_usage
                ;;
            auto-pair | auto-suggestions | fzf-tab | globalias | sudo-docker | syntax-highlighting)
                setting=$(__get_value "${option}")
                setting=$(echo "${setting}" | tr '[:upper:]' '[:lower:]')
                case "${setting}" in
                    on)
                        __set_option "toggles" "${option}" "off"
                        echo "${option} is now turned off."
                        ;;
                    off)
                        __set_option "toggles" "${option}" "on"
                        echo "${option} is now turned on."
                        ;;
                    *)
                        echo "Unknown setting $2 for option $1"
                        __cfg_usage
                        ;;
                esac
                if [[ "${option}" == 'FZF_TAB' ]]; then
                    source "${CUSTOM_FILE}"
                    evaluate_fzf_tab
                elif [[ "${option}" == 'GLOBALIAS' ]]; then
                    if [[ "${setting}" == 'on' ]]; then
                        enable_globalias
                    else
                        disable_globalias
                    fi
                elif [[ "${reload}" == true ]]; then
                    reload
                fi
                ;;
            edit)
                "${EDITOR}" "${CUSTOM_FILE}"
                ;;
            list)
                echo "Contents of ${CUSTOM_FILE}:"
                bat --plain --language sh "${CUSTOM_FILE}"
                ;;
            github-account | github-token | github-user \
                | desktop-dir | nickname | project-dir \
                | ssh-identities | background | theme | wiki-dir)
                setting=$(__get_value "${option}")
                echo "${setting}"
                ;;
            *)
                echo "Unknown command or toggle $1."
                ;;
        esac
    elif [[ "$#" -gt 2 && "$1" == "ssh-identities" ]]; then
        shift
        __set_ssh_identities "$@"
    else
        __cfg_usage
    fi
    return 0
}

evaluate_fzf_tab() {
    if [[ -n "${BASH_VERSION}" ]]; then
        reload
    elif [[ -n "${ZSH_VERSION}" ]]; then
        if [[ "${FZF_TAB}" == 'on' ]]; then
            enable-fzf-tab
            fzf-tab-completion-format
        else
            disable-fzf-tab
            zsh-completion-format
        fi
    fi
}

__set_option() {
    local group="$1"
    local option="$2"
    local setting="$3"
    local env_option query
    env_option=$(echo "${option}" | tr '[:lower:]' '[:upper:]' | sed -e "s/-/_/g")
    query=".${group}.${option} = \"${setting}\""
    yq --inplace eval "${query}" "${DOTFILES_CONFIG_FILE}"
    export "${env_option}"="${setting}"
}

__set_option_background() {
    local background="$1"
    case "${background}" in
        dark | light)
            __set_option "theming" "background" "${background}"
            return 0
            ;;
        system)
            if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
                __set_option "theming" "background" "${background}"
                return 0
            else
                echo "Background system only supported on WSL and macOS."
            fi
            ;;
        *)
            echo "Background must be dark, light or system."
            return 1
            ;;
    esac
}

__set_option_theme() {
    local theme="$1"
    case "${theme}" in
        everforest | gruvbox | solarized | monokai | nord | palenight)
            __set_option "theming" "theme" "${theme}"
            return 0
            ;;
        *)
            echo "Unknown theme: ${theme}"
            return 1
            ;;
    esac
}

__set_ssh_identities() {
    local query first_identity="$1"
    shift
    # join all entries with ", " and surround the result with ""
    # thus, input `one two three` will be rendered into `"one", "two", "three"`
    printf -v query %s "\"${first_identity}" "${@/#/\", \"}" "\""
    query=".ssh.identities = [${query}]"
    yq --inplace eval "${query}" "${DOTFILES_CONFIG_FILE}"
    export SSH_IDENTITIES="${ssh_identities}"
}

__export_yaml() {
    local key_to_load="$1" value
    if [[ "${key_to_load}" == "ssh.identities" ]]; then
        __export_ssh_identities
    elif [[ -n "${key_to_load}" ]]; then
        value=$(yq eval ".${key_to_load}" "${DOTFILES_CONFIG_FILE}")
        echo "${value}"
    else
        local toplevel_keys config item key configuration_array
        toplevel_keys='.toggles, .theming, .directories, .github'    # do not read .ssh here
        config=$(yq eval "${toplevel_keys}" "${DOTFILES_CONFIG_FILE}")
        if [[ -n "${BASH_VERSION}" ]]; then
            readarray -t -d $'\n' configuration_array <<< "${config}"
        elif [[ -n "${ZSH_VERSION}" ]]; then
            # shellcheck disable=SC2296
            configuration_array=("${(@f)config}")
        fi
        for item in "${configuration_array[@]}"; do
            key=$(echo "${item}" | cut --delimiter=':' --fields=1 | tr '[:lower:]' '[:upper:]')
            key=${key//-/_}
            # xargs trims the input
            value=$(echo "${item}" | cut --delimiter=':' --fields=2- | xargs)
            if [[ "${key}" != "BAT_THEME" ]]; then
                # evaluate value if containing ~ or $HOME
                value=$(eval echo "${value}")
            fi
            export "${key}"="${value}"
        done
        __export_ssh_identities
    fi
}

__export_ssh_identities() {
    local ssh_identities
    ssh_identities=$(yq eval '.ssh.identities.[]' "${DOTFILES_CONFIG_FILE}")
    export SSH_IDENTITIES="${ssh_identities}"
}

__load_configuration() {
    if [[ "$#" -eq 0 ]]; then
        # load everything
        __export_yaml
        source "${CUSTOM_FILE}"
    else
        # load the var that $1 is pointing to
        __export_yaml "$1"
    fi
}

__cfg_usage() {
    source "${DOTFILES_DIR}/lib/help.sh"
    colorize-help <<-EOF
		Configure the settings of these dotfiles

		Usage:
		    cfg <option> [setting] [parameters ...]
		    cfg <toggle> [parameters ...]
		    cfg <command> [config-file]

		Options and their settings - prints the current one if none provided:
		    background                Background in Terminal, VS Code and Vim
		                              Settings: dark / light / system
		    desktop-dir               Directory used for command 'desktop'
		    github-account            Account used for git clone completion
		    github-token              Token used for git clone completion
		    github-user               User used for git clone completion
		    nickname                  Nickname of the machine being shown in prompt
		    project-dir               Directory used for command 'project-dir'
		    ssh-identities            List of identity files to be loaded when initializing the shell
		    theme                     Theme / colorscheme in Terminal, VS Code and Vim
		                              Settings: everforest / gruvbox / solarized / monokai / nord / palenight
		    wiki-dir                  Directory used for command 'wiki' and 'update'

		Toggles - all toggle between on and off:
		    auto-pair                 Auto pairing of braces and quotation marks in zsh command line
		    auto-suggestions          Auto suggestion of last matching command in zsh command line
		    fzf-tab                   Tab completion with fzf in zsh command line
		    globalias                 If on, typing a space auto replace aliases and variables and
		                              Ctrl+Space inserts a literal space. If off, it's vice-versa.
		    sudo-docker               Call docker commands with or without sudo
		    syntax-highlighting       Syntax highlighting for commands in zsh

		Parameters:
		    -n, --no-reload           Do not reload the shell after changing the option or toggle

		Commands:
		    help, -h, --help          Print this help
		    edit [<config-file>]      Edit config file
		    list [<config-file>]      Print config file

		Config file:
		    custom                    custom.sh (default, if none provided)
		    dofiles                   config.yml
		    gitconfig                 included local gitconfig
		    sshconfig                 ~/.ssh/config
	EOF
}

__cfg_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local options="auto-pair auto-suggestions background desktop-dir edit \
                   fzf-tab github-account github-token github-user globalias list nickname \
                   sudo-docker syntax-highlighting project-dir ssh-identites theme wiki-dir"
    local background_options
    if [[ "$COMP_CWORD" -eq 1 ]]; then
        COMPREPLY=($(compgen -W "${options}" -- "${cur}"))
    elif [[ "${COMP_CWORD}" -eq 2 ]]; then
        case ${COMP_WORDS[1]} in
            desktop-dir | project-dir | wiki-dir)
                local setting current_dir
                current_dir=$(__get_value "${setting}")
                COMPREPLY=($(compgen -W "${current_dir}" -- "${cur}"))
                ;;
            edit)
                COMPREPLY=($(compgen -W "custom dotfiles gitconfig sshconfig" -- "${cur}"))
                ;;
            list)
                COMPREPLY=($(compgen -W "custom dotfiles gitconfig sshconfig" -- "${cur}"))
                ;;
            background)
                background_options="dark light"
                if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
                    background_options="${background_options} systems"
                fi
                COMPREPLY=($(compgen -W "${background_options}" -- "${cur}"))
                ;;
            theme)
                COMPREPLY=($(compgen -W "everforest gruvbox monokai nord palenight solarized" -- "${cur}"))
                ;;
        esac
    elif [[ "${COMP_CWORD}" -eq 3 && ${COMP_WORDS[1]} == "theme" ]]; then
        background_options="dark light"
        if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
            background_options="${background_options} systems"
        fi
        COMPREPLY=($(compgen -W "${background_options}" -- "${cur}"))
    fi
    return 0
}

# read env var and resolve
__get_value() {
    local key="$1" env_key
    env_key=$(echo "${key}" | tr '[:lower:]' '[:upper:]' | sed -e "s/-/_/g")
    if [[ -n "${BASH_VERSION}" ]]; then
        echo "${!env_key}"
    elif [[ -n "${ZSH_VERSION}" ]]; then
        # shellcheck disable=SC2296
        echo "${(P)env_key}"
    fi
}

__quote_if_has_spaces() {
    awk '/ / { print "\""$0"\"" } /^[^ ]+$/ { print $0 }'
}

if [[ "$1" == '--load' ]]; then
    shift
    __load_configuration "$@"
elif [[ "$1" == '--configure' ]]; then
    shift
    cfg --no-reload "$@"
elif [[ -n "${BASH_VERSION}" && "${__configurer_sourced}" != true ]]; then
    export __configurer_sourced=true
    complete -F __cfg_completion cfg
fi
