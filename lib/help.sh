#!/usr/bin/env bash

# Alternatively to setting LESS_TERMCAP_md in config-shell.sh, use bat for coloring man pages
# No gnu alternative for col found, therefore using short options:
# b = no-backspaces / x = spaces
export MANPAGER="sh -c 'col -xb | bat --language help --theme ansi --plain'"
# fixes colorization issues
export MANROFFOPT="-c"

colorize-help() {
    # color always is necessary because show-help pipes to less afterwards
    bat --plain --language help --theme ansi --color always
}

help() {
    "${EDITOR}" "${DOTFILES_DIR}/README.md"
}

# Open README.md via F1 if in interactive mode
if [[ $- == *i* ]]; then
    if [[ "${BASH_VERSION}" ]]; then
        bind '"\eOP": "help\C-M"'
    elif [[ "${ZSH_VERSION}" ]]; then
        zle -N help
        bindkey -M viins '^[OP' help
    fi
fi
