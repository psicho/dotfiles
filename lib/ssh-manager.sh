#!/usr/bin/env bash

# Adapted from: https://stackoverflow.com/a/48509425

# Usage
# a) With ~/.ssh/config
#    * ssh-add only needs to store identities with a passphrase
#    * Add ssh keys that have a passphrase to ~/.config/dotfiles/config.yml, section ssh.identities (via `cfg edit dotfiles`)
#    * Add-hoc call ssh-load-identities when needed
# b) Without ~/.ssh/config
#   * Add all ssh keys to ~/.config/dotfiles/config.yml, section ssh.identities (via `cfg edit dotfiles`)
#   * Add the call of ssh-load-identities to ~/.config/dotfiles/custom.sh (via `cfg edit`)

__run_ssh_agent() {
    ssh-add -l &>/dev/null
    if [ "$?" -eq 2 ]; then
        # Could not open a connection to your authentication agent.

        # Load stored agent connection info.
        local cache_file="${HOME}/.cache/.ssh-agent"
        test -r "${cache_file}" && \
            eval "$(<"${cache_file}")" >/dev/null

        ssh-add -l &>/dev/null
        if [ "$?" -eq 2 ]; then
            # Start agent and store agent connection info.
            (umask 066; ssh-agent > "${cache_file}")
            eval "$(<"${cache_file}")" >/dev/null
        fi
    fi
}

__ssh_load() {
    local result identity identity_array
    if [[ "$*" == "" ]]; then
        echo "${COL_RED_FG}Nothing to load. Please configure identities in ~/.config/dotfiles/config.yml via 'cfg edit dotfiles'.${COL_RESET}"
        return 1
    fi
    __run_ssh_agent
    ssh-add -l &>/dev/null
    result=$?
    if [[ "${result}" -eq 1 ]]; then
        # The agent has no identities. Load all given ones.
        if [[ "$#" -gt 1 ]]; then
            # identities provided as separate arguments
            identity_array=("$@")
        elif [[ -n "${BASH_VERSION}" ]]; then
            # identities provided as newline terminated string, bash variant
            readarray -t -d $'\n' identity_array <<< "$1"
        elif [[ -n "${ZSH_VERSION}" ]]; then
            # identities provided as newline terminated string, zsh variant
            # shellcheck disable=SC2296
            identity_array=("${(@f)1}")
        fi
        for identity in "${identity_array[@]}"; do
            # evaluate value if containing ~ or $HOME
            identity=$(eval echo "${identity}")
            ssh-add -t 4h "${identity}"
        done
    fi
}

ssh-load-identities() {
    __ssh_load "${SSH_IDENTITIES}"
}

ssh-find-agents() {
    if [[ "${OSTYPE}" == darwin* ]]; then
        pgrep -l ssh-agent
    else
        pgrep --list-full 'ssh-agent'
    fi
}

ssh-list-keys() {
    ssh-add -l
}

ssh-is-remote-session() {
    if [[ -n "${SSH_CLIENT}" || -n "${SSH_TTY}" ]]; then
        echo "true"
        return 0
    elif [[ "${PPID}" -eq 0 ]]; then
        echo "false"
        return 0
    else
        case $(ps -o comm= -p "${PPID}") in
            sshd|*/sshd)
                echo "true"
                return 0
                ;;
        esac
    fi
    echo "false"
    return 1
}

ssh-get-fingerprint() {
    ssh-keygen -E sha256 -lf "$1"
}

if [[ "$1" == "--ssh-is-remote-session" ]]; then
    ssh-is-remote-session
fi
