#!/usr/bin/env bash

## Customize history
# Show timestamp of command execution in history
export HISTTIMEFORMAT="%Y-%m-%d %T "
export HISTFILE="${HOME}/.cache/bash_history"
export HISTSIZE=100000
export HISTFILESIZE=200000
# Append commands to history file
shopt -s histappend

alias h='history'
history-search() {
    # do not print the last line, which is the current call
    history | grep --ignore-case "$@" | head --lines=-1
}
alias hs='history-search'

# Edit the current command line with vim with Ctrl+V,Ctrl+V
# The first Ctrl+V enters the vi mode
bind -m vi-command '"\C-v": edit-and-execute-command'

# Clear screen with Meta+Shift+L
bind -x '"\eL": printf "\ec"'

# Ported from https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/sudo
sudo-command-line() {
    [[ -z "${READLINE_LINE}" ]] && READLINE_LINE=$(fc -ln -0 | sed 's/^[ \t]*//')
    if [[ "${READLINE_LINE}" == sudo\ * ]]; then
        READLINE_LINE="${READLINE_LINE#sudo }"
        ((READLINE_POINT-=5))
    elif [[ "${READLINE_LINE}" == "${EDITOR}"\ * ]]; then
        READLINE_LINE="${READLINE_LINE#"${EDITOR}" }"
        READLINE_LINE="sudoedit ${READLINE_LINE}"
        local len=${#EDITOR}
        ((READLINE_POINT-=len+8))
    elif [[ "${READLINE_LINE}" == "sudoedit"\ * ]]; then
        READLINE_LINE="${READLINE_LINE#sudoedit }"
        READLINE_LINE="${EDITOR} ${READLINE_LINE}"
        local len=${#EDITOR}
        ((READLINE_POINT+=len-8))
    else
        READLINE_LINE="sudo ${READLINE_LINE}"
        ((READLINE_POINT+=5))
    fi
}
# sudo current/last command with Ctrl+F
bind -x '"\C-f":"sudo-command-line"'

transpose_whitespace_words() {
    local prefix=${READLINE_LINE:0:$READLINE_POINT}
    local suffix=${READLINE_LINE:$READLINE_POINT}
    # cursor is at the end of line
    if [[ "${suffix}" =~ ^[[:space:]]*$ && "${prefix}" =~ ([^[:space:]][[:space:]]*)$ ]]; then
        prefix=${prefix%"${BASH_REMATCH[0]}"}
        suffix=${BASH_REMATCH[0]}${suffix}
    fi
    # cursor is in a word (prefix does not end with a space and suffix does not start with a space)
    if [[ "${suffix}" =~ ^[^[:space:]] && "${prefix}" =~ [^[:space:]]+$ ]]; then
        prefix=${prefix%"${BASH_REMATCH[0]}"}
        suffix=${BASH_REMATCH[0]}${suffix}
    fi
    # suffix starts with a space
    if [[ "${suffix}" =~ ^[[:space:]]+ ]]; then
        prefix=${prefix}${BASH_REMATCH[0]}
        suffix=${suffix#"${BASH_REMATCH[0]}"}
    fi
    if [[ "${prefix}" =~ ([^[:space:]]+)([[:space:]]+)$ ]]; then
        local word1=${BASH_REMATCH[1]}
        local space=${BASH_REMATCH[2]}
        prefix=${prefix%"${BASH_REMATCH[0]}"}
        if [[ "${suffix}" =~ [^[:space:]]+ ]]; then
            local word2=${BASH_REMATCH[0]}
            suffix=${suffix#"${word2}"}
            READLINE_LINE="${prefix}${word2}${space}${word1}${suffix}"
            READLINE_POINT=$((${#prefix} + ${#word2} + ${#space} + ${#word1}))
        fi
    fi
}
# Swap words with Ctrl+G
bind -x '"\C-g": transpose_whitespace_words'

repeat_last_word() {
    local prefix=${READLINE_LINE:0:$READLINE_POINT}
    local suffix=${READLINE_LINE:$READLINE_POINT}
    local last_word
    # cursor is behind a non space
    if [[ "${prefix}" =~ [^[:space:]]+$ ]]; then
        last_word=${BASH_REMATCH[0]}
        # cursor is before a non space
        if [[ "${suffix}" =~ ^[^[:space:]]+ ]]; then
            last_word=${last_word}${BASH_REMATCH[0]}
        fi
    elif [[ "${prefix}" =~ ([^[:space:]]+)[[:space:]]+$ ]]; then
        last_word=${BASH_REMATCH[1]}
    fi
    READLINE_LINE=${prefix}${last_word}${suffix}
    READLINE_POINT=$((${#prefix} + ${#last_word}))
}
# Repeat last word with Meta+M
bind -x '"\em": repeat_last_word'

# Go directory up with Ctrl+Alt+K
# keycode 201 does not exist, so using it as a hack
# The usage of the macro hides the 'cd ..'
bind -x '"\201": cd ..'
bind '"\e\C-k":"\C-u\201\C-m"'
# Ctrl+Alt+Up (\e[1;7A) not working, using Ctrl+Up instead
bind '"\e[1;5A":"\C-u\201\C-m"'
# Workaround for macOS, where Ctrl+Opt+K is used by Rectangle - using vt sequences (needs iTerm2 Key Mapping):
# https://en.wikipedia.org/wiki/ANSI_escape_code
bind '"\e[16~":"\C-u\201\C-m"'      # Ctrl+Cmd+k

## Directory cycle
# Ported from https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/dircycle
# Make pushd silent, except for help
pushd() {
    if [[ "$1" == "--help" ]]; then
        command pushd "$@" || return
    else
        command pushd "$@" &>/dev/null || return
    fi
}
cd() {
    if [[ "$#" -eq 0 ]]; then
        command cd || return
    else
        pushd "$@" || return
    fi
}
# keycodes 202 and 203 do not exist, so using it as a hack
# The usage of the macro hides the 'pushd ..'
# Cycle backwards with Ctrl+Alt+H and Ctrl+Alt+Left
bind -x '"\202": pushd +1'
bind '"\e\C-h":"\C-u\202\C-m"'
bind '"\e[1;7D":"\C-u\202\C-m"'
# Cycle forwards with Ctrl+Alt+L and Ctrl+Alt+Right
bind -x '"\203": pushd -0'
bind '"\e\C-l":"\C-u\203\C-m"'
bind '"\e[1;7C":"\C-u\203\C-m"'
# Workaround for macOS, to be in line with up-directory - using vt sequences (needs iTerm2 Key Mapping):
# https://en.wikipedia.org/wiki/ANSI_escape_code
bind '"\e[9~":"\C-u\202\C-m"'       # Ctrl+Cmd+h
bind '"\e[22~":"\C-u\203\C-m"'      # Ctrl+Cmd+l


bind '"\C-\xfe": alias-expand-line'
bind '"\C-\xff": magic-space'

# inspired by https://github.com/ohmyzsh/ohmyzsh/blob/master/plugins/globalias/globalias.plugin.zsh
enable_globalias() {
    # Expand alias and add a space when pressing Space
    bind '" ": "\C-\xfe\C-\xff"'
    # Type a simple space when pressing Ctrl+Space
    bind '"\C-\ ": magic-space'
}
disable_globalias() {
    # Type a simple space when pressing Space
    bind '" ": magic-space'
    # Expand alias and add a space when pressing Ctrl+Space
    bind '"\C-\ ": "\C-\xfe\C-\xff"'
}
if [[ "${GLOBALIAS}" == 'on' ]]; then
   enable_globalias
else
   disable_globalias
fi
