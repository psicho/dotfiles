#!/usr/bin/env bash

declare -A __help_functions=(
    [cfg]="lib/configurer.sh"
    [checkscripts]="lib/shellcheck.sh"
    [color2DGradient]="lib/colors.sh"
    [colorGradient]="lib/colors.sh"
    [count]="lib/count.sh"
    [count-lines]="lib/count.sh"
    [docker-ps]="config/docker-ps.sh"
    [explain]="config/config-shell.sh"
    [fif]="config/fzf-config.sh"
    [fromhex]="lib/colors.sh"
    [git-tools]="lib/git-tools.sh"
    [kube]="config/kube-config.sh"
    [login]="lib/login.sh"
    [dir-navigation]="lib/dir-navigation.sh"
    [todo]="config/config-environment.sh"
    [tohex]="lib/colors.sh"
)
declare -A __help_function_aliases=(
    [dps]="docker-ps"
    [ex]="explain"
    [gt]="git-tools"
    [l]="login"
)
declare -A __help_scripts=(
    [bootstrap.sh]="install/bootstrap.sh"
    [create-user.sh]="install/create-user.sh"
    [install-extra.sh]="install/install-extra.sh"
    [theming-demo.sh]="theming/demo/theming-demo.sh"
    [upgrade.sh]="install/upgrade.sh"
)
declare -A __help_script_aliases=(
    [bootstrap]="bootstrap.sh"
    [create-user]="create-user.sh"
    [install-extra]="install-extra.sh"
    [demo]="theming-demo.sh"
    [upgrade]="upgrade.sh"
)

show-help() {
    # get part after last slash
    local curcmd=${READLINE_LINE##*/}
    # trim trailing spaces
    curcmd=${curcmd% }

    if [[ -z "${curcmd}" ]]; then
        curcmd=$(echo "${!__help_functions[*]} ${!__help_script_aliases[*]}" | \
                    sed 's/ /\n/g' | \
                    sort | \
                    fzf-tmux -p90% --no-multi --preview "${DOTFILES_DIR}/lib/bash/show-help.bash --preview {}")
        if [[ -n "${curcmd}" ]]; then
            READLINE_LINE="${curcmd}"
            # FIXME zle end-of-line
        fi
    fi

    tman "${curcmd}"
    # FIXME zle reset-prompt
}

tman() {
    local curcmd="$1"

    if [[ -n "${curcmd}" ]]; then
        if [[ -n "${TMUX_PANE}" ]]; then
            # Known issue: very short man pages, such as "remove-shell",
            # would fit on a split tmux pane and less will quit.
            # Looks like global --quit-if-one-screen has prevalence due to the usage of a subshell.
            # The below LESS export is working though for --help on custom commands.
            tmux split-window "LESS='--RAW-CONTROL-CHARS' BAT_THEME='${BAT_THEME}'
                               ${DOTFILES_DIR}/lib/bash/show-help.bash --preview '${curcmd}'"
        else
            printf "\n"
            __print_help "${curcmd}"
            printf "\n\n"
        fi
    else
        echo "Please provide the command to document."
    fi
}

__print_help() {
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}/lib/help.sh"
    local curcmd="$1"
    local function_cmd=${__help_function_aliases[$curcmd]:-$curcmd}
    local script_cmd=${__help_script_aliases[$curcmd]:-$curcmd}
    local tldr_option

    if [[ "${OSTYPE}" == darwin* ]]; then
        tldr_option='--color'
    fi

    if [[ -n ${__help_functions[$function_cmd]} ]]; then
        # shellcheck disable=SC1090
        source "${DOTFILES_DIR}/${__help_functions[$function_cmd]}"
        eval "${function_cmd}" --help | less
    elif [[ -n ${__help_scripts[$script_cmd]} ]]; then
        local script_file="${DOTFILES_DIR}/${__help_scripts[$script_cmd]}"
        eval "${script_file}" --help | less
    else
        local tldr_page_exists manpage_exists
        tldr ${tldr_option} "${curcmd}" &>/dev/null
        tldr_page_exists=$?
        man --where "${curcmd}" &>/dev/null
        manpage_exists=$?
        if [[ "${tldr_page_exists}" -eq 0 && "${manpage_exists}" -eq 0 ]]; then
            # both tldr and man page exist (most commands)
            {
                tldr ${tldr_option} "${curcmd}"
                man "${curcmd}" | colorize-help
            } | less
        elif [[ "${tldr_page_exists}" -eq 0 ]]; then
            # only tldr page exists, e.g. exa v0.9.0
            {
                tldr ${tldr_option} "${curcmd}"
                # remove possible colorization from help command, e.g. delta --help
                "${curcmd}" --help 2>/dev/null | sed 's/\x1b\[[0-9;]*m//g' | colorize-help
            } | less
        elif [[ "${manpage_exists}" -eq 0 ]]; then
            # only man page exists, e.g. remove-shell
            man "${curcmd}" | colorize-help
        else
            # neither tldr nor man page exist, e.g. test_help()
            # try --help
            "${curcmd}" --help &>/dev/null
            local help_exists=$?
            if [[ "${help_exists}" -eq 0 ]]; then
                "${curcmd}" --help 2>/dev/null | colorize-help | less
            else
                __print_hint
            fi
        fi
    fi
}

# for debugging purposes
test_help() {
    if [[ "$1" == "--help" || "$1" == "-h" ]]; then
        cat <<-EOF
			Usage
			    test_help [options]
			Options
			    -h, --help: show this help
		EOF
    else
        echo "Call this function with -h or --help."
    fi
}

__print_hint() {
    local help_scripts help_functions
    help_scripts="$(echo "${!__help_script_aliases[@]}" | sed 's/ /\n/g' | sort | tr '\n' ' ')"
    help_functions="$(echo "${!__help_functions[@]}"    | sed 's/ /\n/g' | sort | tr '\n' ' ')"
    cat <<-EOF
		Help is available for:
		 * man pages
		 * if no man page exists: commands with a --help option
		 * tldr pages
		 * scripts:   ${help_scripts}
		 * functions: ${help_functions}
	EOF
    [[ -n "${TMUX_PANE}" ]] && read -n 1 -r
}

if [[ "$1" == "--preview" ]]; then
    shift
    __print_help "$1"
else
    # shellcheck disable=SC2016
    bind -x '"\eH": "show-help"'
fi
