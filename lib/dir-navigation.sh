#!/usr/bin/env bash
# shellcheck disable=SC2164

__dir_navigation_usage() {
    local prog_name="$1"
    local directory_alias="$2"
    local base_dir="$3"
    [[ -n ${directory_alias} ]] && directory_alias=" ${directory_alias}"
    [[ -n ${base_dir} ]] && base_dir=" ${base_dir}"

    colorize-help <<-EOF
		Bookmark automation

		Change to${directory_alias} directory${base_dir} or it's sub dirs.

		Usage:
		    ${prog_name}                 Change to${directory_alias} directory
		    ${prog_name} [subdir]        Change to sub dir of${directory_alias} directory. Can be any substring of it's sub dirs.
		    ${prog_name} [-h] [--help]   Print this help
	EOF
}

__dir_navigation_usage_for_show_help() {
    __dir_navigation_usage "dir-navigation" "" ""

    colorize-help <<-EOF

		dir-navigation can be any of the following:
		    Alias bf / b.                  Changes to dotfiles: ${DOTFILES_DIR}
		    Alias bd                       Changes to desktop:  ${DESKTOP_DIR}
		    Alias bp                       Changes to project:  ${PROJECT_DIR}
		    Alias bw                       Changes to wiki:     ${WIKI_DIR}
		    Alias bs                       Changes to settings: ${WIKI_DIR}/Settings
	EOF
}

dir-navigation() {
    local prog_name="$1"
    local directory_alias="$2"
    local base_dir="$3"
    local target="$4"
    local result_dir return_code

    local cd_command="cd"
    [[ -n "${BASH_VERSION}" ]] && cd_command="pushd"

    if [[ "${prog_name}" == "-h" || "${prog_name}" == "--help" ]]; then
        __dir_navigation_usage_for_show_help
        return
    fi
    if [[ "${target}" == "-h" || "${target}" == "--help" ]]; then
        __dir_navigation_usage "$@"
        return
    fi

    result_dir=$(__dir_selection "${base_dir}" "${target}")
    return_code=$?
    if [[ return_code -eq 0 ]]; then
        eval "${cd_command} '${result_dir}'"
    fi
}

__dir_selection() {
    local subdirs dircount sorted_dirs
    local base_dir="$1"
    local target="$2"
    
    if [[ -z "${target}" ]]; then
        echo "${base_dir}"
        return 0
    elif [[ -d "${base_dir}/${target}" ]]; then
        # in case of an exact match, use that
        echo "${base_dir}/${target}"
        return 0
    fi

    [[ -n "${ZSH_VERSION}" ]] && setopt local_options KSH_ARRAYS

    # it's simply more elegant to use find instead of fd, as we're interested in the filename
    # shellcheck disable=SC2207
    IFS=$'\n' subdirs=($(find "${base_dir}" -mindepth 1 -maxdepth 1 -type d -printf '%f\n' | rg "${target}"))
    dircount=${#subdirs[@]}
    if [[ "${dircount}" -eq "0" ]]; then
        echo "No directory matching '${target}' found under '${base_dir}'." >&2
        return 1
    elif [[ "${dircount}" -eq "1" ]]; then
        echo "${base_dir}/${subdirs[0]}"
        return 0
    else
        echo "Found multiple directories matching '${target}':" >&2
        # shellcheck disable=SC2207
        IFS=$'\n' sorted_dirs=($(sort <<<"${subdirs[*]}"))
        unset IFS
        for i in "${sorted_dirs[@]}"; do
            echo "    ${i}" >&2
        done
        return 1
    fi
}

__dir_navigation_completion() {
    local subdir_list=() subdir
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local base_dir="$3"

    base_dir=$(eval "echo ${base_dir}")
    while IFS=  read -r -d $'\0'; do
        subdir=$(basename "${REPLY}")
        if [[ "${subdir}" == *$cur* ]]; then
            subdir_list+=("${subdir}")
        fi
    done < <(find "${base_dir}" -mindepth 1 -maxdepth 1 -type d -print0)

    COMPREPLY=("${subdir_list[@]}")
    return 0
}

# In non-interactive shell (e.g. tmux help pane), this command would break
if [[ $- == *i* ]]; then
    complete -F __dir_navigation_completion dir-navigation pj bf bd bp bw bs b. desktop wiki
fi

# Directories ("Bookmarks")
alias bf='dir-navigation bf dotfiles "${DOTFILES_DIR}"'
alias bd='dir-navigation bd desktop  "${DESKTOP_DIR}"'
alias bp='dir-navigation bp project  "${PROJECT_DIR}"'
alias bw='dir-navigation bw wiki     "${WIKI_DIR}"'
alias bs='dir-navigation bs settings "${WIKI_DIR}/Settings"'
alias b.=bf
## Backwards compat
alias desktop='dir-navigation desktop desktop "${DESKTOP_DIR}"'
alias wiki='dir-navigation wiki wiki "${WIKI_DIR}"'
alias pj='dir-navigation pj project "${PROJECT_DIR}"'
