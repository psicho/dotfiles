#!/usr/bin/env bash
# shellcheck disable=SC2059

# not allowing length 0 for the parts would be better, but that somehow doesn't work with sed
REPO_REGEX="[a-zA-Z0-9-]*/[a-zA-Z0-9-]*"

get-latest-release-version() {
    local repo="$1"
    local apiurl_template="https://api.github.com/repos/%s/releases/latest"
    local apiurl version

    if [[ ! ${repo} =~ ${REPO_REGEX} ]]; then
        echo "'$1' is not a valid github repo."
        return 1
    fi

    apiurl=$(printf "${apiurl_template}" "${repo}")
    version=$(curl --silent "${apiurl}" | jq .tag_name)

    # strip "" and the optional v
    version="${version:1:-1}"
    [[ ${version:0:1} == "v" ]] && version="${version:1}"
    echo "${version}"
}

get-latest-release-url() {
    local release_template repo version number_of_replacements

    release_template="$1"
    # bash is bad with back references, preferring sed instead
    # shellcheck disable=SC2001
    repo=$(printf "${release_template}" | sed "s#https://github.com/\(${REPO_REGEX}\).*#\1#")
    version=$(get-latest-release-version "${repo}")
    number_of_replacements=$(grep --only-matching "%s" <<< "${release_template}" | wc --lines)
    if [[ "${number_of_replacements}" -eq 1 ]]; then
        printf "${release_template}" "${version}"
    elif [[ "${number_of_replacements}" -eq 2 ]]; then
        printf "${release_template}" "${version}" "${version}"
    else
        return 1
    fi
}

get-all-repos() {
    [[ -n "${ZSH_VERSION}" ]] && setopt local_options KSH_ARRAYS
    local user="$1"
    local token="$2"
    local url="https://api.github.com/orgs/${user}/repos?sort=full_name&per_page=100&page=1"
    local response

    while [[ -n "${url}" ]]; do
        response=$(curl --silent --include --header "Authorization: token ${token}" --request GET "${url}")
        __rematch "${response}" 'link: .*<(.*)>; rel="next"'
        url="${__rematch[1]}"
        __rematch "${response}" '(\[.*\])'
        echo "${__rematch[1]}" | jq --raw-output 'map(select(.archived == false)) | .[].name'
        echo "${__rematch[1]}" | jq --raw-output 'map(select(.archived == true)) | .[].name' | sed --regexp-extended 's/$/:Archived/g'
    done
}

get-current-repo() {
    local repo remote
    if git rev-parse --is-inside-work-tree &>/dev/null; then
        remote=$(git remote get-url origin)
        if [[ "${remote}" =~ git@ ]]; then
            repo=$(echo "${remote}" | cut --delimiter=':' --fields=2-)
        elif [[ "${remote}" =~ https:// ]]; then
            repo=$(echo "${remote}" | cut --delimiter='/' --fields=4-)
        fi
    fi
    if [[ "${repo}" =~ \.git$ ]]; then
        repo="${repo:0:-4}"
    fi
    echo "${repo}"
}

get-current-host() {
    local repo remote
    if git rev-parse --is-inside-work-tree &>/dev/null; then
        remote=$(git remote get-url origin)
        if [[ "${remote}" =~ git@ ]]; then
            repo=$(echo "${remote}" | cut --delimiter='@' --fields=2 | cut --delimiter=':' --fields=1)
            repo=$(ssh -G "${repo}" | rg "^hostname " | cut --delimiter=' ' --fields=2)
        elif [[ "${remote}" =~ https:// ]]; then
            repo=$(echo "${remote}" | cut --delimiter='/' --fields=3)
        fi
        echo "https://${repo}"
    fi
    echo ""
}

# SYNOPSIS
#   __rematch STRING REGEX
# DESCRIPTION
#   Multi-shell implementation of the =~ regex-matching operator;
#   works in: bash, zsh
#
#   Matches STRING against REGEX and returns exit code 0 if they match.
#   Additionally, the matched string(s) is returned in array variable ${__rematch[@]},
#   which works the same as bash's ${BASH_REMATCH[@]} variable: the overall
#   match is stored in the 1st element of ${__rematch[@]}, with matches for
#   capture groups (parenthesized subexpressions), if any, stored in the remaining
#   array elements.
#   NOTE: zsh arrays by default start with index *1*.
# EXAMPLE:
#   __rematch 'This AND that.' '^(.+) AND (.+)\.' # -> ${__rematch[@]} == ('This AND that.', 'This', 'that')
__rematch() {
    typeset exit_code
    unset -v __rematch                      # initialize output variable
    [[ $1 =~ $2 ]]                          # perform the regex test
    exit_code=$?                            # save exit code
    if [[ "${exit_code}" -eq 0 ]]; then     # copy result to output variable
        [[ -n "${BASH_VERSION}" ]] && __rematch=( "${BASH_REMATCH[@]}" )
        # shellcheck disable=SC2154
        [[ -n "${ZSH_VERSION}" ]]  && __rematch=( "${MATCH}" "${match[@]}" )
    fi
    return "${exit_code}"
}
