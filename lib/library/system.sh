#!/usr/bin/env bash

is_arm() {
    if [[ "${OSTYPE}" == darwin* ]]; then
        if [[ $(uname -m) == "arm64" ]]; then
            return 0
        else
            return 1
        fi
    else
        if [[ $(dpkg --print-architecture) == "arm64" ]]; then
            return 0
        else
            return 1
        fi
    fi
}

set_title() {
    title=$1
    printf "\e]0;%s\a" "${title}"
}

get_system_title() {
    if [[ -n "${WSL_DISTRO_NAME}" ]]; then
        printf "%s" "${WSL_DISTRO_NAME%%-*}"
    else
        printf "%s" "$("${DOTFILES_DIR}/lib/prompt-tools.sh" --get-hostname)"
    fi
}

set_system_title() {
    if [[ -z "${TMUX_PANE}" ]]; then
        set_title "$(get_system_title)"
    fi
}

if [[ "$1" == "--get-system-title" ]]; then
    get_system_title
fi
