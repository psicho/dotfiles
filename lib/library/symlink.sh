#!/usr/bin/env bash

symlink-files() {
    local source_dir="$1"
    local target_dir="$2"
    local IFS

    link() {
        from="$1"
        to="$2"
        rm --force "${to}"
        ln --verbose --symbolic "${from}" "${to}"
    }

    if [[ ! -d "${source_dir}" ]]; then
        echo "Source directory ${source_dir} does not exist"
        exit 1
    fi
    if [[ ! -d "${target_dir}" ]]; then
        echo "Target directory ${target_dir} does not exist"
        exit 1
    fi
    echo "Symlinking all files from ${source_dir} to ${target_dir}"

    cd "${source_dir}" || exit 1

    while IFS= read -r -d '' location; do
        link "${source_dir}/${location}" "${target_dir}/${location}"
    done < <(find . -maxdepth 1 -type f -name '*.*' -print0)
}

delete-broken-symlinks() {
    local target_dir="$1"

    if [[ ! -d "${target_dir}" ]]; then
        echo "Directory '${target_dir}' is invalid."
        return 1
    fi

    find "${target_dir}" -maxdepth 1 -type l -exec test ! -e {} \; -exec rm {} +
}
