#!/usr/bin/env bash

__winget_recache() {
    if command -v winget.exe &>/dev/null; then
        local winget_cache_file=~/.cache/winget.cache
        local winget_temp_file=/tmp/winget-export.json
        local package_count
        echo "Updating installed winget package cache in ${winget_cache_file}..."
        winget.exe export --output ${winget_temp_file} 1>/dev/null
        jq --raw-output '.Sources[] | select(.SourceDetails.Name == "winget") | .Packages[].PackageIdentifier' ${winget_temp_file} \
            | sort --ignore-case \
            > "${winget_cache_file}"
        package_count=$(wc --lines ${winget_cache_file} | cut --delimiter=' ' --fields=1)
        echo "Found ${package_count} packages."
    fi
}
