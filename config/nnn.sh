#!/usr/bin/env bash

# https://github.com/jarun/nnn/

# Configuration
nnn_init() {
    if command -v nnn &>/dev/null; then
        # A: no dir auto-enter during filter
        # d: detail mode
        # U: show user and group
        # B: use bsdtar for archives
        # x: show notifications on selection cp, mv, rm completion; copy path to system clipboard on selection; show xterm title
        # Q: no confirmation on quit with multiple contexts
        # u: use selection if available, don't prompt to choose between selection and hovered entry (e.g. for copying, deleting, moving)
        export NNN_OPTS="AdUBxQu"

        if "${DOTFILES_DIR}/lib/ssh-manager.sh" --ssh-is-remote-session >/dev/null; then
            # c: indicates that the opener is a cli-only opener (overrides -e)
            NNN_OPTS="${NNN_OPTS}c"
            export NNN_OPENER="${HOME}/.config/nnn/plugins/nuke"
        fi

        # Colors
        BLK="0B" CHR="0B" DIR="04" EXE="06" REG="00" HARDLINK="06" SYMLINK="06" MISSING="00" ORPHAN="09" FIFO="06" SOCK="0B" OTHER="06"
        export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"
        if is_dark_theme; then
            export NNN_COLORS='7264'
        else
            export NNN_COLORS='0264'
        fi
        export NNN_BATTHEME="${BAT_THEME}"

        export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"
        if command -v cmatrix >/dev/null; then
            # `-u delay` does not work with `-C color` in nnn
            export NNN_LOCKER="cmatrix -b -a -C ${COLOR_CMATRIX:-green}"
        fi
        export NNN_HELP="cat ${DOTFILES_DIR}/docs/nnn-help.txt"

        # Bookmarks
        export NNN_BMS="f:${DOTFILES_DIR};.:${DOTFILES_DIR}"
        [[ -d "${WIKI_DIR}" ]] && NNN_BMS="${NNN_BMS};w:${WIKI_DIR};s:${WIKI_DIR}/Settings"
        [[ -d "${DESKTOP_DIR}" ]] && NNN_BMS="${NNN_BMS};d:${DESKTOP_DIR}"
        [[ -d "${PROJECT_DIR}" ]] && NNN_BMS="${NNN_BMS};p:${PROJECT_DIR}"

        # plugins
        # shellcheck disable=SC2016
        export NNN_PLUG='e:!code "$nnn"*;o:!${DOTFILES_DIR}/config/nnn.sh --open-dir "$nnn"*;x:togglex'
        export NNN_PLUG="${NNN_PLUG};z:autojump;d:diffs;f:fzopen;g:fzcd;q:fzplug;p:preview-tui;n:nuke;.:gitroot;V:x2sel;/:finder;P:rsynccp"
        [[ "${OSTYPE}" == darwin* ]] && NNN_PLUG="${NNN_PLUG};c:cbcopy-mac;v:cbpaste-mac"
        # shellcheck disable=SC2154
        export NNN_ZLUA="${__zlua_path}"
        export NNN_FIFO="${HOME}/.config/nnn/nnn.fifo"
        if [[ "${OSTYPE}" == darwin* ]]; then
            if [[ -n "${TMUX}" ]]; then
                export NNN_PREVIEWIMGPROG='timg -ps'
            else
                export NNN_PREVIEWIMGPROG='timg'
            fi
        fi

        [[ "${OSTYPE}" == darwin* ]] && export NNN_TRASH="trash"

        # takes care about cd on quit
        # adapted from https://github.com/jarun/nnn/tree/master/misc/quitcd
        n() {
            NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

            # shellcheck disable=SC2139
            "${DOTFILES_DIR}/config/nnn.sh" --main "$@"

            [[ ! -f "${NNN_TMPFILE}" ]] || {
                # shellcheck disable=SC1090
                source "${NNN_TMPFILE}"
                rm --force -- "${NNN_TMPFILE}" > /dev/null
            }
        }
    fi
}

# used only if called via 'n --detach'
# https://github.com/jarun/nnn/wiki/Basic-use-cases#detached-text
nnn_detached_text_wrapper() {
    if [[ $(($(tput lines) * 2)) -gt "$(tput cols)" ]]; then
        NNN_SPLIT='h'
    else
        NNN_SPLIT='v'
    fi

    if [[ -n "${TMUX}" ]] ; then
        # tmux splits are inverted
        [[ "$NNN_SPLIT" = "v" ]] && split="h" || split="v"

        tmux split-window "-${split}" "${EDITOR} \"$*\""
    elif [[ "${TERM_PROGRAM}" = "iTerm.app" ]]; then
        [[ "${NNN_SPLIT}" = "h" ]] && split="horizontally" || split="vertically"

        osascript <<-EOF
			tell application "iTerm"
			    tell current session of current window
			        split $split with default profile command "env IS_RELOAD=true ${SHELL} --login -c \"${EDITOR} $1\""
			        tell application "System Events" to keystroke "]" using command down
			    end tell
			end tell
		EOF
    fi
}

nnn_open_dir() {
    local target=$1
    if [[ ! -d "${target}" ]]; then
        # open the parent directory if the hovered element is not a directory
        target=.
    fi
    if [[ "${OSTYPE}" == darwin* ]]; then
        open "${target}"
    elif [[ -n "${WSL_DISTRO_NAME}" ]]; then
        source "${DOTFILES_DIR}/config/config-environment.sh"
        open_directory_in_explorer "${target}"
    fi
}

nnn_main() {
    local fzf_opts

    # Block nesting of nnn in subshells
    [[ "${NNNLVL:-0}" -eq 0 ]] || {
        echo "nnn is already running"
        return
    }

    fzf_opts="
        --layout reverse
        --height 80%
        --cycle
        --ansi
        --no-mouse
        --preview-window right:60%:hidden
        --preview 'echo {} | tr --squeeze-repeats \" \" | cut --delimiter=\" \" --fields=2 | xargs tree -aCI \"\.git\" | less'
        --color 'hl:${COL_FZF_HL},hl+:${COL_FZF_HL_PLUS},pointer:${COL_FZF_POINTER},marker:${COL_FZF_MARKER},bg+:${COL_FZF_BG_PLUS},header:${COL_FZF_HEADER},fg+:${COL_FZF_FG_PLUS}'
        --pointer '󰁕'
        --marker ''
        --prompt '${FZF_PROMPT_CHAR}'
        --info 'inline:  '
        --bind '?:toggle-preview'
    "

    if [[ $1 == "--detach" ]]; then
        # we don't want to override VISUAL in the parent shell, therefore we're running in a subshell here
        # (called via `nnn.sh --main`)
        export VISUAL="${DOTFILES_DIR}/config/nnn.sh --detach"
        shift
    fi
    export FZF_DEFAULT_OPTS="${fzf_opts}"
    export LESS='--RAW-CONTROL-CHARS --no-init --ignore-case'

    # The command builtin allows one to alias nnn to n, if desired, without
    # making an infinitely recursive alias
    command nnn "$@"
}

case "$1" in
    --init)
        nnn_init
        ;;
    --main)
        shift
        nnn_main "$@"
        ;;
    --detach)
        shift
        nnn_detached_text_wrapper "$@"
        ;;
    --open-dir)
        shift
        nnn_open_dir "$@"
        ;;
esac
