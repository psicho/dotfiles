# Note that viins is set as default mode in .zlogin (`bindkey -v`).
# `bindkey -v` equals `bindkey -M viins` and `bindkey -a` equals `bindkey -M vicmd`
# For more info, see http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#Keymaps

# Escape sequences ^[[9~ ^[[16~ ^[[22~ ^[[27~ ^[[30~ ^[[35~ ^[36~ are unused vt sequences and need iTerm2 Key Mapping
# https://en.wikipedia.org/wiki/ANSI_escape_code
# https://github.com/psicho2000/wiki/blob/master/Settings/macOS/macOS-Settings.txt, section iTerm2

# Use some of the Emacs/Bash key bindings that I love
bindkey -M viins '^a' beginning-of-line                 # Ctrl+A
bindkey -M viins '^e' end-of-line                       # Ctrl+E
bindkey -M viins '^[.' insert-last-word                 # Meta+.
bindkey -M viins '^[^?' backward-kill-word              # Meta+Backspace
bindkey -M viins '^[[3;3~' kill-word                    # Meta+Delete
bindkey -M viins '^[[3~' delete-char                    # Delete on macOS
bindkey -M viins '^w' backward-kill-word                # Ctrl+W
bindkey -M viins '^u' kill-whole-line                   # Ctrl+U
bindkey -M viins '^[[27~' backward-kill-line            # Cmd+Backspace
bindkey -M viins '^[[30~' kill-line                     # Cmd+Delete

# Fix home/end in tmux after introduction of 24-bit colors in tmux
bindkey -M viins '^[[1~' beginning-of-line              # Home
bindkey -M viins '^[[4~' end-of-line                    # End
bindkey -M viins '^[[H' beginning-of-line               # Home
bindkey -M viins '^[[F' end-of-line                     # End
# Fix home/end in neovim over tmux (via workaround F7/F8)
bindkey -M viins '^[[18~' beginning-of-line             # F7
bindkey -M viins '^[[19~' end-of-line                   # F8
bindkey -M vicmd '^[[18~' beginning-of-line             # F7
bindkey -M vicmd '^[[19~' end-of-line                   # F8

# Fix backward deletion when switching modes
bind-backspace() {
    local autopair_path
    if [[ "${OSTYPE}" == darwin* ]]; then
        autopair_path="${HOMEBREW_PREFIX}/share/zsh-autopair/autopair.zsh"
    else
        autopair_path="${HOME}/bin/zsh-autopair/autopair.zsh"
    fi
    if [[ ${AUTO_PAIR} == 'on' && -f "${autopair_path}" ]]; then
        # must be done here as key-bindings need to be loaded last
        bindkey -M viins '^?' autopair-delete           # Backspace
    else
        bindkey -M viins '^?' backward-delete-char      # Backspace
    fi
}
bind-backspace

# Beginning search with arrow keys/j/k
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey -M viins '^k' up-line-or-beginning-search       # Ctrl+K
bindkey -M viins '^j' down-line-or-beginning-search     # Ctrl+J
bindkey -M viins '^[OA' up-line-or-beginning-search     # Up on WSL
bindkey -M viins '^[OB' down-line-or-beginning-search   # Down on WSL
bindkey -M viins '^[[A' up-line-or-beginning-search     # Up on macOS
bindkey -M viins '^[[B' down-line-or-beginning-search   # Down on macOS

# Word/char jump
bindkey -M viins '^[[1;5D' backward-word                # Ctrl+Left on WSL
bindkey -M viins '^[^[[D' backward-word                 # Opt+Left on macOS
bindkey -M viins '^[h' backward-word                    # Meta+H
bindkey -M viins '^[[1;5C' forward-word                 # Ctrl+right on WSL
bindkey -M viins '^[^[[C' forward-word                  # Opt+Left on macOS
bindkey -M viins '^[l' forward-word                     # Meta+L
bindkey -M viins '^h' backward-char                     # Ctrl+H
bindkey -M viins '^l' forward-char                      # Ctrl+L

# Undo / redo
bindkey -M viins '^[<' undo                             # Meta+<
bindkey -M viins '^[>' redo                             # Meta+>
bindkey -M viins '^[[35~' undo                          # Cmd+Z
bindkey -M viins '^[[36~' redo                          # Shift+Cmd+Z

# Paste from last cut command
paste-from-cutbuffer() { RBUFFER="${CUTBUFFER}${RBUFFER}" }
zle -N paste-from-cutbuffer
bindkey -M viins '^p' paste-from-cutbuffer              # Ctrl+P

# File rename magic
bindkey -M viins '^[m' copy-prev-shell-word             # Meta+M
swap-words() {
    local wordchars_current="${WORDCHARS}"
    WORDCHARS='~!#$%^&*(){}[]<>?.+;-_/'
    zle transpose-words
    WORDCHARS="${wordchars_current}"
}
zle -N swap-words
bindkey -M viins '^g' swap-words                        # Ctrl+G

# Clear screen
clear-screen() {
    printf "\ec"
    zle reset-prompt
}
zle -N clear-screen
bindkey -M viins '^[L' clear-screen                     # Meta+Shift+L

# Edit command line
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd '^v' edit-command-line                 # Ctrl+V

# Exit even if command line is not empty
exit-zsh() { exit }
zle -N exit-zsh
bindkey '^d' exit-zsh                                   # Ctrl+D
