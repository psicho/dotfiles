#!/usr/bin/env bash

## Do not use any specific tools here (like ripgrep, fdfind, ...) as this file is used in a minimal setup.

# Define editors (Ctrl+X, Ctrl+E opens default editor)
export EDITOR='vim'
export VISUAL='vim'
alias v='vim'

# Define pager
## do not page if output fits on screen, preserve colors, do not clear screen after quitting, smart case searching
export LESS='--quit-if-one-screen --RAW-CONTROL-CHARS --no-init --ignore-case'
## for less 6xx, we need to configure that all private use area characters are printable (see man page)
export LESSUTFCHARDEF='E000-F8FF:p,F0000-FFFFD:p,100000-10FFFD:p'
## configure lesskey as the file for less specific shortcuts
[[ -f "${DOTFILES_DIR}/config/lesskey" ]] && export LESSKEYIN="${DOTFILES_DIR}/config/lesskey"
## less is the default pager. Anyway, we don't want to depend on that being set.
export BAT_PAGER='less'
export MANPAGER='less'
export PAGER='less'

# Highlight section titles in manual pages.
# Need to set the color directly, as we don't have colors.sh on minimal setups.
# shellcheck disable=SC2155
command -v tput >/dev/null && export LESS_TERMCAP_md="$(tput setaf 136)";

mansearch() {
    if [[ -z "$1" ]]; then
        echo "Please provide a man page."
        return 1
    fi
    if [[ -z "$2" ]]; then
        echo "Please provide an option to search for (without hyphens)."
        return 1
    fi
    LESS="$LESS --pattern [-]{1,2}$2\b" man "$1"
}
alias ms=mansearch

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ~='cd ~' # `cd` is probably faster to type though
alias -- -='cd -'
alldirs() {
    if [[ -n "$1" ]]; then
        dirs "$@"
    else
        dirs -v
    fi
}

# ls aliases
alias ll='ls -l --almost-all --human-readable --color --group-directories-first'
## Always use colored output for `ls`
alias ls='ls --color'

# Lists the 20 most used commands
history-stats() {
    fc -l -100000000000 | \
        awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | \
        grep --invert-match "./" | \
        column -c 3 -s " " -t | \
        sort --numeric-sort --reverse | \
        nl | \
        head --lines=20
}
# Enable aliases to be sudo`ed
alias sudo='TERM=xterm sudo '

# Always enable colored `grep` output
# Note: `GREP_OPTIONS="--color=auto"` is deprecated, hence the alias usage.
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'

filter() {
    local filename="$1"
    local filter="$2"

    if [[ "$1" == "-h" || "$1" == "--help" ]]; then
        echo "Continuously filter a file. Usage: filter <filename> <filter string>"
        return 0
    fi

    tail --follow "${filename}" | grep --line-buffered "$filter"
}

# Human readable sizes
alias df='df --human-readable'
alias du='du --human-readable'

# More nice aliases
alias calc='/usr/bin/bc --mathlib'
alias map='telnet mapscii.me'
alias pack='tar --create --gzip --verbose --file'
alias unpack='tar --extract --gunzip --verbose --file'

# Reload the shell (i.e. invoke as a login shell)
reload-shell() {
    if [[ -n "${ZSH_VERSION}" ]]; then
        SHLVL=$((SHLVL-1))
        STAY_IN_DIRECTORY="true" exec zsh --login
    else
        STAY_IN_DIRECTORY="true" exec bash --login
    fi
}
alias reload='reload-shell'
# Go into given start dir, otherwise into home directory if not disabled
if [[ -n "${START_DIR}" ]]; then
    # shellcheck disable=SC2164
    cd "${START_DIR}"
    unset START_DIR
    IS_RELOAD=true
elif [[ -n "${STAY_IN_DIRECTORY}" ]]; then
    unset STAY_IN_DIRECTORY
    # shellcheck disable=SC2034
    IS_RELOAD=true
else
    # shellcheck disable=SC2164
    cd "${HOME}"
fi

# Print each PATH entry on a separate line
path() {
    echo "${PATH}" | sed --regexp-extended 's/:/\n/g'
}

# Disable certificate check for wget and use different location for HTTP Strict Transport Security file
alias wget='wget --no-check-certificate --hsts-file=${HOME}/.cache/.wget-hsts'

# Show my IP
# Alternatives: ifconfig.co, ifconfig.me, ifconfig.io, icanhazip.com
# The first 3 services have Web Frontends with additional info
IP_SERVICE=ifconfig.co
# shellcheck disable=SC2139
alias myip="curl --ipv4 --silent ${IP_SERVICE}"
# shellcheck disable=SC2139
alias myipv6="curl --ipv6 --silent ${IP_SERVICE}"

# create a directory recursively an cd to it
make-dir() {
    # shellcheck disable=SC2164
    mkdir --parents "$@" && cd "$_"
}
alias md='make-dir'
alias rd='rmdir'

# Determine size of a file or total size of a directory
file-size() {
    if [[ "$#" -gt 0 ]]; then
        du --human-readable --summarize -- "$@"
    else
        du --human-readable --summarize .[^.]* ./*
    fi
}
alias fs='file-size'

# git settings
if hash git &>/dev/null; then
    # Use Git's colored diff when available
    diffgit() {
        if [[ "${GIT_PAGER}" =~ ^delta ]]; then
            if [[ $1 == '-s' || $1 == '--simple' ]]; then
                shift
                delta "$@" --features simple
            else
                delta "$@" --features unobtrusive-side-by-side
            fi
        else
            git diff --no-index --color-words "$@"
        fi
    }

    ## this is overwritten when config-environment.sh is loaded - which isn't in minimal installation
    export GIT_PAGER='less'
    alias g='git'
    alias gc='git clone'
    gd() {
        local infotext diffresult git_pager diff_line2
        local OLD_IFS="${IFS}"
        IFS=' '
        git_pager="${GIT_PAGER}"
        if [[ $1 == '-s' || $1 == '--simple' ]]; then
            shift
            [[ "${git_pager}" =~ ^delta ]] && git_pager="${git_pager} --features simple"
        else
            [[ "${git_pager}" =~ ^delta ]] && git_pager="${git_pager} --features unobtrusive-side-by-side"
        fi
        if [[ -n "$(git diff 2>/dev/null)" ]]; then
            infotext="${COL_CYAN_FG}Showing diff${COL_RESET}"
            diffresult=$(git diff --color=always "$@")
        elif [[ -n "$(git diff --cached 2>/dev/null)" ]]; then
            infotext="${COL_CYAN_FG}Showing staged${COL_RESET}"
            diffresult=$(git diff --color=always --cached "$@")
        else
            infotext="${COL_CYAN_FG}Showing last commit${COL_RESET}"
            diffresult=$(git show --color=always "$@")
            diff_line2=$(echo "${diffresult}" | sed --silent 2p)
            if [[ "${diff_line2}" =~ ^Merge: ]]; then
                diff_line2="${diff_line2#* }"
                diff_line2="${diff_line2/ /...}"
                diffresult="${diffresult}\n\n$(git diff --color=always "${diff_line2}")"
            fi
        fi
        echo -e "${infotext}\n${diffresult}" | ${git_pager}
        IFS="${OLD_IFS}"
    }
    alias gp='git push'
    alias gs="git status --short --branch"
    alias gsa="git stash --include-untracked"
    git_update() {
        local result exit_code
        # we want to print the output of git pull to stdout and to result
        # file descriptor 3 is usually unused and only necessary because the outer call would otherwise not catch it
        {
            set -o pipefail     # we want to catch errors on git pull
            result=$(git pull --rebase 2>&1 | tee /dev/fd/3)
            exit_code=$?
        } 3>&1
        if [[ exit_code -eq 0 && ! "${result}" =~ "up to date" ]]; then
            if git config alias.since >/dev/null; then
                # tee omits the paging
                git since HEAD --color=always | tee
            else
                # fallback if no .gitconfig is present (e.g. for minimal installation); tee omits the paging
                gl "HEAD@{1}..HEAD@{0}" HEAD --color=always | tee
            fi
        fi
    }
    alias gu=git_update
    if git config alias.l >/dev/null; then
        alias gl='git l'
    else
        # fallback if no .gitconfig is present (e.g. for minimal installation)
        alias gl="git log --format='%C(3)%h  %C(2)%<(30)%an  %C(4)%cd%C(auto)%d %C(244)%s%C(reset)' \
                  --date=format:'%Y-%m-%d %H:%M:%S' --max-count=30"
    fi
fi

# Show what an alias or function does
explain() {
    local alias_result declarable alias_part print_command=true pager res exported_var
    local command="$1" command_result help_pager

    local options ret plain unescape
    options=$(getopt --options hpu --longoptions help,plain,unescape -- "$@")
    ret=$?
    [[ $ret -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help)
                # shellcheck disable=SC1091
                [[ -f "${DOTFILES_DIR}/lib/help.sh" ]] && source "${DOTFILES_DIR}/lib/help.sh"
                if hash bat &>/dev/null && declare -f colorize-help &>/dev/null; then
                    help_pager="colorize-help"
                else
                    help_pager="cat"
                fi
                eval "${help_pager}" <<-EOF
					Explain alias, function or export. Without args: show all aliases.

					Usage:
					    explain <option>

					Options:
					    -h, --help          Print this help
					    -p, --plain         Show functions without line numbers
					    -u, --unescape      Unescape aliases
				EOF
                return 0
                ;;
            -p | --plain) plain=true;;
            -u | --unescape) unescape=true;;
            --) shift; break;;
        esac
        shift
    done

    if hash bat &>/dev/null; then
        pager='bat --style="numbers,grid" --language sh --color always'
        [[ "${plain}" == true ]] && pager="${pager} --plain"
    else
        pager='cat'
    fi

    # show export if it exists
    exported_var=$(__echo_exported_variable "$1")
    if [[ -n "${exported_var}" ]]; then
        printf "export %s=%s" "$1" "${exported_var}" \
            | bat --plain --language bash --theme ansi
    fi

    if [[ "$#" -eq 0 ]]; then
        # if no arguments provided, print all aliases
        alias_result=$(alias 2>&1 | tr --squeeze-repeats " ")
    else
        alias_result=$(alias "$1" 2>&1 | tr --squeeze-repeats " ")
    fi
    if [[ -n "${alias_result}" && ! "${alias_result}" =~ "not found" ]]; then
        # we have an alias

        # EXPERIMENTAL
        # alias escapes ' in various forms - we want to undo that
        # replace '\'\'' back to ''
        # replace '\'' back to '
        # remove ' at end of line
        # remove '\ at end of line
        # remove first ' after =
        if [[ "${unescape}" == true ]]; then
            alias_result=$(echo "${alias_result}" \
                            | sed "s/'\\\'\\\''/''/g" \
                            | sed "s/'\\\''/'/g" \
                            | sed "s/'$//" \
                            | sed "s/'\\$//" \
                            | sed "s/='/=/")
        fi

        if hash bat &>/dev/null; then
            echo "alias ${alias_result}" | bat --plain --language bash --theme ansi
        else
            echo "alias ${alias_result}"
        fi
        # get the part after = and trim '
        alias_part=${alias_result#*=}
        #[[ "${alias_part}" =~ ^\'.*\'$ ]] && alias_part=${alias_part:1:${#alias_part}-2}
        declarable="${alias_part}"
    else
        # not an alias - must be a command
        declarable="$1"
    fi

    if declare -f "${declarable}" >/dev/null; then
        # declarable is a function
        declare -f "${declarable}" | eval "$pager"
        print_command=false
    elif [[ -n "${alias_part}" ]]; then
        if [[ $(echo "${declarable}" | wc --words) -ne 1 ]]; then
            # if we have an alias_part, but the alias does not point to a single executable, don't print where/command
            print_command=false
        else
            command="${alias_part}"
        fi
    fi

    if [[ "${print_command}" == true ]]; then
        if command -v where &>/dev/null; then
            command_result=$(where "${command}")
        else
            command_result=$(command -v "${command}")
        fi
        res=$?
        [[ "${res}" -ne 0 ]] && return 0    # cannot resolve, thus return
        # where and command can yield multiple results
        while read -r command_part; do
            # respect symlinks in output via eza and ls
            if [[ -f "${command_part}" ]]; then
                # it's a file
                if command -v eza &>/dev/null; then
                    eza --oneline "${command_part}"
                else
                    # shellcheck disable=SC2012
                    ls -g --no-group --time-style=+"" "${command_part}" | awk '{print $4, $5, $6}'
                fi
            else
                # it's most probably a built-in command
                echo "${command_part}"
            fi
        done <<< "${command_result}"
    fi
}
alias ex='explain'


# helper function for preview and exports
__echo_exported_variable() {
    # eval "value=\${$1}"
    # shellcheck disable=SC2154
    # echo "$value"

    local value="$1"
    if [[ -n "${BASH_VERSION}" ]]; then
        echo "${!value}"
    elif [[ -n "${ZSH_VERSION}" ]]; then
        # shellcheck disable=SC2296
        echo "${(P)value}"
    fi
}
