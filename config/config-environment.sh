#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091

##################################
# General aliases & exports
##################################

# https://medium.com/@dubistkomisch/how-to-actually-get-italics-and-true-colour-to-work-in-iterm-tmux-vim-9ebe55ebc2be
export TERM=xterm-256color-italic

# Delete last entry from history
# In bash, this depends upon the PROMPT_COMMAND being set appropriately, so this is environment specific
# In bash, we cannot prevent to write repairhist/rh to history
#   https://stackoverflow.com/q/8473121 does not help, as the last command will be always written after repair
#   i.e. resetting HISTCONTROL / HISTFILE will be always there
repairhist() {
    head --lines=-2 "${HISTFILE}" > "/tmp/tmphistfile"
    mv "/tmp/tmphistfile" "${HISTFILE}"
}
alias rh=repairhist

msg() {
    if [[ "${OSTYPE}" == darwin* ]]; then
        echo "${COL_YELLOW_FG}On macOS, use log instead.${COL_RESET}"
    else
        sudo dmesg --ctime --follow
    fi
}

##################################
# Editors
##################################

# shellcheck disable=SC2139
__define_editors() {
    local editor
    command -v nvim &>/dev/null && editor=nvim || editor=vim
    export EDITOR="${editor}"
    export VISUAL="${editor}"
    alias v="${editor}"
    alias vi="${editor}"
    alias vim="${editor}"
}
__define_editors

##################################
# dotfiles specific aliases
##################################

create-test() {
    if [[ ! -f "test.sh" ]]; then
        printf "#!/usr/bin/env bash\n\n\n" > test.sh
        chmod +x test.sh
        vi test.sh
    fi
}
# shellcheck disable=SC2139
alias demo="${DOTFILES_DIR}"/theming/demo/theming-demo.sh
demo-docker() {
    local docker_ubuntu="registry.gitlab.com/psicho/docker-ubuntu"
    # shellcheck disable=SC2153
    local background="${BACKGROUND}"
    if [[ "$1" == "--pull" || "$1" == "-p" ]]; then
        docker pull "${docker_ubuntu}"
    else
        echo "Call with option -p/--pull to always pull the latest container."
    fi
    if [[ "${background}" == "system" && (-n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin*) ]]; then
        source "${DOTFILES_DIR}/theming/set-theme.sh"
        background=$(get_system_background)
    fi
    docker run --rm --interactive --tty --env="CLIENT_THEME=${THEME}" --env="CLIENT_BG=${background}" "${docker_ubuntu}"
}
nerdfont() {
    local glyphfile="$WIKI_DIR/Settings/Terminal/glyph-list.txt"
    if [[ -f "${glyphfile}" ]]; then
        fzf --preview-window=:hidden < "${glyphfile}"
    fi
}
todo() {
    local pager columns
    local todo_file="${DOTFILES_DIR}/todo.md"

    if command -v lowdown &>/dev/null; then
        columns=$(tput cols)
        pager="lowdown -Tterm --term-width=${columns} --term-column=${columns}"
    else
        pager="bat --plain --wrap character --language markdown"
    fi
    case "$1" in
        -h | --help)
            colorize-help <<-EOF
				Show/edit todo file

				Usage:
				    todo [option]

				Options:
				    -a, --all        Show the complete file. Otherwise, show only part before section 'later'
				    -e, --edit       Edit the file with current editor
				    -h, --help       Show this help
			EOF
            ;;
        -e | --edit)
            "${EDITOR}" "${todo_file}"
            ;;
        -a | --all)
            ${pager} "${todo_file}" | less
            ;;
        *)
            sed '/# later/,$d' "${todo_file}" | head --lines=-1 | ${pager} | less
            ;;
    esac
}

# Updates
## dotlog alias is superior to `less ~/dotfiles-install.log` for 2 reasons: colors work better, continuation logs don't repeat
# shellcheck disable=SC2139
alias dotlog="bat ${HOME}/dotfiles-install.log | less --raw-control-chars"
# shellcheck disable=SC2139
alias update="${DOTFILES_DIR}/install/update.sh && reload"
# shellcheck disable=SC2139
alias upgrade="${DOTFILES_DIR}/install/upgrade.sh"

# Shells
source "${DOTFILES_DIR}"/lib/library/system.sh
bash() {
    [[ -n "${TMUX_PANE}" ]] && printf "\e[6 q" || printf "\e[5 q"
    if [[ "$#" -eq 0 && -t 0 ]]; then
        # running interactively
        [[ "${ZSH_VERSION}" ]] && SHLVL=$((SHLVL+1)) # fix for starship.rs SHLVL
        SHELL="/bin/bash" START_DIR="${PWD}" /usr/bin/env bash --login
        [[ "${ZSH_VERSION}" ]] && SHLVL=$((SHLVL-1)) # fix for starship.rs SHLVL
    else
        # either non interactive (e.g. from pipe) or extra args given
        /usr/bin/env bash "$@"
    fi
}
zsh() {
    if [[ "$#" -eq 0 && -t 0 ]]; then
        # running interactively
        [[ "${BASH_VERSION}" ]] && SHLVL=$((SHLVL-1)) # fix for starship.rs SHLVL
        SHELL="/bin/zsh" START_DIR="${PWD}" /usr/bin/env zsh --login
        [[ "${BASH_VERSION}" ]] && SHLVL=$((SHLVL+1)) # fix for starship.rs SHLVL
    else
        # either non interactive (e.g. from pipe) or extra args given
        /usr/bin/env zsh "$@"
    fi
}
t() {
    if [[ -n "${TMUX_PANE}" ]]; then
        echo "${COL_RED_FG}Already in a tmux pane. Force nesting via tmux."
        return 1
    fi
    local last_venv="${VIRTUAL_ENV}"
    printf "\e[6 q"
    source "${DOTFILES_DIR}/.venv/bin/activate"
    START_DIR="${PWD}" "${DOTFILES_DIR}/.venv/bin/python3" "${DOTFILES_DIR}/lib/tmux-select-session.py"
    deactivate
    [[ -n "${last_venv}" ]] && source "${last_venv}/bin/activate"
    set_system_title
}
alias tl='/usr/bin/env tmux list-sessions'
tn() {
    printf "\e[6 q"
    START_DIR="${PWD}" /usr/bin/env tmux -T overline new-session >/dev/null
    set_system_title
}
tmux() {
    /usr/bin/env tmux -T overline "$@"
    set_system_title
}
ssh() {
    # https://medium.com/@dubistkomisch/how-to-actually-get-italics-and-true-colour-to-work-in-iterm-tmux-vim-9ebe55ebc2be
    # Setting TERM is maybe not necessary anymore, ssh'ing works on remotes with and without these dotfiles
    TERM=xterm-256color /usr/bin/ssh "$@"
    set_system_title
}
# set tmux plugin manager location
if [[ -f "${HOME}/bin/tpm/tpm" ]]; then
    export TPM_PATH="${HOME}/bin/tpm/tpm"
elif [[ -f "${HOMEBREW_PREFIX}/opt/tpm/share/tpm/tpm" ]]; then
    export TPM_PATH=""${HOMEBREW_PREFIX}/opt/tpm/share/tpm/tpm""
fi

##################################
# macOS specific aliases
##################################

if [[ "${OSTYPE}" == darwin* ]]; then
    open_directory_in_finder() {
        local target="$1"
        local subdir="$2"
        local return_code=0
        if [[ -n "${subdir}" ]]; then
            target=$(__dir_selection "${target}" "${subdir}")
            return_code=$?
        fi
        [[ return_code -eq 0 ]] && open "${target}"
        return 0
    }
    alias of='open_directory_in_finder "${DOTFILES_DIR}"'
    alias od='open_directory_in_finder "${DESKTOP_DIR}"'
    alias op='open_directory_in_finder "${PROJECT_DIR}"'
    alias ow='open_directory_in_finder "${WIKI_DIR}"'
    alias os='open_directory_in_finder "${WIKI_DIR}/Settings"'
    alias o.=of
    alias here='open .'
fi

##################################
# WSL specific aliases
##################################

if [[ -n "${WSL_DISTRO_NAME}" ]]; then
    print_wsl_working_directory() {
        local wsl_root current_dir mount_point
        mount_point=$(df --output=target "${PWD}" | tail --lines=1)
        if [[ "${mount_point}" =~ ^/mnt ]]; then
            echo "."
        else
            # uses \\wsl$\... instead of \\wsl.localhost\..., which is produced by .
            wsl_root="\\\\wsl\$\\${WSL_DISTRO_NAME}"
            current_dir="${PWD}"
            current_dir=${current_dir//\//\\}
            printf "%s%s" "${wsl_root}" "${current_dir}"
        fi
    }
    open_directory_in_explorer() {
        local target="$1"
        local subdir="$2"
        local return_code=0

        if [[ -n "${subdir}" ]]; then
            target=$(__dir_selection "${target}" "${subdir}")
            return_code=$?
        fi
        # execute in a subshell, so that the main process stays in the current dir
        (
            if [[ return_code -eq 0 ]]; then
                cd "${target}" || exit 1
                explorer.exe "$(print_wsl_working_directory)"
            fi
        )
        return 0
    }
    alias of='open_directory_in_explorer "${DOTFILES_DIR}"'
    alias od='open_directory_in_explorer "${DESKTOP_DIR}"'
    alias op='open_directory_in_explorer "${PROJECT_DIR}"'
    alias ow='open_directory_in_explorer "${WIKI_DIR}"'
    alias os='open_directory_in_explorer "${WIKI_DIR}/Settings"'
    alias o.=of
    alias here="explorer.exe \$(print_wsl_working_directory); return 0"
    alias explorer='explorer.exe'
    # used by xdg-open
    export BROWSER='powershell.exe -command start'

    find-all() {
        fdfind --hidden "$1" --exclude /mnt --exclude /c --exclude /d --exclude /e /
    }
    if command -v winget.exe &>/dev/null; then
        alias winget='winget.exe'
        alias wg='winget.exe'
        wgu() {
            if [[ "$#" -eq 0 ]]; then
                winget.exe upgrade
            else
                winget.exe upgrade --silent "$@"
            fi
        }
        alias winget-recache='__winget_recache'
    fi
fi

##################################
# Tool specific aliases
##################################

# Use bat (better cat) instead of cat, see https://github.com/sharkdp/bat
alias cat='bat'
bat-theme-preview() {
    if [[ -z "$1" ]]; then
        echo "Please provide a file for preview."
        return 1
    fi
    bat --list-themes | \
        fzf-tmux -p90% --preview="bat --theme={} --color=always $1" \
            --height=100%
}

# https://github.com/eth-p/bat-extras
if [[ -f "${HOME}/bin/bat-extras/src/batgrep.sh" ]]; then
    alias batgrep='$HOME/bin/bat-extras/src/batgrep.sh'
fi

# Use fd as find tool, see https://github.com/sharkdp/fd
export FIND_HIDDEN_COMMAND='fdfind --hidden --no-ignore --exclude ".git" --exclude ".mypy_cache" --follow --color always'
alias fd='fdfind --exclude ".git" --exclude ".mypy_cache" --follow --color always'
# shellcheck disable=SC2139
alias fdh="${FIND_HIDDEN_COMMAND}"

# Use ripgrep for grep, see https://github.com/BurntSushi/ripgrep
alias rg='rg --smart-case'
alias rgh='rg --hidden --glob "!.git" --smart-case'

# make tldr faster to complete, see https://tldr.sh/
# great alternative: https://www.mankier.com/
if [[ -f /usr/local/bin/tldr ]]; then
    complete -W "$(/usr/local/bin/tldr 2>/dev/null --list)" tldr
fi

# https://github.com/jesseduffield/lazydocker/ and https://github.com/jesseduffield/lazygit
if command -v lazydocker &>/dev/null; then
    alias ld='lazydocker'
fi
if command -v lazygit &>/dev/null; then
    alias lg='lazygit'
fi

if command -v kubectl &>/dev/null; then
    alias k='kubectl'
fi

# Use delta as GIT_PAGER if installed. Overrides setting from config-shell.sh
# Observe: `git config core.pager` is additionally used
if command -v delta &>/dev/null; then
    export GIT_PAGER='delta'
fi

# https://github.com/cli/cli
if command -v gh &>/dev/null; then
    alias gh_pr='gh pr create --fill --title'
fi

# tre is a shorthand for tree with hidden files and color enabled, ignoring
# the .git directory, listing directories first. The output gets paged.
if command -v eza &>/dev/null; then
    # https://github.com/eza-community/eza
    alias ll='eza --all --long --group --git --links --time-style=long-iso --header --group-directories-first --icons=always'
    tree-improved() {
        eza --all --long --group --git --links --time-style=long-iso --header --group-directories-first --icons=always \
            --tree --ignore-glob ".git" --color=always "$@" \
            | bat --plain
    }
else
    tree-improved() {
        tree -aC -I '.git' --dirsfirst "$@" | bat --plain
    }
fi
if command -v erd &>/dev/null; then
    if [[ "${OSTYPE}" == darwin* ]]; then
        tree-improved() {
            erd --human --icons --hidden --no-git --dir-order last --layout inverted --color force --sort name --long --time-format iso
        }
    fi
    alias erd-tree='erd --human --icons --hidden --no-git --dir-order last --layout inverted --color force --sort name'
fi
alias tre='tree-improved'

# https://github.com/denilsonsa/prettyping
if [[ "${OSTYPE}" == darwin* ]]; then
    prettyping_path="${HOMEBREW_PREFIX}/bin/prettyping"
else
    prettyping_path="${HOME}/bin/prettyping/prettyping"
fi
if [[ -f "${prettyping_path}" ]]; then
    alias ping='${prettyping_path}'
fi

# Make python3 easier accessible
alias py='python3'
alias pyblack="black --line-length 130 \
                     --exclude '/\.git/' \
                     --exclude '/\.idea/' \
                     --exclude '/\.pytest_cache/' \
                     --exclude '/\.vscode/' \
                     --exclude '/__pycache__/' \
                     --exclude '/env/' \
                     ."
alias pyflake="\${HOME}/.local/bin/autoflake --in-place --remove-all-unused-imports --recursive --exclude '.git,.idea,.pytest_cache,.vscode,env' ."
alias pysort='fd --exclude env py --exec isort'
alias pytest='python3 -m unittest'

ytaudio() {
    local executable exit_code filename
    executable=$(__validate_youtube_dl)
    exit_code=$?
    [[ ${exit_code} -eq 1 ]] && return 1
    if ! command -v AtomicParsley &>/dev/null; then
        echo "Please install AtomicParsley" >&2
        return 1
    fi
    ${executable} --no-playlist --extract-audio --audio-format m4a --audio-quality 0 \
        --add-metadata --embed-thumbnail \
        --output "%(title)s.%(ext)s" "$1"
    filename=$(${executable} --get-filename --output "%(title)s" "$1")
    AtomicParsley "${filename}.m4a" --album "$1" --overWrite
}

ytbatch() {
    local executable exit_code
    executable=$(__validate_youtube_dl)
    exit_code=$?
    [[ ${exit_code} -eq 1 ]] && return 1
    echo "$1" | ${executable} --user-agent Mozilla -a -
    repairhist
}

__validate_youtube_dl() {
    # alternative: yt-dlp
    local executable=youtube-dl
    if ! command -v ${executable} &>/dev/null; then
        echo "Please install ${executable}." >&2
        return 1
    fi
    printf "${executable} version: %s\n" "$(${executable} --version)" >&2
    echo ${executable}
}
