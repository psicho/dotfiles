#!/usr/bin/env bash

load_forgit() {
    # https://github.com/wfxr/forgit
    local forgit_path
    if [[ "${OSTYPE}" == darwin* ]]; then
        forgit_path="${HOMEBREW_PREFIX}/share/forgit/forgit.plugin.sh"
    else
        forgit_path="${HOME}/bin/forgit/forgit.plugin.sh"
    fi
    if [[ -f "${forgit_path}" ]]; then
        export forgit_add='gai'
        export forgit_reset_head='gri'
        export forgit_log='gli'
        # default 'gdi' points to the function below, so deactivate forgit_diff shortcut
        export forgit_diff='forgit_diff'
        export forgit_ignore='gii'
        export FORGIT_COPY_CMD="'${DOTFILES_DIR}/lib/clipboard.sh' --copy"
        FORGIT_PAGER="$(myforgit::diffpager)"
        export FORGIT_PAGER
        export FORGIT_LOG_FZF_OPTS="
            --bind 'ctrl-e:execute(echo {} | cut --delimiter=\" \" --fields=2 | xargs git show | $EDITOR -)'
            --bind 'ctrl-v:execute(echo {} | cut --delimiter=\" \" --fields=2 | xargs git show | code -)'
            --prompt 'Log${FZF_PROMPT_CHAR}'"
        # shellcheck disable=SC1090,SC1091
        source "${forgit_path}"
    fi
    export FORGIT_DIFF_FZF_OPTS="
        --bind 'ctrl-e:execute(echo {} | tr --squeeze-repeats \" \" | cut --delimiter=\" \" --fields=2 | xargs $EDITOR)'
        --bind 'ctrl-v:execute(echo {} | tr --squeeze-repeats \" \" | cut --delimiter=\" \" --fields=2 | xargs code)'
        --bind 'ctrl-y:execute-silent(echo {} \
                                        | tr --squeeze-repeats \" \" \
                                        | cut --delimiter=\" \" --fields=2 \
                                        | ${FORGIT_COPY_CMD})'
        --preview-window right:80%
        --height 100%
        --prompt 'Diff${FZF_PROMPT_CHAR}'"
}

alias gdi=myforgit::git_diff_interactive
alias gfi=myforgit::git_find_interactive
alias gi=myforgit::git_interactive
alias gsi=myforgit::stash_show

myforgit::diffpager() {
    local pager diff_pager
    pager=$(git config core.pager || echo 'cat')
    diff_pager=$(git config pager.diff || echo "${pager}")
    echo "${diff_pager}"
}

# first arg: commit
# second arg: search string within commit
# third arg: filename to filter for
myforgit::show() {
    local commit="$1"
    local search="$2"
    local filter_filename="$3"
    local cmd opts repo less_arg
    git rev-parse --is-inside-work-tree >/dev/null || return 1

    commit=${commit:-HEAD}
    repo="$(git rev-parse --show-toplevel)"
    cmd="${DOTFILES_DIR}/config/forgit.sh --show-command $commit $repo {}"

    if [[ -n "${search}" ]]; then
        less_arg="--pattern '${search}'"
    fi

    # Note: After arg 'query', we must use =. Otherwise and empty arg list won't work.
    opts="${FZF_DEFAULT_OPTS}
        --no-multi
        --exit-0
        --prompt \"Commit${FZF_PROMPT_CHAR}\"
        --bind \"enter:execute(${cmd} | LESS='--RAW-CONTROL-CHARS' less ${less_arg})\"
        --bind 'ctrl-y:execute-silent(source \"${DOTFILES_DIR}/lib/clipboard.sh\" && echo {+} | clipcopy)'
        --query='${filter_filename}'
        --preview \"${cmd}\"
        --preview-window right:80%
        --height 100%
        --header 'Enter: View Change | ^Y: Copy filename'"

    __get_commitish "${commit}"
    # shellcheck disable=SC2086
    if [[ "${__has_parents}" == true ]]; then
        git diff-tree --no-commit-id --name-only -r ${__commitish_file} | FZF_DEFAULT_OPTS="${opts}" "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90%
    else
        git ls-tree --name-only -r ${__commitish_file} | FZF_DEFAULT_OPTS="${opts}" "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90%
    fi
}

myforgit::show_command() {
    local commit="$1"
    local repo="$2"
    local filename="$3"
    local filepath="${repo}/${filename}"
    __get_commitish "${commit}"
    git show --color=always "${commit}" -- "${filepath}" \
       | eval "$(myforgit::diffpager)"
    if [[ "${__is_merge}" == true ]]; then
        git diff --color=always "${__commitish_diff}" -- "${filepath}" \
           | eval "$(myforgit::diffpager)"
    fi
}

myforgit::diff_cached() {
    git rev-parse --is-inside-work-tree >/dev/null || return 1
    local cmd files opts commit repo
    repo="$(git rev-parse --show-toplevel)"
    cmd="echo {} \
            | sed 's/.*]  //' \
            | xargs --replace=% git diff --cached --color=always ${commit} -- '${repo}/%' \
            | $(myforgit::diffpager)"
    opts="${FZF_DEFAULT_OPTS} ${FORGIT_DIFF_FZF_OPTS}
        --no-multi
        --exit-0
        --prompt \"Staged${FZF_PROMPT_CHAR}\"
        --bind \"enter:execute(${cmd} | LESS='--RAW-CONTROL-CHARS' less)\"
        --preview \"${cmd}\"
        --preview-window right:80%
        --height 100%
        --header 'Enter: View Change'"
    git diff --cached --name-status | sed -E 's/^(.)[[:space:]]+(.*)$/[\1]  \2/' |
        FZF_DEFAULT_OPTS="${opts}" "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90%
}

# first arg: commit
# second arg: search string within commit
myforgit::filter() {
    local commit="$1"
    local search="$2"
    local files filtered_files
    IFS=$'\n' read -r -d '' -a files < <( git diff-tree --no-commit-id --name-only -r "${commit}" && printf '\0' )

    filtered_files=""
    for file in "${files[@]}"; do
        if git show --unified=0 "${commit}" -- "${file}" | rg "${search}" &>/dev/null; then
            [[ -n "${filtered_files}" ]] && filtered_files+="\n"
            filtered_files+="${file}"
        fi
    done
    echo -e "${filtered_files}" | "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90% \
        --preview "git show ${commit} -- '{}' | delta" \
        --bind "enter:execute(git show ${commit} -- '{}' \
                    | $(myforgit::diffpager) \
                    | LESS='--RAW-CONTROL-CHARS' less --pattern '${search}')" \
        --header 'Enter: View affected change'
}

# git stash viewer
myforgit::stash_show() {
    local stash_name show_stash_cmd stash_opts fzf_exit_code
    git rev-parse --is-inside-work-tree >/dev/null || return 1
    stash_name='echo {} | cut --delimiter=: --fields=1'
    show_stash_cmd="${stash_name} \
         | xargs --replace=% git stash show --include-untracked --color=always --ext-diff % \
         | $(myforgit::diffpager)"
    stash_opts="
        ${FZF_DEFAULT_OPTS}
        --no-sort
        --no-multi
        --exit-0
        --tiebreak=index
        --preview=\"${show_stash_cmd}\"
        --bind=\"enter:execute(${show_stash_cmd} | LESS='--RAW-CONTROL-CHARS' less)\"
        --bind=\"ctrl-y:execute-silent(${stash_name} \
                                        | tr --delete '[:space:]' \
                                        | ${FORGIT_COPY_CMD})\"
        --bind \"ctrl-p:reload(${stash_name} | git stash pop >/dev/null && git stash list)\"
        --bind \"ctrl-d:reload(${stash_name} | git stash drop >/dev/null && git stash list)\"
        --header '^P: Pop stash | ^D: Drop stash | ^Y: Copy stash name | Enter: Show Details'
        --prompt 'Stash${FZF_PROMPT_CHAR}'
    "
    git stash list | FZF_DEFAULT_OPTS="${stash_opts}" fzf
    fzf_exit_code=$?
    # exit successfully on 130 (ctrl-c/esc)
    [[ ${fzf_exit_code} == 130 ]] && return 0
    return ${fzf_exit_code}
}

# git diff viewer
myforgit::diff() {
    git rev-parse --is-inside-work-tree >/dev/null || return 1
    local opts repo get_files get_file preview_cmd enter_cmd

    repo="$(git rev-parse --show-toplevel)"
    # Construct a null-terminated list of the filenames
    # The input looks like one of these lines:
    #   [R100]  file  ->  another file
    #   [A]     file with spaces
    #   [D]     oldfile
    # And we transform it to this representation for further usage with "xargs -0":
    #   file\0another file\0
    #   file with spaces\0
    #   oldfile\0
    # We have to do a two-step sed -> tr pipe because OSX's sed implementation does
    # not support the null-character directly.
    get_files="echo {} | sed 's/\\\\s*\\\\[.]\\\\s*//' | sed 's/  ->  /\\\n/' | tr '\\\n' '\\\0'"
    # Similar to the line above, but only gets a single file from a single line
    # Gets the new name of renamed files
    get_file="echo {} | sed 's/\\\\s*\\\\[.]\\\\s*//' | sed 's/.*->  //'"
    # Git stashes are named "stash@{x}", which contains the fzf placeholder "{x}".
    # In order to support passing stashes as arguments to _forgit_diff, we have to
    # prevent fzf from interpreting this substring by escaping the opening bracket.
    # The string is evaluated a few subsequent times, so we need multiple escapes.
    git_diff="git diff --color=always"
    preview_cmd="cd '${repo}' && ${get_files} | xargs -0 ${git_diff} --unified=3 -- | $(myforgit::diffpager)"
    enter_cmd="cd '${repo}' && ${get_files} | xargs -0 ${git_diff} --unified=10 -- | $(myforgit::diffpager)"
    opts="
        ${FZF_DEFAULT_OPTS}
        --no-multi
        --exit-0
        --bind=\"enter:execute(${enter_cmd} | LESS='--raw-control-chars' less)\"
        --preview=\"${preview_cmd}\"
        --bind=\"alt-e:execute-silent(${EDITOR} \\\"\$\(${get_file})\\\" >/dev/tty </dev/tty)+refresh-preview\"
        ${FORGIT_DIFF_FZF_OPTS}
        --prompt=\" > \"
    "
    eval "git diff --name-status -- | sed --regexp-extended 's/^([[:alnum:]]+)[[:space:]]+(.*)$/[\1]	\2/'" \
        | sed 's/	/  ->  /2' | expand -t 8 \
        | FZF_DEFAULT_OPTS="${opts}" fzf
    fzf_exit_code=$?
    # exit successfully on 130 (ctrl-c/esc)
    [[ ${fzf_exit_code} == 130 ]] && return 0
    return ${fzf_exit_code}
}

# first arg [optional]: commit
myforgit::git_diff_interactive() {
    if [[ -n "$(git diff 2>/dev/null)" && "$#" -eq 0 ]]; then
        # current diff
        myforgit::diff
    elif [[ -n "$(git diff --cached 2>/dev/null)" && "$#" -eq 0 ]]; then
        # staged changes
        myforgit::diff_cached
    else
        # last or provided commit
        myforgit::show "$@"
    fi
    return 0
}

# args [optional]: filename or starting git commit
myforgit::git_interactive() {
    git rev-parse --is-inside-work-tree >/dev/null || return 1
    local cmd opts fileargs=() commitargs=() filename
    if [[ "$#" -eq 1 ]]; then
        if [[ -f "$1" ]]; then
            fileargs=("--follow" "$1")
            filename="$1"
        elif git cat-file -e "$1^{commit}" 2>/dev/null; then
            commitargs=("$1^..HEAD")
        fi
    fi
    opts="
        ${FZF_DEFAULT_OPTS}
        --no-sort
        --no-multi
        --tiebreak=index
        --bind \"enter:execute(echo {} \
                                | cut --delimiter=' ' --fields=1 \
                                | xargs --replace=% ${DOTFILES_DIR}/config/forgit.sh --show % '' '${filename}')\"
        --bind \"ctrl-y:execute-silent(source '${DOTFILES_DIR}/lib/clipboard.sh' && echo {} \
                                        | cut --delimiter=' ' --fields=1 \
                                        | tr --delete '\n' \
                                        | clipcopy)\"
        --preview \"${DOTFILES_DIR}/config/forgit.sh --preview {} '${filename}'\"
        --preview-window right:60%
        --height 100%
        --header 'Enter: View commit | ^Y: Copy commit hash'"
    git log --format="%C(auto)%h%d %C(244)%ad %C($COLOR_GIT_LOG_FG)%s" \
            --color=always --date=format:'%Y-%m-%d %H:%M:%S' \
            "${fileargs[@]}" "${commitargs[@]}" \
        | FZF_DEFAULT_OPTS="${opts}" "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90%
    return 0
}

# first arg: search string
myforgit::git_find_interactive() {
    local search="$1"
    if [[ -z "${search}" ]]; then
        echo "Please provide a search string."
        return 1
    fi
    git log --format="%C(auto)%h%d %C(244)%ad %C($COLOR_GIT_LOG_FG)%s" \
            --color=always --date=format:'%Y-%m-%d %H:%M:%S' \
            -S"${search}" \
        | "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90% \
            --preview "echo {} | cut --delimiter=' ' --fields=1 \
                        | GIT_PAGER=less xargs --replace=% git show --unified=0 --no-color % \
                        | rg --color always '${search}'" \
            --preview-window right:60% \
            --bind "enter:execute(echo {} \
                        | cut --delimiter=' ' --fields=1 \
                        | xargs --replace=% ${DOTFILES_DIR}/config/forgit.sh --filter % '${search}')" \
            --header 'Enter: View affected files in commit'
    return 0
}

myforgit::preview() {
    local fzf_result="$1"
    local diff_files="$2"
    local commit files colored_files files_output output colored_file parents

    commit="$(echo "${fzf_result}" | cut --delimiter=' ' --fields=1)"
    __get_commitish "${commit}"

    if [[ "${__has_parents}" == true ]]; then
        # shellcheck disable=SC2086
        IFS=' ' files=$(git diff-tree --no-commit-id --name-only -r ${__commitish_file})
    else
        # shellcheck disable=SC2086
        IFS=' ' files=$(git ls-tree --name-only -r ${__commitish_file})
    fi

    colored_files=""
    for file in ${files}; do
        if [[ -f "${file}" ]]; then
            colored_file="$(ls --color=always "${file}")"
        else
            colored_file="${COL_RED_FG}${file}${COL_RESET}"
        fi
        if [[ -z "${colored_files}" ]]; then
            colored_files="${colored_file}"
        else
            colored_files="${colored_files}\n${colored_file}"
        fi
    done
    files_output="${COL_CYAN_FG}Files affected:${COL_RESET}\n${colored_files}\n---"

    # shellcheck disable=SC2086
    output="${files_output}\n$(git show --color=always ${commit} -- ${diff_files})"
    if [[ "${__is_merge}" == true ]]; then
        # shellcheck disable=SC2086
        output="${output}\n$(git diff --color=always "${__commitish_diff}" -- ${diff_files})"
    fi
    echo -e "${output}" | eval "$(myforgit::diffpager)"
}

# return __commitish_file, __commitish_diff, __is_merge, __has_parents as global variables
# important: __commitish_file must be split in subsequent "git diff"
__get_commitish() {
    local commit="$1"
    local parents
    local OLD_IFS="${IFS}"
    # shellcheck disable=SC2207
    IFS=$'\n' parents=($(git cat-file -p "${commit}" | rg '^parent ' | cut --delimiter=' ' --fields=2))
    IFS="${OLD_IFS}"
    if __git_is_merge "${commit}"; then
        [[ -n "${ZSH_VERSION}" ]] && setopt local_options KSH_ARRAYS
        __commitish_file="${parents[0]} ${parents[1]}"
        __commitish_diff="${parents[0]}...${parents[1]}"
        __is_merge=true
        __has_parents=true
    else
        __commitish_file="${commit}"
        __commitish_diff="${commit}"
        __is_merge=false
        if [[ ${#parents[@]} -gt 0 ]]; then
            __has_parents=true
        else
            __has_parents=false
        fi
    fi
}

__git_is_merge() {
    local sha="$1" merge_sha
    merge_sha=$(git rev-list -1 --merges "${sha}~1..${sha}" 2>/dev/null)
    [[ -z "$merge_sha" ]] && return 1 || return 0
}

case "$1" in
    --init)
        load_forgit
        ;;
    --show)
        shift
        myforgit::show "$@"
        ;;
    --show-command)
        shift
        myforgit::show_command "$@"
        ;;
    --filter)
        shift
        myforgit::filter "$@"
        ;;
    --preview)
        shift
        myforgit::preview "$@"
        ;;
esac
