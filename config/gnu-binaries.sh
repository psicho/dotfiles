#!/usr/bin/env bash

if [[ "${OSTYPE}" == darwin* ]]; then
    [[ -x /opt/homebrew/bin/brew ]] && eval "$(/opt/homebrew/bin/brew shellenv)"

    # path is protected and may be loaded multiple times
    if [[ -d "${HOMEBREW_PREFIX}/opt/coreutils/libexec/gnubin" ]]; then
        PATH="${HOMEBREW_PREFIX}/opt/coreutils/libexec/gnubin:${PATH}"
    fi
    if [[ -d "${HOMEBREW_PREFIX}/opt/gnu-getopt/bin" ]]; then
        PATH="${HOMEBREW_PREFIX}/opt/gnu-getopt/bin:${PATH}"
    fi
    if [[ -d "${HOMEBREW_PREFIX}/opt/gawk/libexec/gnubin" ]]; then
        PATH="${HOMEBREW_PREFIX}/opt/gawk/libexec/gnubin:${PATH}"
    fi
    if [[ -d "${HOMEBREW_PREFIX}/opt/grep/libexec/gnubin" ]]; then
        PATH="/opt/homebrew/opt/grep/libexec/gnubin:$PATH"
    fi
    # we must protect manpath ourselves
    if [[ "${__gnu_bin_loaded}" != true ]]; then
        if [[ -d "${HOMEBREW_PREFIX}/opt/findutils/libexec/gnuman" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/findutils/libexec/gnuman:${MANPATH}"
        fi
        if [[ -d "${HOMEBREW_PREFIX}/opt/coreutils/libexec/gnuman" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/coreutils/libexec/gnuman:${MANPATH}"
        fi
        if [[ -d "${HOMEBREW_PREFIX}/opt/gsed/libexec/gnuman" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/gsed/libexec/gnuman:${MANPATH}"
        fi
        if [[ -d "${HOMEBREW_PREFIX}/opt/gnu-getopt/share/man" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/gnu-getopt/share/man:${MANPATH}"
        fi
        if [[ -d "${HOMEBREW_PREFIX}/opt/man-db/libexec/man" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/man-db/libexec/man:${MANPATH}"
        fi
        if [[ -d "${HOMEBREW_PREFIX}/opt/gawk/libexec/gnuman" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/gawk/libexec/gnuman:${MANPATH}"
        fi
        if [[ -d "${HOMEBREW_PREFIX}/opt/grep/libexec/gnuman" ]]; then
            MANPATH="${HOMEBREW_PREFIX}/opt/grep/libexec/gnuman:${MANPATH}"
        fi
    fi
    export __gnu_bin_loaded=true
fi
