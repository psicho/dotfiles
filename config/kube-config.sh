#!/usr/bin/env bash

# Browse Kubernetes
kube() {
    local resource_type resource_type_lower resource_name columns kube_command fzf_opts kubectl_opts

    if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
        __kube_usage
        return 0
    fi

    resource_type="$1"
    [[ -n "${BASH_VERSION}" ]] && resource_type_lower="${resource_type,,}"
    [[ -n "${ZSH_VERSION}" ]] && resource_type_lower="${resource_type:l}"
    shift
    kubectl_opts=$*

    if [[ "${resource_type_lower}" == "event" ]]; then
        columns="NAME:.metadata.name,KIND:.involvedObject.kind,OBJECT:.involvedObject.name,TYPE:.type,REASON:.reason,MESSAGE:.message,LAST SEEN:.series.lastObservedTime"
        kube_command="kubectl get ${kubectl_opts} ${resource_type} --output custom-columns='${columns}'"
    else
        kube_command="kubectl get ${kubectl_opts} ${resource_type} --output wide"
    fi

    resource_name="cut --delimiter=' ' --fields=1 <<<{}"
    if [[ "${kubectl_opts}" == "-A" || "${kubectl_opts}" == "--all-namespaces" ]]; then
        resource_name="tr --squeeze-repeats ' ' <<<{} | cut --delimiter=' ' --fields=2"
        kubectl_opts="--namespace=\$(cut --delimiter=' ' --fields=1 <<<{})"
    fi
    fzf_opts="${FZF_DEFAULT_OPTS}
          --header-lines 1
          --preview-window hidden:80%
          --preview \"${resource_name} | xargs kubectl describe ${kubectl_opts} '${resource_type}'\"
          --prompt \"${resource_type}${FZF_PROMPT_CHAR}\"
          --bind \"ctrl-r:reload(${kube_command})\"
          --bind \"enter:execute(${resource_name} | xargs kubectl describe ${kubectl_opts} ${resource_type} 2>&1 | LESS='' less +G)\"
          --bind \"ctrl-y:execute-silent(source \"${DOTFILES_DIR}/lib/clipboard.sh\" && ${resource_name} | clipcopy)\"
          --header \"Enter: Describe ${resource_type} | ^R: Reload | ^Y: Copy name\"
         "
    if [[ "${resource_type_lower}" == "pod" ]]; then
        fzf_opts="${fzf_opts}
              --bind \"alt-enter:execute(${resource_name} | xargs kubectl logs ${kubectl_opts} 2>&1 | LESS='' less +G)\"
              --header \"Enter: Describe ${resource_type} | Meta+Enter: Logs | ^R: Reload | ^Y: Copy name\"
             "
    elif [[ "${resource_type}" == "secret" ]]; then
        fzf_opts="${fzf_opts}
              --bind \"alt-enter:execute(${resource_name} \
                    | xargs -I% \"${DOTFILES_DIR}/config/kube-config.sh\" --kube-secret % ${kubectl_opts} 2>&1 \
                    | LESS='' less +G)\"
              --header \"Enter: Describe ${resource_type} | Meta+Enter: Show | ^R: Reload | ^Y: Copy name\"
             "
    fi

    eval "${kube_command}" | FZF_DEFAULT_OPTS="${fzf_opts}" "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90%
    return 0
}
kn() {
    kube Node "$@"
}
kp() {
    kube Pod "$@"
}
kr() {
    kube ReplicaSet "$@"
}
kd() {
    kube Deployment "$@"
}
ks() {
    kube Service "$@"
}
ke() {
    kube Event "$@"
}
ki() {
    kube Ingress "$@"
}

# Execute a command in a pod's container
# Synopsis: kube-exec shell name_filter [kubectl options...]
kube-exec() {
    local pod
    local shell="$1"
    local name_filter="$2"
    shift 2
    local options=$*

    # shellcheck disable=SC2086
    pod=$(kubectl get pods ${options} --output custom-columns="NAME:.metadata.name" \
                | grep "${name_filter}" \
                | cut --delimiter=' ' --fields=1)
    if [[ $(echo "${pod}" | wc --lines) -gt 1 ]]; then
        printf "Multiple pods found, specify:\n%s" "${pod}"
    else
        # shellcheck disable=SC2086
        kubectl exec ${options} --stdin --tty "${pod}" -- "${shell}"
    fi
}
alias kes='kube-exec sh'
alias keb='kube-exec bash'

# Show secret
kube-secret() {
    local data date
    local secret_name="$1"
    if [[ -z "${secret_name}" ]]; then
        kubectl get secrets
    else
        shift
        local kubectl_opts=("$@")
        data=$(kubectl get secret "${secret_name}" "${kubectl_opts[@]}" --output=json \
                | jq --raw-output '.data | keys | .[]')
        for date in ${data}; do
            printf "%s: " "${date}"
            kubectl get secret "${secret_name}" "${kubectl_opts[@]}" --output jsonpath="{.data.${date}}" \
                | base64 --decode
            echo
        done
    fi
}
alias ksec='kube-secret'

# Set namespace
kube-set-namespace() {
    local namespace="$1"
    kubectl config set-context --current --namespace="${namespace}"
}
alias kns='kube-set-namespace'

kube-get-node-ips() {
    kubectl get nodes --output wide | awk '{print $7}'
}
alias kip=kube-get-node-ips

kube-copy-node-ip() {
    kube-get-node-ips | tail --lines=1 | clipcopy
}
alias kipc=kube-copy-node-ip

# Accessibility
__kube_usage() {
    colorize-help <<-EOF
		View a Kubernetes resource

		Usage:
		    kube <command> <options>

		Options:
		    -h, --help          Print this help
		    any kubectl options, e.g.: -n {namespace}, -A

		Commands:
		    Deployment          View Kubernetes Deployments (alias: kd)
		    Event               View Kubernetes Events (alias: ke)
		    Ingress             View Kubernetes Ingresses (alias: ki)
		    Node                View Kubernetes Nodes (alias: kn)
		    Pod                 View Kubernetes Pods (alias: kp)
		    ReplicaSet          View Kubernetes ReplicaSets (alias: kr)
		    Secret              View Kubernetes Secrets
		    Service             View Kubernetes Services (alias: ks)
		    <any resource>      View any K8s resource, e.g.: pv, pvc

		    Commands may be written in lower case.
	EOF
    return 0
}

__kube_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local options="Deployment Event Ingress Node Pod ReplicaSet Secret Service"
    if [[ "${COMP_CWORD}" -eq 1 ]]; then
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "${options}" -- "${cur}"))
    fi
    return 0
}

# Main entrypoint
if [[ "$1" == "--kube-secret" ]]; then
    # needed if called from a separate shell
    shift
    kube-secret "$@"
elif [[ -n "${BASH_VERSION}" ]]; then
    complete -F __kube_completion kube
fi
