#!/usr/bin/env sh

# shellcheck disable=SC2154

IFS="$(printf '%b_' '\n')"; IFS="${IFS%_}" # protect trailing \n

echo "prefer: '$NNN_PREFER_SELECTION'"
echo "args: $*"
echo "nnn: '$nnn'"
echo "d1: '$d1'"
echo "f1: '$f1'"
echo "d2: '$d2'"
echo "f2: '$f2'"
echo "d3: '$d3'"
echo "f3: '$f3'"
echo "d4: '$d4'"
echo "f4: '$f4'"
echo "selection:"

selection_file=${NNN_SEL:-${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection}
selection=$(tr '\0' '\n' < "$selection_file")
for file in $selection; do
    echo " * '$file'"
done

read -r _
