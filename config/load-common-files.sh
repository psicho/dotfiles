#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091

# Load configuration
source "${DOTFILES_DIR}"/config/gnu-binaries.sh
source "${DOTFILES_DIR}"/config/config-shell.sh
source "${DOTFILES_DIR}"/config/config-environment.sh
source "${DOTFILES_DIR}"/theming/set-theme.sh --init
## fzf-config.sh configures forgit
source "${DOTFILES_DIR}"/config/fzf-config.sh
source "${DOTFILES_DIR}"/config/forgit.sh --init
source "${DOTFILES_DIR}"/config/kube-config.sh
source "${DOTFILES_DIR}"/config/z.sh
## nnn depends on z.lua
source "${DOTFILES_DIR}"/config/nnn.sh --init
source "${DOTFILES_DIR}"/config/docker-commands.sh
source "${DOTFILES_DIR}"/config/docker-ps.sh

# Load tools
source "${DOTFILES_DIR}"/lib/colors.sh
source "${DOTFILES_DIR}"/lib/dir-navigation.sh
source "${DOTFILES_DIR}"/lib/login.sh
source "${DOTFILES_DIR}"/lib/count.sh
source "${DOTFILES_DIR}"/lib/ssh-manager.sh
source "${DOTFILES_DIR}"/lib/configurer.sh
source "${DOTFILES_DIR}"/lib/shellcheck.sh
source "${DOTFILES_DIR}"/lib/packer.sh
source "${DOTFILES_DIR}"/lib/clipboard.sh
source "${DOTFILES_DIR}"/lib/help.sh
source "${DOTFILES_DIR}"/lib/git-tools.sh

# Load libraries
if [[ -n "${WSL_DISTRO_NAME}" ]]; then
    source "${DOTFILES_DIR}"/lib/library/winget.sh
fi
source "${DOTFILES_DIR}"/lib/library/system.sh
source "${DOTFILES_DIR}"/lib/library/github.sh

# Load solarized8 color changes
modify-palette-solarized

# Load custom configuration
source "${DOTFILES_DIR}"/lib/configurer.sh --load

# Load prompt
export STARSHIP_CONFIG=~/.config/dotfiles/starship.toml
if [[ -n "${BASH_VERSION}" ]]; then
    eval "$(starship init bash)"
    # immediately write history at each shell prompt
    PROMPT_COMMAND="history -a;$PROMPT_COMMAND"
elif [[ -n "${ZSH_VERSION}" ]]; then
    eval "$(starship init zsh)"
fi

# Set system title
set_system_title

[[ -z $IS_RELOAD ]] && command -v fastfetch &>/dev/null && fastfetch
