#!/usr/bin/env bash

__load_brew() {
    local brew_path="/home/linuxbrew/.linuxbrew/bin/brew"
    if [[ -f "${brew_path}" ]]; then
        # execute only once (otherwise shell reload / subshells would add multiple path entries)
        if ! path | grep 'linuxbrew' &>/dev/null; then
            eval "$(${brew_path} shellenv)"
        fi
        if [[ -n "${BASH_VERSION}" ]]; then
            for COMPLETION in "${HOMEBREW_PREFIX}/etc/bash_completion.d/"*; do
                # shellcheck disable=SC1090
                [[ -r "${COMPLETION}" ]] && source "${COMPLETION}"
            done
        else
            FPATH=$FPATH:"${HOMEBREW_PREFIX}"/share/zsh/site-functions
        fi
    fi
}

__load_brew
