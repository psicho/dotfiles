# if current shell is interactive
if [[ $- == *i* ]]; then

    # Meta+D - Paste the selected docker container(s) into the command line
    __dsel() {
        setopt localoptions pipefail no_aliases 2> /dev/null
        local opts
        opts="$FZF_DEFAULT_OPTS
                --preview-window hidden"
        docker ps --format '{{.Names}}' | FZF_DEFAULT_OPTS="$opts" $(__fzfcmd) -m "$@" | while read item; do
            echo -n "${(q)item} "
        done
        local ret=$?
        echo
        return $ret
    }

    fzf-docker-widget() {
        LBUFFER="${LBUFFER}$(__dsel)"
        local ret=$?
        zle reset-prompt
        return $ret
    }
    zle     -N   fzf-docker-widget
    bindkey -M emacs '\ed' fzf-docker-widget
    bindkey -M vicmd '\ed' fzf-docker-widget
    bindkey -M viins '\ed' fzf-docker-widget

fi
