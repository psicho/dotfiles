#!/usr/bin/env bash
# https://github.com/skywind3000/z.lua
# like normal z when used with arguments but displays an fzf prompt when used without.

_select_directory() {
    local option="$1"
    # get all dirs, remove relevance and pass to fzf
    _zlua -l "${option}" 2>&1 | sed 's/^[0-9\.]* *\(.*\)/\1/' | fzf-tmux --nth -1 --delimiter=/ -p90%
}

if [[ "${OSTYPE}" == darwin* ]]; then
    __zlua_path="${HOMEBREW_PREFIX}/opt/z.lua/share/z.lua/z.lua"
else
    __zlua_path="${HOME}"/bin/z.lua/z.lua
fi

[[ ! -f "${__zlua_path}" || -z $(command -v lua 2>/dev/null) ]] && return

if [[ -n "${BASH_VERSION}" ]]; then
    eval "$(lua "${__zlua_path}" --init bash enhanced once fzf-tmux -p90%)"
else
    eval "$(lua "${__zlua_path}" --init zsh enhanced once)"
fi

unalias z 2>/dev/null
if declare -f _zlua >/dev/null; then
    z() {
        [[ "$#" -gt 0 ]] && _zlua "$@" && return
        cd "$(_select_directory)" || return 1
    }
    zz() {
        [[ "$#" -gt 0 ]] && _zlua -c "$@" && return
        cd "$(_select_directory -c)" || return 1
    }
    if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
        o() {
            local target
            if [[ "$#" -gt 0 ]]; then
                target="$(_zlua -e "$@")"
            else
                target="$(_select_directory)"
            fi
            if [[ -z "${target}" ]]; then
                echo "No directory found."
                return 1
            fi
            [[ -n "${WSL_DISTRO_NAME}" ]] && open_directory_in_explorer "${target}"
            [[ "${OSTYPE}" == darwin* ]] && open_directory_in_finder "${target}"
        }
        oo() {
            local target
            if [[ "$#" -gt 0 ]]; then
                target="$(_zlua -c -e "$@")"
            else
                target="$(_select_directory -c)"
            fi
            if [[ -z "${target}" ]]; then
                echo "No directory found."
                return 1
            fi
            [[ -n "${WSL_DISTRO_NAME}" ]] && open_directory_in_explorer "${target}"
            [[ "${OSTYPE}" == darwin* ]] && open_directory_in_finder "${target}"
        }
    fi
fi

__o_completion() {
    local subdir_list=() subdir
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local base_dir="$3"

    base_dir=$(eval "echo ${base_dir}")
    while IFS=  read -r -d $'\0'; do
        subdir=$(basename "${REPLY}")
        if [[ "${subdir}" == *$cur* ]]; then
            subdir_list+=("${subdir}")
        fi
    done < <(find "${base_dir}" -mindepth 1 -maxdepth 1 -type d -print0)

    COMPREPLY=("${subdir_list[@]}")
    return 0
}

# In non-interactive shell (e.g. tmux help pane), this command would break
if [[ $- == *i* ]]; then
    complete -F __o_completion open_directory_in_finder open_directory_in_explorer of od op ow os o.
fi
