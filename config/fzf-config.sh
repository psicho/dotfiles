#!/usr/bin/env bash
# Prerequisites: fdfind, ripgrep, z, bat, batgrep, tree, VS Code, docker

### fzf configuration

__fzf_default_header="\
^G: Back | ^W: Reset filter | ^Space: Toggle mark | \
^A/^U: Toogle/unselect all marks | ^Y: Copy | ^O: Paste | \
?: Toggle Preview | Meta+J/K: Preview ↑/↓ | ^F/^B: Preview Page↑/↓ | \
^E/^V: Open in ${EDITOR}/VS Code"

export FZF_PROMPT_CHAR=' '
export FZF_COMPLETION_TRIGGER=',,'
export FZF_DEFAULT_COMMAND="${FIND_HIDDEN_COMMAND}"
export FZF_DEFAULT_OPTS="
        --header '${__fzf_default_header}'
        --header-first
        --layout reverse
        --height 80%
        --multi
        --cycle
        --ansi
        --no-mouse
        --preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || \
                   ([[ -d {} ]] && (tree -aCI \"\.git\" {} | less)) || \
                   ${DOTFILES_DIR}/config/fzf-config.sh --echo {}'
        --preview-window right:80%
        --color 'hl:${COL_FZF_HL},hl+:${COL_FZF_HL_PLUS},pointer:${COL_FZF_POINTER},marker:${COL_FZF_MARKER},bg+:${COL_FZF_BG_PLUS},header:${COL_FZF_HEADER},fg+:${COL_FZF_FG_PLUS}'
        --pointer '󰁕'
        --marker ''
        --prompt '${FZF_PROMPT_CHAR}'
        --info 'inline:  '
        --bind '?:toggle-preview'
        --bind 'ctrl-space:toggle+down'
        --bind 'ctrl-a:toggle-all'
        --bind 'ctrl-u:deselect-all'
        --bind 'ctrl-e:execute(${EDITOR} {+} < /dev/tty > /dev/tty)'
        --bind 'ctrl-v:execute(code {+})'
        --bind 'alt-j:preview-down'
        --bind 'alt-k:preview-up'
        --bind 'ctrl-f:preview-page-down'
        --bind 'ctrl-b:preview-page-up'
        --bind 'ctrl-o:accept-non-empty'
        --bind 'ctrl-y:execute-silent(source \"${DOTFILES_DIR}/lib/clipboard.sh\" && echo {+} | clipcopy)'
        "
# Meta+C: Change directory
export FZF_ALT_C_COMMAND="${FIND_HIDDEN_COMMAND} --type directory --strip-cwd-prefix"
# Meta+S: Paste files
export FZF_CTRL_T_COMMAND="${FZF_DEFAULT_COMMAND}"
# Ctrl+R: History
export FZF_CTRL_R_OPTS="--preview-window :hidden"

_fzf_compgen_path() {
    fdh . "$1"
}

_fzf_compgen_dir() {
    fdh --type d . "$1"
}

### fzf plugins

# find in aliases
aliases() {
    # We cannot truncate the alias result here:
    # Otherwise, the preview window would not know about it,
    # as the preview is executed in a subshell which does not know the aliases.
    local preview
    preview="echo {} \
                | cut --delimiter='=' --fields=2 \
                | sed s/^\'// \
                | sed s/\'$// \
                | bat --plain --language bash --theme ansi --color always"
    alias | sed 's/alias //g' | bat --plain --language bash --theme ansi --color always \
        | fzf-tmux -p90% --preview "${preview}"  | while read -r item; do
              echo "${item}"
          done
}

# find in exports
exports() {
    export | sed 's/declare -x //g' | cut --delimiter='=' --fields=1 | fzf-tmux -p90% | while read -r item; do
        echo -n "${item}="
        __fzf_echo "${item}"
    done
}

# find-in-file - usage: fif <SEARCH_TERM>
fif() {
    local batgrep_path
    if [[ -f "${HOME}/bin/bat-extras/src/batgrep.sh" ]]; then
        batgrep_path="${HOME}/bin/bat-extras/src/batgrep.sh"
    elif command -v batgrep &>/dev/null; then
        batgrep_path=$(where batgrep)
    fi
    if [[ -z "${batgrep_path}" ]]; then
        echo "Please install batgrep via install-extra.sh"
        return 1
    fi
    local options ret no_ignore
    options=$(getopt --options hI --longoptions help,no-ignore -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __fif_usage; return 0;;
            -I | --no-ignore) no_ignore="--no-ignore";;
            --) shift; break;;
        esac
        shift
    done

    if [ "$#" -eq 0 ]; then
        echo "${COL_RED_FG}Please provide a search string.${COL_RESET}";
        echo
        __fif_usage
        return 1;
    fi
    local files cwd="${PWD}/"
    # for an exact file match to work, we need to
    # 1. add the fullpath for every match (hence the $cwd)
    # 2. surround each match with ^ and $
    # 3. finally, use option --full-path for fdfind
    files=$(rg --files-with-matches --no-messages --smart-case --sort path --hidden ${no_ignore} --glob '!.git' "$1" \
            | xargs echo \
            | sed "s#\ #$|^${cwd}#g")
    if [[ -z "${files}" ]]; then
        echo "No files found matching '$1'."
        return 0
    fi
    # The next sed call must be separated so that we can identify an empty match above.
    cmd="echo '^${cwd}${files}\$' \
        | sed 's/^\(.*\)$/(\1)/' \
        | xargs fdfind --hidden ${no_ignore} --exclude '.git' --full-path --color always --strip-cwd-prefix"
    eval "${cmd}" \
        | "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90% \
            --preview "${batgrep_path} --smart-case --color --context 10 --terminal-width 280 '$1' {}" \
            --bind "enter:execute(LESS='--ignore-case --pattern=$1' bat {})" \
            --bind "ctrl-r:reload(${cmd})" \
            --header 'Enter: View | ^E: Edit | ^R: Reload'
    return 0
}
__fif_usage() {
    colorize-help <<-EOF
		Find in files

		Usage:
		    fif <search-string>

		Options:
		    -h, --help          Print this help
		    -I, --no-ignore     Also search in .gitignore locations
	EOF
    return 0
}

# fzf in hidden files, optional arg: location
fz() {
    local location="${1}"
    local cmd="fdfind --hidden --exclude \.git --color=always"
    if [[ -z "${location}" ]]; then
        cmd="${cmd} --strip-cwd-prefix"
    else
        cmd="${cmd} . ${location}"
    fi
    eval "${cmd}" \
        | "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90% \
            --bind "enter:execute([[ -f {} ]] && LESS='--RAW-CONTROL-CHARS' bat --paging=always {})" \
            --bind "ctrl-r:reload(${cmd})" \
            --header 'Enter: View with bat | ^R: Reload'
    return 0
}

# Browse docker containers
fzd() {
    # Optionally: colorize log preview via ` | ccze -m ansi` (ccze needs to be installed first via apt install ccze)
    local get_id="\$(echo {} | cut --delimiter=\" \" --fields=1)"
    # Note: After arg query, we must use =. Otherwise and empty arg list won't work.
    local opts="
            ${FZF_DEFAULT_OPTS}
            --preview 'docker logs ${get_id}'
            --preview-window right:80%:hidden
            --bind 'ctrl-e:execute(docker exec --interactive --tty ${get_id} bash < /dev/tty > /dev/tty)'
            --bind 'alt-i:execute(docker inspect ${get_id} | bat --language cjson --theme ansi --style numbers)'
            --bind 'alt-e:execute(docker exec --user root ${get_id} bash -c \"apt-get update \
                                    && apt-get install --yes curl vim telnet\" \
                                    && docker exec ${get_id} bash -c \"curl --location https://vipc.de/setup-min \
                                    | bash && exec bash --login\")'
            --bind 'enter:execute(docker logs ${get_id} | LESS=\"--RAW-CONTROL-CHARS\" less --LINE-NUMBERS +G)'
            --bind 'alt-enter:execute(echo {} \
                                        | tr --squeeze-repeats \" \" \
                                        | cut --delimiter=\" \" --fields=2 \
                                        | xargs dive)'
            --bind 'ctrl-y:execute-silent(source \"${DOTFILES_DIR}/lib/clipboard.sh\" && echo {} \
                                            | cut --delimiter=\" \" --fields=1 \
                                            | clipcopy)'
            --bind 'ctrl-r:reload(docker ps --format \"table {{.ID}}\t{{.Image}}\t{{.RunningFor}}\t{{.Status}}\t{{.Ports}}\")'
            --query='$*'
            --header '^E: Exec Bash | Meta+E: Install minimal dotfiles | ^R: Reload | Meta+I: Inspect | Enter: Log | Meta+Enter: Dive | ^Y: Copy container id'
            --header-lines 1
    "
    docker-ps --images-only | FZF_DEFAULT_OPTS="${opts}" "${DOTFILES_DIR}"/config/fzf-tmux-digdown -p90%
    return 0
}

# shellcheck disable=SC1090,SC1091
[[ -n "${ZSH_VERSION}" ]] && source "${DOTFILES_DIR}"/config/fzf-keybinding-plugins.zsh

# helper function for preview and exports
__fzf_echo() {
    # eval "value=\${$1}"
    # shellcheck disable=SC2154
    # echo "$value"

    local value="$1"
    if [[ -n "${BASH_VERSION}" ]]; then
        echo "${!value}"
    elif [[ -n "${ZSH_VERSION}" ]]; then
        # shellcheck disable=SC2296
        echo "${(P)value}"
    fi
}

if [[ "$1" == "--echo" ]]; then
    shift
    __fzf_echo "$@"
fi
