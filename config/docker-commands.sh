#!/usr/bin/env bash
# shellcheck disable=SC2207

if [[ "$("${DOTFILES_DIR}"/lib/configurer.sh --load toggles.sudo-docker)" == 'on' ]]; then
    export SUDO_PREFIX="sudo --preserve-env "
else
    export SUDO_PREFIX=""
fi

# docker
# shellcheck disable=SC2139
alias d="${SUDO_PREFIX}docker"
alias de='docker-exec'
alias di='docker-inspect'
alias drb='docker-run-bash'
alias drs='docker-run-sh'
alias ds='docker-stats'

# docker-compose
# shellcheck disable=SC2139
alias dc="${SUDO_PREFIX}docker-compose"
alias dce='docker-compose-exec'
alias dcer='docker-compose-exec-root'
alias dcl='docker-compose-logs'
alias dcls='docker-compose-list'
alias dci='docker-compose-inspect'
alias dcs='docker-compose-stats'
alias dcu='docker-compose-update'

docker-compose-exec() {
    ${SUDO_PREFIX} docker-compose exec "$1" bash
    set_system_title
}

docker-compose-exec-root() {
    ${SUDO_PREFIX} docker-compose exec --user root "$1" bash
    set_system_title
}

docker-compose-list() {
    ${SUDO_PREFIX} docker-compose exec "$1" sh -c "ls -l --almost-all --human-readable --color=auto --group-directories-first $2"
}

docker-compose-logs() {
    local options ret follow=true
    options=$(getopt --options n --longoptions no-follow -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -n | --no-follow) follow=false;;
            --) shift; break;;
        esac
        shift
    done

    if [[ "${follow}" == true ]]; then
        ${SUDO_PREFIX} docker-compose logs --follow --tail 500 "$@" | bat --paging=never --style=numbers,grid --language log
    else
        ${SUDO_PREFIX} docker-compose --ansi always logs "$@" | less --LINE-NUMBERS +G
    fi
}

docker-compose-inspect() {
    if [[ -z "$1" ]]; then
        echo "Usage:"
        echo "    docker-compose-inspect <service>"
        return 0
    fi
    ${SUDO_PREFIX} docker inspect "$(${SUDO_PREFIX} docker-compose ps --quiet "$1")" | \
        bat --language cjson --theme ansi --style numbers
}

docker-compose-stats() {
    if [[ -z "$1" ]]; then
        echo "Usage:"
        echo "    docker-compose-stats <service>"
        return 0
    fi
    ${SUDO_PREFIX} docker stats "$(${SUDO_PREFIX} docker-compose ps --quiet "$1")" \
        --format "table {{.ID}}\t{{.Name}}\t{{.MemUsage}}\t{{.CPUPerc}}"
}

docker-compose-update() {
    ${SUDO_PREFIX} docker-compose pull "$@"
    ${SUDO_PREFIX} docker-compose up --detach "$@"
}

docker-copy-from-volume() {
    local source_volume="$1"

    if [[ -z "${source_volume}" ]]; then
        echo "Usage:"
        echo "    docker-copy-from-volume <source-volume>"
        return 0
    fi
    ${SUDO_PREFIX} docker run --rm --volume "${source_volume}":/source --volume "${PWD}":/dest alpine cp --recursive /source /dest
}

docker-copy-to-volume() {
    local source_file="$1"
    local target_volume="$2"

    if [[ "$#" -ne 2 ]]; then
        echo "Usage:"
        echo "    docker-copy-to-volume <source-file> <target-volume>"
        return 0
    fi
    ${SUDO_PREFIX} docker run --rm --volume "${PWD}":/source --volume "${target_volume}":/dest alpine cp /source/"${source_file}" /dest
}

docker-exec() {
    ${SUDO_PREFIX} docker exec --interactive --tty "$1" bash
    set_system_title
}

docker-inspect() {
    if [[ -z "$1" ]]; then
        echo "Please provide a container."
        return 1
    fi
    # shellcheck disable=SC2086
    ${SUDO_PREFIX} docker ps --all | \
        rg "$1" | \
        cut --delimiter=' ' --fields=1 | \
        xargs ${SUDO_PREFIX} docker inspect | \
        bat --language cjson --theme ansi --style numbers
}

docker-run-bash() {
    ${SUDO_PREFIX} docker run --rm --interactive --tty --entrypoint bash "$@"
    set_system_title
}

docker-run-sh() {
    ${SUDO_PREFIX} docker run --rm --interactive --tty --entrypoint sh "$@"
    set_system_title
}

docker-stats() {
    if [[ -z "$1" ]]; then
        ${SUDO_PREFIX} docker stats --format "table {{.ID}}\t{{.Name}}\t{{.MemUsage}}\t{{.CPUPerc}}"
    else
        # shellcheck disable=SC2086
        ${SUDO_PREFIX} docker ps | \
            rg "$1" | \
            cut --delimiter=' ' --fields=1 | \
            xargs ${SUDO_PREFIX} docker stats --format "table {{.ID}}\t{{.Name}}\t{{.MemUsage}}\t{{.CPUPerc}}"
    fi
}

docker-stats-sorted() {
    ${SUDO_PREFIX} docker stats --no-stream | awk 'NR<2{print $0;next}{print $0 | "sort --key=2"}'
}

__docker_container_running_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local containers
    containers=$(${SUDO_PREFIX} docker ps --format '{{.Names}}')
    COMPREPLY=($(compgen -W "${containers}" -- "${cur}"))
    return 0
}

__docker_container_all_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local containers
    containers=$(${SUDO_PREFIX} docker ps --all --format '{{.Names}}')
    COMPREPLY=($(compgen -W "${containers}" -- "${cur}"))
    return 0
}

# prerequisite: needs yq (https://github.com/mikefarah/yq)
__docker_compose_service_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local services="" docker_compose_extension_files
    if ls docker-compose* &>/dev/null; then
        # We are in a directory with docker-compose* files
        # get all additional docker-commpose files
        docker_compose_extension_files=$(find . -maxdepth 1 -regex '.*docker-compose.+.yml$' -exec echo {} +)
        # 1. merge them with docker-compose.yml (using operator reduce)
        # 2. parse service names; get only the path; get the last element of the path
        # 3. and sort
        # yq docs: https://mikefarah.gitbook.io/yq/
        # Note: ${docker_compose_extension_files} must not be quoted!
        # shellcheck disable=SC2086,SC2016
        services=$(yq eval-all '. as $item ireduce ({}; . * $item )' docker-compose.yml ${docker_compose_extension_files} |
                    yq eval '.services.* | path | .[-1]' - |
                    sort)
    fi
    COMPREPLY=($(compgen -W "${services}" -- "${cur}"))
    return 0
}

# Used only for bash - an improved zsh version is loaded via .zlogin
__docker_compose_completion() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local commands="build config cp create down events exec images kill logs pause port ps pull push restart rm run start stop top unpause up version"
    if [[ "${COMP_CWORD}" -eq 1 ]]; then
        COMPREPLY=($(compgen -W "${commands}" -- "${cur}"))
    elif [[ "${COMP_CWORD}" -gt 1 ]]; then
        case "${COMP_WORDS[1]}" in
            build | create | events | exec | images | kill | logs | pause | port | ps | pull | push | restart | rm | run | start | stop | top | unpause | up)
                __docker_compose_service_completion
                ;;
            cp)
                __docker_compose_service_completion
                # path completion
                local IFS=$'\n'
                COMPREPLY+=($(compgen -o plusdirs -f -- "${cur}"))
                ;;
            *)
                ;;
        esac
    fi
    return 0
}

# Complete only in interactive mode
if [[ $- == *i* ]]; then
    # docker completion
    complete -F __docker_container_running_completion   de   docker-exec
    complete -F __docker_container_all_completion       di   docker-inspect
    complete -F __docker_container_all_completion       ds   docker-stats

    # docker-compose completion
    complete -F __docker_compose_service_completion     dcl  docker-compose-logs
    complete -F __docker_compose_service_completion     dce  docker-compose-exec
    complete -F __docker_compose_service_completion     dcer docker-compose-exec-root
    complete -F __docker_compose_service_completion     dci  docker-compose-inspect
    complete -F __docker_compose_service_completion     dcls docker-compose-list
    complete -F __docker_compose_service_completion     dcs  docker-compose-stats
    complete -F __docker_compose_service_completion     dcu  docker-compose-update
    if [[ -n "${BASH_VERSION}" ]]; then
        # There exists an improved zsh completion, so use these only for bash
        complete -F __docker_compose_completion         dc   docker-compose
    fi
fi
