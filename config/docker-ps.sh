#!/usr/bin/env bash

alias dps=docker-ps

docker-ps() {
    local options ret watch_opts verbose_command ports filter
    local container_ids id services service service_array output sortkey sortorder
    local name created started docker_status health restarts
    local created_relative started_relative
    local COL_ALT_RESET

    local use_compose=false extend_health=false show_help=false include_images=false include_images_only=false
    local include_ports=false sort_by_name=false verbose=false verbose_only=false watch=false

    # workaround, because column seems to break with COL_RESET="$(tput sgr0)"
    COL_ALT_RESET="\033[0m"

    options=$(getopt --options acehiIpsvVw \
               --longoptions all,compose,extended-health,help,images,images-only,ports,sort-by-name,verbose,verbose-only,watch -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && return 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -a | --all)
                extend_health=true
                include_images=true
                include_ports=true
                ;;
            -c | --compose) use_compose=true;;
            -e | --extended-health) extend_health=true;;
            -h | --help) show_help=true;;
            -i | --images) include_images=true;;
            -I | --images-only) include_images_only=true;;
            -p | --ports) include_ports=true;;
            -s | --sort-by-name) sort_by_name=true;;
            -v | --verbose) verbose=true;;
            -V | --verbose-only) verbose_only=true;;
            -w | --watch) watch=true;;
            --) shift; break;;
        esac
        shift
    done

    filter="$1"

    if [[ "${include_images}" == true && "${include_images_only}" == true ]]; then
        echo "${COL_YELLOW_FG}Cannot include and exclude container name. Please use either --images or --images-only, but not both.${COL_RESET}"
        return 1
    fi
    if [[ "${verbose}" == true && "${verbose_only}" == true ]]; then
        echo "${COL_YELLOW_FG}Cannot combine --verbose and --verbose-only. Please use only one, but not both.${COL_RESET}"
        return 1
    fi

    if [[ "${show_help}" == true ]]; then
        colorize-help <<-EOF
			Convenience for docker ps with options for column selection, filtering, sorting, continuously watching and
			printing the equivalent command.

			Usage:
			    docker-ps [options] [filter]

			Options:
			    -a, --all               Include all. Shorthand for --extended-health --images --ports.
			    -c, --compose           Limit to the compose project in current directory.
			                            In this case, also stopped compose services will be viewed.
			                            Otherwise (i.e., without --compose), only running containers will viewed.
			                            Note: Ensure environment variables are sourced before using this, e.g.:
			                            source environment-variable-file.sh
			    -e, --extended-health   Additionally to health check, include status and restarts
			    -h, --help              Print this help and exit
			    -i, --images            Include docker image name
			    -I, --images-only       Include docker image name (replacing container name)
			    -p, --ports             Include ports
			    -s, --sort-by-name      Sort output by container name
			    -v, --verbose           Print equivalent docker ps command.
			                            Note: --extended-health is not available via docker ps.
			    -V, --verbose-only      Same as --verbose, but exit after printing the command.
			    -w, --watch             Continuously watch docker ps

			Arguments:
			    filter                  Search string to filter the docker ps lines for.
			                            Note: Filtering for ports is only available explicitely - and not via --watch:
			                            docker-ps --ports | rg <filter>
		EOF
        return 0
    fi

    if [[ "${verbose}" == true || "${verbose_only}" == true ]]; then
        template="{{.ID}}"
        [[ "${include_images_only}" == false ]] && template="${template}\t{{.Names}}"
        [[ "${include_images}" == true || "${include_images_only}" == true ]] && template="${template}\t{{.Image}}"
        template="${template}\t{{.RunningFor}}\t{{.Status}}"
        [[ "${include_ports}" == true ]] && template="${template}\t{{.Ports}}"
        verbose_command="${SUDO_PREFIX}docker ps --format 'table ${template}'"
        [[ "${sort_by_name}" == true ]] && verbose_command="${verbose_command} | (sed -u 1q; sort -k2)"
        [[ -n "${filter}" ]] && verbose_command="${verbose_command} | rg '${filter}'"
        echo "${verbose_command}" | sed 's/\t/\\t/g' | bat --plain --language bash
    fi
    [[ "${verbose_only}" == true ]] && return 0

    if [[ "${watch}" == true ]]; then
        watch_opts="${filter}"
        [[ "${use_compose}" == true ]] && watch_opts="${watch_opts} --compose"
        [[ "${extend_health}" == true ]] && watch_opts="${watch_opts} --extended-health"
        [[ "${include_images}" == true ]] && watch_opts="${watch_opts} --images"
        [[ "${include_images_only}" == true ]] && watch_opts="${watch_opts} --images-only"
        [[ "${include_ports}" == true ]] && watch_opts="${watch_opts} --ports"
        [[ "${sort_by_name}" == true ]] && watch_opts="${watch_opts} --sort-by-name"
        watch --color --no-title "${DOTFILES_DIR}/config/docker-ps.sh" --docker-ps "${watch_opts}"
    fi

    if [[ "${use_compose}" == true ]]; then
        if [[ -f "docker-compose.yml" ]]; then
            container_ids=$(${SUDO_PREFIX} docker-compose ps --all --quiet)
        else
            echo "${COL_YELLOW_FG}No docker-compose.yml found. Showing ps of all running containers instead.${COL_RESET}"
            container_ids=$(${SUDO_PREFIX} docker ps --quiet)
        fi
    else
        container_ids=$(${SUDO_PREFIX} docker ps --quiet)
    fi
    services=""
    if [[ -n "${container_ids}" ]]; then
        # If at least one service is created, get service details
        if [[ "${sort_by_name}" == true ]]; then
            if [[ "${include_images_only}" == true ]]; then
                sortkey=3
            else
                sortkey=2
            fi
            sortorder=''
        else
            sortkey=5
            sortorder='--reverse'
        fi
        # shellcheck disable=SC2086
        services=$(${SUDO_PREFIX} docker inspect ${container_ids} \
            --format '{{.Id}},{{.Name}},{{.Config.Image}},{{.Created}},{{.State.StartedAt}},{{.State.Status}},{{if index .State "Health"}}{{.State.Health.Status}}{{end}},{{.RestartCount}}' \
            | sort --key ${sortkey} --field-separator , ${sortorder} \
            | rg "${filter}")
    fi

    # Table header
    output="${COL_BLUE_FG}ID"
    [[ "${include_images_only}" == false ]] && output="${output},Name"
    [[ "${include_images}" == true || "${include_images_only}" == true ]] && output="${output},Image"
    output="${output},Created,Started"
    [[ "${extend_health}" == true ]] && output="${output},Status"
    output="${output},Health"
    [[ "${extend_health}" == true ]] && output="${output},Restarts"
    [[ "${include_ports}" == true ]] && output="${output},Ports"
    output="${output}${COL_ALT_RESET}\n"

    [[ -n "${ZSH_VERSION}" ]] && setopt local_options KSH_ARRAYS
    for service in ${services}; do
        [[ -n "${BASH_VERSION}" ]] && IFS=',' read -r -a service_array <<< "${service}"
        [[ -n "${ZSH_VERSION}" ]] && IFS=',' read -r -A service_array <<< "${service}"
        # Shorten the ID to 12 characters.
        id="${service_array[0]:0:12}"
        # Remove leading slash from container name.
        name="${service_array[1]:1}"
        image="${service_array[2]}"
        created="${service_array[3]}"
        started="${service_array[4]}"
        docker_status="${service_array[5]}"
        health="${service_array[6]}"
        restarts="${service_array[7]}"

        __format_relative_time "${created}"
        created_relative="${__format_relative_time_result}"
        __format_relative_time "${started}"
        started_relative="${__format_relative_time_result}"

        if [[ "${health}" = "healthy" ]]; then
            health="${COL_GREEN_FG}${health}${COL_ALT_RESET}"
        elif [[ "${health}" = "starting" ]]; then
            health="${COL_YELLOW_FG}${health}${COL_ALT_RESET}"
        elif [[ "${health}" = "unhealthy" ]]; then
            health="${COL_RED_FG}${health}${COL_ALT_RESET}"
        fi

        output="${output}${id}"
        [[ "${include_images_only}" == false ]] && output="${output},${name}"
        [[ "${include_images}" == true || "${include_images_only}" == true ]] && output="${output},${image}"
        output="${output},${created_relative},${started_relative}"
        [[ "${extend_health}" == true ]] && output="${output},${docker_status}"
        output="${output},${health}"
        [[ "${extend_health}" == true ]] && output="${output},${restarts}"
        if [[ "${include_ports}" == true ]]; then
            ports=$(${SUDO_PREFIX} docker ps --filter "id=${id}" --format "{{.Ports}}" | sed 's/,/ /g')
            output="${output},${ports}"
        fi
        output="${output}\n"
    done

    printf "%b" "${output}" | column --output-separator "   " --separator , --table
}

# For speed purposes, we don't want to call this function in a subshell.
# Therefore, we return the result in a global variable.
__format_relative_time() {
    local seconds_per_minute seconds_per_hour seconds_per_day seconds_per_month seconds_per_year input now diff
    seconds_per_minute=$((60))
    seconds_per_hour=$((60*60))
    seconds_per_day=$((60*60*24))
    seconds_per_month=$((60*60*24*30))
    seconds_per_year=$((60*60*24*365))

    # convert input to unix timestamp
    input="$(date --date="$1" +%s)"
    now="$(date +'%s')"

    diff=$(( now - input ))

    if [[ ${diff} -lt  ${seconds_per_minute} ]]; then
        __format_relative_time_result="$((diff)) seconds ago"
        return
    elif [[ ${diff} -lt  ${seconds_per_hour} ]]; then
        __format_relative_time_result="$((diff / seconds_per_minute)) minutes ago"
        return
    elif [[ ${diff} -lt  ${seconds_per_day} ]]; then
        __format_relative_time_result="$((diff / seconds_per_hour)) hours ago"
        return
    elif [[ ${diff} -lt  ${seconds_per_month} ]]; then
        __format_relative_time_result="$((diff / seconds_per_day)) days ago"
        return
    elif [[ ${diff} -lt  ${seconds_per_year} ]]; then
        __format_relative_time_result="$((diff / seconds_per_month)) months ago"
        return
    else
        __format_relative_time_result="$((diff / seconds_per_year)) years ago"
        return
    fi
}

if [[ "$1" == "--docker-ps" ]]; then
    shift
    docker-ps "$@"
    exit 0
fi
