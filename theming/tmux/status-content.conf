# Inspired by: https://github.com/arcticicestudio/nord-tmux

#####################
#### Options

set-option -g status on
set-option -g status-interval 1
set-option -g display-panes-time 2000

# Show matrix screensaver after 5 minutes idle
set-option -g lock-after-time 300
set-option -g lock-command "command -v cmatrix >/dev/null && cmatrix -b -a -u 3 -C ${COLOR_CMATRIX}"
bind-key L lock-session

#####################
#### Status

# Layout
set-option -g status-justify left

# Colors (general)
set-option -g status-style bg="${COL_STATUS_BG}",fg="${COL_STATUS_FG}"

# Bars
set-option -g status-right-length 120
set-option -g status-left "#[fg=${COL_STATUS_SESSION_FG},bg=${COL_STATUS_SESSION_BG}] #S #[fg=${COL_STATUS_SESSION_BG},bg=${COL_STATUS_BG},nobold,noitalics,nounderscore]"
set-option -g status-right '#{prefix_highlight}#(eval "${DOTFILES_DIR}/theming/tmux/tmux-airline.sh" $(tmux display-message -p "#{client_width}"))'

# Windows
set-option -g window-status-format "#[fg=${COL_STATUS_BG},bg=${COL_WINDOW_INACTIVE_BG},nobold,noitalics,nounderscore] #[fg=${COL_WINDOW_INACTIVE_FG},bg=${COL_WINDOW_INACTIVE_BG}]#I #[fg=${COL_WINDOW_INACTIVE_FG},bg=${COL_WINDOW_INACTIVE_BG},nobold,noitalics,nounderscore] #[fg=${COL_WINDOW_INACTIVE_FG},bg=${COL_WINDOW_INACTIVE_BG}]#W #('${DOTFILES_DIR}/theming/tmux/window-flags' '#{window_flags}') #[fg=${COL_WINDOW_INACTIVE_BG},bg=${COL_STATUS_BG},nobold,noitalics,nounderscore]"
set-option -g window-status-current-format "#[fg=${COL_STATUS_BG},bg=${COL_WINDOW_ACTIVE_BG},nobold,noitalics,nounderscore] #[fg=${COL_WINDOW_ACTIVE_FG},bg=${COL_WINDOW_ACTIVE_BG}]#I #[fg=${COL_WINDOW_ACTIVE_FG},bg=${COL_WINDOW_ACTIVE_BG},nobold,noitalics,nounderscore] #[fg=${COL_WINDOW_ACTIVE_FG},bg=${COL_WINDOW_ACTIVE_BG}]#W #('${DOTFILES_DIR}/theming/tmux/window-flags' '#{window_flags}') #[fg=${COL_WINDOW_ACTIVE_BG},bg=${COL_STATUS_BG},nobold,noitalics,nounderscore]"
set-option -g window-status-separator ""

#####################
#### Plugins

# tmux-prefix-highlight
set-option -g @prefix_highlight_fg "${COL_STATUS_PREFIX_FG}"
set-option -g @prefix_highlight_bg "${COL_STATUS_PREFIX_BG}"
set-option -g @prefix_highlight_output_prefix "#[fg=${COL_STATUS_PREFIX_BG}]#[bg=${COL_STATUS_BG}]#[nobold]#[noitalics]#[nounderscore]#[bg=${COL_STATUS_PREFIX_BG}]#[fg=${COL_STATUS_PREFIX_FG}]"
set-option -g @prefix_highlight_output_suffix "#[fg=${COL_STATUS_BG}]#[bg=${COL_STATUS_PREFIX_BG}]"
set-option -g @prefix_highlight_copy_mode_attr "fg=${COL_STATUS_PREFIX_BG},bg=${COL_STATUS_PREFIX_FG},bold"

#####################
#### Messages

set-option -g message-style bg="${COL_MESSAGE_BG}",fg="${COL_MESSAGE_FG}"
set-option -g message-command-style bg="${COL_MESSAGE_BG}",fg="${COL_MESSAGE_FG}"

#####################
#### Panes

set-option -g pane-active-border-style "#{?pane_synchronized,fg=$COL_PANE_SYNC_FG,fg=$COL_PANE_ACTIVE_FG}"
set-option -ag pane-active-border-style "#{?pane_synchronized,bg=$COL_PANE_SYNC_BG,bg=$COL_PANE_ACTIVE_BG}"
set-option -g pane-border-style bg="${COL_PANE_INACTIVE_BG}",fg="${COL_PANE_INACTIVE_FG}"

set-option -g display-panes-colour "${COL_DISPLAY_PANES_INACTIVE}"
set-option -g display-panes-active-colour "${COL_DISPLAY_PANES_ACTIVE}"

#####################
#### Clock

set-option -g clock-mode-colour "${COL_CLOCK}"
