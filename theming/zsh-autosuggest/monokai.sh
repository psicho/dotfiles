#!/usr/bin/env bash
# https://github.com/zsh-users/zsh-autosuggestions/blob/master/README.md#suggestion-highlight-style
# Although the doc suggests 24-Bit colorization can be used, the colors will always snap to the closed 8-Bit color
# We get the same result when using fromhex "#hexcolor"

export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=240'
