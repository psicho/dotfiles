#!/usr/bin/env python3
"""
Convert all .minttyrc files in the current directory
to Microsoft Terminal JSON Fragments and a tab delimited file

Example input:
* https://github.com/skylarmb/gruvbox-mintty/blob/master/gruvbox
* https://github.com/arcticicestudio/nord-mintty/blob/develop/src/nord.minttyrc
* https://github.com/iamthad/base16-mintty
* https://github.com/psyanite/palenight-mintty/blob/master/palenight
"""

import os
import textwrap

# path to cloned https://github.com/iamthad/base16-mintty
BASE_PATH = "."
OUTPUT_DIRECTORY = "."

# the order in this map determines the order in tab_delim_file
# tab_delim_file is the input for HexColor.xlsm
color_name_map = {
    "BackgroundColour": "background",
    "ForegroundColour": "foreground",
    "IMECursorColour": "selectionBackground",
    "CursorColour": "cursorColor",
    "Black": "black",
    "BoldBlack": "brightBlack",
    "Red": "red",
    "BoldRed": "brightRed",
    "Green": "green",
    "BoldGreen": "brightGreen",
    "Yellow": "yellow",
    "BoldYellow": "brightYellow",
    "Blue": "blue",
    "BoldBlue": "brightBlue",
    "Magenta": "purple",
    "BoldMagenta": "brightPurple",
    "Cyan": "cyan",
    "BoldCyan": "brightCyan",
    "White": "white",
    "BoldWhite": "brightWhite",
}


def main():
    """
    Convert all .minttyrc files in current directory
    """
    directory = os.fsencode(BASE_PATH)
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".minttyrc"):
            convert_mintty(filename)


def convert_mintty(input_path) -> None:
    """
    Convert given .minttyrc file to Microsoft Terminal JSON Fragments and a tab delimited file
    """
    # default all entries to white
    ordered_colors = ["FFFFFF"] * 20
    color_name_list = list(color_name_map.keys())

    input_file = open(f"{BASE_PATH}/{input_path}", "r", encoding="utf-8")
    for line in input_file:
        name, color = extract(line)
        ordered_colors[color_name_list.index(name)] = color
    input_file.close()

    basename = input_path.replace(".minttyrc", "")
    write_json_fragment(basename, ordered_colors)
    write_tab_delimited_file(basename, ordered_colors)


def write_json_fragment(basename, ordered_colors):
    """
    Write {ordered_colors} as Windows Terminal JSON scheme Fragment
    See https://learn.microsoft.com/en-us/windows/terminal/json-fragment-extensions
    """
    fragment_path = f"{OUTPUT_DIRECTORY}/{basename}.json"
    if os.path.exists(fragment_path):
        os.remove(fragment_path)
    fragment_file = open(fragment_path, "a", encoding="utf-8")
    print(f"Writing {fragment_path}")
    space = " " * 12
    write_json_header(fragment_file)
    fragment_file.write(f'{space}"name": "{basename}"')

    color_names = list(color_name_map.values())
    for i, color in enumerate(ordered_colors):
        fragment_file.write(f',\n{space}"{color_names[i]}": "#{color}"')

    write_json_footer(fragment_file)
    fragment_file.close()


def write_json_header(file):
    """
    Print header for Windows Terminal Fragements
    """
    file.write(
        textwrap.dedent(
            """
            {
                "profiles": [],
                "schemes": [
                    {
            """
        )
    )


def write_json_footer(file):
    """
    Print footer for Windows Terminal Fragements
    """
    file.write(
        textwrap.dedent(
            """
                    }
                ]
            }
            """
        )
    )


def write_tab_delimited_file(basename, ordered_colors):
    """
    Write {ordered_colors} as a tab delimited file to be copied into HexColors.xlsm.
    Sheet Hex-to-Background then colorizes the cells.
    """
    tab_delim_path = f"{OUTPUT_DIRECTORY}/{basename}.txt"
    print(f"Writing {tab_delim_path}")
    if os.path.exists(tab_delim_path):
        os.remove(tab_delim_path)
    tab_delim_file = open(tab_delim_path, "a", encoding="utf-8")

    for i, color in enumerate(ordered_colors):
        tab_delim_file.write(color)
        if i % 2 == 0:
            tab_delim_file.write("\t")
        else:
            tab_delim_file.write("\n")

    tab_delim_file.close()


def extract(line):
    """
    Extract the colors from mintty format
    Input:  "Red  = 204,  36,  29"
    Output: "CC241D"
    """
    line_parts = line.replace(" ", "").split("=")
    key = line_parts[0]
    colors = line_parts[1].split(",")
    red = dec2hex(colors[0])
    green = dec2hex(colors[1])
    blue = dec2hex(colors[2])
    return key, f"{red}{green}{blue}"


def dec2hex(string):
    """
    Convert two-digit decimal to hex
    """
    number = int(string)
    first_digit = dec2hex_single_digit(number // 16)
    second_digit = dec2hex_single_digit(number % 16)
    return f"{first_digit}{second_digit}"


def dec2hex_single_digit(digit):
    """
    Convert one-digit decimal to hex
    """
    return digit if digit <= 9 else chr(digit + 55)


main()
