#!/usr/bin/env bash

__theming_demo_usage() {
    # shellcheck disable=SC1090
    source "${DOTFILES_DIR}/lib/help.sh"
    colorize-help <<-USAGE
		Show a theming demo

		Usage:
		    demo [option...]

		Options:
		    Options 'prepare' and 'sleep' can be combined with 'demo' or 'test'. No other option combination is valid.
		    -c, --cleanup       Reset the changes made by git and package prompt preparation
		    -d, --demo          Show the output required for demos in screenshots
		    -h, --help          Print this help
		    -p, --prepare       Prepare the repo for git and package prompt
		    -s, --sleep=time    Sleep [time] seconds between two tmux send-keys commands. Necessary on fast machines
		                        such as macOS with Apple Silicon. [time] can be a fraction of seconds, e.g. 0.8.
		    -t, --test          Create an example for every use case to be reviewed by a human including demo

		Test notes:
		    0. Ensure background is set to light or dark (system currently not supported)
		    1. Call this script via --test
		    2. Walk through every tmux window
		        * Watch out for the default section of the colors command. These will be used on some remote machines.
		    3. For tmux, additionally call
		        * Prefix + :
		        * Prefix + t
		        * Prefix + q
		    4. For fzf/fzf-tab, additionally call
		        * git <tab><filter><ctrl+space>
		        * cfg <tab><filter><ctrl+space>
		        * fzf<enter><filter><ctrl+space>
		        * gi<enter>

		Demo notes:
		    * Ensure background is set to light or dark (system currently not supported)
		    * Call this script via --prepare --demo
		    * Sizing of terminal: 1384 x 837 px, e.g. using Powertoys screen ruler
		    * Add a png that just shows the base16 color palette
		        * copy base16 palette from HexColor.xlsm, sheet Hex-to-Background to sheet Doku, transposed
		        * screenshot at 100% and crop via Photoshop
		    * Afterwards, reset calling this script via --cleanup

		Quit tmux session:
		    Ctrl+x, Ctrl+f, Enter (session), k, Enter (kill), Enter (current)
	USAGE
    exit 0
}

__perform_cleanup() {
    cd "${DOTFILES_DIR}" || return 0
    git hard-reset
    rm pom.xml
    rm --recursive "${DOTFILES_DIR}/theming/demo/ls-demo/"
    sudo rm --recursive "${DOTFILES_DIR}/theming/demo/special-files"
    if [[ -f ~/.cache/old_nickname ]]; then
        "${DOTFILES_DIR}/lib/configurer.sh" --configure nickname "$(cat ~/.cache/old_nickname)"
    fi
}

__validate_background() {
    # `demo -d` fails on WSL if background is set to `system`
    # -> the send-keys commands are not executed properly
    # introduced by commit 9b8a678 (`set-theme.sh/__initialize_theming:L50` > `set_theme ...`)
    # the powershell call in `get_system_background` is the reason
    # workaround: enforce tester to use light or dark background explicitely

    local background
    background=$("${DOTFILES_DIR}/lib/configurer.sh" --load theming.background)
    if [[ -n "${WSL_DISTRO_NAME}" && "${background}" == "system" ]]; then
        echo "${COL_RED_FG}demo usage for background == system is not supported. Please set background to light or dark. Exiting demo.${COL_RESET}"
        exit 1
    fi
}

__option_number=0
options=$(getopt --options cdhps:t --longoptions cleanup,demo,help,prepare,sleep:,test -- "$@")
ret=$?
[[ $ret -ne 0 ]] && exit 1
eval set -- "${options}"
__cleanup=false; __demo=false; __prepare=false; __test=false; __sleep=0
while true; do
    case "$1" in
        -c | --cleanup)
            __cleanup=true
            ((__option_number++))
            ;;
        -d | --demo)
            __demo=true
            ((__option_number++))
            ;;
        -h | --help)
            __theming_demo_usage
            ;;
        -p | --prepare)
            __prepare=true
            ((__option_number++))
            ;;
        -s | --sleep)
            shift
            __sleep=$1
            ((__option_number++))
            ;;
        -t | --test)
            __test=true
            ((__option_number++))
            ;;
        --)
            shift
            break
            ;;
    esac
    shift
done

# validate option combinations
# allowed: -pd, -pt, -psd, -pst (see usage)
if [[ "${__option_number}" -eq 0 || "${__option_number}" -gt 3 ]]; then
    __theming_demo_usage
elif [[ "${__option_number}" -eq 2 ]]; then
    if [[ "${__prepare}" == false && "${__sleep}" == "0" ]]; then           # prepare or sleep must be set
        echo "${COL_RED_FG}Invalid combination of options.${COL_RESET}"
        __theming_demo_usage
    elif [[ "${__demo}" == false && "${__test}" == false ]]; then           # demo or test must be set
        echo "${COL_RED_FG}Invalid combination of options.${COL_RESET}"
        __theming_demo_usage
    fi
elif [[ "${__option_number}" -eq 3 ]]; then
    if [[ "${__prepare}" == false || "${__sleep}" == "0" ]]; then           # prepare and sleep must be set
        echo "${COL_RED_FG}Invalid combination of options.${COL_RESET}"
        __theming_demo_usage
    elif [[ "${__demo}" == false && "${__test}" == false ]]; then           # demo or test must be set
        echo "${COL_RED_FG}Invalid combination of options.${COL_RESET}"
        __theming_demo_usage
    fi
fi

# validate sleep is set on macOS when using --demo or --test
if [[ "${OSTYPE}" == darwin* && ("${__demo}" == true || "${__test}" == true) && "${__sleep}" == "0" ]]; then
    echo "${COL_RED_FG}Please provide sleep option on macOS for --demo or --test, see help.${COL_RESET}"
    exit 1
fi

if [[ "${__cleanup}" == true ]]; then
    __perform_cleanup
elif [[ "${__prepare}" == true ]]; then
    if tmux list-sessions &>/dev/null; then
        echo "Please close all tmux sessions first."
        exit 1
    fi

    cd "${DOTFILES_DIR}" || return 0

    # prepare package prompt
    cat <<-POM > pom.xml
		<project xmlns="http://maven.apache.org/POM/4.0.0"
		        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
		    <modelVersion>4.0.0</modelVersion>

		    <groupId>gd.wa</groupId>
		    <artifactId>minimal-pom</artifactId>
		    <version>3.2.7</version>
		</project>
	POM

    # prepare git status
    git mv docs/ANSI.md docs/ansi.md
    echo "Demo" >> README.md
    sed --in-place 's/# ToDo/# Not ToDo/' todo.md

    # prepare files for ls
    mkdir --parents "${DOTFILES_DIR}/theming/demo/ls-demo/directory"
    cd "${DOTFILES_DIR}/theming/demo/ls-demo" || return 1
    touch .dotfile compressed.tgz default executable.sh image.gif logfile.log media.mp3 README.md
    chmod +x executable.sh
    ln --symbolic default link
    truncate --size=1k 1k.file
    truncate --size=1M 1M.file

    # prepare special files
    mkdir "${DOTFILES_DIR}/theming/demo/special-files"
    cd "${DOTFILES_DIR}/theming/demo/special-files" || return 1
    mkfifo somefifo
    mkdir other_writable sticky_other_writable sticky
    chmod +t sticky_other_writable sticky
    chmod o+w sticky_other_writable other_writable
    touch broken; ln --symbolic broken orphan; rm broken
    sudo mknod device_character c 42 0
    sudo mknod device_block b 240 0
    touch setuid setgid; chmod u+s setuid; chmod g+s setgid
    if command -v setcap >/dev/null; then
        touch capability; sudo setcap cap_net_raw+ep capability
    fi
    if [[ "${OSTYPE}" == darwin* ]]; then
        nc -lkU socket.sock &
    else
        netcat -lkU socket.sock &
    fi

    # set nickname
    "${DOTFILES_DIR}/lib/configurer.sh" --load theming.nickname > ~/.cache/old_nickname
    "${DOTFILES_DIR}/lib/configurer.sh" --configure nickname WSL
fi

if [[ "${__test}" == true ]]; then
    __validate_background
    tmux new-session -d -s ses -n test -e USER=ubuntu "zsh --login"
    tmux split-window -h -e USER=ubuntu "zsh --login"
    tmux send-keys -t test.1 "colors" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.1 "zd" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.1 "count" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.1 "gs" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.1 'AUTOPAIR_PAIRS=("<" ">")' ENTER; sleep "${__sleep}"
    tmux send-keys -t test.1 "echo -e 'On bright-black background: \e[100m\e[30mBlack \e[31mRed \e[32mGreen \e[33mYellow \e[34mBlue \e[35mMagenta \e[36mCyan \e[37mWhite\e[0m'" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.1 "echo -e '\e[90mbright-black foreground on black background\e[0m'" ENTER
    tmux send-keys -t test.2 "tldr cut" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.2 "bat --language help --theme ansi --plain '${DOTFILES_DIR}/theming/demo/manfile-demo'" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.2 "if test -d '${DOTFILES_DIR}/theming/demo/special-files'; then fd --base-directory '${DOTFILES_DIR}/theming/demo/special-files'; fi" ENTER; sleep "${__sleep}"
    tmux send-keys -t test.2 "if test -d '${DOTFILES_DIR}/theming/demo/special-files'; then ll '${DOTFILES_DIR}/theming/demo/special-files'; fi" ENTER

    tmux new-window -n auto -e USER=ubuntu "zsh --login"
    tmux split-window -t 2 -v -e USER=ubuntu "zsh --login"
    tmux send-keys -t auto.1 "echo 'This is Sparta!'" ENTER; sleep "${__sleep}"
    tmux send-keys -t auto.1 "echo"
    tmux send-keys -t auto.2 'AUTOPAIR_PAIRS=("<" ">")' ENTER; sleep "${__sleep}"
    # shellcheck disable=SC2016
    tmux send-keys -t auto.2 'function do_it() { if [[ -n "$exists" ]]; then echo "This is Sparta"; fi }; symbol_does_not_exist'

    tmux new-window -n cmatrix -e USER=ubuntu "zsh --login"
    # shellcheck disable=SC2046
    eval $(tmux show-environment -gs COL_CMATRIX)
    tmux send-keys -t cmatrix.1 "cmatrix -C ${COL_CMATRIX}" ENTER

    tmux new-window -n lazygit -e USER=ubuntu "zsh --login"
    tmux send-keys -t lazygit.1 "zd" ENTER; sleep "${__sleep}"
    tmux send-keys -t lazygit.1 "lg" ENTER

    tmux new-window -n diff -e USER=ubuntu "zsh --login"
    tmux send-keys -t diff.1 "zd" ENTER; sleep "${__sleep}"
    tmux send-keys -t diff.1 "gd" ENTER
elif [[ "${__demo}" == true ]]; then
    __validate_background
    # no testing scenario, additional tmux windows are needed for demo purpose only
    tmux new-session -d -s ses -n test -e USER=ubuntu
    tmux new-window -n doc -e USER=ubuntu
fi

if [[ "${__test}" == true || "${__demo}" == true ]]; then
    tmux new-window -n demo -e USER=ubuntu "zsh --login"
    tmux split-window -v -l 15 -e USER=ubuntu "zsh --login"
    tmux split-window -t 1 -h -l 50 -e USER=ubuntu "zsh --login"
    tmux send-keys -t demo.1 "vim '${DOTFILES_DIR}/utility/download.sh' '${DOTFILES_DIR}/README.md'" ENTER
    tmux send-keys -t demo.2 "cd '${DOTFILES_DIR}/utility' && fzf --query=sh --height=100% --preview-window right:77%" ENTER
    # sleep needed so that we can focus on file "download.sh" in the next step (otherwise Up is sent before fzf has loaded)
    #sleep 2
    #tmux send-keys -t demo.2 Up
    tmux send-keys -t demo.3 "zd" ENTER; sleep "${__sleep}"
    tmux send-keys -t demo.3 "ll '${DOTFILES_DIR}/theming/demo/ls-demo'" ENTER; sleep "${__sleep}"
    tmux send-keys -t demo.3 "git compactlog --max-count=4" ENTER
    tmux attach-session -d
fi
