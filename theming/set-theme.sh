#!/usr/bin/env bash

# shellcheck disable=SC1090,SC1091
source "${DOTFILES_DIR}/config/gnu-binaries.sh"

########################################################
# Public entrypoints -----------------------------------
########################################################

# This file has two entrypoints:
# 1. changing the theme: set_theme (called by configurer.sh)
# 2. loading dotfiles: --init (called by load-common.sh)

# entrypoint 1
# change static configuration
set_theme() {
    local theme="$1"
    local background="$2"
    local username
    username=$(__get_system_username)

    if [[ "${background}" == "system" && (-n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin*) ]]; then
        background="$(get_system_background)"
    fi
    echo "${background}" > "${HOME}/.cache/background.cache"

    [[ "${theme}" == "solarized" ]] && __load_solarized_colors "${background}"
    __set_vs_code_theme "${theme}" "${background}" "${username}"
    __set_terminal_theme "${theme}" "${background}" "${username}"
    __set_starship_theme "${theme}" "${background}"
    __set_lazygit_theme "${theme}" "${background}"
    __set_git_theme "${theme}" "${background}"
}

# entrypoint 2
# set dynamic environment variables
__initialize_theming() {
    local theme background last_background
    theme=$("${DOTFILES_DIR}/lib/configurer.sh" --load 'theming.theme')
    background=$("${DOTFILES_DIR}/lib/configurer.sh" --load 'theming.background')

    if [[ "${background}" == "system" && (-n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin*) ]]; then
        background="$(get_system_background)"
        if [[ ! -f "${HOME}/.cache/background.cache" ]]; then
            last_background="unset"
        else
            last_background=$(cat "${HOME}/.cache/background.cache")
        fi
        if [[ "${background}" != "${last_background}" ]]; then
            set_theme "${theme}" "system"
        elif [[ "${OSTYPE}" == darwin* ]]; then
            __set_iterm_theme "${theme}" "${background}"
        fi
    elif [[ "${OSTYPE}" == darwin* ]]; then
        __set_iterm_theme "${theme}" "${background}"
    elif "${DOTFILES_DIR}/lib/ssh-manager.sh" --ssh-is-remote-session >/dev/null; then
        __synchronize_client_theme_with_host "${theme}" "${background}"
    elif [[ -f /.dockerenv ]]; then
        # inside container
        __synchronize_client_theme_with_host "${theme}" "${background}"
    fi
    __get_theming "${theme}" "${background}"
}

__synchronize_client_theme_with_host() {
    local theme="$1"
    local background="$2"
    if [[ -n "${CLIENT_THEME}" && "${CLIENT_THEME}" != "${theme}" ||
          -n "${CLIENT_BG}" && "${CLIENT_BG}" != "${background}" ]]; then
        theme=${CLIENT_THEME:-theme}
        background=${CLIENT_BG:-background}
        # shellcheck disable=SC1090
        source "${DOTFILES_DIR}/lib/configurer.sh"
        cfg theme "${theme}" "${background}"
    fi
}

__get_theming() {
    local theme="$1"
    local background="$2"

    [[ "${theme}" == "solarized" ]] && __load_solarized_colors "${background}"
    __get_ls_colors "${theme}" "${background}"
    __get_bat_theme "${theme}" "${background}"
    __get_eza_colors "${theme}" "${background}"
    __get_tmux_theme_file "${theme}" "${background}"
    __get_fzf_colors "${theme}" "${background}"
    __get_zsh_autosuggest_highlight_style "${theme}" "${background}"
    __get_git_theme "${theme}" "${background}"
    __get_tldr_colors "${theme}" "${background}"
    __get_cmatrix_color "${theme}" "${background}"
}

is_dark_theme() {
    # shellcheck disable=SC2153
    local theme="${THEME}"
    # shellcheck disable=SC2153
    local background="${BACKGROUND}"
    [[ -z "${theme}" ]] && theme=$("${DOTFILES_DIR}/lib/configurer.sh" --load 'theming.theme')
    case "${theme}" in
        nord | palenight)
            return 0
            ;;
        *)
            [[ -z "${background}" ]] && background=$("${DOTFILES_DIR}/lib/configurer.sh" --load 'theming.background')
            if [[ "${background}" == "system" && (-n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin*) ]]; then
                background="$(get_system_background)"
            fi
            [[ "${background}" == "dark" ]] && return 0
            ;;
    esac
    return 1
}

get_system_background() {
    if [[ -n "${WSL_DISTRO_NAME}" ]]; then
        local themes_path='HKCU:\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize'
        local themes_key="AppsUseLightTheme"
        local is_light
        is_light=$(powershell.exe -noprofile -nologo -noninteractive \
                "\$a = Get-ItemProperty -Path ${themes_path}; \$a.${themes_key}" \
                | tr --delete '\r')
        if [[ "${is_light}" == "1" ]]; then
            echo "light"
        else
            echo "dark"
        fi
    elif [[ "${OSTYPE}" == darwin* ]]; then
        local is_dark
        is_dark=$(defaults read -g AppleInterfaceStyle 2>/dev/null)
        if [[ "${is_dark}" == "Dark" ]]; then
            echo "dark"
        else
            echo "light"
        fi
    fi
}

# Quick theme changer

fg() {
    local jobs
    jobs=$(jobs -s)
    if [[ -n "${jobs}" ]]; then
        builtin fg
    else
        local theme get_themes_command system_background header
        # delete comments
        # delete empty lines
        # right trim
        get_themes_command="cut --delimiter='#' --fields=1 ${DOTFILES_DIR}/theming/themes \
                            | grep . \
                            | sed 's/ *$//g'"
        if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
            system_background=$(get_system_background)
        else
            system_background=""
            # remove 'System' themes
            get_themes_command="${get_themes_command} | grep --invert-match System"
        fi

        # shellcheck disable=SC2001
        header="$(sed 's/./\U&/'<<<"${THEME}") $(sed 's/./\U&/'<<<"${BACKGROUND}")"
        theme=$(eval "${get_themes_command}" \
                | fzf-tmux -p90% \
                    --header "${header}" \
                    --preview "${DOTFILES_DIR}/theming/preview.py {} ${system_background}")
        if [[ -n "${theme}" ]]; then
            echo "Changing theme to ${theme}"
            theme=$(echo "${theme}" | cut --delimiter='(' --fields=1 | xargs | tr '[:upper:]' '[:lower:]')
            # shellcheck disable=SC2046,SC2116,SC2086
            cfg theme $(echo ${theme})
        fi
    fi
}

bg() {
    if [[ -n $1 ]]; then
        if [[ ! $1 == "light" && ! $1 == "dark" && ! $1 == "system" ]]; then
            echo "${COL_RED_FG}Either use no argument or pass one of light, dark, system."
            return 1
        fi
        cfg background "$1"
    elif [[ "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then

        local background
        background=$(printf "dark\nlight\nsystem" \
                     | fzf-tmux -p90% \
                         --header "current: ${BACKGROUND}" \
                         --preview-window :hidden)
        if [[ -n "${background}" ]]; then
            echo "Changing background to ${background}"
            cfg background "${background}"
        fi
    else
        if [[ "${BACKGROUND}" == "light" ]]; then
            cfg background dark
        else
            cfg background light
        fi
    fi
}

########################################################
# Maps -------------------------------------------------
########################################################

declare -A color_map_vs_code_dark
color_map_vs_code_dark[everforest]="Everforest Dark"
color_map_vs_code_dark[gruvbox]="Gruvbox Dark Medium"
color_map_vs_code_dark[monokai]="Monokai"
color_map_vs_code_dark[nord]="Nord"
color_map_vs_code_dark[palenight]="Palenight (Mild Contrast)"
color_map_vs_code_dark[solarized]="Solarized Dark"

declare -A color_map_vs_code_light
color_map_vs_code_light[everforest]="Everforest Light"
color_map_vs_code_light[gruvbox]="Gruvbox Light Medium"
color_map_vs_code_light[monokai]="Monokai"
color_map_vs_code_light[nord]="Nord"
color_map_vs_code_light[palenight]="Palenight (Mild Contrast)"
color_map_vs_code_light[solarized]="Solarized Light"

declare -A color_map_terminal_dark
color_map_terminal_dark[everforest]="Everforest Dark"
color_map_terminal_dark[gruvbox]="Gruvbox Dark"
color_map_terminal_dark[monokai]="Monokai"
color_map_terminal_dark[nord]="Nord"
color_map_terminal_dark[palenight]="Palenight"
color_map_terminal_dark[solarized]="Solarized Dark"

declare -A color_map_terminal_light
color_map_terminal_light[everforest]="Everforest Light"
color_map_terminal_light[gruvbox]="Gruvbox Light"
color_map_terminal_light[monokai]="Monokai"
color_map_terminal_light[nord]="Nord"
color_map_terminal_light[palenight]="Palenight"
color_map_terminal_light[solarized]="Solarized Light"

declare -A color_map_ls_colors_dark
color_map_ls_colors_dark[gruvbox]="gruvbox-dark"
color_map_ls_colors_dark[monokai]="molokai"
color_map_ls_colors_dark[nord]="nord"
color_map_ls_colors_dark[solarized]="solarized-dark"

declare -A color_map_ls_colors_light
color_map_ls_colors_light[gruvbox]="gruvbox-light"
color_map_ls_colors_light[monokai]="molokai"
color_map_ls_colors_light[nord]="nord"
color_map_ls_colors_light[solarized]="solarized-light"

declare -A color_map_bat_theme_dark
color_map_bat_theme_dark[everforest]="ansi"
color_map_bat_theme_dark[gruvbox]="gruvbox-dark"
color_map_bat_theme_dark[monokai]="Monokai-Extended-Dark"
color_map_bat_theme_dark[nord]="Nord"
color_map_bat_theme_dark[palenight]="ansi"
color_map_bat_theme_dark[solarized]="Solarized (dark)"

declare -A color_map_bat_theme_light
color_map_bat_theme_light[everforest]="ansi"
color_map_bat_theme_light[gruvbox]="gruvbox-light"
color_map_bat_theme_light[monokai]="Monokai-Extended-Dark"
color_map_bat_theme_light[nord]="Nord"
color_map_bat_theme_light[palenight]="ansi"
color_map_bat_theme_light[solarized]="Solarized (light)"

########################################################
# Private functions ------------------------------------
########################################################

__get_zsh_autosuggest_highlight_style() {
    local theme="$1"
    local background="$2"
    local theme_file="${DOTFILES_DIR}/theming/zsh-autosuggest/${theme}-${background}.sh"
    local autosuggestion_path

    if [[ "${OSTYPE}" == darwin* ]]; then
        autosuggestion_path="${HOMEBREW_PREFIX}/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
    else
        autosuggestion_path=/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
    fi

    if [[ -z "${ZSH_VERSION}" || "${AUTO_SUGGESTIONS}" == 'off' || ! -f "${autosuggestion_path}" ]]; then
        return 0
    fi
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/zsh-autosuggest/${theme}.sh"
    fi
    if [[ -f "${theme_file}" ]]; then
        # shellcheck disable=SC1090
        source "${theme_file}"
    else
        unset ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE
    fi
}

__set_starship_theme() {
    local last_venv="${VIRTUAL_ENV}"
    source "${DOTFILES_DIR}/.venv/bin/activate"
    "${DOTFILES_DIR}/.venv/bin/python3" "${DOTFILES_DIR}/theming/starship/write_starship.py" "$1" "$2"
    deactivate
    [[ -n "${last_venv}" ]] && source "${last_venv}/bin/activate"
}

__get_fzf_colors() {
    local theme="$1"
    local background="$2"
    local theme_file
    theme_file="${DOTFILES_DIR}/theming/fzf/${theme}.sh"
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/fzf/${theme}-${background}.sh"
    fi
    if [[ -f "${theme_file}" ]]; then
        # shellcheck disable=SC1090
        source "${theme_file}"
    fi
}

__get_tmux_theme_file() {
    local theme="$1"
    local background="$2"
    # shellcheck disable=SC2153
    TMUX_THEME_FILE="${DOTFILES_DIR}/theming/tmux/${theme}-${background}.conf"
    if [[ ! -f "${TMUX_THEME_FILE}" ]]; then
        TMUX_THEME_FILE="${DOTFILES_DIR}/theming/tmux/${theme}.conf"
    fi
    export TMUX_THEME_FILE
}

__get_ls_colors() {
    local theme="$1"
    local background="$2"
    local vivid_theme
    if [[ "${background}" == "light" ]]; then
        vivid_theme=${color_map_ls_colors_light[${theme}]}
    else
        vivid_theme=${color_map_ls_colors_dark[${theme}]}
    fi
    if command -v vivid >/dev/null && [[ -n "${vivid_theme}" ]]; then
        LS_COLORS=$(vivid generate "$vivid_theme")
        export LS_COLORS
    elif command -v dircolors >/dev/null; then
        # use ansi based self defined LS_COLORS
        eval "$(dircolors --bourne-shell "${DOTFILES_DIR}/theming/dircolors/ansi")"
    fi
}

__get_eza_colors() {
    local theme="$1"
    local background="$2"
    local theme_file

    theme_file="${DOTFILES_DIR}/theming/eza/${theme}-${background}"
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/eza/${theme}"
    fi

    if [[ -f "${theme_file}" ]]; then
        EZA_COLORS=$(__load_eza_colors "${theme_file}")
        export EZA_COLORS
    else
        unset EZA_COLORS
    fi
}

__load_eza_colors() {
    local theme_file="$1"

    # delete comments
    # delete empty lines
    # right trim
    # replace multiple spaces with one
    # replace remaining space with =38;5
    # join lines with separator :
    cut --delimiter='#' --fields=1 "${theme_file}" \
        | grep . \
        | sed 's/ *$//g' \
        | tr --squeeze-repeats ' ' \
        | sed 's/ /=38;5;/g' \
        | tr '\n' ':'
}

__get_bat_theme() {
    local theme="$1"
    local background="$2"
    if [[ "${background}" == "light" ]]; then
        local bat_theme=${color_map_bat_theme_light[$1]}
    else
        local bat_theme=${color_map_bat_theme_dark[$1]}
    fi
    export BAT_THEME="${bat_theme}"
}

__set_vs_code_setting_temp() {
    local key="$1"
    local value="$2"
    local settings_temp="/tmp/settings.vscode.tmp.json"
    local query

    query=".\"${key}\" = \"${value}\""
    # relies on `getconf ARG_MAX` being larger than settings file size
    # see https://stackoverflow.com/a/75991875/21355126
    jq "${query}" <<<"$(cat ${settings_temp})" >"${settings_temp}"
}

__set_vs_code_theme() {
    local color_scheme="$1"
    local brightness="$2"
    local username="$3"
    local settings_path
    local settings_temp="/tmp/settings.vscode.tmp.json"
    local theme query

    if [[ -n "${WSL_DISTRO_NAME}" ]]; then
        settings_path="/mnt/c/Users/${username}/AppData/Roaming/Code/User/settings.json"
    elif [[ "${OSTYPE}" == darwin* ]]; then
        settings_path="/Users/${username}/Library/Application Support/Code/User/settings.json"
    fi

    if [[ -f "${settings_path}" ]]; then
        if [[ "${brightness}" == "dark" ]]; then
            theme=${color_map_vs_code_dark[${color_scheme}]}
        else
            theme=${color_map_vs_code_light[${color_scheme}]}
        fi

        cp "${settings_path}" "${settings_temp}"
        __set_vs_code_setting_temp "workbench.colorTheme" "${theme}"
        # enable auto switching dark/light mode in Windows to affect VS Code
        # needs VS Code setting "window.autoDetectColorScheme": true
        __set_vs_code_setting_temp "workbench.preferredLightColorTheme" "${color_map_vs_code_light[${color_scheme}]}"
        __set_vs_code_setting_temp "workbench.preferredDarkColorTheme" "${color_map_vs_code_dark[${color_scheme}]}"
        __set_vs_code_setting_temp "workbench.preferredHighContrastLightColorTheme" "${color_map_vs_code_light[${color_scheme}]}"
        __set_vs_code_setting_temp "workbench.preferredHighContrastColorTheme" "${color_map_vs_code_dark[${color_scheme}]}"
        cp "${settings_path}" "${settings_path}.backup"
        mv "${settings_temp}" "${settings_path}"
    fi
}

__set_terminal_theme() {
    local color_scheme="$1"
    local brightness="$2"
    local username="$3"
    local wsl_settings_path="/mnt/c/Users/${username}/AppData/Local/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState/settings.json"
    local terminal_color_scheme

    if [[ -n "${WSL_DISTRO_NAME}" && -f "${wsl_settings_path}" ]]; then
        local default_profile query
        if [[ "${brightness}" == "dark" ]]; then
            terminal_color_scheme=${color_map_terminal_dark[${color_scheme}]}
        else
            terminal_color_scheme=${color_map_terminal_light[${color_scheme}]}
        fi
        default_profile=$(jq --raw-output '.defaultProfile' "${wsl_settings_path}")
        query=".profiles.list=[.profiles.list[] | select (.guid == \"${default_profile}\") .colorScheme = \"${terminal_color_scheme}\"]"

        jq "${query}" "${wsl_settings_path}" >/tmp/settings.terminal.tmp.json
        mv /tmp/settings.terminal.tmp.json "${wsl_settings_path}"
    elif [[ "${OSTYPE}" == darwin* ]]; then
        __set_iterm_theme "${color_scheme}" "${brightness}"
    fi
}

__set_iterm_theme() {
    local last_venv="${VIRTUAL_ENV}"
    if [[ "${background}" == "dark" ]]; then
        terminal_color_scheme=${color_map_terminal_dark[${theme}]}
    else
        terminal_color_scheme=${color_map_terminal_light[${theme}]}
    fi
    source "${DOTFILES_DIR}/.venv/bin/activate"
    "${DOTFILES_DIR}/.venv/bin/python3" "${DOTFILES_DIR}/theming/terminal/set_iterm_theme.py" \
        "${theme}" "${background}" "${terminal_color_scheme}"
    deactivate
    [[ -n "${last_venv}" ]] && source "${last_venv}/bin/activate"
}

__set_lazygit_theme() {
    local theme="$1"
    local background="$2"
    local theme_file="${DOTFILES_DIR}/theming/lazygit/${theme}-${background}.yml"
    local lazygit_config_dir="${HOME}/.config/lazygit"

    [[ ! -d "${lazygit_config_dir}" ]] && return 0

    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/lazygit/${theme}.yml"
    fi

    if [[ -f "${theme_file}" ]]; then
        # shellcheck disable=SC2016
        yq eval-all '. as $item ireduce ({}; . * $item )' \
            "${DOTFILES_DIR}/config/lazygit-config.yml" \
            "${theme_file}" \
            > "${lazygit_config_dir}/config.yml"
    else
        # reset to the default colors
        cp "${DOTFILES_DIR}/config/lazygit-config.yml" "${lazygit_config_dir}/config.yml"
    fi
}

__get_git_theme() {
    local theme="$1"
    local background="$2"
    local theme_file="${DOTFILES_DIR}/theming/git/${theme}-${background}.sh"
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/git/${theme}.sh"
    fi

    if [[ -f "${theme_file}" ]]; then
        # shellcheck disable=SC1090
        source "${theme_file}"
    else
        export COLOR_GIT_LOG_FG=white
    fi
}

__set_git_theme() {
    local theme="$1"
    local background="$2"
    local gitconfig="${HOME}/.config/dotfiles/gitconfig.local"
    local theme_file="${DOTFILES_DIR}/theming/git/${theme}-${background}.sh"
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/git/${theme}.sh"
    fi
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/git/default.sh"
    fi

    # shellcheck disable=SC1090
    source "${theme_file}"
    git config --file "${gitconfig}" --replace-all color.status.header "${COLOR_GIT_STATUS_HEADER}"
    git config --file "${gitconfig}" --replace-all delta.unobtrusive-line-numbers.line-numbers-minus-style "${COLOR_GIT_LINE_NUMBERS}"
    git config --file "${gitconfig}" --replace-all delta.unobtrusive-line-numbers.line-numbers-zero-style "${COLOR_GIT_LINE_NUMBERS}"
    git config --file "${gitconfig}" --replace-all delta.unobtrusive-line-numbers.line-numbers-plus-style "${COLOR_GIT_LINE_NUMBERS}"

    git config --file "${gitconfig}" --replace-all delta.minus-style "${COLOR_GIT_DIFF_FG} red"
    git config --file "${gitconfig}" --replace-all delta.minus-non-emph-style "${COLOR_GIT_DIFF_FG} red"
    git config --file "${gitconfig}" --replace-all delta.minus-empty-line-marker-style "${COLOR_GIT_DIFF_FG} red"
    git config --file "${gitconfig}" --replace-all color.diff.oldMoved "red ${COLOR_GIT_DIFF_FG}"

    git config --file "${gitconfig}" --replace-all delta.plus-style "${COLOR_GIT_DIFF_FG} green"
    git config --file "${gitconfig}" --replace-all delta.plus-non-emph-style "${COLOR_GIT_DIFF_FG} green"
    git config --file "${gitconfig}" --replace-all delta.plus-empty-line-marker-style "${COLOR_GIT_DIFF_FG} green"
    git config --file "${gitconfig}" --replace-all color.diff.newMoved "green ${COLOR_GIT_DIFF_FG}"

    # git config creates tabs instead of spaces; we want to clean that up
    expand --tabs=4 --initial "${gitconfig}" > /tmp/gitconfig.tmp
    mv /tmp/gitconfig.tmp "${gitconfig}"
}

__load_solarized_colors() {
    local background="$1"
    # shellcheck disable=SC1090
    source "${DOTFILES_DIR}/theming/solarized-colors/solarized-${background}.sh"
}

__get_tldr_colors() {
    if [[ -f /usr/local/bin/tldr ]]; then
        local theme="$1"
        local background="$2"
        local theme_file="${DOTFILES_DIR}/theming/tldr/${theme}-${background}.sh"
        if [[ ! -f "${theme_file}" ]]; then
            theme_file="${DOTFILES_DIR}/theming/tldr/${theme}.sh"
        fi
        if [[ -f "${theme_file}" ]]; then
            # shellcheck disable=SC1090
            source "${theme_file}"
        fi
    fi
}

__get_cmatrix_color() {
    local theme="$1"
    local background="$2"
    local theme_file="${DOTFILES_DIR}/theming/cmatrix/${theme}-${background}.sh"
    if [[ ! -f "${theme_file}" ]]; then
        theme_file="${DOTFILES_DIR}/theming/cmatrix/${theme}.sh"
    fi
    if [[ -f "${theme_file}" ]]; then
        # shellcheck disable=SC1090
        source "${theme_file}"
    else
        export COLOR_CMATRIX=green
    fi
}

__get_system_username() {
    if command -v powershell.exe >/dev/null; then
        powershell.exe -noprofile -command 'whoami' \
            | tr --delete '\r' \
            | cut --delimiter="\\" --fields=2
    else
        whoami
    fi
}

if [[ "$1" == "--init" ]]; then
    __initialize_theming
fi
