#!/usr/bin/env bash
# https://www.mankier.com/1/fzf#--color
# Note: some colors must be explicitely reset using suffix ":regular", otherwise fzf automatically takes their bold counterpart.
# Some colors don't even work with regular, like COL_FZF_HL and COL_FZF_HL_PLUS.

# Highlighted substrings
export COL_FZF_HL='#859900'
# Highlighted substrings (current line)
export COL_FZF_HL_PLUS='#859900'
# Pointer to the current line
export COL_FZF_POINTER=${_COL_SOLARIZED_BLUE}:regular
# Multi-select marker
export COL_FZF_MARKER=${_COL_SOLARIZED_GREEN}:regular
# Text (current line)
export COL_FZF_FG_PLUS=${_COL_SOLARIZED_EMPHASIZED_CONTENT}:regular
# Background (current line)
export COL_FZF_BG_PLUS=${_COL_SOLARIZED_BACKGROUND_HIGHLIGHT}:regular
# Header
export COL_FZF_HEADER=${_COL_SOLARIZED_BLUE}:regular

# https://github.com/Aloxaf/fzf-tab/wiki/Configuration
export FZF_TAB_DEFAULT_COLOR=$'\e['"38;5;${_COL_SOLARIZED_PRIMARY_CONTENT}m"
export FZF_TAB_GROUP_COLORS=(
    $'\e['"38;5;${_COL_SOLARIZED_RED}m" \
    $'\e['"38;5;${_COL_SOLARIZED_GREEN}m" \
    $'\e['"38;5;${_COL_SOLARIZED_YELLOW}m" \
    $'\e['"38;5;${_COL_SOLARIZED_BLUE}m" \
    $'\e['"38;5;${_COL_SOLARIZED_MAGENTA}m" \
    $'\e['"38;5;${_COL_SOLARIZED_CYAN}m" \
    $'\e['"38;5;${_COL_SOLARIZED_ORANGE}m" \
    $'\e['"38;5;${_COL_SOLARIZED_VIOLET}m" \
    $'\e['"38;2;110;195;121m" \
    $'\e['"38;2;159;195;110m" \
    $'\e['"38;2;195;173;110m"
)
