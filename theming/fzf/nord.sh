#!/usr/bin/env bash
# https://www.mankier.com/1/fzf#--color
# Note: some colors must be explicitely reset using suffix ":regular", otherwise fzf automatically takes their bold counterpart.
# Some colors don't even work with regular, like COL_FZF_HL and COL_FZF_HL_PLUS.

# Highlighted substrings
export COL_FZF_HL=148
# Highlighted substrings (current line)
export COL_FZF_HL_PLUS=154
# Pointer to the current line
export COL_FZF_POINTER=blue
# Multi-select marker
export COL_FZF_MARKER=bright-green
# Text (current line)
export COL_FZF_FG_PLUS=cyan
# Background (current line)
# "#677590"
# bright-black
export COL_FZF_BG_PLUS="#3C4252"
# Header
export COL_FZF_HEADER=blue

# https://github.com/Aloxaf/fzf-tab/wiki/Configuration
unset FZF_TAB_DEFAULT_COLOR
export FZF_TAB_GROUP_COLORS=(
    $'\e[38;5;2m' $'\e[38;5;3m' $'\e[38;5;4m' $'\e[38;5;5m' $'\e[38;5;6m'  $'\e[38;5;30m'  \
    $'\e[38;5;73m' $'\e[38;5;103m' $'\e[38;2;159;109;114m' $'\e[38;2;200;145;54m' $'\e[38;2;179;87;49m'
)
