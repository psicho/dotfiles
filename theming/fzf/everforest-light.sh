#!/usr/bin/env bash
# https://www.mankier.com/1/fzf#--color
# Note: some colors must be explicitely reset using suffix ":regular", otherwise fzf automatically takes their bold counterpart.
# Some colors don't even work with regular, like COL_FZF_HL and COL_FZF_HL_PLUS.

# Highlighted substrings
export COL_FZF_HL=148
# Highlighted substrings (current line)
export COL_FZF_HL_PLUS=154
# Pointer to the current line
export COL_FZF_POINTER=blue
# Multi-select marker
export COL_FZF_MARKER=green
# Text (current line)
export COL_FZF_FG_PLUS="#5C6A72"
# Background (current line)
export COL_FZF_BG_PLUS="#D8D3BA"
# Header
export COL_FZF_HEADER=blue

# https://github.com/Aloxaf/fzf-tab/wiki/Configuration
export FZF_TAB_DEFAULT_COLOR=$'\e[38;2;92;106;114m'
export FZF_TAB_GROUP_COLORS=(
    $'\e[38;5;1m' $'\e[38;5;2m' $'\e[38;5;3m' $'\e[38;5;4m' $'\e[38;5;5m' $'\e[38;5;6m' \
    $'\e[38;2;230;126;128m' $'\e[38;2;230;152;117m' $'\e[38;2;219;188;127m' $'\e[38;2;167;192;128m' $'\e[38;2;127;187;179m'
)
