#!/usr/bin/env bash
# https://www.mankier.com/1/fzf#--color
# Note: some colors must be explicitely reset using suffix ":regular", otherwise fzf automatically takes their bold counterpart.
# Some colors don't even work with regular, like COL_FZF_HL and COL_FZF_HL_PLUS.

# Highlighted substrings
export COL_FZF_HL=112
# Highlighted substrings (current line)
export COL_FZF_HL_PLUS=106
# Pointer to the current line
export COL_FZF_POINTER=cyan
# Multi-select marker
export COL_FZF_MARKER=bright-green
# Text (current line)
export COL_FZF_FG_PLUS=bright-white
# Background (current line)
export COL_FZF_BG_PLUS=white
# Header
export COL_FZF_HEADER=blue

# https://github.com/Aloxaf/fzf-tab/wiki/Configuration
export FZF_TAB_DEFAULT_COLOR=$'\e[38;5;8m'
export FZF_TAB_GROUP_COLORS=(
    $'\e[38;5;12m' $'\e[38;5;2m' $'\e[38;5;3m' $'\e[38;5;5m' $'\e[38;5;1m' $'\e[38;5;6m' \
    $'\e[38;5;11m' $'\e[38;5;24m' $'\e[38;5;38m' $'\e[38;5;138m' $'\e[38;5;166m'
)
