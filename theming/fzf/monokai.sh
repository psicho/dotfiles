#!/usr/bin/env bash
# https://www.mankier.com/1/fzf#--color
# https://github.com/Aloxaf/fzf-tab/wiki/Configuration#default-color
# Note: some colors must be explicitely reset using suffix ":regular", otherwise fzf automatically takes their bold counterpart.
# Some colors don't even work with regular, like COL_FZF_HL and COL_FZF_HL_PLUS.

# Highlighted substrings
export COL_FZF_HL=148
# Highlighted substrings (current line)
export COL_FZF_HL_PLUS=154
# Pointer to the current line
export COL_FZF_POINTER=blue
# Multi-select marker
export COL_FZF_MARKER=green
# Text (current line)
export COL_FZF_FG_PLUS="#F4BF75"
# Background (current line)
export COL_FZF_BG_PLUS="#49483E"
# Header
export COL_FZF_HEADER=blue

# https://github.com/Aloxaf/fzf-tab/wiki/Configuration
export FZF_TAB_DEFAULT_COLOR=$'\e[38;2;244;191;117m'
export FZF_TAB_GROUP_COLORS=(
    $'\e[38;5;1m' $'\e[38;5;2m' $'\e[38;5;3m' $'\e[38;5;4m' $'\e[38;5;5m' $'\e[38;5;6m' \
    $'\e[38;5;9m' $'\e[38;5;10m' $'\e[38;5;11m' $'\e[38;5;12m' $'\e[38;5;13m'
)
