#!/usr/bin/env bash
# https://www.mankier.com/1/fzf#--color
# Note: some colors must be explicitely reset using suffix ":regular", otherwise fzf automatically takes their bold counterpart.
# Some colors don't even work with regular, like COL_FZF_HL and COL_FZF_HL_PLUS.

# Highlighted substrings
export COL_FZF_HL=148
# Highlighted substrings (current line)
export COL_FZF_HL_PLUS=154
# Pointer to the current line
export COL_FZF_POINTER=blue
# Multi-select marker
export COL_FZF_MARKER=bright-green
# Text (current line)
export COL_FZF_FG_PLUS=cyan
# Background (current line)
export COL_FZF_BG_PLUS="#4B5170"
# Header
export COL_FZF_HEADER=blue

# https://github.com/Aloxaf/fzf-tab/wiki/Configuration
unset FZF_TAB_DEFAULT_COLOR
unset FZF_TAB_GROUP_COLORS
