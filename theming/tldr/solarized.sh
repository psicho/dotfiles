#!/usr/bin/env bash
# https://github.com/raylee/tldr-sh-client#customization

export TLDR_HEADER='white onblue'
export TLDR_QUOTE='green'
# not setting TLDR_DESCRIPTION leads to using terminal's foreground color, which is best
export TLDR_CODE='yellow'
export TLDR_PARAM='blue'
