#!/usr/bin/env bash
# https://github.com/altercation/solarized

# Instead of using colors 8, 10, 11, 12, 14 as suggested by altercation, we're deliberatly using the solarized colors in base256 range.
# 1. Some tools (like Ansible) print with bright black foreground. The default color 10 is too dark for that.
# 2. Many tools rely on 10, 11, 12 and 14 being bright-green, bright-yellow, bright-blue and bright-cyan, not grey.

export _COL_SOLARIZED_BACKGROUND=230
export _COL_SOLARIZED_BACKGROUND_HIGHLIGHT=254
export _COL_SOLARIZED_PRIMARY_CONTENT=66
export _COL_SOLARIZED_SECONDARY_CONTENT=247
export _COL_SOLARIZED_EMPHASIZED_CONTENT=242

export _COL_SOLARIZED_RED=1
export _COL_SOLARIZED_GREEN=2
export _COL_SOLARIZED_YELLOW=3
export _COL_SOLARIZED_BLUE=4
export _COL_SOLARIZED_MAGENTA=5
export _COL_SOLARIZED_CYAN=6
export _COL_SOLARIZED_ORANGE=9
export _COL_SOLARIZED_VIOLET=13
