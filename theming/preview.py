#!/usr/bin/env python3

import json
import os
import sys
from pathlib import Path

color_name_order = [ "black", "red", "green", "yellow", "blue", "purple", "cyan", "white" ]

def hex2rgb(color):
    red = color[1:3]
    green = color[3:5]
    blue = color[5:7]
    return int(red, 16), int(green, 16), int(blue, 16)

def bright(color_name):
    first_char = color_name[0:1]
    return f"bright{first_char.upper()}{color_name[1:]}"

def print_color(red, green, blue):
    print(f"\033[48;2;{red};{green};{blue}m{' ' * 9}", end = "")

def print_colors(style):
    for color_name in color_name_order:
        red, green, blue = hex2rgb(terminal_scheme[style(color_name)])
        print_color(red, green, blue)

# read arguments
theme = sys.argv[1].split("(")[0].strip()
if len(sys.argv) >= 3:
    system_background = sys.argv[2].capitalize()
else:
    system_background = ""

terminal_theme_file_name = theme[0:1].lower() + theme[1:].split(" ")[0]
dotfiles_dir = os.environ['DOTFILES_DIR']
terminal_theme_file = open(f"{dotfiles_dir}/theming/terminal/{terminal_theme_file_name}.json")
json_schemes = json.load(terminal_theme_file)["schemes"]
interpolated_theme = theme.replace("System", system_background)
terminal_scheme = list(filter(lambda scheme: scheme["name"] == interpolated_theme, json_schemes))[0]
terminal_theme_file.close()
bg_red, bg_green, bg_blue = hex2rgb(terminal_scheme["background"])

# print border in background color
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 82}")
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 82}")
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 5}", end = '')

# print dark colors
print_colors(lambda color_name : color_name)

# print border in background color
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 5}", end = '')
print()
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 5}", end = '')

# print bright colors
print_colors(bright)

# print border in background color
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 5}", end = '')
print()
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 82}")
print(f"\033[48;2;{bg_red};{bg_green};{bg_blue}m{' ' * 82}")
