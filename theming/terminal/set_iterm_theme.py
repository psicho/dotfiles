#!/usr/bin/env python3

# Prerequisite: it2setcolor (iTerm2 > Menu > Install Shell Integration)
# it2setcolor temporarily overwrites the default scheme.

import json
import os
import sys
from pathlib import Path
from subprocess import Popen


def setcolor(name, value):
    Popen([f"{home_path}/.iterm2/it2setcolor", name, value])


# maps key names from Windows Terminal to iTerm2
# https://iterm2.com/documentation-utilities.html
keymapping = {
    "background": "bg",
    "foreground": "fg",
    "selectionBackground": "selbg",
    # currently, curbg does not work (does not have any effect)
    "cursorColor": "curbg",
    "black": "black",
    "brightBlack": "br_black",
    "red": "dred",
    "brightRed": "br_red",
    "green": "green",
    "brightGreen": "br_green",
    "yellow": "yellow",
    "brightYellow": "br_yellow",
    "blue": "blue",
    "brightBlue": "br_blue",
    "purple": "magenta",
    "brightPurple": "br_magenta",
    "cyan": "cyan",
    "brightCyan": "br_cyan",
    "white": "white",
    "brightWhite": "br_white",
}

# read args
color_scheme = sys.argv[1]  # name of the file
brightness = sys.argv[2]  # dark or light
terminal_color_scheme = sys.argv[3]  # name of the scheme in the file

# get scheme
dotfiles_dir = os.getenv("DOTFILES_DIR")
terminal_theme_file = open(f"{dotfiles_dir}/theming/terminal/{color_scheme}.json")
json_schemes = json.load(terminal_theme_file)["schemes"]
for scheme in json_schemes:
    if scheme["name"] == terminal_color_scheme:
        selected_scheme = scheme

# set colors
home_path = str(Path.home())
for key, value in keymapping.items():
    hexcolor = selected_scheme[key][1:]
    setcolor(keymapping[key], hexcolor)
    if key == "background":
        setcolor("curfg", hexcolor)
