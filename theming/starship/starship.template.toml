# https://starship.rs/config/
# Powerline prompt, inspired by https://starship.rs/presets/pastel-powerline.html
"$schema" = 'https://starship.rs/config-schema.json'

format = """
[](${config['name_bg']})${"\\"}
$username${"\\"}
${"${custom.hostname-local}\\"}
${"${custom.hostname-remote}\\"}
% if config['separator_style'] == 'thin':
[${config['separator_char']}](fg:${config['separator_fg']} bg:${config['directory_bg']})${"\\"}
% else:
[${config['separator_char']}](fg:${config['name_bg']} bg:${config['directory_bg']})${"\\"}
% endif
$directory${"\\"}
% if config['separator_style'] == 'thin':
[${config['separator_char']}](fg:${config['separator_fg']} bg:${config['git_bg']})${"\\"}
% else:
[${config['separator_char']}](fg:${config['directory_bg']} bg:${config['git_bg']})${"\\"}
% endif
$git_branch${"\\"}
$git_commit${"\\"}
$git_state${"\\"}
$git_status${"\\"}
$git_metrics${"\\"}
% if config['separator_style'] == 'thin':
[${config['separator_char']}](fg:${config['separator_fg']} bg:${config['package_bg']})${"\\"}
% else:
[${config['separator_char']}](fg:${config['git_bg']} bg:${config['package_bg']})${"\\"}
% endif
$package${"\\"}
$java${"\\"}
$nodejs${"\\"}
$python${"\\"}
$helm${"\\"}
$kubernetes${"\\"}
$aws${"\\"}
% if config['separator_style'] == 'thin':
[${config['separator_char']}](fg:${config['separator_fg']} bg:${config['shell_bg']})${"\\"}
% else:
[${config['separator_char']}](fg:${config['package_bg']} bg:${config['shell_bg']})${"\\"}
% endif
$shell${"\\"}
$shlvl${"\\"}
$cmd_duration${"\\"}
${"${custom.time-symbol}\\"}
$time${"\\"}
% if config['separator_style'] == 'thin':
[${config['separator_char_end']}](fg:${config['shell_bg']})${"\\"}
% else:
[${config['separator_char_end']}](fg:${config['shell_bg']})${"\\"}
% endif
$line_break${"\\"}
$character
"""

[username]
show_always = true
style_user = "fg:${config['name_fg_local']} bg:${config['name_bg']}"
style_root = "fg:${config['name_fg_local']} bg:${config['name_bg']}"
format = '[$user]($style)'

[custom.hostname-local]
# use env variable DOTFILES_DIR without curly braces - otherwise mako tries to replace it
command = "$DOTFILES_DIR/lib/prompt-tools.sh --get-hostname"
shell = "zsh"                                                       # only with this hacky workaround custom modules work in bash
description = "The system hostname (local)"
style = "fg:${config['name_fg_local']} bg:${config['name_bg']}"
when = "! $DOTFILES_DIR/lib/ssh-manager.sh --ssh-is-remote-session"
format = "[@$output ]($style)"

[custom.hostname-remote]
# use env variable DOTFILES_DIR without curly braces - otherwise mako tries to replace it
command = "$DOTFILES_DIR/lib/prompt-tools.sh --get-hostname"
shell = "zsh"                                                     # only with this hacky workaround custom modules work in bash
description = "The system hostname (remote)"
style = "fg:${config['name_fg_remote']} bg:${config['name_bg']}"
when = "$DOTFILES_DIR/lib/ssh-manager.sh --ssh-is-remote-session"
format = "[@$output ]($style)"

[directory]
style = "fg:${config['directory_fg']} bg:${config['directory_bg']}"
format = "[ $path ]($style)"

[git_branch]
symbol = ""
style = "fg:${config['git_fg']} bg:${config['git_bg']}"
format = "[ $symbol $branch ]($style)"

[git_commit]
style = "fg:${config['git_fg']} bg:${config['git_bg']}"
format = "[ \\($hash$tag\\) ]($style)"

[git_metrics]
disabled = false
added_style = "fg:${config['git_added']} bg:${config['git_bg']}"
deleted_style = "fg:${config['git_deleted']} bg:${config['git_bg']}"
format = '([+$added ]($added_style))([-$deleted ]($deleted_style))'

[git_state]
cherry_pick = " PICKING"
style = "fg:${config['git_state']} bg:${config['git_bg']}"
format = '[\($state( $progress_current/$progress_total)\) ]($style)'

[git_status]
ahead = "↑$count"
behind = "↓$count"
diverged = "⇕↑$ahead_count↓$behind_count"
untracked = "?$count"
stashed = '\$$count'
conflicted = "=$count"
modified = "!$count"
staged = "+$count"
renamed = "»$count"
deleted = "✘$count"
style = "fg:${config['git_fg']} bg:${config['git_bg']}"
format = '[(\[$all_status$ahead_behind\] )]($style)'

[package]
symbol = " "
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = "[ $symbol($version) ]($style)"

[java]
symbol = " "
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = "[$symbol($version) ]($style)"

[nodejs]
symbol = " "
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = '[$symbol($version) ]($style)'

[python]
symbol = " "
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = '[$symbol$pyenv_prefix($version)(\($virtualenv\)) ]($style)'

[helm]
disabled = false
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = "[ $symbol($version) ]($style)"

[kubernetes]
disabled = false
symbol = "⎈ "
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = '[ $symbol$context( \($namespace\) )]($style)'

[aws]
disabled = false
symbol = "󰸏 "
style = "fg:${config['package_fg']} bg:${config['package_bg']}"
format = '[ $symbol($profile )(\($region\) )(\[$duration\] )]($style)'

[shell]
disabled = false
bash_indicator = "bash"
style = "fg:${config['shell_fg']} bg:${config['shell_bg']}"
format = "[ $indicator]($style)"

[shlvl]
disabled = false
symbol = " "
style = "fg:${config['shell_fg']} bg:${config['shell_bg']}"
format = "[ $symbol$shlvl]($style)"

[cmd_duration]
style = "fg:${config['shell_fg']} bg:${config['shell_bg']}"
format = "[ $duration]($style)"

[custom.time-symbol]
# use env variable DOTFILES_DIR without curly braces - otherwise mako tries to replace it
command = "$DOTFILES_DIR/theming/starship/time-symbol.sh"
shell = "zsh"                                               # only with this hacky workaround custom modules work in bash
description = "Time symbol showing the current hour"
style = "fg:${config['shell_fg']} bg:${config['shell_bg']}"
when = true
format = "[ $output]($style)"

[time]
disabled = false
style = "fg:${config['shell_fg']} bg:${config['shell_bg']}"
format = '[ $time ]($style)'

[character]
success_symbol = "[](fg:${config['character_success']})"
error_symbol = "[](fg:${config['character_error']})"
vimcmd_symbol = "[](fg:${config['character_vimcmd']})"
