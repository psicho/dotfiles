"""
Execute the mako template starship.template.toml injecting colors of current theme.
"""
import os
import re
import sys

import yaml
from mako.template import Template


def eval_string(expression, string):
    """
    Replace variables in string with the value from the according environment variable
    """
    match = expression.match(string)
    if match is None:
        return string
    if match.group(2) is not None:
        return os.getenv(match.group(2))
    if match.group(1) is not None:
        return os.getenv(match.group(1))
    return None


def collect_properties(obj, expression, path="") -> dict[str, object]:
    """
    Transform a yaml into a property like style, flat key/value dict
    Input:
        ---
        name:
            fg:
                local: white
    Output:
        props[name.fg.local] = white
    """
    props: dict[str, object] = {}
    if isinstance(obj, dict):
        for key in obj:
            props.update(collect_properties(obj[key], expression, path + f"_{key}"))
    elif isinstance(obj, str):
        props[path[1:]] = eval_string(expression, obj)
    elif isinstance(obj, int):
        props[path[1:]] = obj
    return props


def get_starship_config(dotfiles_dir):
    """
    Get the starship config file for the current theme and load it's yaml
    """
    theme = sys.argv[1]
    background = sys.argv[2]
    theme_file = f"{dotfiles_dir}/theming/starship/{theme}-{background}.yml"
    if not os.path.exists(theme_file):
        theme_file = f"{dotfiles_dir}/theming/starship/{theme}.yml"
    with open(theme_file, "r", encoding="utf-8") as stream:
        try:
            starship_configuration = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return starship_configuration


home = os.getenv("HOME")
dotfiles_dir = os.getenv("DOTFILES_DIR")
reg_exp = re.compile(r"^\$(\{([A-Z_]+)\}|([A-Z_]+))$")
properties = collect_properties(get_starship_config(dotfiles_dir), reg_exp)

with open(f"{dotfiles_dir}/theming/starship/starship.template.toml", "r", encoding="utf-8") as template:
    starship_config = Template(template.read()).render(config=properties)
with open(f"{home}/.config/dotfiles/starship.toml", "w", encoding="utf-8") as starship_conf_file:
    starship_conf_file.write(starship_config)
