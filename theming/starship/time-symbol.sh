#!/usr/bin/env bash

__get_time_symbol() {
    local time hour minute
    time=$(date +%I:%M)
    hour=${time%:*}
    minute=${time#*:}
    [[ "${minute}" -ge 30 ]] && hour=$((hour+1))
    case "${hour}" in
        1 | 01 | 13) echo "" ;;
        2 | 02) echo "" ;;
        3 | 03) echo "" ;;
        4 | 04) echo "" ;;
        5 | 05) echo "" ;;
        6 | 06) echo "" ;;
        7 | 07) echo "" ;;
        8 | 08) echo "" ;;
        9 | 09) echo "" ;;
        10) echo "" ;;
        11) echo "" ;;
        12) echo "" ;;
    esac
}

__get_time_symbol
