#!/usr/bin/env bash
# https://git-scm.com/docs/git-config#Documentation/git-config.txt-color

export COLOR_GIT_LOG_FG=${_COL_SOLARIZED_PRIMARY_CONTENT}
export COLOR_GIT_STATUS_HEADER=${_COL_SOLARIZED_YELLOW}
export COLOR_GIT_LINE_NUMBERS="bold black"
export COLOR_GIT_DIFF_FG=white
