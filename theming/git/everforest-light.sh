#!/usr/bin/env bash
# https://git-scm.com/docs/git-config#Documentation/git-config.txt-color

export COLOR_GIT_LOG_FG="#5C6A72"
export COLOR_GIT_STATUS_HEADER="#5C6A72"
export COLOR_GIT_LINE_NUMBERS=white
export COLOR_GIT_DIFF_FG="#323D43"
