#!/usr/bin/env bash
# https://git-scm.com/docs/git-config#Documentation/git-config.txt-color

export COLOR_GIT_LOG_FG="#D3C6AA"
export COLOR_GIT_STATUS_HEADER="#D3C6AA"
export COLOR_GIT_LINE_NUMBERS=white
export COLOR_GIT_DIFF_FG="#F8F0DC"
