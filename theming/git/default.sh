#!/usr/bin/env bash
# https://git-scm.com/docs/git-config#Documentation/git-config.txt-color

export COLOR_GIT_LOG_FG=white
export COLOR_GIT_STATUS_HEADER=white
export COLOR_GIT_LINE_NUMBERS="bold black"
export COLOR_GIT_DIFF_FG=white
