#!/usr/bin/env bash

echo -e "\e[1mbold\e[0m"
echo -e "\e[2mdim\e[0m"
echo -e "\e[3mitalic\e[0m"
echo -e "\e[4munderline\e[0m"
echo -e "\e[5mblink\e[0m"
echo -e "\e[6mflicker\e[0m"
echo -e "\e[7minverted\e[0m"
echo -e "\e[8mhidden\e[0m"
echo -e "\e[9mstrikethrough\e[0m"
echo -e "\e[53moverline\e[0m"
