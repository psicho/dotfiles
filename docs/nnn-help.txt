 NAVIGATION
        󰜷 / k  Up                             󰜮 / j  Down
        󰜱 / h  Parent                   Ret / 󰜴 / l  Open
    Pg󰜷 / Pg󰜮  Page up/Page down            ^u / ^d  Half Page up/half Page down
       g / ^a  Top                           G / ^e  End
            ~  Go to Home directory               /  Go to root directory
            @  Go to start directory              -  Go to previous directory
         ', '  Go to first file           ', <char>  Go to next entry starting with <char>
            J  Jump to entry/offset              ^j  Toggle auto-advance on open
            ,  Mark current location         b / ^/  Select bookmark
          1-4  Switch to context
          Tab  Cycle context              Shift+Tab  New context
     Esc, Esc  Quit all (without cd)              Q  Quit all contexts (without cd)
           ^g  Quit all and cd                    q  Quit current context

 FILTER & PROMPT
            /  Filter                            ^n  Toggle type-to-nav
            .  Toggle hidden                     ^l  Toggle last filter
          Esc  Exit prompt

 FILES
            o  Open with (macOS only)             n  Create new/link
            f  File stats                         d  Detail mode toggle
           ^r  Rename/duplicate                   r  Batch rename
            z  Archive                            e  Edit file internally in $VISUAL
            *  Toggle exe on hovered              >  Export list
    Space / +  Toggle selection                 m-m  Select range/clear
            a  Select all                         A  Invert selection
            p  Copy selection here                w  Copy/move selection as
            v  Move selection here                E  Edit selection in list
       x / ^x  Delete or trash                    S  Summarize selected files' size
            X  Delete (rm -rf)                  Esc  Send to FIFO

 MISC
     Meta+Key  Select plugin                 ;, Key  Select plugin
            c  Connect remote (sshf)              u  Unmount remote/archive
       t / ^t  Sort toggles                       s  Manage session
            T  Set time type                      0  Lock
           ^l  Redraw                             ?  Help
       ! / ^]  Shell                              ]  Cmd prompt
                                                  =  Launch app
 SORT TOGGLES
            a  apparent disk usage (ncdu)         d  disk usage (ncdu)
            e  extension                          s  size
            t  time                               v  version
            r  reverse                            Capital letters reverse aswell
            c  clear

 BOOKMARKS
        f / .  dotFiles                           d  Desktop
            w  Wiki                               s  Settings
            p  Projects

 PLUGINS
            d  Show diffs of file/directory       V  Paste clipboard into selection path
            f  Go to dir/file and open it         g  Go to dir/file in subdir
            e  Open in Visual Studio Code         o  Open in Finder/Explorer
            p  Preview                            P  Copy with progress (rsynccp)
            n  Open with nuke                     q  Execute any plugin
            x  Toggle exe on selection            z  Go to directory via z.lua
            .  Go to git root                     /  Arbitrarily filter
            c  Copy current into macOS clipboard  v  Paste macOS clipboard into current dir
               (single file or dir)                  (single or multiple files only)

 COMMAND LINE ARGS
     --detach  Open a detached pane when editing a file via `e`

 KNOWHOW
   * Bookmarks: either use `NNN_BMS` and `,` or create custom bookmarks via `B` - combination is not possible
   * `w` opens editor to select target file names
   * Selection path (written by x2sel) is used by move, copy, list, etc. (see `selpath` in code)
   * Copy current file here: ^R, Ret, <enter new name>
   * Once a remote is connected, it will remain available using the given configuration. If the configuration needs to be changed (i.e., host dir change), unmount first (using `u`).
   * Preview (preview-tui)
      * Video/images: sixel is available in iTerm2, but not in WSL (`tmux display -p '#{client_termfeatures}'`)
      * In tmux, some image previews show up only if split to bottom, not if split to right
   * For debugging, call plugin via fzplug (`;q`), which shows the error before continuing
   * `man nnn`
   * https://github.com/jarun/nnn/wiki

---------------------------------------------------------------------------

