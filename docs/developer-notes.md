## Architecture

| Intel/AMD | Raspberry Pi | Command |
| --------- | ------------ | ------- |
| amd64     | arm64 	   | `dpkg --print-architecture` or<br>`dpkg-architecture --query DEB_HOST_ARCH` |
| x86_64    | aarch64	   | `arch` or<br>`dpkg-architecture --query DEB_HOST_GNU_CPU` or<br>`uname --machine`|

| macOS (Apple Silicium) | Command |
| ---------------------- | ------- |
| arm64                  | `arch` or<br>`uname --machine` |

## How to package a compiled programm

[Create a `.deb`](https://ubuntuforums.org/showthread.php?t=910717)

## Standard output and standard error

* 1 is standard out (`STDOUT`)
* 2 is standard error (`STDERR`)

`>&1` redirects **to** standard out.  
_Example_:
```bash
echo "output" >&1
```

`>&2` redirects **to** standard error.  
_Example_:
```bash
echo "error" >&2
```

`1>target` redirects **standard out** to `target`, which is the default and therefore equivalent to `>target`. Prefer `>target`.  
_Example_:
```bash
std_err_out() {
    echo "error" >&2
    echo "output" >&1
}
std_err_out >/dev/null  # preferred
std_err_out 1>/dev/null # equivalent
```

`2>target` redirects **standard error** to `target`  
_Example_:
```bash
std_err_out() {
    echo "error" >&2
    echo "output" >&1
}
std_err_out 2>/dev/null
```

`&>target` redirects both **standard out and standard error** to `target`, which is equivalent to `>target 2>&1` / `1>target 2>&1` / `2>target 1>&2`. Prefer `&>target`.  
_Example_:
```bash
std_err_out() {
    echo "error" >&2
    echo "output" >&1
}
std_err_out &>/dev/null      # preferred, but non-posix
std_err_out >/dev/null 2>&1  # equivalent
std_err_out 1>/dev/null 2>&1 # equivalent
std_err_out 2>/dev/null 1>&2 # equivalent
```

## Record keypresses
 * `cat -p`
 * `xxd -psd`
 * `xxd`, afterwards press `Ctrl+D` to send termination signal
 * `showkey` (on macOS via brew install) / `showkey -a` (on Linux)
 * `sed -n l`

## If checks

A direct call returns the return code:
```bash
if command -v executable &>/dev/null; then ...
```
Here, we don't want to see stdout in our current shell and dump the output.

A call in a subshell returns the stdout:
```bash
if [[ -z ${var} && $(command -v executable) ]]; then ...
```
This way, the second condition is true if and only if the command prints anything into stdout.

Alternatively, we can chain commands to check for the return code itself:
```bash
[[ -z ${var} ]] && command -v executable &>/dev/null ...
# alternative
if [[ -z ${var} ]] && command -v executable &>/dev/null; then ...
```
