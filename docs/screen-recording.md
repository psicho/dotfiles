# VHS
https://github.com/charmbracelet/vhs

Evaluate newest version (currently v0.8.0)

* Use `Screenshot` for still images
* Add config option for theme
* Disable auto-suggestions for most demos
* Switch to wanted theme before executing vhs
* Migrate all demos
    * document shortcuts (or find a way to show these)
* Integrate artifacts in README.md
* Integrate artifacts in GitLab Pages using workflow from below
* Finalize this documentation and link from [README.md](https://gitlab.com/psicho/dotfiles#help--docs-discoverability)

## Findings (v0.2.0)

### unsolved issues
* Does not work well with an interactive shell setting like in these dotfiles -> fixed in newest version?
* Starts in bash, then switches to zsh (why?) -> fixed in v0.4.0?
* Resets prompt -> fixed in v0.4.0?
* Prints resetting action at start -> fixed in v0.4.0?

see:
* https://github.com/charmbracelet/vhs/issues/39 (known issue)
* https://github.com/charmbracelet/vhs/pull/55 (PR introducing feature `Set Shell`)
* https://github.com/charmbracelet/vhs/pull/212/ (PR trying to fix parts of it, but making it worse: user shell files are not loaded at all)

### issues with a possible solution
* missing timer
    * workaround: use webm in a html page using GitLab Pages (see below)
* missing to show shortcuts, such as ctrl+z, alt+c etc.
    * workaround: document these in the description

### ToDo

* eval: install CaskaydiaCove from https://github.com/ryanoasis/nerd-fonts/releases (see install-vhs.sh)

## Installation
`install-vhs.sh`
```bash
#!/usr/bin/env bash

source "${DOTFILES_DIR}"/lib/library/github.sh

# install ttyd
system_machine=$(uname --machine)
# shellcheck disable=SC1090
filename_template="https://github.com/tsl0922/ttyd/releases/download/%s/ttyd.${system_machine}"
release_url=$(get-latest-release-url "${filename_template}")
sudo curl --location "${release_url}" --output /usr/local/bin/ttyd
sudo chmod +x /usr/local/bin/ttyd

# install CaskaydiaCove NerdFont
filename_template="https://github.com/ryanoasis/nerd-fonts/releases/download/v%s/CascadiaCode.tar.xz"
release_url=$(get-latest-release-url "${filename_template}")
curl --location "${release_url}" --output /tmp/CascadiaCode.tar.xz
mkdir --parents ~/.local/share/fonts/
tar --extract --xz --verbose --file=/tmp/CascadiaCode.tar.xz --directory="${HOME}/.local/share/fonts" --wildcards "*.ttf"
fc-cache -f -v
rm /tmp/CascadiaCode.tar.xz
# `fc-list` shows all fonts
# `fc-list : family | sort | uniq` shows all font families, sorted

# Install vhs
# https://github.com/charmbracelet/vhs#installation
sudo mkdir --parents /etc/apt/keyrings
curl --fail  --silent --show-error --location https://repo.charm.sh/apt/gpg.key \
    | sudo gpg --dearmor --output /etc/apt/keyrings/charm.gpg
echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" \
    | sudo tee /etc/apt/sources.list.d/charm.list
sudo apt-get update
sudo apt-get install --yes vhs ffmpeg
```

## Example tape
`demo.tape`:
```
Output public/demo.webm
Require echo

Set FontFamily "CaskaydiaCove Nerd Font"
Set FontSize 24
Set Width 892
Set Height 600
Set Padding 1
Set Theme { "name": "Everforest Dark", "background": "#323D43", "foreground": "#D3C6AA", "selection": "#5D4251", "cursor": "#F9E9C8", "black": "#374247", "brightBlack": "#404C51", "red": "#E67E80", "brightRed": "#E67E80", "green": "#A7C080", "brightGreen": "#A7C080", "yellow": "#DBBC7F", "brightYellow": "#DBBC7F", "blue": "#7FBBB3", "brightBlue": "#7FBBB3", "magenta": "#D699B6", "brightMagenta": "#D699B6", "cyan": "#83C092", "brightCyan": "#83C092", "white": "#859289", "brightWhite": "#9DA9A0" }
Set PlaybackSpeed 0.25

Set Shell "zsh"
Env IS_RELOAD "true"

Hide
Sleep 2s
Type "source $HOME/.zlogin" Enter
Type "cd $DOTFILES_DIR" Enter
Type "clear" Enter
Sleep 2s
Show

Type "echo 'Welcome to VHS World!'" Sleep 500ms  Enter
Type "echo 'Use Glyphs: 🕙 '" Enter
Sleep 1s

Type "fzf"
Enter
Type "proj"
Sleep 500ms
Enter
```

## Deployment
`.gitlab-ci.yml`:
```yml
pages:
  stage: deploy
  image: registry.gitlab.com/psicho/docker-ubuntu
  script:
    - cd /home/ubuntu/dotfiles
    - git checkout pages-test
    - /home/ubuntu/dotfiles/install/install-vhs.sh
    - vhs /home/ubuntu/dotfiles/demo.tape
  artifacts:
    paths:
      - public
```

## GitLab Pages
`public/index.html`:
```html
<body>
    Demo video here:<br>
    <video controls loop>
        <source src="demo.webm" type="video/webm">
    </video>
</body>
```

* Left menu > Deployments > Pages
* https://gitlab.com/psicho/dotfiles/pages
* https://psicho.gitlab.io/dotfiles/

# Alternative: asciinema

* does not support own fonts (i.e., CaskaydiaCove NF)
* does not support custom theme
* problably needs hosting the player (possible, but extra resources necessary) instead of running it in the cloud - see https://blog.asciinema.org/post/self-hosting/

Evaluate new version: https://docs.asciinema.org/manual/asciicast/v2/
