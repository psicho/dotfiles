## Rename an existing user

```bash
"${DOTFILES_DIR}"/install/create-user.sh tempuser tempuser
sudoedit /etc/wsl.conf
    [user]
    default=tempuser
wsl --shutdown
sudo usermod -l demoname oldname
sudoedit /etc/wsl.conf
    [user]
    default=demoname
wsl --shutdown
```
