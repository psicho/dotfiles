# shellcheck shell=bash
# shellcheck disable=SC1090,SC1091

# use readlink's short option to be compatible with macOS: gnu-binaries.sh is not loaded yet
DOTFILES_DIR=$(dirname "$(dirname "$(readlink -f "${HOME}"/.bash_profile)")")
export DOTFILES_DIR

if [[ "${OSTYPE}" == darwin* ]]; then
    if [[ -r "/opt/homebrew/etc/profile.d/bash_completion.sh" ]]; then
        source "/opt/homebrew/etc/profile.d/bash_completion.sh"
    fi
    if [[ -d "/opt/homebrew/bin" ]]; then
        PATH="/opt/homebrew/bin:$PATH"
    fi
fi

source "${DOTFILES_DIR}"/config/load-common-files.sh
source "${DOTFILES_DIR}"/lib/bash/plugins.bash
source "${DOTFILES_DIR}"/lib/bash/show-help.bash

eval "$(fzf --bash)"
# Meta-S - Paste the selected file path(s) into the command line
bind -m emacs-standard -x '"\es": fzf-file-widget'
bind -m vi-command -x '"\es": fzf-file-widget'
bind -m vi-insert -x '"\es": fzf-file-widget'

# load fzf-tab-completion
if [[ -f ~/bin/fzf-tab-completion/bash/fzf-bash-completion.sh && "${FZF_TAB}" == 'on' ]]; then
    export FZF_COMPLETION_OPTS="--preview-window=right:0%:hidden"
    source "${HOME}"/bin/fzf-tab-completion/bash/fzf-bash-completion.sh
    bind -x '"\t": fzf_bash_completion'
fi
