# use readlink's short option to be compatible with macOS: gnu-binaries.sh is not loaded yet
DOTFILES_DIR=$(dirname "$(dirname "$(readlink -f "${HOME}"/.zlogin)")")
export DOTFILES_DIR

### Aliases
# search in history (fzf's Ctrl+R is better, though)
alias h='history -i -50'
alias hs='history -i 0 | rg --smart-case'

# Print each PATH entry on a separate line
fpath() {
    echo "${FPATH}" | sed -E 's/:/\n/g'
}

# ensure (F)PATH entries remain unique after shell reload
typeset -U PATH FPATH path fpath

# suffix aliases - open the following files with vim directly
alias -s {java,json,log,md,txt,xml,yaml,yml}=vim

# Set ZSH_CACHE_DIR to the path where cache files should be created
export ZSH_CACHE_DIR="${HOME}/.cache/zsh/"

### General settings
setopt LONG_LIST_JOBS         # print job notifications in the long format by default
setopt AUTO_CD                # if a command is issued that can’t be executed as a normal command,
                              # and the command is the name of a directory, perform the cd command to that directory
setopt AUTO_PUSHD             # make cd push the old directory onto the directory stack
setopt FLOW_CONTROL           # use Ctrl+S / Ctrl+Q to stop and continue flow
setopt SH_WORD_SPLIT          # split fields on unquoted parameter expansions (bash compatibility)
# will be used for kill-word and others
WORDCHARS='~!#$%^&*(){}[]<>?.+;-_'

### History
HISTSIZE=1000000
SAVEHIST=1000000
HISTFILE=~/.cache/zsh/history
# History command configuration
setopt SHARE_HISTORY          # share history between terminals
setopt EXTENDED_HISTORY       # record timestamp of command in HISTFILE
setopt HIST_EXPIRE_DUPS_FIRST # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt HIST_IGNORE_SPACE      # do not enter commands into history that start with a space
setopt HIST_VERIFY            # show command with history expansion to user before running it

### Loading

# Use vi command mode; must be done before loading everything else
bindkey -v

# bashcompinit and compinit must be loaded before calling complete (via load-common-files)
autoload -U +X bashcompinit && bashcompinit
# workaround macOS compatibility issues by creating a second .zcompdump file
# for further explanation, see compinit's invocation in lib/zsh/completion.zsh
[[ "${OSTYPE}" == darwin* ]] && autoload -Uz compinit && compinit -u -C -d "${HOME}/.zcompdump"
source "${DOTFILES_DIR}"/config/load-common-files.sh
# completion.zsh uses vars defined in load-common-files.
# Also, we need to bashcompinit and compinit in completion.zsh
source "${DOTFILES_DIR}"/lib/zsh/completion/completion.zsh

eval "$(fzf --zsh)"
# Meta-S - Paste the selected file path(s) into the command line
bindkey -M emacs '\es' fzf-file-widget
bindkey -M vicmd '\es' fzf-file-widget
bindkey -M viins '\es' fzf-file-widget

# load extra completions overwriting existing ones
source "${DOTFILES_DIR}"/lib/zsh/completion/_git

# Needs to be defined after alias `reload` in config-shell.sh (sourced through load-common-files.sh)
alias reload='reload_zsh'

# IMPORTANT: libs dir-navigation and buffer-toggle must be loaded
# after configure-vi-mode / `bindkey -v` to properly work (hence the numbering)
for library in "${DOTFILES_DIR}"/lib/zsh/*.zsh; do
    source "${library}"
done

### zsh plugins

# https://github.com/hlissner/zsh-autopair/
# Must be sourced before syntax highlighting
if [[ "${OSTYPE}" == darwin* ]]; then
    autopair_path="${HOMEBREW_PREFIX}/share/zsh-autopair/autopair.zsh"
else
    autopair_path="${HOME}/bin/zsh-autopair/autopair.zsh"
fi
if [[ "${AUTO_PAIR}" == 'on' && -f "${autopair_path}" ]]; then
    # otherwise AUTOPAIR_BKSPC_WIDGET would be set to vi-backward-delete-char,
    # which breaks after reload with a last command
    AUTOPAIR_BKSPC_WIDGET="backward-delete-char"
    source "${autopair_path}"
fi

# fzf-tab needs to be sourced after compinit, but before plugins which will
# wrap widgets like zsh-autosuggestions or fast-syntax-highlighting
if [[ -f ~/bin/fzf-tab/fzf-tab.zsh ]]; then
    source ~/bin/fzf-tab/fzf-tab.zsh
    zstyle ':fzf-tab:*' prefix '● '
    zstyle ':fzf-tab:complete:*' extra-opts --preview-window=:hidden
    # must be loaded after fzf-tab is sourced and after vi mode is enabled
    evaluate_fzf_tab
fi

if [[ "${OSTYPE}" == darwin* ]]; then
    autosuggestion_path="${HOMEBREW_PREFIX}/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
else
    autosuggestion_path=/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
fi
if [[ "${AUTO_SUGGESTIONS}" == 'on' && -f "${autosuggestion_path}" ]]; then
    source "${autosuggestion_path}"
fi

autoload -U url-quote-magic bracketed-paste-magic
zle -N self-insert url-quote-magic
zle -N bracketed-paste bracketed-paste-magic

# Fix conflict of bracketed-paste-magic with autosuggestions
# https://forum.endeavouros.com/t/tip-better-url-pasting-in-zsh/6962
pasteinit() {
    OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
    zle -N self-insert url-quote-magic
}
pastefinish() {
    zle -N self-insert "${OLD_SELF_INSERT}"
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish
ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(bracketed-paste)

if [[ "${OSTYPE}" == darwin* ]]; then
    syntax_highlighting_path="${HOMEBREW_PREFIX}/opt/zsh-fast-syntax-highlighting/share/zsh-fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"
else
    syntax_highlighting_path="${HOME}/bin/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"
fi
if [[ "${SYNTAX_HIGHLIGHTING}" == 'on' && -f "${syntax_highlighting_path}" ]]; then
    zle -N up-line-or-beginning-search
    zle -N down-line-or-beginning-search
    source "${syntax_highlighting_path}"
fi

### key bindings - must be loaded last
#   - otherwise auto-pair breaks after a reload
#   - and we want to overwrite some bindings of the plugins
source "${DOTFILES_DIR}"/config/key-bindings.zsh

# must be called after 7-reload.zsh (contains set_last_command()
# and key-bindings.zsh (contains bind-backspace()) have been sourced
set_last_command
