" Inspired by https://github.com/amix/vimrc

if !empty(glob("$VIMRUNTIME/defaults.vim"))
    source $VIMRUNTIME/defaults.vim
endif

""""""""""""""
""""" Settings
""""""""""""""

" save viminfo in vim's directory
if isdirectory("$HOME/.config/nvim")
    set viminfo+=n~/.config/nvim/viminfo
endif
" Use global directory for swap files
set directory^=/tmp/vimswap//
" set encoding to utf-8
set encoding=utf-8
" Expand env vars in filenames containing curly brackets, e.g. ${HOME}/file.txt
set isfname+={,}
" minimal number of lines to always keep visible above the cursor when scrolling
set scrolloff=5
" set history size
set history=500
" use 4 spaces when pressing tab and use auto-/smart indenting
set tabstop=4
set shiftwidth=4
set autoindent
set smartindent
" use spaces instead of tabs
set expandtab
" show number in current line and relative number in all others
set number relativenumber
" block: Ctrl+v marks whole block (even across EOL) / all (extends block mode): cursor may be additionally placed after EOL
set virtualedit=block
" when turned on, show \n, \t and space
set listchars=eol:$,tab:>-,space:·
" allow a new buffer to be created when current one has not been saved yet
set hidden
" autocompletion
set wildmenu
set wildmode=list:longest,full
" Search down into subfolders. Provides tab-completion for all file-related tasks.
set path+=**
" highlight search results and use search behaviour as in modern browsers
" for smartcase to work, ignorecase must be set first
set hlsearch
set incsearch
set ignorecase
set smartcase
" Enable modelines in files
set modeline
" Always show current filename in status
set statusline=%F%m%r%<\ %=%l/%L\ [%p%%],\ Row\ %v
highlight statusline ctermbg=white ctermfg=black
set laststatus=2
" Use a more visible color when highlighting matching parens
highlight MatchParen ctermbg=blue
" Split more naturally
set splitbelow
set splitright
" Enable mouse support
set mouse=a
" Enable cursor blinking
set guicursor+=a:blinkwait175-blinkoff150-blinkon175
" Always highlight current line
set cursorline
highlight CursorLine cterm=NONE ctermbg=236
highlight CursorLineNr cterm=NONE ctermbg=236 ctermfg=251
highlight LineNr cterm=None ctermbg=235 ctermfg=246

" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view

""""""""""""""""""
""""" Key mappings
""""""""""""""""""

" Keymappings from NeoVim: :help default-mappings

" https://vi.stackexchange.com/questions/2350/how-to-map-alt-key
" However, mapping Alt+J/K leads to bad behaviour after quickly pessing Esc + J/K in insert mode, because the mapping then overlaps
if !has('nvim')
    execute "set <M-i>=\ei"
    execute "set <M-h>=\eh"
    execute "set <M-l>=\el"
endif

" Define a leader key
let mapleader=' '

" Toggle explorer
noremap <F6> :20%Lexplore<CR>

" Toggle showing invisible characters
noremap <leader>q :set list!<CR><C-L>

" Ctrl+j/k moves current line down/up
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" Enable blocked mappings on German keyboards for tag navigation
nnoremap <leader>l <C-]>
nnoremap g<leader>l g<C-]>

" Navigate tag back. In most Console Hosts, ^t is mapped to new tab - therefore an additional mapping is necessary.
nmap <leader>h <C-t>

" Copy until end of line
nnoremap Y y$

" Paste register below/above current line
nnoremap <leader>o o<Esc>p
nnoremap <leader>O O<Esc>p

" Paste register 0
nnoremap <leader>p "0p
nnoremap <leader>P "0P
vnoremap <leader>p "0p
vnoremap <leader>P "0P

" Paste over currently selected text without yanking it
" Differentiate if cursor is at EOL or not
vnoremap <expr> p col(".") == col("$")-1 ? '"_dp"':'"_dP'

" Count number of occurrences of word under cursor
nnoremap <leader>n :%s/<c-r><c-w>//gn<CR>
" Count number of occurrences of visual selection
vnoremap <leader>n :<c-u>%s/<c-r>*//gn<CR>

" Keep selection after indent in visual mode
vnoremap < <gv
vnoremap > >gv

" Reload config
nnoremap <leader><C-r> :so $HOME/.vimrc<BAR>nohlsearch<CR>

" Disable Ex Mode
nnoremap Q <Nop>

" Replace all occurrences of current word
nmap <leader>x :%s/<C-r><C-w>//g<Left><Left>

" Insert single character
nnoremap <M-i> i_<Esc>r

" Inspired by https://github.com/nvie/vim-togglemouse/blob/master/plugin/toggle_mouse.vim
" Toggles mouse mode and line numbers
" Only do this when not done yet for this buffer
if !exists('b:loaded_toggle_copy_mode')
    let b:loaded_toggle_copy_mode = 1
    function! s:ToggleCopyMode()
        if &mouse == ''
            let &mouse = 'a'
            set number
            set relativenumber
            echo 'Mouse is for Vim (navigation)'
        else
            let &mouse = ''
            set nonumber
            set norelativenumber
            echo 'Mouse is for terminal (copy)'
        endif
    endfunction
    noremap <F12> :call <SID>ToggleCopyMode()<CR>
    inoremap <F12> <Esc>:call <SID>ToggleCopyMode()<CR>a
endif

" Fix home/end for neovim in tmux with xterm-256color-italic
" https://github.com/neovim/neovim/issues/9012
" Using <C-a>, <C-b> would break native vim functionality, so using function keys instead
nnoremap <F7> <Home>
nnoremap <F8> <End>
vnoremap <F7> <Home>
vnoremap <F8> <End>
inoremap <F7> <Home>
inoremap <F8> <End>

" Center search results
nnoremap n nzz
nnoremap N Nzz

" Switch buffers
nnoremap <M-h> :bprevious<CR>
nnoremap <M-l> :bnext<CR>

" Save buffer
nnoremap <leader>s :w<CR>

" Close buffer
nnoremap <leader>w :bd<CR>

" Close all buffers except the current one
nnoremap <leader>e :%bd\|e#\|bd#<CR>

" Redraw screen, cancel search highlighting, do diff update
nnoremap <C-L> <Cmd>nohlsearch<Bar>diffupdate<CR><C-L>

" Cancel search highlighting. Works only in nvim.
if has('nvim')
    nnoremap <ESC> :nohlsearch<Bar>:echo<CR>
endif

""""" dotfiles Keymappings

nnoremap <F1> :e ${DOTFILES_DIR}/README.md<CR>

function! s:GetVisualSelection()
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)
    if len(lines) == 0
        return ''
    endif
    let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]
    return join(lines, "\n")
endfunction

" Open current selection as a GitHub repo in the browser (WSL only)
function! s:GHR() range
    let selection = s:GetVisualSelection()
    call system('${DOTFILES_DIR}/lib/git-tools.sh --open ' . selection)
endfunction
vnoremap <leader>z :call <SID>GHR()<CR>

""""""""""""""
""""" Commands
""""""""""""""

" Create the `tags` file (may need to install ctags first)
command! MakeTags !ctags -R .

""""""""""""""""
""""" Automation
""""""""""""""""

" Jump to the last position when opening a file
autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$")
    \ |   exe "normal! g'\""
    \ | endif

" Delete trailing white space on save
function! StripTrailingWhitespace()
    let save_cursor = getpos('.')
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfunction
augroup StripTrailingWhitespace
    autocmd!
    " Don't strip markdown files (where trailing whitespace indicates a line break)
    let fileTypeToIgnore = ['markdown']
    autocmd BufWritePre * if index(fileTypeToIgnore, &ft) < 0 | call StripTrailingWhitespace() | endif
augroup END

" In mouse mode, active tab shows relative line numbers, inactive shows absolute line numbers
augroup numbertoggle
    autocmd!
    " do not switch relativenumber in insert mode
    autocmd BufEnter,FocusGained,InsertLeave * if mode() != 'i' && &mouse == 'a' | set relativenumber | endif
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" Set cursor back to beam (vertical bar) when leaving vim.
" Not necessary for zsh, where the cursor is set automatically, but for bash.
autocmd VimLeave * set guicursor=a:ver1-blinkon1

" Use defined clipboard
augroup DynamicYank
    autocmd!
    autocmd TextYankPost *
        \   if v:event.operator ==# 'y'
        \ |     call system('${DOTFILES_DIR}/lib/clipboard.sh --copy', @0)
        \ | endif
augroup END

"""""""""""""
""""" Plugins
"""""""""""""
if empty(glob('$HOME/.vim/plugged'))
    autocmd VimEnter * PlugInstall --sync
endif
let result = system('${DOTFILES_DIR}/lib/ssh-manager.sh --ssh-is-remote-session')
let is_ssh = v:shell_error == 0
let result = system('command -v ctags')
let ctags_installed = v:shell_error == 0
let ctags_version = system('ctags --version | grep Universal')
if has('nvim') && !empty(glob('$HOME/.config/nvim/autoload/plug.vim')) ||
   \ !empty(glob('$HOME/.vim/autoload/plug.vim'))
    call plug#begin('~/.vim/plugged')
        Plug 'dense-analysis/ale'
        " Workaround: latest which-key needs nvim >= 0.9; installing latest version supporting older nvim
        Plug 'folke/which-key.nvim', { 'commit': 'c1958e2' }
        Plug 'ggandor/leap.nvim'
        Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
        Plug 'junegunn/fzf.vim'
        Plug 'LucHermitte/lh-brackets'
        Plug 'LucHermitte/lh-vim-lib', { 'commit': '17d75d6' }
        if !is_ssh && ctags_installed && stridx(ctags_version, "Universal") >= 0
            Plug 'ludovicchabant/vim-gutentags'
        endif
        Plug 'luukvbaal/nnn.nvim'
        Plug 'tpope/vim-fugitive'       " used by fzf.vim
        Plug 'tpope/vim-repeat'
        Plug 'tpope/vim-surround'
        if empty(glob('~/.vim/plugged/YouCompleteMe')) ||
            \ !empty(glob('~/.vim/plugged/YouCompleteMe/third_party/ycmd/ycm_core*.so'))
            Plug 'ycm-core/YouCompleteMe'
        endif
        " Themes
        Plug 'arcticicestudio/nord-vim'
        Plug 'drewtempelmeyer/palenight.vim'
        Plug 'lifepillar/vim-solarized8'
        Plug 'morhetz/gruvbox'
        Plug 'sainnhe/everforest'
        Plug 'sickill/vim-monokai'
        " Status line and tab line
        Plug 'kyazdani42/nvim-web-devicons' " used for lualine
        Plug 'nvim-lualine/lualine.nvim'
        if !has('nvim')
            " do not load (and install) by default - only manually
            " otherwise nvim will load both airline and lualine
            Plug 'vim-airline/vim-airline'
        endif
    call plug#end()
endif
if !empty(glob('$HOME/.vim/plugged/fzf.vim'))
    " Workaround for https://github.com/junegunn/fzf.vim/issues/1399
    autocmd FileType fzf autocmd WinLeave <buffer> close!

    " Colorize current line the same as CursorLine
    " https://github.com/junegunn/fzf/blob/master/README-VIM.md#explanation-of-gfzf_colors
    let g:fzf_colors = { 'bg+': ['bg', 'CursorLine', 'CursorColumn'] }

    " fzf related mappings
    " fuzzy find files in the working directory (where you launched Vim from)
    nmap <leader>f :Files<CR>
    " fuzzy find lines in the current file
    nmap <leader>/ :BLines<CR>
    " fuzzy find an open buffer
    nmap <leader>b :Buffers<CR>
    " fuzzy find text in the working directory
    nmap <leader>r :Rg<CR>
    " fuzzy find Vim commands (like Ctrl-Shift-P in VSC)
    nmap <leader>c :Commands<CR>
    " fuzzy find git commits
    nmap <leader>g :Commits<CR>
    " fuzzy find history
    nnoremap <leader>y :History<CR>
    " fuzzy find in tags
    function! s:SelectTags(scope, mode) range
        let tagCommand = a:scope == 'buffer' ? 'BTags' : 'Tags'
        if a:mode == 'n'
            execute tagCommand
        elseif a:mode == 'v'
            let selection = s:GetVisualSelection()
            execute tagCommand . ' ' . selection
        endif
    endfunction
    vnoremap <leader>t :call <SID>SelectTags('buffer', 'v')<CR>
    nnoremap <leader>t :call <SID>SelectTags('buffer', 'n')<CR>
    vnoremap <leader>gt :call <SID>SelectTags('project', 'v')<CR>
    nnoremap <leader>gt :call <SID>SelectTags('project', 'n')<CR>


    " fzf Files: use options of FZF_DEFAULT_OPTS
    command! -bang -nargs=? Files call fzf#vim#files(<q-args>, {}, <bang>0)
    " fzf Commits: make preview 50%
    command! -bang -nargs=? Commits call fzf#vim#commits({'options': ['--preview-window', 'right:50%', '--preview', 'echo {} | cut --delimiter=" " --fields=2 | xargs git show --color=always | $(git config core.pager || echo "less")']}, <bang>0)
    " fzf BLines & Commands: hide preview
    command! -bang -nargs=? BLines call fzf#vim#buffer_lines({'options': ['--preview-window', 'right:0%:hidden']}, <bang>0)
    command! -bang -nargs=? Commands call fzf#vim#commands({'options': ['--preview-window', 'right:0%:hidden']}, <bang>0)

    " let Rg find hidden files
    command! -bang -nargs=* Rg
      \ call fzf#vim#grep(
      \   'rg --hidden --column --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
      \   fzf#vim#with_preview(), <bang>0)

    " fuzzy find mappings
    nnoremap <silent> <leader><tab> :<c-u>call fzf#vim#maps('n', {'options': ['--preview-window', 'right:0%:hidden']})<cr>
    xnoremap <silent> <leader><tab> :<c-u>call fzf#vim#maps('x', {'options': ['--preview-window', 'right:0%:hidden']})<cr>
    onoremap <silent> <leader><tab> :<c-u>call fzf#vim#maps('o', {'options': ['--preview-window', 'right:0%:hidden']})<cr>
    " Insert mode completion
    imap <c-x><c-f> <plug>(fzf-complete-path)
    imap <expr> <c-x><c-l> fzf#vim#complete#line({'options': ['--preview-window', 'right:0%:hidden']})
    " Complete from words of current buffer
    inoremap <expr> <C-k> fzf#vim#complete(fzf#wrap({
                                            \ 'source': uniq(sort(split(join(getline(1,'$'),"\n"),'\W\+'))),
                                            \ }))
endif
if !empty(glob('$HOME/.vim/plugged/ale'))
    let g:ale_sign_error = ''
    let g:ale_sign_warning = ''
    let g:ale_linters = {'markdown': [], 'sh': ['shellcheck'], 'yaml': ['yamllint']}
    " we prefer some of the values yaml allows: https://yaml.org/type/bool.html
    " https://yamllint.readthedocs.io/en/stable/rules.html?highlight=truthy#module-yamllint.rules.truthy
    let g:ale_yaml_yamllint_options ='-d "{extends: default, rules: {truthy: {allowed-values: [\"true\", \"false\", \"on\", \"off\"]}}}"'
    noremap <F5> :ALEToggle<CR>
    nmap <silent> <C-Up> <Plug>(ale_previous_wrap)
    nmap <silent> <C-Down> <Plug>(ale_next_wrap)
endif
if !empty(glob('$HOME/.vim/plugged/lh-brackets'))
    let g:marker_define_jump_mappings=0
    let g:usemarks=0
    " do not replace ~ by </del> in markdown files
    let g:cb_disable_default = { '~': 'i' }
endif
if !empty(glob('$HOME/.vim/plugged/which-key.nvim')) && has('nvim')
    lua << EOF
    require('which-key').setup {
        window = {
            border = "single", -- none, single, double, shadow
            position = "bottom", -- bottom, top
        },
    }
EOF
endif
if !empty(glob('$HOME/.vim/plugged/leap.nvim')) && has('nvim')
    lua require('leap').add_default_mappings()
endif
if !empty(glob('$HOME/.vim/plugged/lualine.nvim')) && has('nvim')
    lua << EOF
    require('lualine').setup {
        tabline = {
            lualine_a = {'buffers'}
        }
    }
EOF
endif
if !empty(glob('$HOME/.vim/plugged/vim-airline')) && !has('nvim')
    let g:airline_powerline_fonts = 1
    let g:airline#extensions#tabline#enabled = 1
endif
if !empty(glob('$HOME/.vim/plugged/nnn.nvim')) && has('nvim')
    lua require("nnn").setup()
    noremap <F6> :NnnExplorer<CR>
endif
if !empty(glob('$HOME/.vim/plugged/YouCompleteMe'))
    let g:ycm_key_list_select_completion = ['<TAB>', '<Down>', '<C-j>']
    let g:ycm_key_list_previous_completion = ['<S-TAB>', '<Up>', '<C-k>']
    let g:ycm_key_list_stop_completion = ['<C-y>', '<CR>']
    let g:ycm_collect_identifiers_from_tags_files = 1
endif

"""""""""""""
""""" Theming
"""""""""""""

if !empty(glob('$HOME/.vim/plugged/everforest'))
    " Set contrast. Available values: 'hard', 'medium'(default), 'soft'
    " Needs to be placed before setting colorscheme.
    let g:everforest_background = 'soft'
    let g:everforest_better_performance = 1
endif
if !empty(glob('$HOME/.vim/plugged/palenight.vim'))
    " https://github.com/drewtempelmeyer/palenight.vim#overriding-colors
    let g:palenight_color_overrides = { 'cursor_grey': { 'gui': '#4B5170', "cterm": "8", "cterm16": "8" } }
    let g:palenight_terminal_italics = 1
endif
if !empty(glob('$HOME/.vim/plugged/gruvbox'))
    let g:gruvbox_italic = 1
endif

if has('termguicolors')
    set termguicolors
endif

let s:schemes = ["\e[33mLight & dark themes \e[34mDark only schemes \e[100mBackgrounds",
               \ "\e[33meverforest", "\e[33mgruvbox", "\e[33msolarized8", "\e[33mmonokai",
               \ "\e[34mnord", "\e[34mpalenight",
               \ "\e[100mdark", "\e[100mlight"]

" Initialize colorscheme
let s:env_scheme = $THEME
if s:env_scheme == '' || s:env_scheme == 'solarized'
    let s:env_scheme = 'solarized8'
endif
silent! exec 'colorscheme' s:env_scheme
let g:current_scheme = s:env_scheme

" Initialize background
let s:env_background = $BACKGROUND
if s:env_background == 'system'
    let s:env_background = ''
    if !empty(glob('$HOME/.cache/background.cache'))
        let s:env_background = readfile(glob('$HOME/.cache/background.cache'), '', 1)[0]
    endif
elseif s:env_background == ''
    let s:env_background = 'dark'
endif
let &background = s:env_background
let g:current_background = s:env_background

function! s:GetPlainSchemes()
    let plain_schemes = copy(s:schemes)
    call remove(plain_schemes, 0)
    let i = 0
    while i < len(plain_schemes)
        let plain_schemes[i] = substitute(plain_schemes[i], "\e[\[0-9\]\*m", "", "")
        let i += 1
    endwhile
    call remove(plain_schemes, index(plain_schemes, 'dark'))
    call remove(plain_schemes, index(plain_schemes, 'light'))
    return plain_schemes
endfunction

function! s:CycleColorscheme(direction)
    let plain_schemes = s:GetPlainSchemes()
    let current_index = index(plain_schemes, g:current_scheme)
    let current_index += a:direction
    if current_index == len(plain_schemes)
        let current_index = 0
    endif
    call s:SetScheme(plain_schemes[current_index])
endfunction

function! s:SetBackground(bg)
    let g:current_background = a:bg
    let &background = g:current_background
    call system('${DOTFILES_DIR}/lib/configurer.sh --configure background ' . g:current_background)
    echon "\rBackground: " g:current_background ' ('g:current_scheme')'
    echon
endfunction

function! s:SetScheme(selection) abort
    let plain_selection = substitute(a:selection, "\e[\[0-9\]\*m", "", "")
    if plain_selection == "dark" || plain_selection == "light"
        call s:SetBackground(plain_selection)
        return
    endif
    " this fixes toggling background for themes that do not have light background
    let &background = g:current_background
    let g:current_scheme = plain_selection
    exec 'colorscheme' g:current_scheme
    let external_schema = g:current_scheme == 'solarized8' ? 'solarized' : g:current_scheme
    call system('${DOTFILES_DIR}/lib/configurer.sh --configure theme ' . external_schema)
    echon "\rScheme: " g:current_scheme ' ('g:current_background')'
    echon
endfunction

function! s:ToggleBackground()
    if g:current_background ==# 'dark'
        call s:SetBackground('light')
    else
        call s:SetBackground('dark')
    endif
endfunction

function! s:ThemeChooser()
    " source must be string or list (funcref not supported), see https://github.com/junegunn/fzf/blob/master/README-VIM.md#fzfrun
    call fzf#run(#{
                \ source: s:schemes,
                \ sink: function('s:SetScheme'),
                \ options: ['--preview-window=:hidden', '--header', '', '--prompt=Themes> ', '--no-multi', '--header-lines=1', '--no-mouse'],
                \ window: { 'width': 0.5, 'height': 0.5 }
                \})
endfunction

command! CycleColorschemePrev :call s:CycleColorscheme(-1)
command! CycleColorschemeNext :call s:CycleColorscheme(1)
command! ToggleBackground :call s:ToggleBackground()
command! ThemeChooser :call s:ThemeChooser()
nnoremap <silent> <F3> :CycleColorschemePrev<CR>
nnoremap <silent> <F4> :CycleColorschemeNext<CR>
nnoremap <silent> <leader><F3> :ToggleBackground<CR>
nnoremap <silent> <leader><F4> :ThemeChooser<CR>
