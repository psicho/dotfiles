#!/usr/bin/env bash

# https://github.com/ycm-core/YouCompleteMe#linux-64-bit

if [[ "$1" != "--minimal" && "$1" != "--all" ]]; then
    echo "Please provide either --minimal or --all as argument."
    exit 1
fi

if [[ "$1" == "--all" ]]; then
    sudo apt-get update
    sudo apt-get install --yes build-essential cmake vim-nox python3-dev mono-complete golang nodejs openjdk-17-jdk openjdk-17-jre npm
    "${HOME}/.vim/plugged/YouCompleteMe/install.py" --all
elif [[ "$1" == "--minimal" ]]; then
    sudo apt-get update
    sudo apt-get install --yes build-essential cmake python3-dev
    "${HOME}/.vim/plugged/YouCompleteMe/install.py"
fi
