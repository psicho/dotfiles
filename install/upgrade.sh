#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091,SC2031

__pre_upgrade() {
    if [[ "$1" == "-h" || "$1" == "--help" ]]; then
        source "${DOTFILES_DIR}/lib/help.sh"
        colorize-help <<-EOF
			Upgrade all dotfiles dependencies to their latest versions

			Usage:
			    upgrade.sh [options ...]

			Examples:
			    upgrade.sh -h
			    upgrade.sh -a
			    source upgrade.sh -l

			Options:
			    -a, --all           Upgrade extra packages (if not provided: do not upgrade them)
			    -h, --help          Print this help
			    -l, --load-only     Load this file only. Must be the sole arg. Used for upgrading: source this file with -l and call specific functions.
		EOF
        exit 0
    fi

    cd "${DOTFILES_DIR}" || exit 1
    git pull
    "${DOTFILES_DIR}/install/upgrade.sh" --do_upgrade "$@"
}

__do_upgrade() {
    {
        cat <<-EOF
			${COL_MAGENTA_FG}############################
			${COL_MAGENTA_FG}$(date +'%Y-%m-%d %H:%M:%S %Z')
			${COL_MAGENTA_FG}Starting dotfiles upgrade...
			${COL_MAGENTA_FG}############################${COL_RESET}
		EOF

        if [[ ! "$1" == "-a" && ! "$1" == "--all" ]]; then
            echo "${COL_YELLOW_FG}Not upgrading extra packages. If you want to do so, call this script with '-a' or '--all'.${COL_RESET}"
        fi

        source "${DOTFILES_DIR}/install/bootstrap.sh" --load-only

        if [[ "${OSTYPE}" == darwin* ]]; then
            __do_upgrade_macos "$@"
        else
            __do_upgrade_linux "$@"
        fi

        # upgrade Vim Plug and Vim Plugins
        if command -v nvim; then
            nvim --headless +PlugUpgrade +qall
        fi
        /usr/bin/env vim +PlugUpgrade +qall
        /usr/bin/env vim +PlugUpdate +qall

        # update configuration
        __configure_gitignore "${DOTFILES_DIR}"
        __merge_config

        # migrate themes after config has been updated with new theme keys
        __migrate_themes

        # update symlinks
        "${DOTFILES_DIR}/install/unlink-dotfiles.sh"
        "${DOTFILES_DIR}/install/symlink-dotfiles.sh"

        # delete zcompdump if existing, so that autocompletion will be regenerated with next login
        rm --force ~/.cache/zsh/zcompdump ~/.zcompdump
    } 2>&1 | tee --append "${HOME}/dotfiles-install.log"
    # reload
    exec zsh --login
}

__do_upgrade_linux() {
    # migrate
    # fill this section with function calls whenever necessary
    __migrate_to_venv
    __migrate_exa_to_eza
    __migrate_nvim_symlink
    __remove_deprecated_tmux_plugin

    # upgrade debian packages
    sudo apt-get update
    sudo apt-get upgrade --yes

    # upgrade external tools from bootstrap
    __install_external_tools

    # upgrade extra packages
    if [[ "$1" == "-a" || "$1" == "--all" ]]; then
        for directory in "${HOME}/bin/"*/ ; do
            cd "${directory}" || continue
            git remote prune origin
            git pull
        done
        "${DOTFILES_DIR}/install/install-extra.sh" --no-log
    fi
}

__do_upgrade_macos() {
    brew update
    brew upgrade

    # upgrade external tools from bootstrap
    __install_starship
    __configure_bat
    __install_terminfo

    # upgrade extra packages
    if [[ "$1" == "-a" || "$1" == "--all" ]]; then
        for directory in "${HOME}/bin/"*/ ; do
            cd "${directory}" || continue
            git pull
        done
        cp "${DOTFILES_DIR}/config/lazygit-config.yml" "${HOME}/.config/lazygit/config.yml"
        "${HOMEBREW_PREFIX}/opt/tpm/share/tpm/bin/install_plugins"
        "${HOME}/install/install-youtube-dl.sh"
    fi
}

####################
# general migrations
####################

# migrate config.yml to newest template retaining the configured values
__merge_config() {
    if [[ -f ~/.config/dotfiles/config.yml ]]; then
        echo "${COL_CYAN_FG}Merging config.yml...${COL_RESET}"
        mv ~/.config/dotfiles/config.yml ~/.config/dotfiles/config.yml.old
        cp "${DOTFILES_DIR}/config/config.template.yml" ~/.config/dotfiles/config.yml
        if [[ -z "${BASH_VERSION}" ]]; then
            bash -c "source ${DOTFILES_DIR}/install/upgrade.sh --load-only && __merge_config_recursive '.'"
        else
            __merge_config_recursive '.'
        fi
        rm ~/.config/dotfiles/config.yml.old
    fi
}

# works only in bash
__merge_config_recursive() {
    local current_key="$1"
    local new_config_file="${HOME}/.config/dotfiles/config.yml"
    local old_config_file="${HOME}/.config/dotfiles/config.yml.old"
    local keys next_key old_value query
    tag=$(yq eval "${current_key} | tag" "${new_config_file}")
    case "${tag}" in
        '!!map')
            keys=$(yq eval "${current_key} | keys | .[]" "${new_config_file}")
            for key in ${keys}; do
                old_has_key=$(yq eval "${current_key} | has(\"${key}\")" "${old_config_file}")
                [[ "${old_has_key}" == "false" ]] && continue
                if [[ "${current_key}" == "." ]]; then
                    next_key=".${key}"
                else
                    next_key="${current_key}.${key}"
                fi
                __merge_config_recursive "${next_key}"
            done
            ;;
        '!!seq')
            old_value=$(yq eval "${current_key}.[]" "${old_config_file}")
            # replace newline by ", " and surround with ""
            query="\"${old_value//$'\n'/\", \"}\""
            query="${current_key} = [${query}]"
            yq --inplace eval "${query}" "${new_config_file}"
            ;;
        *)
            old_value=$(yq eval "${current_key}" "${old_config_file}")
            query="${current_key} = \"${old_value}\""
            yq --inplace eval "${query}" "${new_config_file}"
            ;;
    esac
}

__migrate_themes() {
    local theme background

    # copy terminal themes to Microsoft Windows Terminal directory
    copy_terminal_themes

    theme="$(yq eval '.theming.theme' "${HOME}/.config/dotfiles/config.yml")"
    background="$(yq eval '.theming.background' "${HOME}/.config/dotfiles/config.yml")"
    source "${DOTFILES_DIR}/theming/set-theme.sh"
    set_theme "${theme}" "${background}"
}

#############################
# version specific migrations
#############################

__migrate_to_venv() {
    pip3 uninstall --yes mako pyyaml
    __prepare_python "${DOTFILES_DIR}"
}

__migrate_exa_to_eza() {
    if command -v exa &>/dev/null; then
        source "${DOTFILES_DIR}/lib/library/system.sh"
        if is_arm; then
            sudo apt-get remove --yes exa
            sudo apt-get autoremove --yes
        else
            sudo rm /usr/local/bin/exa \
                    /usr/share/fish/completions \
                    /usr/share/bash-completion/completions/exa \
                    /usr/local/share/zsh/site-functions/_exa \
                    /usr/local/share/man/man1/exa.1 \
                    /usr/local/share/man/man5/exa_colors.5
        fi
    fi
}

__migrate_nvim_symlink() {
    rm --force "${HOME}/.config/nvim/init.vim"
    ln --verbose --symbolic "${HOME}/.vimrc" "${HOME}/.config/nvim/init.vim"
}

__remove_deprecated_tmux_plugin() {
    rm --recursive --force "${HOME}/bin/tmux-mem-cpu-load"
}

#############################
# main
#############################

[[ "$1" == "-l" || "$1" == "--load-only" ]] && return 0

if [[ -z "${BASH_VERSION}" ]]; then
    echo "Upgrade must be executed in Bash."
    return 1
fi

if [[ "$1" == "--do_upgrade" ]]; then
    shift
    __do_upgrade "$@"
else
    __pre_upgrade "$@"
fi
