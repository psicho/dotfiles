#!/usr/bin/env bash

# shellcheck disable=SC1090,SC1091
source "${DOTFILES_DIR}"/lib/library/github.sh

machine=$(uname --machine)
filename_template="https://github.com/solidiquis/erdtree/releases/download/v%s/erd-v%s-${machine}-unknown-linux-musl.tar.gz"
release_url=$(get-latest-release-url "${filename_template}")
curl --location "${release_url}" --output /tmp/erd.tar.gz
tar --extract erd --gunzip --verbose --file=/tmp/erd.tar.gz --directory=/home/markus
rm /tmp/erd.tar.gz
sudo mv erd /usr/local/bin

erd --completions zsh | sudo tee /usr/local/share/zsh/site-functions/_erd >/dev/null
erd --completions bash | sudo tee /usr/share/bash-completion/completions/erd >/dev/null
