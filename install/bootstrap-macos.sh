#!/usr/bin/env bash
# shellcheck disable=SC1091

__script_dir=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
__dotfiles_dir=$(dirname "${__script_dir}")

echo "${COL_CYAN_FG}Installing Homebrew...${COL_RESET}"
/usr/bin/env bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
eval "$(/opt/homebrew/bin/brew shellenv)"

echo "${COL_CYAN_FG}Installing bootstrap tools...${COL_RESET}"
brew install \
        fd \
        jq \
        ripgrep \
        tmux \
        yq \
        tree \
        bash \
        bash-completion \
        coreutils \
        gnu-getopt \
        gnu-sed \
        gnu-tar \
        grep \
        gawk \
        findutils \
        less \
        telnet \
        fzf \
        bat \
        man-db \
        watch \
        util-linux \
        7zip \
        lzip \
        rar

echo "${COL_CYAN_FG}Installing symbolic links for gnu binaries...${COL_RESET}"
# use short options as long as gnu binaries are not symlinked
ln -s "${HOMEBREW_PREFIX}"/bin/fd /usr/local/bin/fdfind
ln -s "${HOMEBREW_PREFIX}"/bin/gsed /usr/local/bin/sed
ln -s "${HOMEBREW_PREFIX}"/bin/gtar /usr/local/bin/tar
ln -s "${HOMEBREW_PREFIX}"/bin/gxargs /usr/local/bin/xargs
ln -s "${HOMEBREW_PREFIX}"/bin/gfind /usr/local/bin/find
ln -s "${HOMEBREW_PREFIX}"/opt/util-linux/bin/column /usr/local/bin/column
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/gcatman /usr/local/bin/catman
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/glexgrog /usr/local/bin/lexgrog
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/gman /usr/local/bin/man
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/gman-recode /usr/local/bin/man-recode
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/gmandb /usr/local/bin/mandb
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/gmanpath /usr/local/bin/manpath
ln -s "${HOMEBREW_PREFIX}"/opt/man-db/bin/gwhatis /usr/local/bin/whatis

yq shell-completion zsh | sudo tee /usr/local/share/zsh/site-functions/_yq >/dev/null

source "${__dotfiles_dir}/config/gnu-binaries.sh"
source "${__dotfiles_dir}/install/bootstrap.sh" --load-only
__prepare_python "${__dotfiles_dir}"
__install_dotfiles
__install_starship
__configure_bat
__install_terminfo

echo "${COL_CYAN_FG}Installing extra tools...${COL_RESET}"
brew install \
        cmatrix \
        gh \
        lua \
        shellcheck \
        universal-ctags \
        yamllint \
        zsh-autosuggestions \
        neovim \
        vivd \
        lazygit \
        lazydocker \
        tldr \
        as-tree \
        git-delta \
        dive \
        eza \
        z.lua \
        bat-extras \
        forgit \
        zsh-fast-syntax-highlighting \
        zsh-autopair \
        prettyping \
        tpm \
        erdtree

echo "${COL_CYAN_FG}Configuring vim and nvim...${COL_RESET}"
curl --fail --location --output "${HOME}/.vim/autoload/plug.vim" --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
/usr/bin/env vim +PlugInstall +qall
curl --fail --location --output "${HOME}/.config/nvim/autoload/plug.vim" --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
rm --force "${HOME}/.config/nvim/init.vim"
ln --verbose --symbolic "${__dotfiles_dir}/dotfiles/.vimrc" "${HOME}/.config/nvim/init.vim"

echo "${COL_CYAN_FG}Configuring lazygit...${COL_RESET}"
mkdir "${HOME}/.config/lazygit"
cp "${__dotfiles_dir}/config/lazygit-config.yml" "${HOME}/.config/lazygit/config.yml"

echo "${COL_CYAN_FG}Setting git pager to delta...${COL_RESET}"
sed --in-place "s/pager = less/pager = delta/g" "${HOME}/.config/dotfiles/gitconfig.local"

echo "${COL_CYAN_FG}Installing fzf-tab...${COL_RESET}"
mkdir "${HOME}/bin"
git clone https://github.com/Aloxaf/fzf-tab "${HOME}/bin/fzf-tab"

echo "${COL_CYAN_FG}Installing fzf-tab...${COL_RESET}"
git clone https://github.com/lincheney/fzf-tab-completion "${HOME}/bin/fzf-tab-completion"


echo "${COL_CYAN_FG}Installing tmux plugins...${COL_RESET}"
"${HOMEBREW_PREFIX}/opt/tpm/share/tpm/bin/install_plugins"

echo "${COL_CYAN_FG}Installing erdtree completions...${COL_RESET}"
erd --completions zsh | sudo tee /usr/local/share/zsh/site-functions/_erd >/dev/null

export DOTFILES_DIR="${__dotfiles_dir}"
"${__dotfiles_dir}/install/install-youtube-dl.sh"

echo "${COL_YELLOW_FG}Setup complete.${COL_RESET}"
echo "${COL_YELLOW_FG}Manual steps:${COL_RESET}"
echo "${COL_YELLOW_FG}  * Enable unrar: call 'unrar' in terminal > show in Finder > Ctrl+Click > Open${COL_RESET}"
echo "${COL_YELLOW_FG}  * Install Docker Desktop for Mac${COL_RESET}"
echo "${COL_YELLOW_FG}  * Enable auto completion for docker: 'ln -s /Applications/Docker.app/Contents/Resources/etc/docker.zsh-completion /usr/local/share/zsh/site-functions/_docker'${COL_RESET}"
