#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091

source "${DOTFILES_DIR}/lib/library/github.sh"
source "${DOTFILES_DIR}/lib/colors.sh"

__install_extra_packages() {
    local release_url filename_template host_arch system_kernel system_machine

    host_arch=$(dpkg --print-architecture)
    system_kernel=$(uname --kernel-name)
    system_machine=$(uname --machine)
    source "${DOTFILES_DIR}/lib/library/system.sh"

    [[ ! -d "${HOME}/bin" ]] && mkdir --parents "${HOME}/bin"

    echo "${COL_CYAN_FG}Installing extra packages with apt...${COL_RESET}"
    sudo apt-get update
    # https://github.com/abishekvashok/cmatrix
    # https://github.com/cli/cli
    # lua is needed by z.lua
    # https://www.shellcheck.net/ or https://github.com/koalaman/shellcheck
    # software-properties-common is a precondition for add-apt-repository which in turn is needed for adoptopenjdk
    # unzip is needed to install as-tree
    # https://github.com/adrienverge/yamllint (used for linting with A.L.E.)
    sudo apt-get install --yes \
                 cmatrix \
                 gh \
                 lua5.3 \
                 shellcheck \
                 universal-ctags \
                 unzip \
                 yamllint \
                 zsh-autosuggestions

    __install_nvim

    # https://github.com/sharkdp/vivid
    filename_template="https://github.com/sharkdp/vivid/releases/download/v%s/vivid_%s_${host_arch}.deb"
    release_url=$(get-latest-release-url "${filename_template}")
    curl --location "${release_url}" --output /tmp/vivid.deb
    sudo dpkg -i /tmp/vivid.deb
    rm /tmp/vivid.deb

    # https://github.com/jesseduffield/lazygit
    echo "${COL_CYAN_FG}Installing lazygit...${COL_RESET}"
    if is_arm; then
        filename_template="https://github.com/jesseduffield/lazygit/releases/download/v%s/lazygit_%s_${system_kernel}_arm64.tar.gz"
    else
        filename_template="https://github.com/jesseduffield/lazygit/releases/download/v%s/lazygit_%s_${system_kernel}_${system_machine}.tar.gz"
    fi
    release_url=$(get-latest-release-url "${filename_template}")
    curl --location "${release_url}" --output /tmp/lazygit.tar.gz
    sudo tar --directory=/usr/local/bin/ --extract --gunzip --verbose --file=/tmp/lazygit.tar.gz lazygit
    rm /tmp/lazygit.tar.gz
    [[ ! -d "${HOME}/.config/lazygit" ]] && mkdir "${HOME}/.config/lazygit"
    cp "${DOTFILES_DIR}/config/lazygit-config.yml" "${HOME}/.config/lazygit/config.yml"

    # https://github.com/jesseduffield/lazydocker
    echo "${COL_CYAN_FG}Installing lazydocker...${COL_RESET}"
    if is_arm; then
        filename_template="https://github.com/jesseduffield/lazydocker/releases/download/v%s/lazydocker_%s_${system_kernel}_arm64.tar.gz"
    else
        filename_template="https://github.com/jesseduffield/lazydocker/releases/download/v%s/lazydocker_%s_${system_kernel}_${system_machine}.tar.gz"
    fi
    release_url=$(get-latest-release-url "${filename_template}")
    curl --location "${release_url}" --output /tmp/lazydocker.tar.gz
    sudo tar --directory=/usr/local/bin/ --extract --gunzip --verbose --file=/tmp/lazydocker.tar.gz lazydocker
    rm /tmp/lazydocker.tar.gz

    # https://tldr.sh/ and https://github.com/raylee/tldr-sh-client
    echo "${COL_CYAN_FG}Installing tldr...${COL_RESET}"
    sudo curl https://raw.githubusercontent.com/raylee/tldr/master/tldr --output /usr/local/bin/tldr && sudo chmod +x /usr/local/bin/tldr

    # https://github.com/jez/as-tree
    if ! is_arm; then
        echo "${COL_CYAN_FG}Installing as-tree...${COL_RESET}"
        filename_template="https://github.com/jez/as-tree/releases/download/%s/as-tree-%s-${system_kernel}.zip"
        release_url=$(get-latest-release-url "${filename_template}")
        curl --location "${release_url}" --output /tmp/as-tree.zip
        sudo unzip -o /tmp/as-tree.zip -d /usr/local/bin/
        rm /tmp/as-tree.zip
    fi

    # https://github.com/dandavison/delta
    echo "${COL_CYAN_FG}Installing delta...${COL_RESET}"
    filename_template="https://github.com/dandavison/delta/releases/download/%s/git-delta_%s_${host_arch}.deb"
    release_url=$(get-latest-release-url "${filename_template}")
    curl --location "${release_url}" --output /tmp/delta.deb
    sudo dpkg --install /tmp/delta.deb
    rm /tmp/delta.deb
    echo "${COL_CYAN_FG}Setting git pager to delta...${COL_RESET}"
    sed --in-place "s/pager = less/pager = delta/g" "${HOME}/.config/dotfiles/gitconfig.local"

    # https://github.com/wagoodman/dive
    if ! is_arm; then
        echo "${COL_CYAN_FG}Installing dive...${COL_RESET}"
        filename_template="https://github.com/wagoodman/dive/releases/download/v%s/dive_%s_${system_kernel}_${host_arch}.deb"
        release_url=$(get-latest-release-url "${filename_template}")
        curl --location "$release_url" --output /tmp/dive.deb
        sudo apt-get install --yes /tmp/dive.deb
        rm /tmp/dive.deb
    fi

    # https://github.com/eza-community/eza
    echo "${COL_CYAN_FG}Installing eza...${COL_RESET}"
    [[ ! -d "/etc/apt/keyrings" ]] && sudo mkdir --parents /etc/apt/keyrings
    curl --fail --silent --show-error --location https://raw.githubusercontent.com/eza-community/eza/main/deb.asc \
        | sudo gpg --batch --yes --dearmor --output /etc/apt/keyrings/gierens.gpg
    echo "deb [signed-by=/etc/apt/keyrings/gierens.gpg] http://deb.gierens.de stable main" | sudo tee /etc/apt/sources.list.d/gierens.list
    sudo chmod 644 /etc/apt/keyrings/gierens.gpg /etc/apt/sources.list.d/gierens.list
    sudo apt-get update
    sudo apt-get install --yes eza

    __git_clone "https://github.com/skywind3000/z.lua"
    __git_clone "https://github.com/eth-p/bat-extras"
    __git_clone "https://github.com/wfxr/forgit"
    __git_clone "https://github.com/zdharma-continuum/fast-syntax-highlighting"
    __git_clone "https://github.com/Aloxaf/fzf-tab"
    __git_clone "https://github.com/lincheney/fzf-tab-completion"
    __git_clone "https://github.com/hlissner/zsh-autopair"
    __git_clone "https://github.com/denilsonsa/prettyping"
    __git_clone "https://github.com/tmux-plugins/tpm"
    __install_tmux_plugins
}

__install_nvim() {
    sudo add-apt-repository ppa:neovim-ppa/stable --yes
    sudo apt-get update
    sudo apt-get install neovim --yes
    curl --fail --location --output "${HOME}/.config/nvim/autoload/plug.vim" --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    rm --force "${HOME}/.config/nvim/init.vim"
    ln --verbose --symbolic "${HOME}/.vimrc" "${HOME}/.config/nvim/init.vim"
}

__git_clone() {
    local repo="$1"
    # get part after last slash
    local repo_name=${repo##*/}
    # strip .git
    repo_name=${repo_name//.git/}

    if [[ ! -d "${HOME}/bin/${repo_name}" ]]; then
        echo "${COL_CYAN_FG}Installing ${repo_name}...${COL_RESET}"
        git clone "${repo}" "${HOME}/bin/${repo_name}"
    fi
}

__install_tmux_plugins() {
    echo "${COL_CYAN_FG}Installing tmux plugins...${COL_RESET}"
    "${HOME}/bin/tpm/bin/install_plugins"
}

__extra_usage() {
    # shellcheck disable=SC1090
    source "${DOTFILES_DIR}/lib/help.sh"
    colorize-help <<-EOF
		Install additional, optional dependencies

		Usage:
		    install-extra.sh [options]

		Options:
		    -h, --help          Print this help
		    -n, --no-log        Do not write to log
	EOF
    exit 0
}

__log_start() {
    cat <<-EOF
		${COL_MAGENTA_FG}#######################################
		${COL_MAGENTA_FG}$(date +'%Y-%m-%d %H:%M:%S %Z')
		${COL_MAGENTA_FG}Starting dotfiles extra installation...
		${COL_MAGENTA_FG}#######################################${COL_RESET}
	EOF
}

__read_options() {
    __do_log=true
    local options ret
    options=$(getopt --options hn --longoptions help,no-log -- "$@")
    ret=$?
    [[ "${ret}" -ne 0 ]] && exit 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -h | --help) __extra_usage;;
            -n | --no-log) __do_log=false;;
            --) shift; break;;
        esac
        shift
    done
}

__read_options "$@"
if [[ "${__do_log}" == true ]]; then
    {
        __log_start
        __install_extra_packages
    } 2>&1 | tee --append "${HOME}/dotfiles-install.log"
else
    __install_extra_packages
fi
