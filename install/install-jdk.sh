#!/usr/bin/env bash

if [[ $# -eq 0 || ! $1 =~ ^[0-9]+$ ]]; then
    echo "Please provide the java version, e.g. 8 or 15. The respective AdoptOpenJDK Hotspot (OpenJDK beginning from v17) will be installed."
fi
__java_version=$1

echo "${COL_CYAN_FG}Installing JDK ${__java_version}...${COL_RESET}"
if [[ $__java_version -ge 17 ]]; then
    sudo apt-get install --yes "openjdk-${__java_version}-jdk"
else
    curl --silent https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
    sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
    sudo apt-get install --yes "adoptopenjdk-${__java_version}-hotspot"
fi
