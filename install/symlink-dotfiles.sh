#!/usr/bin/env bash

__script_dir=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
__dotfiles_dir=$(dirname "${__script_dir}")

# shellcheck disable=SC1090,SC1091
source "${__dotfiles_dir}/lib/library/symlink.sh"
delete-broken-symlinks "${HOME}"
symlink-files "${__dotfiles_dir}/dotfiles" "${HOME}"
