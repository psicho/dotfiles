#!/usr/bin/env bash

# install kubectl
sudo curl --location "https://dl.k8s.io/release/$(curl --location --silent https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" --output /usr/local/bin/kubectl
sudo chmod +x /usr/local/bin/kubectl
kubectl completion zsh | sudo tee /usr/local/share/zsh/site-functions/_kubectl >/dev/null
kubectl completion bash | sudo tee /usr/share/bash-completion/completions/kubectl >/dev/null

# install helm
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" \
    | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install --yes helm
helm completion zsh | sudo tee /usr/local/share/zsh/site-functions/_helm >/dev/null
helm completion bash | sudo tee /usr/share/bash-completion/completions/helm >/dev/null

# install k3d
#curl --silent https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
#k3d completion zsh | sudo tee /usr/local/share/zsh/site-functions/_k3d >/dev/null
#k3d completion bash | sudo tee /usr/share/bash-completion/completions/k3d >/dev/null

# install minikube
#sudo curl --location https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 --output /usr/local/bin/minikube
#sudo chmod +x /usr/local/bin/minikube
#minikube completion zsh | sudo tee /usr/local/share/zsh/site-functions/_minikube >/dev/null
#minikube completion bash | sudo tee /usr/share/bash-completion/completions/minikube >/dev/null
