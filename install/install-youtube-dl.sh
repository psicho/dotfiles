#!/usr/bin/env bash

# install or update youtube-dl nightly
# Alternatively, we could install yt-dlp (https://github.com/yt-dlp/yt-dlp/wiki/Installation)
# But version 2023.12.30 creates m4a files 3-4x the size as current nightly build (2024.02.23)

# shellcheck disable=SC1090,SC1091
source "${DOTFILES_DIR}/lib/library/github.sh"

echo "Installing latest youtube-dl nightly build from https://github.com/ytdl-org/ytdl-nightly/releases..."

if [[ ! -f /usr/local/bin/python ]]; then
    sudo ln --symbolic /usr/bin/python3 /usr/local/bin/python
fi
if [[ "${OSTYPE}" == darwin* && ! -f /Library/Developer/CommandLineTools/usr/bin/python ]]; then
    sudo ln --symbolic /Library/Developer/CommandLineTools/usr/bin/python3 /Library/Developer/CommandLineTools/usr/bin/python
fi

filename_template="https://github.com/ytdl-org/ytdl-nightly/releases/download/%s/youtube-dl"
release_url=$(get-latest-release-url "${filename_template}")
sudo curl --location "${release_url}" --output /usr/local/bin/youtube-dl
sudo chmod +x /usr/local/bin/youtube-dl

if [[ "${OSTYPE}" == darwin* ]]; then
    # install support tools on macOS
    if ! command -v fprobe &>/dev/null; then
        brew install fprobe
    fi
    if ! command -v ffmpeg &>/dev/null; then
        brew install ffmpeg
    fi
    if ! command -v AtomicParsley &>/dev/null; then
        brew install atomicparsley
    fi
else
    # install support tools on Ubuntu
    if ! command -v AtomicParsley &>/dev/null; then
        sudo apt-get install --yes atomicparsley
    fi
fi

echo "Version installed:"
youtube-dl --version
