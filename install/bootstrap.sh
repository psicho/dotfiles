#!/usr/bin/env bash
# shellcheck disable=SC1090,SC1091
# Prerequisites: apt, sudo

__bootstrap() {
    __init_bootstrap "$@"
    [[ "${__do_bootstrapping}" == false ]] && return 0
    {
        __print_start_message
        __upgrade_and_install
        __clone_dotfiles
        # yq and python deps needed by install dotfiles
        __install_yq
        __prepare_python "${__install_location}/dotfiles"
        source "${__install_location}/dotfiles/config/gnu-binaries.sh"
        __install_dotfiles
        __install_external_tools
        __install_extra_packages
        __initialize_dotfiles_theming
        __delete_bootstrap_script
    } 2>&1 | tee --append "${HOME}/dotfiles-install.log"
    __reboot_or_reload
}

__init_bootstrap() {
    __script_dir=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
    __script_path="${__script_dir}/$(basename -- "$0")"

    __branch="main"
    __extra_packages=false
    __keyfile=""
    __do_bootstrapping=true
    __machine_name=""
    __silent_install=false
    __install_location=""

    local options ret
    options=$(getopt --options ab:hk:ln:s --longoptions all,branch:,help,keyfile:,load-only,name:,silent -- "$@")
    ret=$?
    [[ $ret -ne 0 ]] && exit 1
    eval set -- "${options}"
    while true; do
        case "$1" in
            -a | --all) __extra_packages=true;;
            -b | --branch) shift; __branch="$1";;
            -h | --help) __bootstrap_usage;;
            -k | --keyfile)
                shift
                __keyfile=$(realpath "$1")
                if [[ ! -f "${__keyfile}" ]]; then
                    echo "Keyfile ${__keyfile} does not exist. Exiting."
                    exit 1
                fi
                ;;
            -l | --load-only) __do_bootstrapping=false;;
            -n | --name) shift; __machine_name="$1";;
            -s | --silent) __silent_install=true;;
            --) shift; break;;
        esac
        shift
    done
    if [[ -n "${DOTFILES_DIR}" ]]; then
        __install_location=$(dirname "${DOTFILES_DIR}")
    else
        __install_location=${1:-${HOME}}
    fi

    __reset=$(tput sgr0)
    __cyan=$(tput setaf 37)    # info
    __yellow=$(tput setaf 229) # user input required / warning
    __orange=$(tput setaf 166) # log start
}

__print_start_message() {
    if [[ "${__do_bootstrapping}" == true ]]; then
        cat <<-EOF
			${__orange}##############################
			${__orange}$(date +'%Y-%m-%d %H:%M:%S %Z')
			${__orange}Starting dotfiles bootstrap...
			${__orange}##############################${__reset}
		EOF
        echo "${__yellow}Install location is set to '${__install_location}'.${__reset}"
        if [[ "${__extra_packages}" == false ]]; then
            echo "${__yellow}Not installing extra packages. If you want to do so, call this script with '-a' or '--all'.${__reset}"
        fi
    fi
}

__upgrade_and_install() {
    echo "${__cyan}Upgrading & installing basic packages...${__reset}"
    sudo apt-get update
    sudo apt-get upgrade --yes
    # https://github.com/sharkdp/fd
    # https://stedolan.github.io/jq/ and https://github.com/stedolan/jq, needed for github api access
    # https://github.com/BurntSushi/ripgrep
    sudo apt-get install --yes \
                 curl \
                 fd-find \
                 git \
                 grep \
                 jq \
                 python3-venv \
                 ripgrep \
                 software-properties-common \
                 tmux \
                 tree \
                 vim \
                 zsh
    if [[ -n "${WSL_DISTRO_NAME}" ]]; then
        sudo apt-get install --yes xdg-utils
    fi
}

__clone_dotfiles() {
    echo "${__cyan}Cloning  dotfiles...${__reset}"
    cd "${__install_location}" || (echo "Install location '${__install_location}' not found, exiting." && exit 1)
    if [[ ! -d "${__install_location}/dotfiles" ]]; then
        if [[ -n "${__keyfile}" ]]; then
            eval "$(ssh-agent -s)" && ssh-add "${__keyfile}"
            git clone git@gitlab.com:psicho/dotfiles.git --branch "${__branch}"
        else
            git clone https://gitlab.com/psicho/dotfiles.git --branch "${__branch}"
        fi
    fi
}

__install_dotfiles() {
    # Symlinking dotfiles
    echo "${__cyan}Symlinking dotfiles...${__reset}"
    cd "${__install_location}" || (echo "Install location '${__install_location}' not found, exiting." && exit 1)
    if [[ ! -d "${__install_location}/dotfiles" ]]; then
        if [[ -n "${__keyfile}" ]]; then
            eval "$(ssh-agent -s)" && ssh-add "${__keyfile}"
            git clone git@gitlab.com:psicho/dotfiles.git
        else
            git clone https://gitlab.com/psicho/dotfiles.git
        fi
    fi
    dotfiles/install/symlink-dotfiles.sh

    # Create directories used by dotfiles
    echo "${__cyan}Creating directories...${__reset}"
    [[ ! -d /tmp/vimswap ]] && mkdir --parents /tmp/vimswap
    [[ ! -d "${HOME}/.config/dotfiles" ]] && mkdir --parents "${HOME}/.config/dotfiles"
    [[ ! -d "${HOME}/.cache/zsh" ]] && mkdir --parents "${HOME}/.cache/zsh"
    touch "${HOME}/.cache/zsh/history"

    # Create custom config files
    local custom_location="${HOME}/.config/dotfiles/custom.sh"
    __copy_config "${__install_location}/dotfiles/config/custom.example.sh" "${custom_location}"
    __copy_config "${__install_location}/dotfiles/config/config.template.yml" "${HOME}/.config/dotfiles/config.yml"
    result=$?
    if [[ "${result}" && -n "${__keyfile}" ]]; then
        "${__install_location}/dotfiles/lib/configurer.sh" --configure ssh-identities "${__keyfile}"
    fi

    # Create initial gitconfig.local
    local gitconfig="${HOME}/.config/dotfiles/gitconfig.local"
    __copy_config "${__install_location}/dotfiles/config/gitconfig.local.example" "${gitconfig}"
    __configure_gitignore "${__install_location}/dotfiles"

    # Export machine name as NICKNAME
    if [[ -z "${__machine_name}" && "${__silent_install}" == false ]]; then
        echo "${__yellow}Please enter name of the machine to be displayed in prompt:${__reset}"
        read -r __machine_name
    fi
    if [[ -n "${__machine_name}" ]]; then
        echo "${__cyan}Setting machine name ${__machine_name}...${__reset}"
        "${__install_location}/dotfiles/lib/configurer.sh" --configure nickname "${__machine_name}"
    fi

    if [[ "${OSTYPE}" != darwin* ]]; then
        echo "${__cyan}Changing default shell to zsh...${__reset}"
        sudo chsh --shell /bin/zsh "$(whoami)"
    fi
}

__configure_gitignore() {
    local dotfiles_dir="$1"
    [[ ! -d "${HOME}/.config/git" ]] && mkdir --parents "${HOME}/.config/git"
    cp "${dotfiles_dir}/config/gitignore-global" "${HOME}/.config/git/ignore"
}

# should be called after lazygit is installed (in install-extra.sh)
__initialize_dotfiles_theming() {
    local theme background
    theme=$("${__install_location}/dotfiles/lib/configurer.sh" --load theming.theme)
    background=$("${__install_location}/dotfiles/lib/configurer.sh" --load theming.background)
    copy_terminal_themes
    source "${__install_location}/dotfiles/theming/set-theme.sh"
    set_theme "${theme}" "${background}"
}

__copy_config() {
    local source="$1"
    local target="$2"
    if [[ -f "${target}" ]]; then
        echo "${__cyan}${target} already exists. Skipping creation.${__reset}"
        return 1
    else
        echo "${__cyan}Creating ${target}${__reset}"
        cp "${source}" "${target}"
        return 0
    fi
}

# https://learn.microsoft.com/en-us/windows/terminal/json-fragment-extensions
copy_terminal_themes() {
    if command -v powershell.exe >/dev/null; then
        local windows_username application install_path
        windows_username=$(powershell.exe -noprofile -command 'whoami' \
            | tr --delete '\r' \
            | cut --delimiter="\\" --fields=2)
        for theme_file in "${__install_location}"/dotfiles/theming/terminal/*.json; do
            application=$(basename "${theme_file:0:-5}")
            install_path="Users/${windows_username}/AppData/Local/Microsoft/Windows Terminal/Fragments/${application}/"
            echo "Copying terminal theme: ${application} ==> /mnt/c/${install_path}"
            powershell.exe -windowstyle hidden -noprofile -command "if (!(Test-Path 'C:/$install_path')) { New-Item -Type Directory -Path 'C:/$install_path' }"
            cp "${theme_file}" "/mnt/c/${install_path}"
        done
    fi
}

# must be called *after* dotfiles installation
__install_external_tools() {
    __install_fzf
    __install_starship
    __install_bat
    __configure_bat
    __install_vim_plugins
    __install_terminfo
    __install_yq
}

__install_fzf() {
    source "${__install_location}/dotfiles/lib/library/github.sh"
    echo "${__cyan}Installing fzf...${__reset}"
    local host_arch template release_url source_url version

    # install executable
    host_arch=$(dpkg --print-architecture)
    template="https://github.com/junegunn/fzf/releases/download/v%s/fzf-%s-linux_${host_arch}.tar.gz"
    release_url=$(get-latest-release-url "${template}")
    curl --location "${release_url}" --output /tmp/fzf.tar.gz
    sudo tar --directory=/usr/local/bin/ --extract --gunzip --verbose --file=/tmp/fzf.tar.gz fzf
    rm /tmp/fzf.tar.gz

    # install fzf-tmux and man pages from source
    template="https://github.com/junegunn/fzf/archive/refs/tags/v%s.tar.gz"
    source_url=$(get-latest-release-url "${template}")
    version=$(get-latest-release-version junegunn/fzf)
    curl --location "${source_url}" --output /tmp/fzf-sources.tar.gz
    tar --directory=/tmp/ --extract --gunzip --file=/tmp/fzf-sources.tar.gz
    sudo cp "/tmp/fzf-${version}/bin/fzf-tmux" /usr/local/bin/
    sudo mkdir --parents /usr/local/share/man/man1
    sudo cp "/tmp/fzf-${version}/man/man1/fzf.1" "/tmp/fzf-${version}/man/man1/fzf-tmux.1" /usr/local/share/man/man1
    rm --recursive --force "/tmp/fzf-${version}" fzf-sources.tar.gz
}

__install_starship() {
    # https://starship.rs
    echo "${__cyan}Installing Starship...${__reset}"
    curl --fail --silent --show-error --location https://starship.rs/install.sh --output starship-install.sh
    sudo sh starship-install.sh --yes
    rm starship-install.sh
}

# https://github.com/sharkdp/bat
__install_bat() {
    source "${__install_location}/dotfiles/lib/library/github.sh"

    echo "${__cyan}Installing bat...${__reset}"
    local host_arch template release_url bat_location
    host_arch=$(dpkg --print-architecture)
    template="https://github.com/sharkdp/bat/releases/download/v%s/bat_%s_${host_arch}.deb"
    release_url=$(get-latest-release-url "${template}")
    curl --location "${release_url}" --output /tmp/bat.deb
    sudo apt-get install --yes /tmp/bat.deb
    rm /tmp/bat.deb
}

__configure_bat() {
    echo "${__cyan}Configuring bat...${__reset}"
    source "${__install_location}/dotfiles/lib/library/symlink.sh"
    bat_location="$(bat --config-dir)"
    declare -a subdirs=("syntaxes" "themes")
    for subdir in "${subdirs[@]}"; do
        [[ ! -d "${bat_location}/${subdir}" ]] && mkdir --parents "${bat_location}/${subdir}"
        delete-broken-symlinks "${bat_location}/${subdir}"
        symlink-files "${__install_location}/dotfiles/theming/bat/${subdir}" "${bat_location}/${subdir}"
    done
    bat cache --build
}

__install_vim_plugins() {
    echo "${__cyan}Installing vim plugins...${__reset}"
    rm --recursive --force "${HOME}/.vim"
    curl --fail --location --output "${HOME}/.vim/autoload/plug.vim" --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    /usr/bin/env vim +PlugInstall +qall
}

__install_terminfo() {
    echo "${__cyan}Installing terminfo config...${__reset}"
    # https://medium.com/@dubistkomisch/how-to-actually-get-italics-and-true-colour-to-work-in-iterm-tmux-vim-9ebe55ebc2be
    tic -x "${__install_location}/dotfiles/config/xterm-256color-italic.terminfo"
    tic -x "${__install_location}/dotfiles/config/tmux-256color.terminfo"
}

__install_yq() {
    local filename_template release_url arch
    # https://github.com/mikefarah/yq
    # cannot install via snap, as snap's yq does not have access to hidden directories in home
    echo "${__cyan}Installing yq...${__reset}"
    source "${__install_location}/dotfiles/lib/library/github.sh"
    arch=$(dpkg --print-architecture)
    filename_template="https://github.com/mikefarah/yq/releases/download/v%s/yq_linux_${arch}"
    release_url=$(get-latest-release-url "${filename_template}")
    sudo curl --location "${release_url}" --output /usr/local/bin/yq
    sudo chmod a+x /usr/local/bin/yq
    yq shell-completion zsh | sudo tee /usr/local/share/zsh/site-functions/_yq >/dev/null
    yq shell-completion bash | sudo tee /usr/share/bash-completion/completions/yq >/dev/null
}

__install_extra_packages() {
    if [[ "${__extra_packages}" == true ]]; then
        echo "${__cyan}Installing extra packages...${__reset}"
        export DOTFILES_DIR="${__install_location}/dotfiles"
        "${__install_location}/dotfiles/install/install-extra.sh" "--no-log"
    fi
}

__prepare_python() {
    local dotfiles_dir="$1"
    local last_venv="${VIRTUAL_ENV}"
    cd "${dotfiles_dir}" || return 1
    sudo apt-get update --yes
    # needed for the upgrade path
    sudo apt-get install python3-venv --yes
    python3 -m venv .venv
    source .venv/bin/activate
    python3 -m pip install mako pyyaml
    deactivate
    [[ -n "${last_venv}" ]] && source "${last_venv}/bin/activate"
}

__delete_bootstrap_script() {
    cd "${__script_dir}" || return 1
    # shellcheck disable=SC2046
    if [[ ! $(git rev-parse --is-inside-work-tree 2>/dev/null) ]]; then
        echo "${__cyan}Deleting bootstrap script...${__reset}"
        rm "${__script_path}"
    fi
}

__reboot_or_reload() {
    # delete zcompdump if existing, so that autocompletion will be regenerated with next login
    rm --force "${HOME}/.cache/zsh/zcompdump" "${HOME}/.zcompdump"
    cat <<-EOF
		${__yellow}Setup complete. For more info, read README.md or call
		    Meta+Shift+H: Show help on functions and scripts
		    help / F1:    Show README in Vim (then, search and navigate with Space+/)${__reset}
	EOF

    if [[ "${__silent_install}" == false ]]; then
        if [[ -n "${WSL_DISTRO_NAME}" || "${OSTYPE}" == darwin* ]]; then
            # in WSL and macOS just reload the shell
            echo "${__cyan}Reloading zsh...${__reset}"
            exec zsh --login
        else
            cat <<-EOF
				${__yellow}Reboot necessary. Reboot?
				    y: yes
				    n: no (manual reboot required)${__reset}
			EOF
            read -n 1 -r # read one character and accept backslashes literally (-r)
            echo         # move to a new line
            if [[ "${REPLY}" =~ ^[Yy]$ ]]; then
                sudo reboot
            else
                echo "${__cyan}Reloading zsh...${__reset}"
                exec zsh --login
            fi
        fi
    fi
}

__bootstrap_usage() {
    local pager dotfiles_dir
    if [[ -d "${__install_location}" ]]; then
        dotfiles_dir="${__install_location}/dotfiles"
    else
        dotfiles_dir="${DOTFILES_DIR}"
    fi
    if [[ $(command -v bat) && -f "${dotfiles_dir}/lib/help.sh" ]]; then
        source "${dotfiles_dir}/lib/help.sh"
        pager="colorize-help"
    else
        pager="cat"
    fi
    eval "${pager}" <<-EOF
		Install these dotfiles along with all dependencies

		Usage:
		    bootstrap.sh [-a] [-n name] [-s] [-k keyfile] [install-location]
		    bootstrap.sh -h
		    source bootstrap.sh -l

		Options:
		    -a, --all           Install extra packages (calls install-extra.sh).
		    -b, --branch        Use provided branch instead of default.
		    -h, --help          Print this help.
		    -k, --keyfile       Clone dotfiles with SSH using keyfile. If not provided, clone with HTTPS.
		    -l, --load-only     Load this file only. Must be the sole arg. Used for upgrading: source this file with -l and call specific functions.
		    -n, --name          Name of the machine. No prompt for machine name will appear.
		    -s, --silent        Silent install. No prompt for machine name will appear and no reboot/reload will take place.
	EOF
    exit 1
}

__bootstrap "$@"
