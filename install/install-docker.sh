#!/usr/bin/env bash

# Install docker
echo "${COL_CYAN_FG}Installing docker...${COL_RESET}"
sudo mkdir --parents /etc/apt/keyrings
curl --fail --silent --show-error --location https://download.docker.com/linux/ubuntu/gpg \
	| sudo gpg --dearmor --output /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release --codename --short) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install --yes docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo usermod --append --groups docker "$(whoami)"

# Install docker-compose
echo "${COL_CYAN_FG}Installing docker-compose...${COL_RESET}"
sudo curl --location --output /usr/local/bin/docker-compose \
	"https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname --kernel-name)-$(uname --machine)"
sudo chmod +x /usr/local/bin/docker-compose
# alternative: install as a plugin
# sudo apt-get install --yes docker-compose-plugin
# sudo ln --symbolic /usr/libexec/docker/cli-plugins/docker-compose /usr/bin/docker-compose

# Print WSL note
if [[ -n "${WSL_DISTRO_NAME}" ]]; then
    bat --plain --color always <<-EOF
		${COL_YELLOW_FG}This is a WSL distro. Configure docker daemon autostart as follows:${COL_RESET}
		$ sudoedit /etc/wsl.conf

		[boot]
		command="service docker start"
		# systemd is sometimes not compatible with calling windows executables from WSL:
		#   run-detectors: unable to find an interpreter for
		# another known issue:
		#   zsh: exec format error: <windows program name>
		#   https://github.com/microsoft/WSL/issues/8952
		#systemd=true

		${COL_YELLOW_FG}Then shutdown and restart WSL.${COL_RESET}
EOF
fi
