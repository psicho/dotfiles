#!/usr/bin/env bash

if [[ "${OSTYPE}" == darwin* ]]; then
    brew install fastfetch
else
    sudo add-apt-repository ppa:zhangsongcui3371/fastfetch
    sudo apt-get update
    sudo apt-get install --yes fastfetch
fi
