#!/usr/bin/env bash

__update() {
    local repo target_dir="$1"
    repo=$(basename "${target_dir}")
    echo "${COL_BLUE_FG}Updating ${repo}...${COL_RESET}"
    cd "${target_dir}" || return 1
    # shellcheck disable=SC1091
    STAY_IN_DIRECTORY="true" source "${DOTFILES_DIR}/config/config-shell.sh"
    git_update
}

{
    cat <<-EOF
		${COL_MAGENTA_FG}############################
		${COL_MAGENTA_FG}$(date +'%Y-%m-%d %H:%M:%S %Z')
		${COL_MAGENTA_FG}Updating...
		${COL_MAGENTA_FG}############################${COL_RESET}
	EOF
    [[ -d "${WIKI_DIR}" ]] && __update "${WIKI_DIR}"
    __update "${DOTFILES_DIR}"
} 2>&1 | tee --append "${HOME}/dotfiles-install.log"
