#!/usr/bin/env bash

cd "${HOME}" || exit 1
curl --location https://gitlab.com/psicho/dotfiles/-/raw/main/config/config-shell.sh --output .bash_profile
curl --location https://gitlab.com/psicho/dotfiles/-/raw/main/dotfiles/.inputrc --output .inputrc
curl --location https://gitlab.com/psicho/dotfiles/-/raw/main/dotfiles/.vimrc --output .vimrc
curl --location https://gitlab.com/psicho/dotfiles/-/raw/main/lib/bash/plugins.bash --output plugins.bash
echo "source '${HOME}/plugins.bash'" >> .bash_profile
echo "source '${HOME}/.bash_profile'" > .bashrc
