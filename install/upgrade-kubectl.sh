#!/usr/bin/env bash

# variables: arch current latest kubectl_version

if command -v kubectl >/dev/null; then
    current=$(kubectl version 2>/dev/null | grep "Client Version" | cut --delimiter=':' --fields=2 | xargs)
fi
latest=$(curl --location --silent https://dl.k8s.io/release/stable.txt)
if [[ "${OSTYPE}" == darwin* ]]; then
    arch="darwin/arm64"
else
    arch="linux/amd64"
fi

echo "Detected architecture: ${arch}"
echo "Get the desired version from https://kubernetes.io/releases/."
echo "Latest version: ${latest}"
[[ -n ${current} ]] && echo "Current version: ${current}"
echo "Which version do you want to install?"
read -r kubectl_version

if [[ "${kubectl_version}" != v* ]]; then
    kubectl_version="v${kubectl_version}"
fi

sudo curl --location --fail "https://dl.k8s.io/release/${kubectl_version}/bin/${arch}/kubectl" --output /usr/local/bin/kubectl
