#!/usr/bin/env bash

if [[ "${OSTYPE}" == darwin* ]]; then
    brew install npm
else
    sudo apt-get update
    sudo apt-get install npm --yes
fi

git clone https://github.com/IonicaBizau/git-stats.git "${HOME}/bin/git-stats"
cd "${HOME}/bin/git-stats" || exit 1
sudo npm install --global git-stats

git clone https://github.com/IonicaBizau/git-stats-importer.git "${HOME}/bin/git-stats-importer"
cd "${HOME}/bin/git-stats-importer" || exit 1
sudo npm install --global git-stats-importer
