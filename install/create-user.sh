#!/usr/bin/env bash

__has_space() {
    [[ "$1" != "${1%[[:space:]]*}" ]] && return 0 || return 1
}

__validate() {
    # check that at least two arguments are provided
    if [[ "$#" -le 2 || "$1" == "-h" || "$1" == "--help" ]]; then
        local pager
        if [[ $(command -v bat) && -f ${DOTFILES_DIR}/lib/help.sh ]]; then
            # shellcheck disable=SC1091
            source "${DOTFILES_DIR}/lib/help.sh"
            pager="colorize-help"
        else
            pager="cat"
        fi
        eval "${pager}" <<-EOF
			Create a user

			Usage:
			    create-user.sh <username> <fullname> <keydir>

			Arguments:
			    username    The username without spaces
			    fullname    The full name of the user (used as description in /etc/passwd)
			    keydir      The directory containing the keys (optionally, defaults to "${HOME}/.ssh")
		EOF
        exit 1
    fi

    local username="$1"
    local keydir="$3"

    # check that username has no space
    if __has_space "${username}"; then
        echo "${__red}The username may not contain a space.${__reset}"
        exit 1
    fi

    # check that key directory exists
    if [[ ! -d "${keydir}" ]]; then
        echo "${__red}Key directory ${keydir} does not exists.${__reset}"
        exit 1
    fi

    # check that ${username}_id_rsa.pub exists
    if [[ ! -f "${keydir}/${username}_id_rsa.pub" ]]; then
        echo "${__red}File ${keydir}/${username}_id_rsa.pub does not exist.${__reset}"
        exit 1
    fi

    # check that user does not yet exist
    if id --user "${username}" &>/dev/null; then
        echo "${__red}User ${username} already exists.${__reset}"
        exit 1
    fi

    # check that new home directory does not yet exist
    if [[ -d "/home/${username}" ]]; then
        echo "${__red}Home directory /home/${username} of new user already exists.${__reset}"
        exit 1
    fi
}

__create_user() {
    local username="$1"
    local fullname="$2"
    local keydir=${3:-"$HOME/.ssh"}
    local current_group
    current_group="$(id --group --name)"

    __validate "${username}" "${fullname}" "${keydir}"

    # create user
    sudo useradd "${username}" --gid "${current_group}" --groups docker --shell /bin/bash --comment "${fullname}"
    # create home directory
    sudo install --owner "${username}" --group "${current_group}" --directory "/home/${username}/.ssh"
    # move key files
    sudo /bin/mv "${keydir}/${username}_id_rsa.pub" "/home/${username}/.ssh/authorized_keys"
    if [[ -f "${keydir}/git_with_pass_rsa" ]]; then
        sudo /bin/mv "${keydir}/git_with_pass_rsa" "/home/${username}/.ssh"
        sudo chmod 0600 "/home/${username}/.ssh/git_with_pass_rsa"
    fi
    # set ownage
    sudo chown --recursive "${username}:${current_group}" "/home/${username}"
    # put user in sudoers
    sudo sh -c "echo \"${username} ALL=(ALL) NOPASSWD:ALL\" > /etc/sudoers.d/10-${username}"
    sudo chmod 0440 "/etc/sudoers.d/10-${username}"

    # delete this script if created temporarily
    cd "${__script_dir}" || return 1
    # shellcheck disable=SC2046
    if [ ! $(git rev-parse --is-inside-work-tree 2>/dev/null) ]; then
        rm "${__script_path}"
    fi

    echo "${__cyan}User ${username} successfully created.${__reset}"
}

__script_dir="$(dirname "${BASH_SOURCE[0]:-$0}")"
__script_path="${__script_dir}/$(basename -- "$0")"
__reset=$(tput sgr0)
__cyan=$(tput setaf 37)    # info
__red=$(tput setaf 203)    # error
__create_user "$@"
