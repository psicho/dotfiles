#!/usr/bin/env bash

# nerd, emoji, icons, none
# variant nerd's contrast is too low on light backgrounds in WSL
# variant emoji has problems with some emojis using wide characters
variant=nerd
[[ -n $1 ]] && variant=$1

install_nnn_from_source() {
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}"/lib/library/github.sh

    filename_template="https://github.com/jarun/nnn/releases/download/v%s/nnn-v%s.tar.gz"
    release_url=$(get-latest-release-url "${filename_template}")
    curl --location "${release_url}" --output /tmp/nnn.tar.gz
    tar --extract --gunzip --file=/tmp/nnn.tar.gz --directory=/tmp
    version=$(get-latest-release-version jarun/nnn)

    if [[ "${OSTYPE}" == darwin* ]]; then
        brew install pkg-config ncurses readline
    else
        sudo apt-get install pkg-config libncursesw5-dev libreadline-dev
    fi

    cd "/tmp/nnn-${version}" || return
    case "${variant}" in
        "none") arg="";;
        "emoji") arg="O_EMOJI=1";;
        "nerd") arg="O_NERD=1";;
        "icons") arg="O_ICONS=1";;
    esac
    sudo make strip install "${arg}"

    cd || return
    rm -rf "/tmp/nnn-${version}" /tmp/nnn.tar.gz
}

install_lowdown() {
    if [[ "${OSTYPE}" == darwin* ]]; then
        brew install lowdown
    else
        sudo apt-get install lowdown --yes
    fi
}

# Note: libarchive brings bsdtar for handling multiple archive formats; an alternative is atool.
if [[ "${OSTYPE}" == darwin* ]]; then
    brew install rclone libarchive
    brew install --cask rar
    brew install timg
    # nnn is also available from brew, but currently without icon support
    install_nnn_from_source
else
    # shellcheck disable=SC1090,SC1091
    source "${DOTFILES_DIR}"/lib/library/github.sh

    [[ "${variant}" == "none" ]] && variant=musl
    filename_template="https://github.com/jarun/nnn/releases/download/v%s/nnn-${variant}-static-%s.x86_64.tar.gz"
    release_url=$(get-latest-release-url "${filename_template}")
    curl --location "${release_url}" --output /tmp/nnn.tar.gz
    tar --extract "nnn-${variant}-static" --gunzip --verbose --file=/tmp/nnn.tar.gz --directory=/home/markus
    rm /tmp/nnn.tar.gz
    sudo mv "nnn-${variant}-static" /usr/local/bin/nnn

    sudo apt-get update
    sudo apt-get install sshfs rclone libarchive-tools rar unrar archivemount --yes
fi

# Install plugins
rm --recursive --force "${XDG_CONFIG_HOME:-$HOME/.config}/nnn/plugins"
curl --location --silent https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs --output-dir /tmp --remote-name
# shellcheck disable=SC1091
source /tmp/getplugs master
rm /tmp/getplugs

# Install completions
nnn_version=$(nnn -V)
curl "https://raw.githubusercontent.com/jarun/nnn/refs/tags/v${nnn_version}/misc/auto-completion/zsh/_nnn" \
    | sed --expression='1s/$/ n/' \
    | sudo tee /usr/local/share/zsh/site-functions/_nnn >/dev/null
curl "https://raw.githubusercontent.com/jarun/nnn/refs/tags/v${nnn_version}/misc/auto-completion/bash/nnn-completion.bash" \
    | sudo tee /usr/share/bash-completion/completions/nnn >/dev/null

# Install man page
[[ ! -d /usr/local/share/man/man1 ]] && sudo mkdir --parents /usr/local/share/man/man1
curl "https://raw.githubusercontent.com/jarun/nnn/refs/tags/v${nnn_version}/nnn.1" \
    | sudo tee /usr/local/share/man/man1/nnn.1 >/dev/null

install_lowdown

if [[ "${OSTYPE}" == darwin* ]]; then
    echo "${COL_YELLOW_FG}1. Install macFUSE and SSHFS:${COL_RESET} https://macfuse.github.io/"
    echo "   See also https://www.petergirnus.com/blog/how-to-use-sshfs-on-macos"
    echo "   Systemeinstellungen > Dateien & Ordner > iTerm > Netzwerkvolumes ☑️"
    echo "${COL_YELLOW_FG}2. Install archivemount via MacPorts:${COL_RESET}"
    echo "   sudo port install archivemount"
fi
